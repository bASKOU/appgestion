<?php

namespace UserBundle\Services;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use UserBundle\Email\UserMailer;
use UserBundle\Entity\User;

class UserCreationListener {

    /**
     * @var UserMailer
     */
    private $usermailer;

    public function __construct(UserMailer $userMailer)
    {
        $this->usermailer = $userMailer;
    }

    public function postPersist(LifecycleEventArgs $args) {

        $entity = $args->getObject();

        // on envoie un mail que pour les entités user
        if(!$entity instanceof User) {
            return;
        }

        $this->usermailer->sendNewNotification($entity);
    }

}