<?php

namespace UserBundle\Services;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Class AfterLoginRedirection
 *
 * @package AppBundle\AppListener
 */
class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{
    private $router;
    /**
     * AfterLoginRedirection constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request        $request
     *
     * @param TokenInterface $token
     *
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $actif = $token->getUser()->getIsActive();
        $roles = $token->getRoles();
        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);

        if($actif == 1) {
            if (in_array('ROLE_SUPER_SUDALYS', $rolesTab, true)) {
                // c'est un utilisateur Sudalys : on le rediriger vers l'espace admin
                $redirection = new RedirectResponse($this->router->generate('sudalys_gestion_dashboard_admin'));
            } else if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $rolesTab, true)) {
                // c'est un utilisateur administratif : on le redirige vers le dashboard adminisratif
                $redirection = new RedirectResponse($this->router->generate('sudalys_gestion_dashboard_administratif'));
            } else if (in_array('ROLE_SUDALYS', $rolesTab, true)) {
                // c'est un utilisaeur lambda : on le rediriger vers l'accueil
                $redirection = new RedirectResponse($this->router->generate('sudalys_gestion_dashboard_user'));
            }
            return $redirection;
        }else{
            $redirection = new RedirectResponse($this->router->generate('login'));
            $request->getSession()->getFlashBag()->add('info', 'Votre compte a été désactivé, veuillez contacter votre adminsitrateur réseau.');
            return $redirection;
        }
    }
}