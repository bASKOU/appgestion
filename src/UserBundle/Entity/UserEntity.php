<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UserEntity
 *
 * @ORM\Table(name="user_entity")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserEntityRepository")
 * @UniqueEntity(
 *     fields={"bddName"},
 *     errorPath="bddName",
 *     ignoreNull = true,
 *     message="Le nom de base de données est déjà utilisé !"
 * )
 *
 */
class UserEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_2", type="string", length=255, nullable=true)
     */
    private $adresse2;

    /**
     * @var integer
     *
     * @ORM\Column(name="code_postal", type="integer")
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=10)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=10, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="bdd_name", type="string", length=75, unique=true, nullable=true)
     *
     */
    private $bddName;
    /**
     * @var int
     * @ORM\Column(name="avancement", type="smallint", nullable=true)
     */
    private $avancement;

    /**
     * @var decimal
     * @ORM\Column(name="x_coord", type="decimal", precision=13, scale=10, nullable=true)
     */
    private $xCoord;

    /**
     * @var decimal
     * @ORM\Column(name="y_coord", type="decimal", precision=13, scale=10, nullable=true)
     */
    private $yCoord;

    /**
     * @var string
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     *
     * @Assert\File(mimeTypes={ "image/png" })
     */
    private $logo;

    private $tempLogo;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;


    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return UserEntity
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return UserEntity
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     *
     * @return UserEntity
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return UserEntity
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return UserEntity
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return UserEntity
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return int
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return UserEntity
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set bddName
     *
     * @param string $bddName
     *
     * @return UserEntity
     */
    public function setBddName($bddName)
    {
        $this->bddName = $bddName;

        return $this;
    }

    /**
     * Get bddName
     *
     * @return string
     */
    public function getBddName()
    {
        return $this->bddName;
    }

    /**
     * Set avancement
     *
     * @param string $avancement
     *
     * @return UserEntity
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement
     *
     * @return string
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return UserEntity
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set xCoord
     *
     * @param string $xCoord
     *
     * @return UserEntity
     */
    public function setXCoord($xCoord)
    {
        $this->xCoord = $xCoord;

        return $this;
    }

    /**
     * Get xCoord
     *
     * @return string
     */
    public function getXCoord()
    {
        return $this->xCoord;
    }

    /**
     * Set yCoord
     *
     * @param string $yCoord
     *
     * @return UserEntity
     */
    public function setYCoord($yCoord)
    {
        $this->yCoord = $yCoord;

        return $this;
    }

    /**
     * Get yCoord
     *
     * @return string
     */
    public function getYCoord()
    {
        return $this->yCoord;
    }

    /**
     * Set logo.
     *
     * @param string $logo
     *
     * @return UserEntity
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    public function setTempLogo($tempLogo)
    {
        $this->tempLogo = $tempLogo;

        return $this;
    }

    public function getTempLogo()
    {
        return $this->tempLogo;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return UserEntity
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
