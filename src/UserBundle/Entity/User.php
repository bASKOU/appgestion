<?php

namespace UserBundle\Entity;

use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Devis;
use Doctrine\ORM\Mapping as ORM;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Attachement;
use GestionBundle\Entity\Attribution;
use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\Facture;
use GestionBundle\Entity\FactureFournisseur;
use GestionBundle\Entity\FacturePrestataire;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=75, unique=true, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=75)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=75)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=75, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $mobile;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     */
    private $isActive;

    /**
     * @var string
     * @ORM\Column(name="token", type="string", length=64, nullable=true)
     */
    private $token;

    /**
     * @var date
     * @ORM\Column(name="validity", type="datetime")
     */
    private $validity;

    /**
     * @var float
     * @ORM\Column(name="portefeuille", type="float", nullable=true)
     */
    private $portefeuille;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\UserEntity")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userEntity;

    /**
     * @var Attribution[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attribution", mappedBy="user")
     */
    private $attributions;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="user")
     */
    private $commandes;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandeFournisseur", mappedBy="user")
     */
    private $commandeFournisseur;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandePrestataire", mappedBy="user")
     */
    private $commandePrestataire;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="user")
     */
    private $factureFournisseur;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="user")
     */
    private $facturePrestataire;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="user")
     */
    private $devis;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="user")
     */
    private $attachements;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="user")
     */
    private $factures;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Organisme", mappedBy="user")
     */
    private $organismes;

    /**
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Contact", mappedBy="user")
     */
    private $contacts;

    public function getRolesForm($role){

        if ($role[0] == 'ROLE_SUPER_SUDALYS' ) {
            $roles = [
                     'Administrateur' => 'ROLE_ADMIN',
                     'Utilisateur Sudalys' => 'ROLE_SUDALYS',
                     'Super Admin Sudalys' => 'ROLE_SUPER_SUDALYS',
                     'Administrif Sudalys' => 'ROLE_SUDALYS_ADMINISTRATIF'
            ];
        }else if ($role[0] == 'ROLE_SUDALYS_ADMINISTRATIF'){
            $roles = [
                    'Administrateur' => 'ROLE_ADMIN',
                    'Utilisateur Sudalys' => 'ROLE_SUDALYS',
                    'Administrif Sudalys' => 'ROLE_SUDALYS_ADMINISTRATIF'];
        }else if($role[0] == 'ROLE_SUDALYS') {
            $roles = [
                'Utilisateur Sudalys' => 'ROLE_SUDALYS'
            ];
        }
        return $roles;
    }

    public function __construct()
    {
         $this->isActive = true;
         $this->attributions = new ArrayCollection();
         $this->commandes = new ArrayCollection();
         $this->commandeFournisseur = new ArrayCollection();
         $this->commandePrestataire= new ArrayCollection();
         $this->factureFournisseur = new ArrayCollection();
         $this->facturePrestataire = new ArrayCollection();
         $this->devis = new ArrayCollection();
         $this->attachements = new ArrayCollection();
         $this->factures = new ArrayCollection();
         $this->organismes = new ArrayCollection();
         $this->contacts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone( $telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getRoles()
    {
        return $this->roles;
    }
    
    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set userEntity
     *
     * @param UserEntity $userEntity
     *
     * @return User
     */
    public function setUserEntity($userEntity)
    {
        $this->userEntity = $userEntity;

        return $this;
    }

    /**
     * Get userEntity
     *
     * @return UserEntity
     */
    public function getUserEntity()
    {
        return $this->userEntity;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set validity
     *
     * @param \DateTime $validity
     *
     * @return User
     */
    public function setValidity($validity)
    {
        $this->validity = $validity;

        return $this;
    }

    /**
     * Get validity
     *
     * @return \DateTime
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * Set portefeuille
     * 
     * @param float $portefeuille
     * 
     * @return User
     */
    public function setPortefeuille($portefeuille)
    {
        $this->portefeuille = $portefeuille;

        return $this;
    }

    /**
     * Get portefeuille
     * 
     * @return float
     */
    public function getPortefeuille()
    {
        return $this->portefeuille;
    }

     /**
      * @return Collection|Attribution[]
      */
     public function getAttributions()
     {
         return $this->attributions;
     }

     public function addAttributions(Attribution $attribution)
     {
         if (!$this->attributions->contains($attribution)) {
             $this->attributions[] = $attribution;
             $attribution->setUser($this);
         }

         return $this;
     }

     public function removeAttributions(Attribution $attribution)
     {
         if ($this->attributions->contains($attribution)) {
             $this->attributions->removeElement($attribution);
             // set the owning side to null (unless already changed)
             if ($attribution->getUser() === $this) {
                 $attribution->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|Commande[]
      */
     public function getCommandes()
     {
         return $this->commandes;
     }

     public function addCommande(Commande $commande)
     {
         if (!$this->commandes->contains($commande)) {
             $this->commandes[] = $commande;
             $commande->setUser($this);
         }

         return $this;
     }

     public function removeCommande(Commande $commande)
     {
         if ($this->commandes->contains($commande)) {
             $this->commandes->removeElement($commande);
             // set the owning side to null (unless already changed)
             if ($commande->getUser() === $this) {
                 $commande->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|CommandeFournisseur[]
      */
     public function getCommandeFournisseur()
     {
         return $this->commandeFournisseurs;
     }

     public function addCommandeFournisseur(CommandeFournisseur $commandeFournisseur)
     {
         if (!$this->commandeFournisseur->contains($commandeFournisseur)) {
             $this->commandeFournisseur[] = $commandeFournisseur;
             $commandeFournisseur->setUser($this);
         }

         return $this;
     }

     public function removeCommandeFournisseur(CommandeFournisseur $commandeFournisseur)
     {
         if ($this->commandeFournisseur->contains($commandeFournisseur)) {
             $this->commandeFournisseur->removeElement($commandeFournisseur);
             // set the owning side to null (unless already changed)
             if ($commandeFournisseur->getUser() === $this) {
                 $commandeFournisseur->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|CommandePrestataire[]
      */
     public function getCommandePrestataire()
     {
         return $this->commandePrestataire;
     }

     public function addCommandePrestataire(CommandePrestataire $commandePrestataire)
     {
         if (!$this->commandePrestataire->contains($commandePrestataire)) {
             $this->commandePrestataire[] = $commandePrestataire;
             $commandePrestataire->setUser($this);
         }

         return $this;
     }

     public function removeCommandePrestataire(CommandePrestataire $commandePrestataire)
     {
         if ($this->commandePrestataire>contains($commandePrestataire)) {
             $this->commandePrestataire->removeElement($commandePrestataire);
             // set the owning side to null (unless already changed)
             if ($commandePrestataire->getUser() === $this) {
                 $commandePrestataire->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|FactureFournisseur[]
      */
     public function getFactureFournisseur()
     {
         return $this->factureFournisseurs;
     }

     public function addFactureFournisseur(FactureFournisseur $factureFournisseur)
     {
         if (!$this->factureFournisseur->contains($factureFournisseur)) {
             $this->factureFournisseur[] = $factureFournisseur;
             $factureFournisseur->setUser($this);
         }

         return $this;
     }

     public function removeFactureFournisseur(FactureFournisseur $factureFournisseur)
     {
         if ($this->factureFournisseur->contains($factureFournisseur)) {
             $this->factureFournisseur->removeElement($factureFournisseur);
             // set the owning side to null (unless already changed)
             if ($factureFournisseur->getUser() === $this) {
                 $factureFournisseur->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|FacturePrestataire[]
      */
     public function getFacturePrestataire()
     {
         return $this->facturePrestataire;
     }

     public function addFacturePrestataire(FacturePrestataire $facturePrestataire)
     {
         if (!$this->facturePrestataire->contains($facturePrestataire)) {
             $this->facturePrestataire[] = $facturePrestataire;
             $facturePrestataire->setUser($this);
         }

         return $this;
     }

     public function removeFacturePrestataire(FacturePrestataire $facturePrestataire)
     {
         if ($this->facturePrestataire->contains($facturePrestataire)) {
             $this->facturePrestataire->removeElement($facturePrestataire);
             // set the owning side to null (unless already changed)
             if ($facturePrestataire->getUser() === $this) {
                 $facturePrestataire->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|Devis[]
      */
     public function getDevis()
     {
         return $this->devis;
     }

     public function addDevi(Devis $devi)
     {
         if (!$this->devis->contains($devi)) {
             $this->devis[] = $devi;
             $devi->setUser($this);
         }

         return $this;
     }

     public function removeDevi(Devis $devi)
     {
         if ($this->devis->contains($devi)) {
             $this->devis->removeElement($devi);
             // set the owning side to null (unless already changed)
             if ($devi->getUser() === $this) {
                 $devi->setUser(null);
             }
         }

         return $this;
     }

     /**
      * @return Collection|Attachement[]
      */
     public function getAttachements()
     {
         return $this->attachements;
     }

     public function addAttachements(Attachement $attachement)
     {
         if (!$this->attachements->contains($attachement)) {
             $this->attachements[] = $attachement;
             $attachement->setUser($this);
         }

         return $this;
     }

     public function removeAttachements(Attachement $attachement)
     {
         if ($this->attachements->contains($attachement)) {
             $this->attachements->removeElement($attachement);
             // set the owning side to null (unless already changed)
             if ($attachement->getUser() === $this) {
                 $attachement->setUser(null);
             }
         }

         return $this;
     }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures()
    {
        return $this->factures;
    }

    public function addFacture(Facture $facture)
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setUser($this);
        }

        return $this;
    }

    public function removeFacture(Facture $facture)
    {
        if ($this->factures->contains($facture)) {
            $this->factures->removeElement($facture);
            // set the owning side to null (unless already changed)
            if ($facture->getUser() === $this) {
                $facture->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Organisme[]
     */
    public function getOrganismes()
    {
        return $this->organismes;
    }
}
