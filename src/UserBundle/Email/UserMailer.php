<?php

namespace UserBundle\Email;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use UserBundle\Entity\User;

class UserMailer {

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    private $message;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;

    }

    public function sendNewNotification(User $user, $adresseWeb){

        $token = $user->getToken();
        $this->message = \Swift_Message::newInstance()
            ->setSubject('Saisie de vos Identifiants de Connexion')
            ->setFrom('gestit@sudalys.fr')
            ->setTo($user->getEmail())
            ->setBody('Merci de cliquer sur ce lien pour enregistrer votre mot de passe de connexion: 
                        '.$adresseWeb.'/registration/'.$token, 'text/plain')
        ;
        $this->mailer->send($this->message);
    }
}