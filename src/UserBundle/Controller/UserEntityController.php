<?php

namespace UserBundle\Controller;

use Sudalys\UserBundle\Form\EntityEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sudalys\UserBundle\Entity\UserEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserEntityController extends Controller
{
    public function indexAction()
    {
        $entityId = $this->getUser()->getUserEntity()->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:UserEntity')->find($entityId);
        if (null === $entity) {
            throw new NotFoundHttpException("L'entité d'id ".$entityId." n'existe pas.");
        }
        return $this->render('UserBundle:Entity:index.html.twig', array(
            'entity' => $entity
        ));
    }
    public function EntityEditAction(Request $request)
    {
        $entityId = $this->getUser()->getUserEntity()->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SudalysUserBundle:UserEntity')->find($entityId);
        if (null === $entity) {
            throw new NotFoundHttpException("L'entity d'id " . $entityId . " n'existe pas.");
        }
        $form = $this->get('form.factory')->create(EntityEditType::class, $entity);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Entité bien modifiée.');
            return $this->redirectToRoute('sudalys_entity_view', array('id' => $entity->getId()));
        }

        return $this->render('UserBundle:Entity:edit.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }
}