<?php

namespace UserBundle\Controller;

use UserBundle\Form\UserRegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;



class RegistrationController extends Controller {

    public function registrationAction($token, Request $request){
        // On récupère l'user $id
        $user = $this->getDoctrine()
            ->getManager()
            ->getRepository('UserBundle:User')
            ->findOneBy(array('token'=>$token));

        if (null === $user) {
            $request->getSession()->getFlashBag()->add('info', 'le token n\'est pas valide');
            return $this->redirectToRoute('login');
        }
        /* On récupère la date du jour et la date de validité du token */
        $date = date('Y-m-d H:i:s');
        $userDate = $user->getValidity()->format('Y-m-d H:i:s');

        /* si la date du jour est inférieur à la date de validity du token on arrête le traitement. */
        if ( $userDate < $date){
            $request->getSession()->getFlashBag()->add('info', 'La date de validité du token de sécurité est expirée valide;');
            return $this->redirectToRoute('login');

        }else {
            $form = $this->get('form.factory')->create(UserRegistrationType::class, $user);
            // On vérifie la requête = Post, on crée le lien requête <-> formulaire et on vérifie que les valeurs entrées sont valides
            if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {


                //On encode le password
                $encoder = $this->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                // on enregistre User dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $request->getSession()->getFlashBag()->add('info', 'Votre mot de passe a bien été enregistré !');
                return $this->redirectToRoute('login');
            }

            return $this->render('UserBundle:User:registration.html.twig', array(
                'user' => $user,
                'form' => $form->createView()
            ));
        }
    }

}