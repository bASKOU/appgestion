<?php

namespace UserBundle\Controller;

use DateTime;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\UserEditType;
use UserBundle\Form\UserType;
use UserBundle\Form\LostPassType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserController extends Controller
{
    private function getValidity()
    {
        /* Génération de la date de validité du token date du jour + 48 heures */
        $date = date('Y-m-d H:i:s');
        $validity = date("Y-m-d H:i:s", strtotime("+2 day", strtotime($date)));
        return new DateTime($validity);
    }
    private function getToken()
    {
        /* Génération du token temporaire pour la finalisation de l'inscription de l'utilisteur */
        $chaine = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $melange = str_shuffle($chaine);
        return substr($melange, 0, 32);
    }
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;

    public function __contruct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * Méthode pour affaicher la liste des utilisateurs
     * @return Response
     */
    public function indexAction()
    {
        $id = $this->getUser()->getUserEntity()->getId();

        $listUsers = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')->findBy(['userEntity' => $id]);

        // On donne toutes les informations nécessaires à la vue
        return $this->render('UserBundle:User:index.html.twig', array(
            'listUsers' => $listUsers
        ));
    }

    /**
     * Méthode pour afficher les détails d'un utilisateur
     * @param $id
     * @return Response
     */
    public function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        if (null === $user) {
            throw new NotFoundHttpException("L'utilisateur d'id ".$id." n'existe pas.");
        }
            return $this->render('UserBundle:User:view.html.twig', array(
                'user' => $user
            ));
    }

    /**
     * Méthode pour ajouter un utilisateur
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $role = $this->getUser()->getRoles()[0];
        $entity = $this->getUser()->getUserEntity();
        $user = new User();

        $form = $this->get('form.factory')->create(UserType::class, $user, ['role' => $role]);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $token = $this->getToken();
            $user->setToken($token);

            $dateValide= $this->getValidity();
            $user->setValidity($dateValide);

            $user->setUserEntity($entity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $mail = $this->get('email.user.mailer');
            $mail->sendNewNotification($user, $this->getParameter('adresse_web'));
            $request->getSession()->getFlashBag()->add('erreur', 'Utilisateur bien enregistré.');
            return $this->redirectToRoute('sudalys_users_view', array('id' => $user->getId()));
        }
        return $this->render('UserBundle:User:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Méthode pour éditer un utilisateur
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $role = $this->getUser()->getRoles()[0];
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        if (null === $user) {
            throw new NotFoundHttpException("L'utilisateur d'id " . $id . " n'existe pas.");
        }
        $form = $this->get('form.factory')->create(UserEditType::class, $user, ['role' => $role]);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Utilisateur bien modifié.');
            return $this->redirectToRoute('sudalys_users_view', array('id' => $user->getId()));
        }

        return $this->render('UserBundle:User:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Méthode pour supprimer un utilisateur
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        if (null === $user) {
            throw new NotFoundHttpException("L'utilisateur d'id " . $id . " n'existe pas.");
        }
        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'utilisateur contre cette faille
        $form = $this->get('form.factory')->create();
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->remove($user);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', "L'utilisateur a bien été supprimé.");
            return $this->redirectToRoute('sudalys_users_home');
        }
        return $this->render('UserBundle:User:delete.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Méthode pour récupérer le mot de passe perdu d'un utilisateur
     * Attention pas encore testé
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function lostPassAction (Request $request)
    {
        $user = new User();

        $form = $this->get('form.factory')->create(LostPassType::class, $user);
        // On vérifie la requête = Post, on crée le lien requête <-> formulaire et on vérifie que les valeurs entrées sont valides
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $donnees = $request->get('lost_pass');

            $user = $this->getDoctrine()
            ->getManager()
            ->getRepository('UserBundle:User')
            ->findOneBy(array('email'=>$donnees['email']));

            if (null === $user) {
                $request->getSession()->getFlashBag()->add('info', 'L\'adresse mail n\'éxiste pas');
                return $this->redirectToRoute('login');
            } elseif( $user->getIsActive() == FALSE ) {
                $request->getSession()->getFlashBag()->add('info', 'Le compte utilisateur est désactivé');
                return $this->redirectToRoute('login');
            }else{
                $user->setPassword(null);
            // on enregistre User dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $mail = $this->get('email.user.mailer');
                $mail->sendNewNotification($user, $this->getParameter('adresse_web'));
                $request->getSession()->getFlashBag()->add('info', "Un email pour créer votre nouveau mot de passe vous a été envoyé.");
                return $this->redirectToRoute('login');
            }
        }

        return $this->render('UserBundle:User:lostPass.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));
    }

    /**
     * Méthode pour envoyer un nouvel email de confirmation pour activer le compte
     * @param $userId
     * @return RedirectResponse
     */
    public function relanceMailAction($userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        $token = $this->getToken();
        $user->setToken($token);

        $dateValide= $this->getValidity();
        $user->setValidity($dateValide);

        $em->persist($user);

        try {
            $em->flush();
            $mail = $this->get('email.user.mailer');
            $response = $mail->sendNewNotification($user, $this->getParameter('adresse_web'));
            if ($response['status'] === 'success') {
                $this->addFlash('success', "Un nouvel email a été envoyé à l'utilisateur pour valider son compte.");
            } else {
                $this->addFlash('danger', "Une erreur c'est produite lors de l'envoi de l'email, vueillez réessayer.");
            }
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'utilisateur');
            $this->addFlash('warning', $message);
        }

        return $this->redirectToRoute('sudalys_users_home');
    }
}
