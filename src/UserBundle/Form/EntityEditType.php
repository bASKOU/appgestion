<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntityEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    $builder
    ->add('nom',     TextType::class)
    ->add('adresse',    TextType::class)
    ->add('adresse_2',    TextType::class, array("empty_data"=>"", "required"=>false))
    ->add('code_postal',    TextType::class)
    ->add('ville',    TextType::class)
    ->add('email',   EmailType::class, [
        'attr' => [
            'pattern' => '^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{1,4}$'
        ]
    ])
    ->add('telephone',    TextType::class)
    ->add('mobile',    TextType::class)
    ->add('Enregistrer',      SubmitType::class);

    // On ajoute une fonction qui va écouter un évènement
    $builder->addEventListener(
    FormEvents::PRE_SET_DATA,
        function(FormEvent $event) { // La fonction à exécuter lorsque l'évènement est déclenché
        // On récupère notre objet User sous-jacent
        $user = $event->getData();

            if (null === $user) {
            return; // On sort de la fonction sans rien faire lorsque $user vaut null
            }
        }
    );
}

    public function configureOptions(OptionsResolver $resolver)
    {
    $resolver->setDefaults(array(
        'data_class' => 'UserBundle\Entity\UserEntity'
        ));
    }
}