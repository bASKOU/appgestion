<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',     TextType::class, [
                'required' => true
            ])
            ->add('prenom',    TextType::class, [
                'required' => true
            ])
            ->add('email',   EmailType::class)
        ;
        if ($options['role'] === 'ROLE_SUPER_SUDALYS') {
            $builder
                ->add('roles', ChoiceType::class, array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices'  => array(
                        'Utilisateur' => 'ROLE_SUDALYS',
                        'Administratif' => 'ROLE_SUDALYS_ADMINISTRATIF',
                        'Super-Administrateur' => 'ROLE_SUPER_SUDALYS'
                    ),
                    'required' => true
                ));
        }
        if ($options['role'] === 'ROLE_SUDALYS') {
            $builder
                ->add('roles', ChoiceType::class, array(
                    'multiple' => true,
                    'expanded' => true,
                    'choices'  => array(
                        'Utilisateur' => 'ROLE_SUDALYS'
                    ),
                    'required' => true
                ));
        }
        $builder
            ->add('isActive', ChoiceType::class, array(
                'label' => 'Actif',
                'multiple' => false,
                'expanded' => false,
                'choices'  => array(
                    'Actif ' => '1',
                    'Inactif' => '0'
                ),
                'required' => true
            ))
            ->add('telephone', TelType::class, [
                    'label' => 'Téléphone',
                    'required' => false]
            )
            ->add('mobile', TelType::class, [
                    'required' => false]
            )
            ->add('Enregistrer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
            'role' => null
        ));
    }
}