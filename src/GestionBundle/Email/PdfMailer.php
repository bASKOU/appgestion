<?php

namespace GestionBundle\Email;

//! classe non utilisée
//TODO : utiliser cette classe comme service afin de décharger le controller d'exportPDF de la répétition de méthodes superflues pour envoyer les mails
class PdfMailer {

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $content
     * @param $filename
     * @param $numeroOperation
     * @param $userName
     */
    public function sendMail($content, $filename, $numeroOperation, $userName){

        $attachment = new \Swift_Attachment($content, $filename, 'application/pdf');

        $message = new \Swift_Message('Bonjour, veuillez trouver ci-joint le devis concernant le projet dont nous avons parlé ce matin au téléphone.\r\n En attente de votre retour, je reste à votre entière disposition, \n Cordialement, \r\n Nicolas Thiriet');
        $message
            ->setSubject('Devis n° '.$numeroOperation)
            ->setFrom(array('gestit@sudalys.fr' => $userName))
            ->setTo('c.ahetz-etcheber@sudalys.fr')
            ->attach($attachment);

        $this->mailer->send($message);
    }
}