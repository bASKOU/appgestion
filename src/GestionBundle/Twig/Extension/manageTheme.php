<?php

namespace GestionBundle\Twig\Extension;

class manageTheme extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'manage_theme';
    }
}
