<?php


namespace GestionBundle\Services;


use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ComboChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Options\VAxis;

/**
 * Class ChartsService
 * @package GestionBundle\Services
 * Service de gestion des graphiques
 */

class ChartsService
{
    const ANIMATION_STARTUP = true;
    const ANIMATION_DURATION = 1000;
    const CHART_AREA_HEIGHT = '80%';
    const CHART_AREA_WIDTH = '80%';
    /**
     * @var string[]
     */
    private $monthName = [1 => "JAN", 2 =>  "FEV", 3 => "MAR", 4 => "AVR", 5 => "MAI", 6 => "JUN", 7 => "JUL", 8 => "AOU", 9 => "SEP", 10 =>  "OCT", 11 => "NOV", 12 => "DEC"];

    private function clearMonthNumber(&$months) {
        foreach ($months as $key => $month) {
            $months[$key] = intval(substr(strval($month), 0, -2));
        }
    }

    public function generateChartEvoROPAffaire($arrayToDataTable)
    {
        $chart = new ComboChart();
        $chart->getData()->setArrayToDataTable($arrayToDataTable);
        $chart->getOptions()->getAnimation()->setStartup(self::ANIMATION_STARTUP);
        $chart->getOptions()->getAnimation()->setDuration(self::ANIMATION_DURATION);
        $chart->getOptions()->getChartArea()->setHeight(self::CHART_AREA_HEIGHT);
        $chart->getOptions()->getChartArea()->setWidth(self::CHART_AREA_WIDTH);

        $vAxisAmount = new VAxis();
        $vAxisAmount->setTitle('Montant en €');
        $vAxisAmount->getMinorGridlines()->setColor('none');
        $vAxisMargin = new VAxis();
        $vAxisMargin->setTitle('Marge brut en %');
        $vAxisMargin->getGridlines()->setColor('none');
        $chart->getOptions()->setVAxes([$vAxisAmount, $vAxisMargin]);

        $seriesProd = new \CMEN\GoogleChartsBundle\GoogleCharts\Options\ComboChart\Series();
        $seriesProd->setType('bars');
        $seriesProd->setTargetAxisIndex(0);
        $seriesCharges = new \CMEN\GoogleChartsBundle\GoogleCharts\Options\ComboChart\Series();
        $seriesCharges->setType('lines');
        $seriesCharges->setTargetAxisIndex(0);
        $seriesMargin = new \CMEN\GoogleChartsBundle\GoogleCharts\Options\ComboChart\Series();
        $seriesMargin->setType('line');
        $seriesMargin->setTargetAxisIndex(1);
        $chart->getOptions()->setSeries([$seriesProd, $seriesCharges, $seriesMargin]);
        $chart->getOptions()->setLineWidth(4);
        $chart->getOptions()->setPointShape('circle');
        $chart->getOptions()->setPointsVisible(true);

        $chart->getOptions()->getHAxis()->setTitle('Mois');
        $chart->getOptions()->setColors(['#fd0000', '#34a96b', '#bfbfbf']);

        return $chart;
    }

    /**
     * @param $materPerMonth
     * @param $prestaPerMonth
     * @param $estimPrestaPerMonth
     * @param $estimFournPerMonth
     * @param $prodPerMonth
     * @param $months
     * @param $years
     * @return mixed
     */
    public function arrayToTableForResultOP($materPerMonth, $prestaPerMonth, $estimPrestaPerMonth, $estimFournPerMonth, $prodPerMonth, $months, $years)
    {
        $arrayToDataTable[] = ['Mois', 'Production', ['role' => 'tooltip'], 'Charges', ['role' => 'tooltip'], 'Marge brut en %', ['role' => 'tooltip']];
        $chargesPerMonth = [];
        $marginPerMonth = [];
        $this->clearMonthNumber($months);
        foreach ($materPerMonth as $key => $month) {
            $chargesPerMonth[$key] = $materPerMonth[$key] + $prestaPerMonth[$key] + $estimPrestaPerMonth[$key] + $estimFournPerMonth[$key];
            $marginPerMonth[$key] = 0;
            if ($chargesPerMonth[$key] != (float) 0) {
                $marginPerMonth[$key] = ($prodPerMonth[$key] - $chargesPerMonth[$key]);
                if ($prodPerMonth[$key] != (float) 0) {
                    $marginPerMonth[$key] = ($marginPerMonth[$key] / $prodPerMonth[$key]) * 100;
                }
            }
            $arrayToDataTable[] = [
                $this->monthName[$months[$key]] . ' ' . $years[$key],
                $prodPerMonth[$key],
                number_format($prodPerMonth[$key], 2, ',', ' ') . ' €',
                $chargesPerMonth[$key],
                number_format($chargesPerMonth[$key], 2, ',', ' ') . ' €',
                $marginPerMonth[$key],
                number_format($marginPerMonth[$key], 2, ',', '') . " %"
            ];
        }

        return $arrayToDataTable;
    }

    /**
     * @param $perMonth
     * @param $tempTot
     */
    public function setPerMonth(&$perMonth, $tempTot)
    {
        foreach ($perMonth as $key => $month) {
            if (!empty($tempTot[$key])) {
                $perMonth[$key] = (float) $tempTot[$key];
            } else {
                $perMonth[$key] = (float) 0;
            }
        }
    }

    /**
     * @param $perMonth
     * @param $entities
     * @param $dateType
     * @return array
     */
    public function getTempTot($perMonth, $entities, $dateType) {
        $tempTot = [];
        $temp = 'get' . ucfirst($dateType);
        foreach ($entities as $entity) {
            $index = array_search(intval($entity->$temp()->format('my')), $perMonth);
            if (!empty($tempTot[$index])) {
                $tempTot[$index] += (float) $entity->getTotalHT();
            } else {
                $tempTot[$index] = (float) $entity->getTotalHT();
            }
        }

        return $tempTot;
    }

    /**
     * @param $perMonth
     * @param $factures
     * @param $dateType
     * @return array
     */
    public function getTempTotFacture($perMonth, $factures, $dateType) {
        $tempTot = [];
        $temp = 'get' . ucfirst($dateType);
        foreach ($factures as $facture) {
            $index = array_search(intval($facture->$temp()->format('my')), $perMonth);
            $totalEstim = $this->dispatchEstimationOfFacture($facture, $tempTot, $perMonth);
            $remaining = (float) $facture->getTotalHT() - $totalEstim;
            if (!empty($tempTot[$index])) {
                $tempTot[$index] += $remaining;
            } else {
                $tempTot[$index] = $remaining;
            }
        }

        return $tempTot;
    }

    /**
     * @param $estimPerMonth
     * @param $estimations
     * @return array
     */
    public function getTempTotEstim($estimPerMonth, $estimations) {
        $tempTot = [];
        foreach ($estimations as $estimation) {
            if ($estimation->getFacture() == null) {
                $index = array_search(intval($estimation->getDate()->format('my')), $estimPerMonth);
                if (!empty($tempTot[$index])) {
                    $tempTot[$index] += (float)$estimation->getTotalHT();
                } else {
                    $tempTot[$index] = (float)$estimation->getTotalHT();
                }
            }
        }

        return $tempTot;
    }

    /**
     * @param $facture
     * @param $tempTot
     * @param $perMonth
     * @return int
     */
    private function dispatchEstimationOfFacture($facture, &$tempTot, $perMonth)
    {
        $total = 0;
        foreach ($facture->getEstimation() as $estimation) {
            $index = array_search(intval($estimation->getDate()->format('my')), $perMonth);
            if (!empty($tempTot[$index])) {
                $tempTot[$index] += (float)$estimation->getTotalHT();
            } else {
                $tempTot[$index] = (float)$estimation->getTotalHT();
            }
            $total += $estimation->getTotalHT();
        }

        return $total;
    }
}