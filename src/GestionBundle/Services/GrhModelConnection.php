<?php


namespace GestionBundle\Services;


use PDO;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GrhModelConnection
{
    protected $connection;
    protected $PdoAssoc;

    public function __construct()
    {
        global $kernel;
        $this->connection = $kernel->getContainer()->get('doctrine')->getConnection('grh');
        $this->PdoAssoc = PDO::FETCH_ASSOC;
    }
}