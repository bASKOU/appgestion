<?php


namespace GestionBundle\Services;


use DateTime;
use GestionBundle\Entity\InfoPrestataire;

class CheckDocumentPrestataireValidity
{
    /**
     * @var bool
     */
    public $hasChanged = false;

    public function __construct(InfoPrestataire &$infoPrestataire)
    {
        if ($infoPrestataire->getIsEtrangere() == false) {
            $infoPrestataire->setIsValidKbis($this->checkKbisValidity($infoPrestataire->getDateKbis()));
            $infoPrestataire->setIsValidResponsabiliteProfessionnel($this->checkAssuranceProValidity($infoPrestataire->getDateResponsabiliteProfessionnel()));
            $infoPrestataire->setIsValidAttestationFiscale($this->checkAttestationFiscalValidity($infoPrestataire->getDateAttestationFiscale()));
            $infoPrestataire->setIsValidAttestationURSSAF($this->checkAttestationURSSAFValidity($infoPrestataire->getDateAttestationURSSAF()));
            if ($infoPrestataire->getDateRegistreProfessionelCertifInscription()) {
                $infoPrestataire->setIsValidRegistreProfessionelCertifInscription($this->check6Months($infoPrestataire->getDateRegistreProfessionelCertifInscription()));
            }
            if ($infoPrestataire->getDateAttestationImmatriculation()) {
                $infoPrestataire->setIsValidAttestationImmatriculation($this->check6Months($infoPrestataire->getDateAttestationImmatriculation()));
            }
        } else {
            $infoPrestataire->setIsValidAttestationImmatriculation($this->check6Months($infoPrestataire->getDateAttestationImmatriculation()));
            $infoPrestataire->setIsValidSocialEtranger($this->check6Months($infoPrestataire->getDateSocialEtranger()));
            $infoPrestataire->setIsValidRegistreProfessionelCertifInscription($this->check6Months($infoPrestataire->getDateRegistreProfessionelCertifInscription()));
        }
    }

    /**
     * Ajout du nombre de mois sélectionné
     * @param DateTime $date
     * @param int $monthsToAdd
     * @return DateTime
     */
    private function addMonths($date, $monthsToAdd) {
        $tmpDate = clone $date;
        $tmpDate->modify('first day of +'.(int) $monthsToAdd.' month');

        if($date->format('j') > $tmpDate->format('t')) {
            $daysToAdd = $tmpDate->format('t') - 1;
        }else{
            $daysToAdd = $date->format('j') - 1;
        }

        $tmpDate->modify('+ '. $daysToAdd .' days');

        return $tmpDate;
    }

    /**
     * @param DateTime $validity
     * @return bool
     */
    private function choiceReturn(DateTime $validity)
    {
        $today = new DateTime('now');
        if (strtotime($validity->format('Y-m-d H:i:s')) < strtotime($today->format('Y-m-d H:i:s'))) {
            $this->hasChanged = true;
            return false;
        } else {
            return true;
        }
    }

    /**
     * Méthode pour vérifier la validaité d'un kbis
     * @param DateTime $dateKbis
     * @return bool
     */
    public function checkKbisValidity(DateTime $dateKbis)
    {
        $validity = $this->addMonths($dateKbis, 3);
        return $this->choiceReturn($validity);
    }

    /**
     * Méthode pour vérifier la validaité d'une attestation fiscale
     * @param DateTime $dateAttestationFiscal
     * @return bool
     */
    public function checkAttestationFiscalValidity(DateTime $dateAttestationFiscal)
    {
        $validity = $this->addMonths($dateAttestationFiscal, 6);
        return $this->choiceReturn($validity);
    }

    /**
     * Méthode pour vérifier la validaité d'une attestation URSSAF
     * @param DateTime $dateAttestationURSSAF
     * @return bool
     */
    public function checkAttestationURSSAFValidity(DateTime $dateAttestationURSSAF)
    {
        $validity = $this->addMonths($dateAttestationURSSAF, 6);
        return $this->choiceReturn($validity);
    }

    /**
     * Méthode pour vérifier
     * @param $dateAssu
     * @return bool
     */
    public function checkAssuranceProValidity($dateAssu)
    {
        return $this->choiceReturn($dateAssu);
    }

    public function check6Months($date)
    {
        $validity = $this->addMonths($date, 6);
        return $this->choiceReturn($validity);
    }
}