<?php


namespace GestionBundle\Services;


class ExceptionHandler
{
    public function getException($e)
    {
        switch ($e) {
            case strpos($e, '[23505]') != false :
                return [23505, $this->get_string_between($e)];
                break;
            case strpos($e, '[23502]') != false :
                return [23502, $this->get_string_between($e)];
                break;
            case strpos($e, '[23503]') != false :
                return [23503, $this->get_string_between($e)];
                break;
            case strpos($e, '[08000]') != false ||  strpos($e, '[08001]') != false || strpos($e, '[08002]') != false || strpos($e, '[08003]') != false || strpos($e, '[08004]') != false || strpos($e, '[08005]') != false || strpos($e, '[08006]') != false:
                return [8000, $this->get_string_between($e)];
                break;
            default:
                return 'Une erreur s\'est produite, veuillez réessayer...';
                break;
        }
    }

    // Fonction qui permet de gérer les exceptions
    public function exceptionHandler($code, $item=null) {
        switch ($code[0]) {
            case 23505:
                return $this->selectErrorMessage($code[1], $item);
            case 23502:
                return 'Un élément requis n\'a pas était renseigné, vérifiez que tous les champs comportant une astérisque soit rempli.';
            case 23503:
                return 'Cette élément ne peut être supprimer car il est lié à un autre élément.';
            case 8000:
                return 'Erreur de connexion avec la base de données, contactez l\'administrateur.';
            default:
                return 'Une erreur s\'est produite, veuillez réessayer...';
        }
    }

    private function selectErrorMessage($column ,$item) {
        switch ($item) {
            case 'thème':
                return 'Impossible d\'ajouter ce ' . $item . ' car le nom ' . $column . ' existe déjà';
            case 'article' || 'nom' || 'email':
                return 'Impossible d\'ajouter cet ' . $item . ' car il existe déjà';
            case 'telephone':
                return 'Impossible d\'ajouter ce numéro de téléphone' . $column . ' car il existe déjà';
            case 'mobile':
                return 'Impossible d\'ajouter ce numéro de téléphone mobile' . $column . ' car il existe déjà';
            case 'avoir':
                return 'Cette facture possède déja un ' . $item . ' reportez-vous à la section avoir pour le modifier.';
            case 'organisme':
                return 'Impossible de créer cet organisme car le ' .$column . ' est présent dans un autre organisme.';
            case 'orgaMAJ':
                return 'Impossible de modifier cet organisme car le ' . $column . ' est présent dans un autre organisme.';
            case 'agenceMAJ':
                return 'Impossible de modifier cette agence car le ' . $column . ' est présent dans une autre agence.';
            case 'agence':
                return 'Impossible de créer cette agence car le ' . $column . ' est présent dans une autre agence.';
            case 'contact':
                return 'Impossible de créer ce contact car le ' . $column . ' est présent dans un autre contact.';
            case 'contactMAJ':
                return 'Impossible de modifier ce contact car le ' . $column . ' est présent dans un autre contact.';
            case 'utilisateur':
                return 'Le' . $column . ' existe déjà dans un ' . $item . '.';
            default:
                return 'Ajout impossible car ce terme existe déjà';
        }
    }

    private function get_string_between($string){
        $start = '=(';
        $end = ')';
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}