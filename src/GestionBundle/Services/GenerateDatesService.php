<?php


namespace GestionBundle\Services;


use DateTime;

class GenerateDatesService
{
    /**
     * @var string[]
     */
    private $monthName = [1 => "JAN", 2 =>  "FEV", 3 => "MAR", 4 => "AVR", 5 => "MAI", 6 => "JUN", 7 => "JUL", 8 => "AOU", 9 => "SEP", 10 =>  "OCT", 11 => "NOV", 12 => "DEC"];

    /**
     * @var string[]
     */
    private $fullMonthName = [1 => "Janvier", 2 =>  "Février", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "Juillet", 8 => "Août", 9 => "Septembre", 10 =>  "Octobre", 11 => "Novembre", 12 => "Décembre"];

    /**
     * Ajout du nombre de mois sélectionné
     * @param DateTime $date
     * @param int $monthsToAdd
     * @return DateTime
     */
    public function addMonths($date, $monthsToAdd) {
        $tmpDate = clone $date;
        $tmpDate->modify('first day of +'.(int) $monthsToAdd.' month');

        if($date->format('j') > $tmpDate->format('t')) {
            $daysToAdd = $tmpDate->format('t') - 1;
        }else{
            $daysToAdd = $date->format('j') - 1;
        }

        $tmpDate->modify('+ '. $daysToAdd .' days');
        return $tmpDate;
    }

    public function generateListOfMonthsAndYearFromDateDebut(DateTime $date)
    {
        $today = new DateTime('today');
        $dates = [];
        $dates[] = ['monthNumber' => $date->format('my'), 'month' => $this->monthName[intval($date->format('m'))], 'year' => $date->format('y'), 'dateTime' => clone $date];
        while (strtotime($today->format('Y-m')) > strtotime($date->format('Y-m'))) {
            $date->modify('+ 1 month');
            $dates[] = ['monthNumber' => $date->format('my'), 'month' => $this->monthName[intval($date->format('m'))], 'year' => $date->format('y'), 'dateTime' => clone $date];
        }

        return $dates;
    }

    public function generateListOfMonthsAndYearFromDate(DateTime $date, $diff)
    {
        $dates = [];
        $i = 0;
        while ($i < $diff) {
            $date->modify('+ 1 month');
            $dates[] = ['monthNumber' => $date->format('my'), 'month' => $this->monthName[intval($date->format('m'))], 'year' => $date->format('y'), 'dateTime' => clone $date];
            $i++;
        }

        return $dates;
    }

    public function generateListOfMonthsAndYearFromStartAndEnd($dateDebut, $dateFin)
    {
        $dates = [];
        $dates[] = ['monthNumber' => $dateDebut->format('my'), 'month' => $this->monthName[intval($dateDebut->format('m'))], 'year' => $dateDebut->format('y'), 'dateTime' => clone $dateDebut];
        while (strtotime($dateFin->format('Y-m')) > strtotime($dateDebut->format('Y-m'))) {
            $dateDebut->modify('+ 1 month');
            $dates[] = ['monthNumber' => $dateDebut->format('my'), 'month' => $this->monthName[intval($dateDebut->format('m'))], 'year' => $dateDebut->format('y'), 'dateTime' => clone $dateDebut];
        }

        return $dates;
    }

    public function getDateForCheckAvancementCommand()
    {
        $today = new DateTime('today');
        $month = intval($today->format('m'));
        $year = intval($today->format('Y'));
        if (intval($today->format('d')) < 15) {
            if ($month === 1) {
                $month = 12;
                $year -= 1;
            } else {
                $month -= 1;
            }
            $today->modify('- 1 month');
        }

        return ['dateObject' => $today, 'monthInt' => $month, 'yearInt' => $year, 'fullMonthName' => $this->fullMonthName[$month]];
    }

    public function getDateForCheckAvancementCommandNotComplete()
    {
        $today = new DateTime('today');
        $month = intval($today->format('m'));
        $year = intval($today->format('Y'));
        if ($month === 1) {
            $month = 12;
            $year -= 1;
        } else {
            $month -= 1;
        }
        $today->modify('- 1 month');

        return ['dateObject' => $today, 'monthInt' => $month, 'yearInt' => $year, 'fullMonthName' => $this->fullMonthName[$month]];
    }
}