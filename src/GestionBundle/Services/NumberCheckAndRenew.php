<?php


namespace GestionBundle\Services;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class NumberCheckAndRenew
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var string[]
     */
    private $arrayOfFirstIndex = ['devis' => 'DE-', 'facture' => 'FA-', 'factureRecus' => 'FAR-', 'commandeEmise' => 'DA-', 'affaire' => 'AF', 'situation' => 'ST', 'commandeRecus' => 'CMD', 'estimationPrestation' => 'EPP', 'estimationFournisseur' => 'EPF'];

    /**
     * NumberCheckAndRenew constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Méthode pour vérifier si le mois présent dans la BDD et le même que le mois en cours
     * dans le cas contraire on change la valeur du mois
     * @param $month
     */
    private function checkMonth(&$month)
    {
        $thisMonth = (new DateTime())->format('m');
        $month != $thisMonth ? $month = $thisMonth : null;
    }

    /**
     * Méthode pour vérifier si l'année' présent dans la BDD et le même que l'année en cours
     * dans le cas contraire on change la valeur de l'année
     * @param $year
     */
    private function checkYear(&$year)
    {
        $thisYear = (new DateTime())->format('Y');
        $year != $thisYear ? $year = $thisYear : null;
    }

    /**
     * Méthode pour compléter l'id et ajouter des zéros
     * @param $id
     * @return string
     */
    private function addZero($id)
    {
        $toString = strval($id); // Integer to string
        $length = strlen($toString); // Calcul de length du string
        $loop = 3 - $length; // On soustrait la length du string à 3
        $complement = ''; // initialisation d'un variable string vide
        if ($loop > 0) { // Si loop est supérieur à 0
            for ($i = 1; $i <= $loop; $i++) {
                $complement .= '0'; // on concatène des 0 en string
            }
        }
        return $complement . $toString; // retourne la concaténation du complément en 0 et de l'id en string
    }

    /**
     * Méthode pour générer une nomenclature de numéro d'opération propre
     * ? dans le cas où on voudrais pouvoir réinitialiser le numéro de fin, passer par une contante de classe plutot que par l'id
     * @return string
     */
    private function createNumero($index) {
        $id = 1;
        $id = $this->addZero($id);
        $timestamp = (new \DateTime())->format('Y-m');
        $file = $this->arrayOfFirstIndex[$index];
        return $file . $timestamp . '-' . $id;
    }

    /**
     * Méthode utilisé pour récupérer le dernier numéro d'un élément selon l'argument passé
     * @param $id
     * @return int
     */
    private function getLastId($id, $arg)
    {
        if ($arg === 'attachement') {
            $sql = "SELECT CASE WHEN numero_attachement ISNULL THEN 'INDT' ELSE numero_attachement END FROM attachement WHERE commande_id = $id ORDER BY numero_attachement DESC LIMIT 1";
        } elseif ($arg === 'commande') {
            $sql = "SELECT CASE WHEN numero_commande ISNULL THEN 'INDT' ELSE numero_commande END FROM commande WHERE affaire_id = $id ORDER BY numero_commande DESC LIMIT 1";
        } elseif ($arg === 'avoir') {
            $sql = "SELECT CASE WHEN numero_avoir ISNULL THEN 'INDT' ELSE numero_avoir END FROM avoir WHERE facture_id = $id ORDER BY numero_avoir DESC LIMIT 1";
        } elseif ($arg === 'affaire') {
            $sql = "SELECT CASE WHEN numero_affaire ISNULL THEN 'INDT' ELSE numero_affaire END FROM affaire ORDER BY id DESC LIMIT 1";
        }
        $numAff = $this->em->getConnection()->FetchAll($sql);
        if ($numAff) {
            $temp = explode('-', $numAff[0]['numero_' . $arg]);
            $id = intval($temp[1]);
        } else {
            $id = 0;
        }
        return $id + 1;
    }

    /**
     * Méthode pour eviter les conflits de numéro de facture, il génère un nouveau numéro de facrture à partir du dernier devis créer
     * Dans le cas d'une création de commande ety facture en chaîne, la facture généré n'a pas de numéro, donc il faut récupérer les 2 dernier
     * et verifier si la dernière facture à un numéro ou non
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberFactureRecus()
    {
        $sql = "SELECT numero_facture FROM facture_prestataire ORDER BY id DESC LIMIT 2"; // Requête SQL afin de récupérer les deux derniers numéros de factures prestataire
        $result = $this->em->getConnection()->FetchAll($sql); // Stock le résultat de la requête dans un variable sous forme d'array
        if (!empty($result[1])) { // Si il y a un second résultat
            $result[0]['numero_facture'] == null ? $result[0]['numero_facture'] = $result[1]['numero_facture'] : null; // Vérifie si le premier élément de l'array n'est pas null, sinon on récupère le deuxième
        }
        $sql = "SELECT numero_facture FROM facture_fournisseur ORDER BY id DESC LIMIT 2"; // Requête SQL afin de récupérer les deux derniers numéros de factures fournisseur
        $resultBis = $this->em->getConnection()->FetchAll($sql); // Stock le résultat de la requête dans un variable sous forme d'array
        if (!empty($resultBis[1])) { // Si il y a un second résultat
            $resultBis[0]['numero_facture'] == null ? $resultBis[0]['numero_facture'] = $resultBis[1]['numero_facture'] : null; // Vérifie si le premier élément de l'array n'est pas null, sinon on récupère le deuxième
        }
        if (!empty($result[0]) && !empty($resultBis[0])) { // Si les 2 array on à l'index 0 une valeur
            $exp = explode('-', $result[0]['numero_facture']); // On split le string en array
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            $tempNumber = intval($exp[3]) + 1; // On récupère la valeur à l'index 3 que l'on incrémente
            $expBis = explode('-', $resultBis[0]['numero_facture']); // Idem pour le second
            $tempNumberBis = intval($expBis[3]) + 1;
            if ($tempNumberBis < $tempNumber) { // On vérifie quelle valeur est la plus élevé
                if ($timestampYear != $exp[1]) {
                    $tempNumber = '001';
                } else {
                    $tempNumber = intval($exp[3]) + 1;
                }
                $exp[3] = strval($this->addZero($tempNumber)); // Puis on l'assigne
            } else {
                if ($timestampYear != $exp[1]) {
                    $tempNumberBis = '001';
                } else {
                    $tempNumberBis = intval($exp[3]) + 1;
                }
                $exp[3] = strval($this->addZero($tempNumberBis));
            }
        } elseif (!empty($result[0])) { // Si le premier array n'est pas vide
            $exp = explode('-', $result[0]['numero_facture']); // On split le string en array
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber)); // On assigne la valeur incrémenté
        } elseif (!empty($resultBis[0])) { // Si le deuxième array n'est pas vide on fait la même chose que cité précédemment
            $exp = explode('-', $resultBis[0]['numero_facture']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
        } else { // Si les deux arrays son vide
            return $this->createNumero('factureRecus'); // On créé un nouveau numéro à 001
        }
        return join('-', $exp);
    }

    /**
     * Méthode pour eviter les conflits de numéro de devis, il génère un nouveau numéro de devis à partir du dernier devis créer
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberCommandeEmise()
    {
        $sql = "SELECT numero_commande_presta FROM commande_prestataire ORDER BY id DESC LIMIT 1";
        $result = $this->em->getConnection()->FetchAll($sql);
        $sql = "SELECT numero_commande_fourn FROM commande_fournisseur ORDER BY id DESC LIMIT 1";
        $resultBis = $this->em->getConnection()->FetchAll($sql);
        if (!empty($result[0]) && !empty($resultBis[0])) {
            $exp = explode('-', $result[0]['numero_commande_presta']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            $tempNumber = intval($exp[3]) + 1;
            $expBis = explode('-', $resultBis[0]['numero_commande_fourn']);
            $tempNumberBis = intval($expBis[3]) + 1;
            if ($tempNumberBis < $tempNumber) {
                if ($timestampYear != $exp[1]) {
                    $tempNumber = '001';
                } else {
                    $tempNumber = intval($exp[3]) + 1;
                }
                $exp[3] = strval($this->addZero($tempNumber));
            } else {
                if ($timestampYear != $exp[1]) {
                    $tempNumberBis = '001';
                } else {
                    $tempNumberBis = intval($exp[3]) + 1;
                }
                $exp[3] = strval($this->addZero($tempNumberBis));
            }
        } elseif (!empty($result[0])) {
            $exp = explode('-', $result[0]['numero_commande_presta']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
        } elseif (!empty($resultBis[0])) {
            $exp = explode('-', $resultBis[0]['numero_commande_fourn']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
        } else {
            return $this->createNumero('commandeEmise');
        }
        return join('-', $exp);
    }

    /**
     * Méthode pour eviter les conflits de numéro de devis, il génère un nouveau numéro de devis à partir du dernier devis créer
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberDevis()
    {
        $sql = "SELECT numero_devis FROM Devis ORDER BY id DESC LIMIT 1";
        $result = $this->em->getConnection()->FetchAll($sql);
        if (!empty($result[0])) {
            $exp = explode('-', $result[0]['numero_devis']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
            return join('-', $exp);
        } else {
            return $this->createNumero('devis');
        }
    }

    /**
     * Méthode pour eviter les conflits de numéro de facture, il génère un nouveau numéro de facrture à partir du dernier devis créer
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberFacture()
    {
        $sql = "SELECT numero_facture FROM Facture ORDER BY id DESC LIMIT 1";
        $result = $this->em->getConnection()->FetchAll($sql);
        if (!empty($result[0])) {
            $exp = explode('-', $result[0]['numero_facture']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
            return join('-', $exp);
        } else {
            return $this->createNumero('facture');
        }
    }

    /**
     * Méthode pour générer le numéro d'avoir
     * @param $factureId
     * @return string
     */
    public function numeroAvoir($factureId)
    {
        $sql = "SELECT numero_facture FROM facture WHERE id = $factureId";
        $result = $this->em->getConnection()->FetchAll($sql);
        if ($result) {
            $facture = $result[0]['numero_facture'];
            $tabTemp = explode('-', $facture);
            $facture = $tabTemp[1];
        } else {
            $facture  = "000";
        }
        $debut = 'AV';
        $year = date('y');
        $numero = $this->getLastId($factureId, 'avoir');
        $complement = $this->addZero($numero);
        return $debut . $year . $facture  . '-' . $complement;
    }

    /**
     * @param $affaireId
     * @return string
     */
    public function numeroCommande($affaireId)
    {
        $sql = "SELECT numero_affaire FROM affaire WHERE id = $affaireId";
        $result = $this->em->getConnection()->FetchAll($sql);
        if ($result) {
            $affaire = $result[0]['numero_affaire'];
            $tabTemp = explode('-', $affaire);
            $affaire = $tabTemp[1];
        } else {
            $affaire  = "000";
        }
        $debut = 'CMD';
        $year = date('y');
        $numero = $this->getLastId($affaireId, 'commande');
        $toString = strval($numero);
        $length = strlen($toString);
        $loop = 3 - $length;
        $complement = '';
        for ($i = 1; $i <= $loop; $i++) {
            $complement .= '0';
        }

        return $debut . $year . $affaire  . '-' . $complement . $toString;
    }

    /**
     * Méthode pour générer le numéro d'un attachement
     * @param $commandeId
     * @return string
     */
    public function numeroAttachement($commandeId)
    {
        $sql = "SELECT numero_commande FROM commande WHERE id = $commandeId";
        $result = $this->em->getConnection()->FetchAll($sql);
        if ($result) {
            $commande = $result[0]['numero_commande'];
            $tabTemp = explode('-',$commande);
            $commande = $tabTemp[1];
        } else {
            $commande = "000";
        }
        $debut = 'ST';
        $year = date('y');
        $numero = $this->getLastId($commandeId, 'attachement');
        $complement = $this->addZero($numero);
        return $debut . $year . $commande . '-' . $complement;
    }

    /**
     * Fonction utilisé pour créer un numero d'affaire
     * @return string
     */
    public function numeroAffaire()
    {
        $id = $this->getLastId(null, 'affaire');
        $debut = 'CA';
        $anne = date('y');
        $complement = $this->addZero($id);
        return $debut . $anne . '-' . $complement;
    }

    /**
     * Méthode pour éviter les conflits de numéro d'estimation', il génère un nouveau numéro d'estimation' à partir de la dernière estimation créer
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberEstimationPrestation()
    {
        $sql = "SELECT numero FROM estimation_prestation ORDER BY id DESC LIMIT 1";
        $result = $this->em->getConnection()->FetchAll($sql);
        if (!empty($result[0])) {
            $exp = explode('-', $result[0]['numero']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
            return join('-', $exp);
        } else {
            return $this->createNumero('estimationPrestation');
        }
    }

    /**
     * Méthode pour éviter les conflits de numéro d'estimation', il génère un nouveau numéro d'estimation' à partir de la dernière estimation créer
     * Si il n'y a pas de devis invoque la méthode createNumero
     * @return string
     */
    public function generateFromLastNumberEstimationFournisseur()
    {
        $sql = "SELECT numero FROM estimation_fournisseur ORDER BY id DESC LIMIT 1";
        $result = $this->em->getConnection()->FetchAll($sql);
        if (!empty($result[0])) {
            $exp = explode('-', $result[0]['numero']);
            $timestampYear = $exp[1];
            $this->checkYear($exp[1]);
            $this->checkMonth($exp[2]);
            if ($timestampYear != $exp[1]) {
                $tempNumber = '001';
            } else {
                $tempNumber = intval($exp[3]) + 1;
            }
            $exp[3] = strval($this->addZero($tempNumber));
            return join('-', $exp);
        } else {
            return $this->createNumero('estimationFournisseur');
        }
    }
}