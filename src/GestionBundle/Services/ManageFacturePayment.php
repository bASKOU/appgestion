<?php


namespace GestionBundle\Services;


class ManageFacturePayment
{
    public function checkPaymentDate(&$factures)
    {
        $inLate = [];
        $today = new \DateTime('today');
        foreach ($factures as $key => $facture) {
            if ($facture->getDateEcheance()) {
                if (strtotime($today->format('Y-m-d H:i:s')) > strtotime($facture->getDateEcheance()->format('Y-m-d H:i:s'))) {
                    unset($factures[$key]);
                    $facture->setEcart(intval($facture->getDateEcheance()->diff($today)->format('%a')));
                    $inLate[$key] = $facture;
                }
            }
        }
        return $inLate;
    }
}