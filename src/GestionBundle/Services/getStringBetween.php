<?php


namespace GestionBundle\Services;


class getStringBetween
{
    protected $start;
    protected $end;

    public function __construct($start, $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    public function getString($string)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $this->start);
        if ($ini == 0) return '';
        $ini += strlen($this->start);
        $len = strpos($string, $this->end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}