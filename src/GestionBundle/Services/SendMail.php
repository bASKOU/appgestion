<?php


namespace GestionBundle\Services;

use GestionBundle\Entity\Affaire;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendMail
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var ContainerInterface
     */
    private $container;
    private $message;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->container = $container;
    }

    private function setConfig($html, $setTo, $subject) {
        $this->message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom('gestit@sudalys.fr')
            ->setTo($setTo)
            ->setBody($html, 'text/html')
        ;
    }

    public function sendWithoutFlash($html, $setTo, $subject) {
        $this->setConfig($html, $setTo, $subject);
        $message = '';
        $status = 'error';
        try {
            $this->mailer->send($this->message);
            $status = 'success';
            $message = 'Email a été envoyé';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return ['status' => $status, 'message' => $message];
    }

    /**
     * @param Affaire $affaire
     * @return array
     */
    public function sendAvancementNotDoneAffaire(Affaire $affaire, $date) {
        $subject = "Gest'it - Alerte - Avancement affaire non réalisé";
//        $setTo = $affaire->getAttributions()[0]->getUser()->getEmail();
        $setTo = 'c.ahetz-etcheber@sudalys.fr';
        $url = $this->container->getParameter('adresse_web');

        $html = $this->container->get('templating')->render('GestionBundle:Export:AlerteAvancement.html.twig', [
            'affaire' => $affaire,
            'url' => $url,
            'date' => $date
        ]);

        return $this->sendWithoutFlash($html, $setTo, $subject);
    }

    public function sendCompleteAvancementAffaires()
    {
        $subject = "Gest'it - Information - Validation des avancements.";
        $setTo = $this->container->getParameter('mail_responsable');

        $url = $this->container->getParameter('adresse_web');

        $html = $this->container->get('templating')->render('GestionBundle:Export:AvancementComplet.html.twig', [
            'url' => $url
        ]);

        return $this->sendWithoutFlash($html, $setTo, $subject);
    }
}