<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\ProduitFournisseur;
use GestionBundle\Form\ProduitFournisseurType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProduitFournisseurController extends Controller
{
    private $exceptionHandler;

    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * Méthode pour passer les props privées en public afin de les retourner en JSON
     * @param $produitFournisseur
     * @return array
     */
    private function hydradeObjectProduitFournisseur($produitFournisseur)
    {
        $produitFournisseurProps = ['id', 'designation', 'description', 'montantUnit', 'parent', 'dateValid', 'dateCreat', 'referenceFournisseur', 'prixFixe', 'organisme'];
        $toReturn = [];
        foreach ($produitFournisseurProps as $prop) {
            if ($prop == 'organisme') {
                $toReturn[$prop] = $produitFournisseur->getOrganisme()->getId();
            } else {
                $temp = 'get' . ucfirst($prop);
                $toReturn[$prop] = $produitFournisseur->$temp();
            }
        }
        return $toReturn;
    }

    private function hydradeObjectForEqual($a)
    {
        $produitFournisseurProps = ['designation', 'description', 'montantUnit', 'parent', 'organisme'];
        $toReturn = [];
        foreach ($produitFournisseurProps as $prop) {
            if ($prop == 'organisme') {
                $toReturn[$prop] = $a->getOrganisme()->getId();
            } else {
                $temp = 'get' . ucfirst($prop);
                $toReturn[$prop] = $a->$temp();
            }
        }
        return $toReturn;
    }

    private function checkEqualEntity($a, $b)
    {
        $toReturn = false;
        $arrA = $this->hydradeObjectForEqual($a);
        $arrB = $this->hydradeObjectForEqual($b);
        $arrDiff = array_diff($arrA, $arrB);
        if (empty($arrDiff[0])) {
            $toReturn = true;
        }
        return $toReturn;
    }

    /**
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse|Response
     */
    public function createForOrgaAction(Request $request, $orgaId)
    {
        $product = new ProduitFournisseur();
        $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $product->setOrganisme($orga);
        $form = $this->createForm(ProduitFournisseurType::class, $product);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $code = 400;
                $status = 'error';
                $message = 'Mauvaise requête.';
                $dataProduct = [];
                $em = $this->getDoctrine()->getManager();
                $data = $_POST['gestionbundle_produitfournisseur'];
                unset($data['_token']);
                foreach ($data as $key => $value) {
                    if ($key == "montantUnit" && $value != '') {
                        $value = str_replace(' ', '', str_replace(',', '.', $value));
                        $temp = 'set' . ucfirst($key);
                        $product->$temp((float)$value);
                    } else if ($value != '') {
                        $temp = 'set' . ucfirst($key);
                        $product->$temp($value);
                    }
                }
                $this->hydradeObjectProduitFournisseur($product);
                $existingProduct = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->findBy(['designation' => $product->getDesignation(), 'organisme' => $orga]);
                $parent = '';
                $match = null;
                if ($existingProduct) {
                    foreach ($existingProduct as $produit) {
                        if (!$produit->getParent()) {
                            $parent = $produit;
                        }
                        if ($this->checkEqualEntity($produit, $product)) {
                            $match = $produit;
                            goto haveMatch;
                        }
                    }
                }
                if ($match) {
                    haveMatch:
                    $dataProduct = $this->hydradeObjectProduitFournisseur($match);
                    $code = 200;
                    $status = 'success';
                    $message = 'Ce produit existe déjà pour ce fournisseur.';
                } else {
                    if ($parent) {
                        $product->setParent($parent->getId());
                    }
                    $product->setDateCreat(new \DateTime('now'));
                    $em->persist($product);
                    try {
                        $em->flush();
                        $status = 'success';
                        $code = 200;
                        $message = 'Le produit fournisseur à bien été créé';
                        $dataProduct = $this->hydradeObjectProduitFournisseur($product);
                    } catch (\Exception $e) {
                        $code = 200;
                        $message = $this->exceptionHandler->getException($e);
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $dataProduct], $code);
            }
        }

        return $this->render('GestionBundle:Default:createProduitFournisseur.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour récupérer la liste des produits d'un fournisseur
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function fournisseurProductListAction(Request $request, $orgaId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        $data = [];

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                $products = $orga->getProduitFournisseur();
                if (!empty($products[0])) {
                    $code = 200;
                    $status = 'products';
                    $message = 'Liste des produits du fournisseur chargé.';
                    foreach ($products as $product) {
                        array_push($data, $this->hydradeObjectProduitFournisseur($product));
                    }
                } else {
                    $code = 200;
                    $status = 'noproducts';
                    $message = 'Ce fournisseur n\'a aucun produit enregistré. Créez le premier produit';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
    }
}