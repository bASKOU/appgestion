<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\EstimationPrestation;
use GestionBundle\Entity\FacturePrestataire;
use GestionBundle\Form\CommandePrestataireType;
use GestionBundle\Form\EditEstimationPrestationType;
use GestionBundle\Form\EstimationPrestationType;
use GestionBundle\Form\FacturePrestataireChaineType;
use GestionBundle\Form\FacturePrestataireType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FacturePrestataireController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;

    /**
     * FacturePrestataireController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode utilisé pour calculé le montant restant d'une commande de materiel
     * @param $existingFacture
     * @param $totalHT
     * @return int
     */
    private function ramainingTotalCommande($existingFacture, $totalHT)
    {
        if (!empty($existingFacture[0])) {
            $tempTotal = 0;
            foreach ($existingFacture as $exist) {
                if ($exist->getValidation() == 1) {
                    $tempTotal += $exist->getTotalHT();
                }
            }
            $totalHT -= $tempTotal;
        }

        return $totalHT;
    }

    /**
     * Méthode pour envoyer le formulaire de saisie de la facture par le service administratif
     * @param Request $request
     * @param $commandeId
     * @return JsonResponse|Response
     */
    public function saisieAdministrativeAction(Request $request, $commandeId)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = new FacturePrestataire(); // Instanciation d'une nouvelle facture de Prestataire
        // Récupération de la commande de rattachement de la facture
        $commandePrestataire = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);
        // Récupération de l'affaire de la commande si spécifié
        $commandePrestataire->getAffaire() ? $facture->setAffaire($commandePrestataire->getAffaire()): $facture->setAffaire(null);
        // On récupère les données directement de la commande
        $facture->setAdresseFacturation($commandePrestataire->getAdresseFacturation());
//        $facture->setAdresseLivraison($commandePrestataire->getAdresseLivraison());
        $facture->setUser($commandePrestataire->getUser());
        $facture->setPrestataire($commandePrestataire->getPrestataire());
        $facture->setContact($commandePrestataire->getContact());
        $facture->setEtat(0); // L'etat de la facture est mise en attente de validation
        $facture->setValidation(0); // La facture doit être validé par le CP
        $facture->setDateSaisie(new \DateTime('now')); // La date de saisie de la facture par le service administratif
        $facture->setCommandePrestataire($commandePrestataire);
        $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFactureRecus()); // On génère un numéro de facture propre à sudalys
        // On récupère les factures validé déja existante
        $existingFacture = $this->getDoctrine()->getRepository(FacturePrestataire::class)->findBy(['commandePrestataire' => $commandePrestataire, 'validation' => 1]);
        $totalHT = $this->ramainingTotalCommande($existingFacture, $commandePrestataire->getTotalHT()); // On récupère le total des factures valide
        // Si le total retourné est à 0
        if ($totalHT === 0) {
            $commandePrestataire->setEtat(2); // Modification de l'état à cloturé
            $em->persist($commandePrestataire);
            $em->flush();
            // Affichage du message indiquant que la commande est cloturé
            $this->addFlash('warning', 'La commande Prestataire est clôturé, impossible de créer une nouvelle facture depuis cette commande');
            // Redirection vers la page de gestion des commande de matériel
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . "#commandeMateriel");
        }
        // Sinon on continue
        $facture->setTotalHT($totalHT);

        $form = $this->createForm(FacturePrestataireType::class, $facture);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $code = 200;
            $message = '';
            $status = 'error';
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $fileName = $safeFilename . '-' . uniqid() . '.pdf';
                $file->move(
                    $this->getParameter('pdf_facture_directory'), $fileName
                );
                return new JsonResponse($fileName);
            } else {
                $facture->setTotalTTC($facture->getTotalHT() * (1 + $facture->getTVA()));
                $em->persist($facture);
                try {
                    $em->flush();
                    $url = $this->getParameter('adresse_web');
                    $status = 'success';
                    $message = "La saisie de la facture a bien été enregistré.";
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:factureFournisseurValidation.html.twig', [
                        'facture' => $facture,
                        'url' => $url
                    ]);
                    $response = $mail->sendWithoutFlash($html, $facture->getAffaire()->getAttributions()[0]->getUser()->getEmail(), "Gest'it - Demande de validation facture reçus");
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail de demande de validation a été envoyé au chef de projet';
                    } else {
                        $status = 'error';
                        $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet";
                    }
                } catch (\Exception $e) {
                    $status = 'error';
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
                }

                return new JsonResponse(['status' => $status, 'message' => $message], $code);
            }
        }

        return $this->render('GestionBundle:Default:createSaisieFacturePrestataire.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour visualiser la facture Prestataire avec la commande pour la valider ou la refuser
     * @param $factureId
     * @return Response
     */
    public function viewAction($factureId)
    {
        $facturePrestataire = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);
        $cmd = $facturePrestataire->getCommandePrestataire();

        $user = $this->getUser();

        $form = $this->createForm(CommandePrestataireType::class, $cmd, ['role' => $user->getRoles()]);

        return $this->render('GestionBundle:Default:viewValideFacturePrestataire.html.twig', [
            'form' => $form->createView(),
            'commandePrestataire' => $cmd,
            'view' => 'yes',
            'duplicate' => 'no',
            'user' => $user,
            'facturePrestataire' => $facturePrestataire
        ]);
    }

    /**
     * Methode pour valider une facture Prestataire
     * @param Request $request
     * @param $factureId
     * @return RedirectResponse
     */
    public function valideAction(Request $request, $factureId)
    {
        $status = 'danger';
        $message = '';

        $facturePrestataire = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);

        $facturePrestataire->setValidation(1);
        $facturePrestataire->setDateValidation(new \DateTime('now'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($facturePrestataire);
        try {
            $em->flush();
            $message = "La facture a bien été validé.";
            $status = 'success';
            $url = $this->getParameter('adresse_web');
            $mail = $this->get('email.action.mailer');
            $html = $this->renderView('GestionBundle:Export:factureFournisseurValid.html.twig', [
                'facture' => $facturePrestataire,
                'url' => $url
            ]);
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), "Gest'it - Retour de validation d'une facture reçus");
            if ($response['status'] === 'success') {
                $message .= " Un email pour informer le service administratif a été envoyé";
            } else {
                $status = 'warning';
                $message .= " Une erreur s'est produite lors de l'envoi de l'email au service administratif";
            }
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            dump($e); die();
        }

        $this->addFlash($status, $message);

        return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $facturePrestataire->getAffaire()->getId()]) . '#facturePrestation');
    }

    /**
     * Methode pour refuser une facture Prestataire
     * @param Request $request
     * @param $factureId
     * @return RedirectResponse
     */
    public function refuseAction(Request $request, $factureId)
    {
        $status = 'danger';
        $message = '';
        $facturePrestataire = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);

        $facturePrestataire->setValidation(2);
        $facturePrestataire->setDateValidation(new \DateTime('now'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($facturePrestataire);

        try {
            $em->flush();
            $status = 'success';
            $message = "La facture a bien été refusé.";
            $url = $this->getParameter('adresse_web');
            $mail = $this->get('email.action.mailer');
            $html = $this->renderView('GestionBundle:Export:factureFournisseurValid.html.twig', [
                'facture' => $facturePrestataire,
                'url' => $url
            ]);
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), "Gest'it - Retour de validation d'une facture reçus");
            if ($response['status'] === 'success') {
                $message .= " Un email pour informer le service administratif a été envoyé";
            } else {
                $status = 'warning';
                $message .= " Une erreur s'est produite lors de l'envoi de l'email au service administratif";
            }
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
        }

        $this->addFlash($status, $message);

        return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $facturePrestataire->getAffaire()->getId()]) . '#facturePrestation');
    }

    /**
     * Méthode pour envoyer le formulaire de saisie de la facture par le service administratif
     * @param Request $request
     * @param $factureId
     * @return Response
     */
    public function createAfterCommandeAction(Request $request, $factureId)
    {
        $facture = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);
        $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFactureRecus());

        $existingFacture = $facture->getCommandePrestataire()->getFacturePrestataire();
        $totalHT = $this->ramainingTotalCommande($existingFacture, $facture->getCommandePrestataire()->getTotalHT());

        $facture->setTotalHT($totalHT);

        $form = $this->createForm(FacturePrestataireChaineType::class, $facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = '';
            $status = 'danger';
            $em = $this->getDoctrine()->getManager();
            /** @var UploadedFile $file */
            $file = $request->files->get('gestionbundle_factureprestataire')['documentAttache'];
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.pdf';
            $file->move(
                $this->getParameter('pdf_facture_directory'), $fileName
            );
            $facture->setDocumentAttache($fileName);
            $facture->setTotalTTC($facture->getTotalHT() * (1 + $facture->getTVA()));
            $em->persist($facture);
            try {
                $url = $this->getParameter('adresse_web');
                $em->flush();
                $status = 'success';
                $message = "La saisie de la facture a bien été enregistré.";
                $mail = $this->get('email.action.mailer');
                $html = $this->renderView('GestionBundle:Export:factureFournisseurValidation.html.twig', [
                    'facture' => $facture,
                    'url' => $url
                ]);
                $response = $mail->sendWithoutFlash($html, $facture->getAffaire()->getAttributions()[0]->getUser()->getEmail(), "Gest'it - Demande de validation facture reçus");
                if ($response['status'] === 'success') {
                    $message .= ' Un mail de demande de validation a été envoyé au chef de projet';
                } else {
                    $status = 'warning';
                    $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet";
                }
                $cmd = $facture->getCommandePrestataire();
                $cmd->setEtat(2);
                $em->persist($cmd);
                $em->flush();
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
                dump($e); die();
            }
            $this->addFlash($status, $message);

            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#facture');
        }

        return $this->render('GestionBundle:Default:createFacturePrestataireChaine.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour enregistrer le réglement d'une facture Prestataire
     * @param Request $request
     * @param $factureId
     * @return JsonResponse|Response
     */
    public function reglementAction(Request $request, $factureId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';

        $facture = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);
        $attacheDoc = $facture->getDocumentAttache();

        $form = $this->createForm(FacturePrestataireType::class, $facture, ['type' => 'reglement']);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $facture->setDocumentAttache($attacheDoc);
            $facture->setEtat(1);
            $em->persist($facture);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "Le réglement de la facture reçus a bien été enregistré.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }
        return $this->render('GestionBundle:Default:createSaisieFacturePrestataire.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour enregistrer le debit d'une facture Prestataire
     * @param Request $request
     * @param $factureId
     * @return JsonResponse|Response
     */
    public function debitAction(Request $request, $factureId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';

        $facture = $this->getDoctrine()->getRepository(FacturePrestataire::class)->find($factureId);
        $attacheDoc = $facture->getDocumentAttache();

        $form = $this->createForm(FacturePrestataireType::class, $facture, ['type' => 'debit']);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $facture->setDocumentAttache($attacheDoc);
            $facture->setEtat(2);
            $em->persist($facture);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "Le débit de la facture reçus a bien été enregistré.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }
        return $this->render('GestionBundle:Default:createSaisieFacturePrestataire.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }
}