<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\InfoFournisseur;
use GestionBundle\Entity\InfoPrestataire;
use GestionBundle\Entity\TypeContact;
use GestionBundle\Form\ClientAjaxType;
use GestionBundle\Form\FournisseurAjaxType;
use GestionBundle\Form\PrestataireAjaxType;
use Transliterator;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Organisme;
use GestionBundle\Form\CreateOrgaType;
use GestionBundle\Form\EditContactType;
use GestionBundle\Form\joinContactType;
use GestionBundle\Form\joinContactAjaxType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrgaController extends Controller
{
    public $exceptionHandler = "";
    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }
    /**
     * Méthode qui récupère la liste des affaires dans lesquelles l'organsme est présent
     * @return array
     */
    private function getAffairesForOrga($orgaId)
    {
        // récupère la liste intégrale des affaires
        $affaires = $this->getDoctrine()->getRepository(Affaire::class)->findAll();
        $linkedAffaires = []; // Initialise un array vide dans lequel on stock les affaires
        foreach ($affaires as $affaire) { // Boule dans toutes les affaires
            foreach ($affaire->getDevis() as $devis) {
                if ($devis->getClient()->getId() == $orgaId) {
                    if ($this->searchInArray($devis->getAffaire()->getDesignationAffaire(), $linkedAffaires) == 0) {
                        array_push($linkedAffaires, ['id' => $devis->getAffaire()->getId(), 'designation' => $devis->getAffaire()->getDesignationAffaire(), 'numero' => $devis->getAffaire()->getNumeroAffaire(), 'attributions' => $devis->getAffaire()->getAttributions()]);
                    }
                }
            }
            foreach ($affaire->getCommandes() as $cmdr) {

                if ($cmdr->getClient()->getId() == $orgaId) {
                    if ($this->searchInArray($cmdr->getAffaire()->getDesignationAffaire(), $linkedAffaires) == 0) {
                        array_push($linkedAffaires, ['id' => $cmdr->getAffaire()->getId(), 'designation' => $cmdr->getAffaire()->getDesignationAffaire(), 'numero' => $cmdr->getAffaire()->getNumeroAffaire(), 'attributions' => $cmdr->getAffaire()->getAttributions()]);
                    }
                }
            }
            foreach ($affaire->getFactures() as $facture) {
                if ($facture->getClient()->getId() == $orgaId) {
                    if ($this->searchInArray($facture->getAffaire()->getDesignationAffaire(), $linkedAffaires) == 0) {
                        array_push($linkedAffaires, ['id' => $facture->getAffaire()->getId(), 'designation' => $facture->getAffaire()->getDesignationAffaire(), 'numero' => $facture->getAffaire()->getNumeroAffaire(), 'attributions' => $facture->getAffaire()->getAttributions()]);
                    }
                }
            }
            foreach ($affaire->getCommandeFournisseur() as $cmde) {
                if ($cmde->getFournisseur()->getId() == $orgaId) {
                    if ($this->searchInArray($cmde->getAffaire()->getDesignationAffaire(), $linkedAffaires) == 0) {
                        array_push($linkedAffaires, ['id' => $cmde->getAffaire()->getId(), 'designation' => $cmde->getAffaire()->getDesignationAffaire(), 'numero' => $cmde->getAffaire()->getNumeroAffaire(), 'attributions' => $cmde->getAffaire()->getAttributions()]);
                    }
                }
            }
        }
        return $linkedAffaires;
    }

    /**
     * Méthode qui vérifie si l'affaire est déjà présente dans la liste
     * Si elle est présente retourne 1 sinon retourne 0
     * @param $needle
     * @param $haystacks
     * @return int
     */
    private function searchInArray($needle, $haystacks)
    {
        $transliterator = Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: Lower(); :: NFC;', Transliterator::FORWARD);
        $key = [];
        $flag = 0;
        foreach ($haystacks as $haystack) {
            if (!empty($haystack['designation'])) {
                $transliterator->transliterate($haystack['designation']) === $transliterator->transliterate($needle) ? array_push($key, 1) : array_push($key, 0);
            }
        }
        if (in_array(1, $key)) {
            $flag = 1;
        }
        return $flag;
    }

    private function sendFournisseurRequest($type, $organisme) {
        $arrToReturn = ['status' => 'error', 'message' => ''];

        $html = $this->renderView('GestionBundle:Export:fournisseurValidation.html.twig', [
            'url' => $this->getParameter('app_url'),
            'user' => $this->getUser(),
            'type' => $type,
            'organisme' => $organisme,
            'index' => $type
        ]);

        if ($type === 'organismeFournisseur') {
            $mail = $this->get('email.action.mailer');
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), 'Gest\'it - Demande de référencement fournisseur');
        } else {
            $mail = $this->get('email.action.mailer');
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), 'Gest\'it - Demande de référencement prestataire');
        }
        if ($response['status'] === 'success') {
            $arrToReturn['status'] = 'success';
            $arrToReturn['message'] = 'Un email de demande de référencement fournisseur a été envoyé au service administratif.';
        } else {
            $arrToReturn['message'] = 'Mais une erreur s\'est produite lors de l\'envoi de l\'email, contactez le service administratif pour informer de la demande de référencement.';
        }

        return $arrToReturn;
    }

    /**
     * Méthode pour construire un array à partir d'un object doctrine
     * @param $orga
     * @return array
     */
    private function adresseBuilder($orga)
    {
        $adresse = [];
        $adresse['adresse'] = $orga->getAdresse();
        $adresse['adresseComplementaire1'] = $orga->getAdresseComplementaire1();
        $adresse['adresseComplementaire2'] = $orga->getAdresseComplementaire2();
        $adresse['stal'] = $orga->getStal();
        $adresse['ville'] = $orga->getVille();
        $adresse['nom'] = $orga->getNom();
        $adresse['id'] = $orga->getId();
        return $adresse;
    }

    /**
     * Méthode qui permet de récupérer récursivement la liste complète des organismes clients liés par un parent ou enfant
     * La liste de client passe en référence pour éviter toute erreur d'écrasement de la variable lors d'un return
     * @param $organisme
     * @param $listeClients
     */
    private function getOrganismeClientRecursive($organisme, &$listeClients)
    {
        $enfants = $organisme->getEnfants();
        $parent = $organisme->getParent();
        if ($parent != null) {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent->getNom()];
        } else {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent];
        }
        if ($enfants != null) {
            foreach ($enfants as $enfant) {
                if (empty($listeClients[$enfant->getId()]) && $enfant->getType() == 1) {
                    $this->getOrganismeClientRecursive($enfant, $listeClients);
                }
            }
        }
        if ($parent != null) {
            if (empty($listeClients[$parent->getId()]) && $parent->getType() == 1) {
                $listeClients[$parent->getId()] = ['id' => $parent->getId(), 'nom' => $parent->getNom()];
                $this->getOrganismeClientRecursive($parent, $listeClients);
            }
        }
    }

    /**
     * Méthode qui permet de récupérer récursivement la liste complète des organismes clients liés par un parent ou enfant
     * La liste de client passe en référence pour éviter toute erreur d'écrasement de la variable lors d'un return
     * @param $organisme
     * @param $listeClients
     */
    private function getOrganismeFournisseurRecursive($organisme, &$listeClients)
    {
        $enfants = $organisme->getEnfants();
        $parent = $organisme->getParent();
        if ($parent != null) {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent->getNom()];
        } else {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent];
        }
        if ($enfants != null) {
            foreach ($enfants as $enfant) {
                if (empty($listeClients[$enfant->getId()]) && $enfant->getType() == 0) {
                    $this->getOrganismeClientRecursive($enfant, $listeClients);
                }
            }
        }
        if ($parent != null) {
            if (empty($listeClients[$parent->getId()]) && $parent->getType() == 0) {
                $listeClients[$parent->getId()] = ['id' => $parent->getId(), 'nom' => $parent->getNom()];
                $this->getOrganismeClientRecursive($parent, $listeClients);
            }
        }
    }

    /**
     * Méthode qui permet de récupérer récursivement la liste complète des organismes clients liés par un parent ou enfant
     * La liste de client passe en référence pour éviter toute erreur d'écrasement de la variable lors d'un return
     * @param $organisme
     * @param $listeClients
     */
    private function getOrganismePrestataireRecursive($organisme, &$listeClients)
    {
        $enfants = $organisme->getEnfants();
        $parent = $organisme->getParent();
        if ($parent != null) {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent->getNom()];
        } else {
            $listeClients[$organisme->getId()] = ['id' => $organisme->getId(), 'nom' => $organisme->getNom(), 'parent' => $parent];
        }
        if ($enfants != null) {
            foreach ($enfants as $enfant) {
                if (empty($listeClients[$enfant->getId()]) && $enfant->getType() == 1) {
                    $this->getOrganismeClientRecursive($enfant, $listeClients);
                }
            }
        }
        if ($parent != null) {
            if (empty($listeClients[$parent->getId()]) && $parent->getType() == 1) {
                $listeClients[$parent->getId()] = ['id' => $parent->getId(), 'nom' => $parent->getNom()];
                $this->getOrganismeClientRecursive($parent, $listeClients);
            }
        }
    }

    /**
     * fonction qui va nous permettre de créer un nouvel organisme
     * @return Response
     */
    public function manageAction()
    {
        $clients = $this->getDoctrine()->getRepository(Organisme::class)->findBy(['type' => 1]);
        $fournisseurs = $this->getDoctrine()->getRepository(Organisme::class)->findBy(['type' => 0]);
        $prestataires = $this->getDoctrine()->getRepository(Organisme::class)->findBy(['type' => 2]);

        return $this->render('GestionBundle:Default:manageOrga.html.twig', [
            'clients' => $clients,
            'fournisseurs' => $fournisseurs,
            'prestataires' => $prestataires
        ]);
    }

    /**
     * Méthode pour afficher les détails d'un oragnisme avec ses contacts
     * Ici on puet voir la list des contacts, ajouter, éditer et supprimer un contact
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse|Response
     */
    public function showAction(Request $request, $orgaId)
    {
        // Initialise les variables nécessaires
        $status = 'error';
        $message = 'Mauvaise Requête';
        $code = 400;
        $data = [];
        // Entity manager
        $em = $this->getDoctrine()->getManager();
        // Initialise un nouveau contact
        $contact = new Contact();
        // Récupère l'organisme concerné
        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        // Récupère la liste des contacts de l'organisme
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findBy(['organisme' => $organisme]);
        // Récupère la liste des affaires liées à l'organisme
        $linkedAffaires = $this->getAffairesForOrga($organisme->getId());
        // Génération de la form pour joindre un contact
        $formCreateContact = $this->createForm(joinContactType::class, $contact, ['role' => $this->getUser()->getRoles()]);
        // Génération de la form pour mettre à jour l'organisme
        $formOrga = $this->createForm(CreateOrgaType::class, $organisme, ['role' => $this->getUser()->getRoles()]);
        // Ajout l'handler aux forms
        $formCreateContact->handleRequest($request);
        $formOrga->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) { // Si la méthode est POST
                // Récupère la formData
                $formData = $request->request->get('join_contact');
                unset($formData['organisme']);
                unset($formData['_token']);
                unset($formData['type']);
                foreach ($formData as $key => $value) {
                    if ($value == "") {
                        unset($formData[$key]);
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $contact->$temp($value);
                    }
                }
                $contact->setOrganisme($organisme);
                $contact->setUser($this->getUser());
                $em->persist($contact); // Persiste l'entité
                // Récupère les données à renvoyer
                $id = $contact->getId();
                $nom = $contact->getNom();
                $prenom = $contact->getPrenom();
                $email = $contact->getEmail();
                $telephone = $contact->getTelephone();
                $mobile = $contact->getMobile();
                $data = ['id' => $id,'nom' => $nom, 'prenom' => $prenom, 'email' => $email, 'telephone' => $telephone, 'mobile' => $mobile];
                // Gestion de l'erreur de requête SQL
                try {
                    $em->flush();
                    $code = 201;
                    $status = 'success';
                    $message = 'Contact ajouter à l\'organisme avec succès';
                } catch (\Exception $e) {
                    $status = 'error';
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            } elseif ($request->isMethod('PUT')) { // SI la méthode est PUT
                // Récupère les données
                $formData = $request->request->get('create_orga');
                foreach ($formData as $prop => $value) { // Itère dans les données
                    if ($prop == 'parent' && $value != "") {
                        $parent = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        $temp = ucfirst($prop); // Capitalise la première lettre du mot
                        $prop = 'set' . $temp; // Concatène pour ajouter la string "set"
                        $organisme->$prop($parent); // Assigne la valeur dynamiquement
                    } else if ($value != "") {
                        $temp = ucfirst($prop); // Capitalise la première lettre du mot
                        $prop = 'set' . $temp; // Concatène pour ajouter la string "set"
                        $organisme->$prop($value); // Assigne la valeur dynamiquement
                    }
                }
                if (!$organisme->getUser()) {
                    $organisme->setUser($this->getUser());
                }
                $data = $formData; // Récupère les donnée pour renvoi
                $em->persist($organisme); // Met à jour l'entité
                try { // Gestion des erreur SQL
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'Organisme modifier avec succès';
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            } else {
                $code = 405;
                $message = 'Méthode non supporté';
            }
            // Retourne le JSON
            return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
        }
        $typeContact = $this->getDoctrine()->getRepository(TypeContact::class)->findAll();
        return $this->render('GestionBundle:Default:detailOrga.html.twig', [
            'organisme' => $organisme,
            'contacts' => $contacts,
            'linkedAffaires' => $linkedAffaires,
            'form_orga' => $formOrga->createView(),
            'form_create_contact' => $formCreateContact->createView(),
            'typeContact' => $typeContact
        ]);
    }

    /**
     * Fonction pour créer un client à partir d'une requête AJAX
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function createClientAjaxAction(Request $request)
    {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $em = $this->getDoctrine()->getManager();
        $orga = new Organisme();

        $form = $this->createForm(ClientAjaxType::class, $orga);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod(('POST'))) {
                $data = $request->request->get('client_ajax');
                unset($data['_token']);
                $listePays = $this->get('liste.pays');
                $orga->setPays($listePays->getPays($orga->getPays()));
                $orga->setActif(true);
                $orga->setType(1);
                $orga->setFournisseurValide(false);
                $orga->setPrestataireValide(false);
                $orga->setUser($this->getUser());
                $em->persist($orga);
                try {
                    $em->flush();
                    $code = 200;
                    $message = 'Le client a bien été créé.';
                    $status = 'success';
                    $data['id'] = $orga->getId();
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
                if ($status === 'success') {
                    if (!$orga->getFournisseurValide() && $orga->getType() != 1 && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles()) == false) {
                        $resultEmail = $this->sendFournisseurRequest('orga', $orga);
                        $status = $resultEmail['status'];
                        $message .= ' ' . $resultEmail['message'];
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
            }
        }

        return $this->render('GestionBundle:Default:createOrga.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Fonction pour créer un client à partir d'une requête AJAX
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function addClientAjaxAction(Request $request, $orgaId)
    {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $em = $this->getDoctrine()->getManager();
        $orga = new Organisme();

        $form = $this->createForm(ClientAjaxType::class, $orga);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod(('POST'))) {
                $data = $request->request->get('client_ajax');
                unset($data['_token']);
                $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                $listePays = $this->get('liste.pays');
                $orga->setPays($listePays->getPays($orga->getPays()));
                $orga->setParent($organisme);
                $orga->setActif(true);
                $orga->setType(1);
                $orga->setFournisseurValide(false);
                $orga->setUser($this->getUser());
                $em->persist($orga);
                try {
                    $em->flush();
                    $code = 200;
                    $message = 'Le client a bien été créé.';
                    $status = 'success';
                    $data['id'] = $orga->getId();
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
                if ($status === 'success') {
                    if (!$orga->getFournisseurValide() && $orga->getType() != 1 && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles()) == false) {
                        $resultEmail = $this->sendFournisseurRequest('orga', $orga);
                        $status = $resultEmail['status'];
                        $message .= ' ' . $resultEmail['message'];
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
            }
        }

        return $this->render('GestionBundle:Default:createOrga.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Fonction pour créer un fournisseur à partir d'une requête AJAX
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function createFournisseurAjaxAction(Request $request)
    {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $em = $this->getDoctrine()->getManager();
        $orga = new Organisme();

        $form = $this->createForm(FournisseurAjaxType::class, $orga, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod(('POST'))) {
                $data = $request->request->get('fournisseur_ajax');
                unset($data['_token']);
                $listePays = $this->get('liste.pays');
                $orga->setPays($listePays->getPays($orga->getPays()));
                $orga->setActif(true);
                $orga->setType(0);
                if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles()) == false) {
                    $orga->setFournisseurValide(false);
                } else {
                    $orga->setFournisseurValide(true);
                }
                $orga->setPrestataireValide(false);
                $orga->setUser($this->getUser());
                $em->persist($orga);
                $infoFournisseur = new InfoFournisseur();
                $infoFournisseur->setFournisseur($orga);
                $infoFournisseur->setRIB(false);
                $em->persist($infoFournisseur);
                $orga->setInfoFournisseur($infoFournisseur);
                try {
                    $em->flush();
                    $code = 200;
                    $message = 'Le fournisseur a bien été créé.';
                    $status = 'success';
                    $data['id'] = $orga->getId();
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
                if ($status === 'success') {
                    if (!$orga->getFournisseurValide() && $orga->getType() != 1 && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles()) == false) {
                        $resultEmail = $this->sendFournisseurRequest('organismeFournisseur', $orga);
                        $status = $resultEmail['status'];
                        $message .= ' ' . $resultEmail['message'];
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
            }

            return $this->render('GestionBundle:Default:createOrga.html.twig', [
                'form' => $form->createView()
            ]);
        }
    }

    /**
     * Fonction pour créer un prestataire à partir d'une requête AJAX
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function createPrestataireAjaxAction(Request $request)
    {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $em = $this->getDoctrine()->getManager();
        $orga = new Organisme();

        $form = $this->createForm(PrestataireAjaxType::class, $orga, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod(('POST'))) {
                $data = $request->request->get('fournisseur_ajax');
                unset($data['_token']);
                $listePays = $this->get('liste.pays');
                $orga->setPays($listePays->getPays($orga->getPays()));
                $orga->setActif(true);
                $orga->setType(2);
                $orga->setFournisseurValide(false);
                $orga->setPrestataireValide(false);
                $orga->setUser($this->getUser());
                $infoPrestataire = new InfoPrestataire();
                $infoPrestataire->setPrestataire($orga);
                $infoPrestataire->setDateInfo(new \DateTime('today'));
                $orga->getPays() === "France" ? $infoPrestataire->setIsEtrangere(false): $infoPrestataire->setIsEtrangere(true);
                $em->persist($infoPrestataire);
                $orga->setInfoPrestataire($infoPrestataire);
                $em->persist($orga);
                $code = 200;
                try {
                    $em->flush();
                    $message = 'Le prestataire a bien été créé.';
                    $status = 'success';
                    $data['id'] = $orga->getId();
                } catch (\Exception $e) {
                    $message = $this->exceptionHandler->getException($e);
                }
                if ($status === 'success') {
                    if (!$orga->getFournisseurValide() && $orga->getType() != 1 && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles()) == false) {
                        $resultEmail = $this->sendFournisseurRequest('organismePrestataire', $orga);
                        $status = $resultEmail['status'];
                        $message .= ' ' . $resultEmail['message'];
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
            }
        }

        return $this->render('GestionBundle:Default:createOrga.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour changer l'état de l'organisme
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function setEtatAction(Request $request, $orgaId)
    {
        $code = 405;
        $status = 'error';
        $message = 'Mauvaise requête.';
        $active = 0;
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                $em = $this->getDoctrine()->getManager();
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                if ($orga->getActif() === true) {
                    $orga->setActif(false);
                    $active = 0;
                } else {
                    $orga->setActif(true);
                    $active = 1;
                }
                $em->persist($orga);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'L\'organisme est inactif, vous pouvez le trouver dans l\'onglet Organismes inactfs';
                } catch (\Exception $e) {
                    $code = 500;
                    $message = 'Une erreur s\'est produite, veuillez réessayer...';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'active' => $active], $code);
    }

    /**
     * Méthode pour supprimer un organisme
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function deleteAction(Request $request, $orgaId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';

            if ($request->isXmlHttpRequest()) {
                $em = $this->getDoctrine()->getManager();

                $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                if ($request->isMethod('DELETE')) {
                    $em->remove($organisme);
                    try {
                        $em->flush();
                        $code = 200;
                        $status = 'success';
                        $message = 'L\'organisme a bien était supprimé';
                    } catch (\Exception $e) {
                        dump($e); die();
                        $code = 500;
                        $message = 'Une erreur s\'est produite, veuillez réessayer...';
                    }
                } else if ($request->isMethod('GET')) {
                    if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                        $devis = $organisme->getClientDevis()->toArray();
                        $commandes = $organisme->getClientCommande()->toArray();
                        $commandeFournisseur = $organisme->getCommandeFournisseur()->toArray();
                        $attachements = $organisme->getClientAttachement()->toArray();
                        $factures = $organisme->getClientFacture()->toArray();
                        if (empty($devis) && empty($commandes) && empty($commandeFournisseur) && empty($attachements) && empty($factures)) {
                            $code = 200;
                            $status = 'success';
                            $message = 'Vous êtes sur le point de supprimer un organisme. En supprimant l\'organisme tous les contacts liès seront effacé aussi. Cette action est irréversible. Etes-vous sûr de vouloir le supprimer ?';
                        } else {
                            $code = 200;
                            $message = 'L\'organisme ne peut pas être supprimé';
                        }
                    } else {
                        $code = 200;
                        $message = 'Vous n\'avez pas les droits nécessaire pour supprimer un organisme';
                    }
                }
            }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour récupèrer le liste des agences d'un organisme pour le devis
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function getClientListAction(Request $request, $orgaId, $cmd=null)
    {
        if ($request->isXmlHttpRequest()) {
            if($request->isMethod('GET')) {
                try {
                    $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                    $client = $orga->getEnfants()->toArray();
                    $clients = [];
                    foreach ($client as $key => $object) {
                        if ($cmd === "oui") {
                            if ($object->getType() != 1) {
                                $clients[$key] = ['id' => $object->getId(), 'nom' => $object->getNom()];
                            }
                        } else {
                            if ($object->getType() >= 1) {
                                $clients[$key] = ['id' => $object->getId(), 'nom' => $object->getNom()];
                            }
                        }
                    }
                    $clients = array_values($clients);
                    array_unshift($clients, ['id' => $orga->getId(), 'nom' => $orga->getNom()]);
                    return new JsonResponse(['clients' => $clients], 200);
                } catch (\Exception $e) {
                    return new JsonResponse(['error' => 'Une erreur s\'est produite, veuillez réessayer...'], 500);
                }
            }
        }
        return new JsonResponse(['error' => 'Mauvaise requête'], 400);
    }

    public function getOrganismeClientAction($orgaId)
    {
        $listeClients = [];
        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $this->getOrganismeClientRecursive($organisme, $listeClients);
        ksort($listeClients);
        $listeClients = array_values($listeClients);
        return new JsonResponse(['clients' => $listeClients], 200);
    }

    /**
     * Méthode pour mettre à jour un contact
     * @param Request $request
     * @param $contactId
     * @return JsonResponse|Response
     */
    public function editContactAction(Request $request, $contactId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        $data = [];

        $em = $this->getDoctrine()->getManager();

        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($contactId);

        $formEditContact = $this->createForm(EditContactType::class, $contact, ['role' => $this->getUser()->getRoles()]);
        $formEditContact->handleRequest($request);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('PUT')) {
                foreach ($contact->getType() as $item) {
                    $contact->removeType($item);
                }
                $data = $request->request->get('edit_contact');
                $data['organisme'] = $this->getDoctrine()->getRepository(Organisme::class)->find($data['organisme']);
                if (is_array($data['type'])) {
                    foreach ($data['type'] as $type) {
                        $type = $this->getDoctrine()->getRepository(TypeContact::class)->find($type);
                        $contact->setType($type);
                    }
                } else {
                    $type = $this->getDoctrine()->getRepository(TypeContact::class)->find($data['type']);
                    $contact->setType($type);
                }
                unset($data['type']);
                unset($data['_token']);
                foreach ($data as $prop => $value) { // Itère dans les données
                    if ($value != "") {
                        $temp = ucfirst($prop); // Capitalise la première lettre du mot
                        $prop = 'set' . $temp; // Concatène pour ajouter la string "set"
                        $contact->$prop($value); // Assigne la valeur dynamiquement
                    }
                }
                if (!$contact->getUser()) {
                    $contact->setUser($this->getUser());
                }
                $em->persist($contact);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'Contact modifier avec succès';
                } catch (\Exception $e) {
                    dump($e); die();
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
            }

            return $this->render('GestionBundle:Default:editContact.html.twig', [
                'form' => $formEditContact->createView()
            ]);
        } else {
            // Mettre en place une page d'erreur
        }
    }

    /**
     * Méthode pour supprimer un contact
     * @param Request $request
     * @param $contactId
     * @return JsonResponse
     */
    public function deleteContactAction(Request $request, $contactId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';

        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $contact = $this->getDoctrine()->getRepository(Contact::class)->find($contactId);
            if ($request->isMethod('DELETE')) {
                $em->remove($contact);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'Le contact a bien était supprimé';
                } catch (\Exception $e) {
                    $code = 500;
                    $message = 'Une erreur s\'est produite, veuillez réessayer...';
                }
            } else if ($request->isMethod('GET')) {
                $devis = $contact->getDevis()->toArray();
                $commandes = $contact->getCommandes()->toArray();
                $commandeFournisseur = $contact->getCommandeFournisseur()->toArray();
                $attachements = $contact->getAttachements()->toArray();
                $factures = $contact->getFactures()->toArray();
                if (empty($devis) && empty($commandes) && empty($commandeFournisseur) && empty($attachements) && empty($factures)) {
                    $code = 200;
                    $status = 'success';
                    $message = 'Vous êtes sur le point de supprimer un contact. Cette action est irréversible. Etes-vous sûr de vouloir le supprimer ?';
                } else {
                    $code = 200;
                    $message = 'Le contact ne peut pas être supprimé';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message]);
    }

    /**
     * Méthode pour récupèrer le liste des contact d'un organisme pour le devis
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function getContactListAction(Request $request, $orgaId)
    {
        if ($request->isXmlHttpRequest()) {
            if($request->isMethod('GET')) {
                try {
                    $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                    $listeOrga = [];
                    if ($orga->getType() == 1) {
                        $this->getOrganismeClientRecursive($orga, $listeOrga);
                    } elseif ($orga->getType() == 0) {
                        $this->getOrganismeFournisseurRecursive($orga, $listeOrga);
                    } elseif ($orga->getType() == 2) {
                        $this->getOrganismePrestataireRecursive($orga, $listeOrga);
                    }
                    $contacts = [];
                    foreach ($listeOrga as $object) {
                        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($object['id']);
                        $contact = $organisme->getContacts()->toArray();
                        foreach($contact as $cont) {
                            if (in_array('Opérationnel', $cont->getType()->toArray())) {
                                $contacts[$cont->getId()] = ['id' => $cont->getId(), 'nom' => $cont->getNom(), 'prenom' => $cont->getPrenom()];
                            }
                        }
                    }
                    ksort($contacts);
                    $contacts = array_values($contacts);
                    return new JsonResponse(['contacts' => $contacts], 200);
                } catch (\Exception $e) {
                    return new JsonResponse(['error' => 'Une erreur s\'est produite, veuillez réessayer...'], 500);
                }
            }
        }
        return new JsonResponse(['error' => 'Mauvaise requête'], 400);
    }

    /**
     * Méthode pour créer un contact pour un organisme depuis un devis
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse|Response
     */
    public function createContactOrgaDevisAction(Request $request, $orgaId)
    {
        // Initialisation des variable nécessaire pour le retour en JSON
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        // Récupère le manageur d'entité
        $em = $this->getDoctrine()->getManager();
        $contact = new Contact(); // Invoque un nouveau contact
        // Récupère l'organisme auquel il faurt rattacher le contact
        $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        // Créé le formulaire
        $form = $this->createForm(joinContactAjaxType::class, $contact, ['role' => $this->getUser()->getRoles()]);
        // Ajout le gestionnaire de requête
        $form->handleRequest($request);
        // Si la requête est AJAX
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod(('POST'))) { // Si la méthode est POST
                $contact->setOrganisme($orga); // On assigne l'organisme
                $contact->setUser($this->getUser()); // Assigne l'utilisateur
                $data = $request->request->get('join_contact_ajax'); // Récupère le résultat de la requête Ajax
                $em->persist($contact); // Persist l'objet doctrine
                try { // POur le gestion des erreurs on test les instructions suivantes
                    $em->flush();
                    $data['id'] = $contact->getId();
                    $code = 200;
                    $message = 'Le contact à bien été créé';
                    $status = 'success';
                } catch (\Exception $e) { // EN cas d'erreur
                    $message = $this->exceptionHandler->getException($e); // Récupération du code d'erreur
                    $code = 200;
                }
                // Retourn le JSON
                return new JsonResponse(['data' => $data, 'status' => $status, 'message' => $message], $code);
            }
        }
        // Retourne le template du formulaire en HTML
        return $this->render('GestionBundle:Default:createContact.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour validé un fournisseur de matèriel
     * @param Request $request
     * @param null $orgaId
     * @return JsonResponse
     */
    public function validFournisseurAction(Request $request, $orgaId=null)
    {
        // Initialisation des variable nécessaire pour le retour en JSON
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        // Si la requête est XmlHttpRequest
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) { // SI la méthode est POST
                $em = $this->getDoctrine()->getManager(); // Gestionnaire d'entité doctrine
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId); // Récupération d el'organisme
                $orga->setFournisseurValide(true); // Change l'état du fournisseur
                $em->persist($orga); // Persist l'objet doctrine
                $fournisseur = $orga; // Assigne l'organisme à la variable fournisseur (pour la sémentique)
                try { // Test
                    $em->flush();
                    $code = 200;
                    $message = 'Référencement du fournisseur enregistrer.';
                    $status = 'success';
                    $html = $this->renderView('GestionBundle:Export:acceptedFournisseur.html.twig', [
                        'fournisseur' => $fournisseur
                    ]);
                    $mail =$this->get('email.action.mailer');
                    $response = $mail->sendWithoutFlash($html, $fournisseur->getUser()->getEmail(), 'Gest\'it - Validation référencement fournisseur ' . $fournisseur->getNom());
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail informant le chef de projet a été envoyé.';
                    } else {
                        $status = 'error';
                        $message .= ' Le mail informant le chef de projet n\'a été envoyé. Veuillez l\'en informer.';
                    }
                } catch (\Exception $e) { // Si erreur
                    $code= 200;
                    $message = $this->exceptionHandler->getException($e); // Asignation du code d'erreur
                }
            }
        }
        // retourne les données en JSON
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour refuser un fournisseur
     * @param Request $request
     * @param null $orgaId
     * @return JsonResponse
     */
    public function refuseFournisseurAction(Request $request, $orgaId)
    {
        // Initialisation des variable nécessaire pour le retour en JSON
        $code = 200;
        $status = 'error';
        $message = 'Mauvaise requête';
        // Si la requête est XmlHttpRequest
        if ($request->isXmlHttpRequest()) { // Si la méthode est POST
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $fournisseur = '';
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                $orga->setFournisseurValide(false);
                if ($orga->getType() == 0) {
                    $orga->setActif(0);
                } elseif ($orga->getType() == 2) {
                    $orga->setType(1);
                }
                $em->persist($orga);
                $fournisseur = $orga;
                try {
                    $em->flush();
                    $code = 200;
                    $message = 'Refus du fournisseur enregistré.';
                    $status = 'success';
                    $html = $this->renderView('GestionBundle:Export:refusFournisseur.html.twig', [
                        'fournisseur' => $fournisseur
                    ]);
                    $mail = $this->get('email.action.mailer');
                    $response = $mail->sendWithoutFlash($html, $fournisseur->getUser()->getEmail(), 'Gest\'it - Refus référencement fournisseur ' . $fournisseur->getNom());
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail informant le chef de projet a été envoyé.';
                    } else {
                        $status = 'error';
                        $message .= ' Mais le mail informant le chef de projet n\'a été envoyé. Veuillez l\'en informer.';
                    }
                } catch (\Exception $e) {
                    $code= 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            }
        }
        // Retourne les données
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * @param $orgaId
     * @return JsonResponse
     */
    public function getAdresseAction($orgaId)
    {
        // Récupère l'organisme ciblé
        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $listeOrganismes = [];
        $this->getOrganismeClientRecursive($organisme, $listeOrganismes);
        $listAdresse = []; // Initialise un array vide
        foreach($listeOrganismes as $orga) { // Pour chaque enfant
            $org = $this->getDoctrine()->getRepository(Organisme::class)->find($orga['id']);
            array_push($listAdresse, $this->adresseBuilder($org)); // Ajoute à l'array les données de l'adresse
        }
        // Retourne l'array constitué
        return new JsonResponse($listAdresse);
    }

    /**
     * Méthode pour ajouter un contact à un fournisseur ou à un prestataire
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse|Response
     */
    public function createContactReferencementAction(Request $request, $orgaId)
    {
        $contact = new Contact();
        $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $contact->setOrganisme($orga);
        $form = $this->createForm(joinContactAjaxType::class, $contact, ['role' => $this->getUser()->getRoles(), 'type' => true]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $status = 'error';
            $code = 200;
            $message = '';
            $contact->setUser($this->getUser());
            if (!in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                foreach ($contact->getType() as $type) {
                    $contact->removeType($type);
                }
                $contact->setType($this->getDoctrine()->getRepository(TypeContact::class)->findOneBy(['type' => 'Référencement']));
            }
            $contact->setOrganisme($orga);
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);

            try {
                $em->flush();
                $status = 'success';
                $message = 'Le contact pour établir le référencement a été enregistré';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'contact');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createContact.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour valider un prestataire manuellement
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function validPrestataireAction(Request $request, $orgaId)
    {
        // Initialisation des variable nécessaire pour le retour en JSON
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        // Si la requête est XmlHttpRequest
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) { // SI la méthode est POST
                $em = $this->getDoctrine()->getManager(); // Gestionnaire d'entité doctrine
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId); // Récupération d el'organisme
                $orga->setPrestataireValide(true); // Change l'état du fournisseur
                $em->persist($orga); // Persist l'objet doctrine
                $prestataire = $orga; // Assigne l'organisme à la variable fournisseur (pour la sémentique)
                try { // Test
                    $em->flush();
                    $code = 200;
                    $message = 'Référencement du prestataire enregistrer.';
                    $status = 'success';
                    $html = $this->renderView('GestionBundle:Export:acceptedFournisseur.html.twig', [
                        'prestataire' => $prestataire
                    ]);
                    $mail =$this->get('email.action.mailer');
                    $response = $mail->sendWithoutFlash($html, $prestataire->getUser()->getEmail(), 'Gest\'it - Validation référencement prestataire ' . $prestataire->getNom());
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail informant le chef de projet a été envoyé.';
                    } else {
                        $status = 'error';
                        $message .= ' Le mail informant le chef de projet n\'a été envoyé. Veuillez l\'en informer.';
                    }
                } catch (\Exception $e) { // Si erreur
                    $code= 200;
                    $message = $this->exceptionHandler->getException($e); // Asignation du code d'erreur
                }
            }
        }
        // retourne les données en JSON
        return new JsonResponse(['status' => $status, 'message' => $message], $code);

    }

    /**
     * Méthode pour refuser un prestataire
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function refusePrestataireAction(Request $request, $orgaId)
    {
        // Initialisation des variable nécessaire pour le retour en JSON
        $code = 200;
        $status = 'error';
        $message = 'Mauvaise requête';
        // Si la requête est XmlHttpRequest
        if ($request->isXmlHttpRequest()) { // Si la méthode est POST
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                $orga->setPrestataireValide(false);
                $orga->setActif(0);
                $em->persist($orga);
                $prestataire = $orga;
                try {
                    $em->flush();
                    $code = 200;
                    $message = 'Refus du prestataire enregistré.';
                    $status = 'success';
                    $html = $this->renderView('GestionBundle:Export:refusFournisseur.html.twig', [
                        'prestataire' => $prestataire
                    ]);
                    $mail = $this->get('email.action.mailer');
                    $response = $mail->sendWithoutFlash($html, $prestataire->getUser()->getEmail(), 'Gest\'it - Refus référencement prestataire ' . $prestataire->getNom());
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail informant le chef de projet a été envoyé.';
                    } else {
                        $status = 'error';
                        $message .= ' Mais le mail informant le chef de projet n\'a été envoyé. Veuillez l\'en informer.';
                    }
                } catch (\Exception $e) {
                    $code= 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            }
        }
        // Retourne les données
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }
}