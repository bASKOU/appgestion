<?php

namespace GestionBundle\Controller\Export;

use mpdf\mpdf;
use Mpdf\Output\Destination;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\Facture;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Attachement;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Entity\ArticleFacture;
use GestionBundle\Entity\ArticleAttachement;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use GestionBundle\Entity\ArticleLibrattachement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Ce controller a pour but de récupérer les informations d'une opération, au clic d'un bouton dans la page de détail d'une affaire, et de soit
 * -> l'exporter en PDF via la librairie mPdf et proposer le document en téléchargement à l'utilisateur
 * -> le transformer en PDF et envoyer le document en PJ d'un mail
 */

 //? Etudier la possibilité de transformer l'envoi de mail en service afin d'alléger les fonctions du controller

 //TODO : Finir d'implémenter la méthode de mail pour les autres opérations

 //TODO 2 :Refactorization possible pour l'envoi de mail en Service afin de décharger le controller


class PDFController extends Controller
{

    //! ---------------------------------------------------------------- DEVIS ------------------------------------------------------------------------- !//

    //** Fonction qui va permettre de récupérer via son id en parametre de route, et de le transformer en PDF */
    public function devisToPdfAction($devisId)
    {
        // on récupère le devis via son id
        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId);
        // et on récupère tous les articles qui lui sont liés
        $linkedArticleDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
            'devis' => $devisId
        ]);

        // on créé un objet mpdf, et on lui définis certains paramètres de base
        $mpdf = new \Mpdf\Mpdf([
            'tempDir' => 'uploads/temp',
            'default_font' => 'calibri',
            'mode' => 'utf-8', 
            'format' => 'A4', 
            'orientation' => 'P',
            'margin_left' => 8,
            'margin_right' => 8,
            'margin_top' => 0,
            'margin_bottom' => 15,
            'margin_header' => 8,
            'margin_footer' => 8,
            'aliasNbPg' => '[pagetotal]',
            'defaultPageNumStyle' => '1',
            'pagenumPrefix' => 'Gest\'it ',
        ]);

        // on rends une vue twig selon un template bien définis pour correspondre à l'objet mpdf
        $html = $this->renderView('GestionBundle:Export:devisToPDF.html.twig', [
            'devis' => $devis,
            'linkedArticleDevis' => $linkedArticleDevis,
        ]);
        // plutot que de rendre la vue au naviguateur, on utilise une méthode de mpdf pour transformer notre vue html twig en PDF
        $mpdf->WriteHTML($html);
        // on propose le document en téléchargment à l'utilisateur
        $mpdf->Output($devis->getNumeroDevis().'.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }

    //! ---------------------------------------------------------------- ATTACHEMENTS ------------------------------------------------------------------------- !//

    public function attachementToPdfAction($attachementId)
    {
        $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($attachementId);
        $librattachement = false;
            
        $linkedArticleAttachement = $this->getDoctrine()->getRepository(ArticleAttachement::class)->findBy([
            'attachement' => $attachementId
        ]);

        if (empty($linkedArticleAttachement)) {
            $linkedArticleAttachement = $this->getDoctrine()->getRepository(ArticleLibrattachement::class)->findBy([
                'attachement' => $attachementId
            ]);
            $librattachement = true;
        }

        $mpdf = new \Mpdf\Mpdf([
            'tempDir' => 'uploads/temp',
            'default_font' => 'calibri',
            'mode' => 'utf-8', 
            'format' => 'A4', 
            'orientation' => 'P',
            'margin_left' => 8,
            'margin_right' => 8,
            'margin_top' => 8,
            'margin_bottom' => 15,
            'margin_header' => 8,
            'margin_footer' => 8,
            'aliasNbPg' => '[pagetotal]',
            'defaultPageNumStyle' => '1',
            'pagenumPrefix' => 'Gest\'it ',
        ]);

        $html = $this->renderView('GestionBundle:Export:attachementToPDF.html.twig', [
            'attachement' => $attachement,
            'linkedArticleAttachement' => $linkedArticleAttachement,
            'librattachement' => $librattachement
        ]);

        $mpdf->WriteHTML($html);
        $mpdf->Output('attachement_cmd_'.$attachement->getCommande()->getNumeroCommande().'.pdf', \Mpdf\Output\Destination::DOWNLOAD);


        // return $this->render('GestionBundle:Export:attachementToPDF.html.twig', [
        //     'attachement' => $attachement,
        //     'linkedArticleAttachement' => $linkedArticleAttachement,
        // ]);

    }


    //! ---------------------------------------------------------------- FACTURE ------------------------------------------------------------------------- !//

    public function factureToPdfAction($factureId)
    {
        $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);

        $linkedArticleFacture = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
            'facture' => $factureId
        ]);

        $linkedOperation = $facture->getLinkedOperation();
        if($this->getDoctrine()->getRepository(Commande::class)->find($linkedOperation)) {
            $commande = $this->getDoctrine()->getRepository(Commande::class)->find($linkedOperation);
            $numero = $commande->getNumeroCommande();
            $parentOperation = "commande";
            $attachement = null;
        } else {
            $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($linkedOperation);
            $commande = $attachement->getCommande();
            $numero = $commande->getNumeroCommande();
            $parentOperation = "attachement";
        }

        $mpdf = new \Mpdf\Mpdf([
            'tempDir' => 'uploads/temp',
            'default_font' => 'calibri',
            'mode' => 'utf-8', 
            'format' => 'A4', 
            'orientation' => 'P',
            'margin_left' => 8,
            'margin_right' => 8,
            'margin_top' => 8,
            'margin_bottom' => 15,
            'margin_header' => 8,
            'margin_footer' => 8,
            'aliasNbPg' => '[pagetotal]',
            'defaultPageNumStyle' => '1',
            'pagenumPrefix' => 'Gest\'it ',
            'useActiveForms' => true
        ]);

        $html = $this->renderView('GestionBundle:Export:factureToPDF.html.twig', [
            'facture' => $facture,
            'linkedArticleFacture' => $linkedArticleFacture,
            'numero' => $numero,
            'parentOperation' => $parentOperation,
            'attachement' => $attachement
        ]);

         $mpdf->WriteHTML($html);
         $mpdf->Output($facture->getNumeroFacture().'.pdf', \Mpdf\Output\Destination::DOWNLOAD);


        return $this->render('GestionBundle:Export:factureToPDF.html.twig', [
            'facture' => $facture,
            'linkedArticleFacture' => $linkedArticleFacture,
            'numero' => $numero,
            'parentOperation' => $parentOperation,
            'attachement' => $attachement
        ]);

    }
}