<?php

namespace GestionBundle\Controller;

use DateTime;
use GestionBundle\Entity\AvancementCommandeFournisseur;
use GestionBundle\Entity\AvancementCommandePrestataire;
use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Facture;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\Attachement;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Entity\ArticleAffaire;
use GestionBundle\Entity\ArticleCommande;
use GestionBundle\Form\CreateAffaireType;
use GestionBundle\Entity\AvancementAffaire;
use GestionBundle\Entity\AvancementCommande;
use GestionBundle\Services\ChartsService;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GestionBundle\Form\CreateAffaireDevisType;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AffaireController
 * @package GestionBundle\Controller
 * Contrôleur des actions ciblant une affaire
 */
class AffaireController extends Controller
{
    // Liste des propriétés nécessaire dans plusieurs méthodes
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;
    /**
     * @var \DateTime
     */
    private $today;
    /**
     * @var string
     */
    private $month;
    /**
     * @var string
     */
    private $year;
    /**
     * @var \DateTime
     */
    private $last;

    /**
     * AffaireController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $today = new \DateTime('today'); // Récupère la date d'aujourd'hui
        if (intval($today->format('d')) < 16) { // Si le jour du mois est inférieur à 16
            $today = $today->modify('-1 month'); // On set la date du jour au mois précédent pour gérer l'avancement du mois précédent
        }
        $this->exceptionHandler = new ExceptionHandler(); // Invocation de la class de gestion d'exception
        $this->createNumber = $numberCheckAndRenew; // Invocation du service de génération des numéros
        $this->today = $today; // Stockage de la date dans une propriété de l'objet
        $this->month = $today->format('m'); // Récupération du mois
        $this->year = $today->format('Y'); // Récupération de l'année
        $this->last = clone $this->today; // Ici clonage de l'objet date
        $this->last->modify('-1 month'); // On soustrait 1 month à la date actuelle
    }

    /**
     * Méthode pour trier par date croissante les devis
     * @param $devis
     */
    private function orderByDateDevis(&$devis)
    {
        usort($devis, function($a, $b) {
            if (strtotime($a->getDateDevis()->format('Y-m-d H:i:s')) === strtotime($b->getDateDevis()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateDevis()->format('Y-m-d H:i:s')) < strtotime($b->getDateDevis()->format('Y-m-d H:i:s'))) ? +1 : -1;
        });
    }

    /**
     * Méthode pour trier par date croissante les commandes
     * @param $commande
     */
    private function orderByDateCommande(&$commande)
    {
        if (!empty($commande[0])) {
            usort($commande, function ($a, $b) {
                if (strtotime($a->getDateCommande()->format('Y-m-d H:i:s')) === strtotime($b->getDateCommande()->format('Y-m-d H:i:s'))) {
                    return 0;
                }
                return (strtotime($a->getDateCommande()->format('Y-m-d H:i:s')) < strtotime($b->getDateCommande()->format('Y-m-d H:i:s'))) ? -1 : +1;
            });
        }
    }

    /**
     * Méthode pour trier par date croissante les attachements
     * @param $situation
     */
    private function orderByDateSituation(&$situation)
    {
        if (!empty($situation[0])) {
            usort($situation, function ($a, $b) {
                if (strtotime($a->getDateAttachement()->format('Y-m-d H:i:s')) === strtotime($b->getDateAttachement()->format('Y-m-d H:i:s'))) {
                    return 0;
                }
                return (strtotime($a->getDateAttachement()->format('Y-m-d H:i:s')) < strtotime($b->getDateAttachement()->format('Y-m-d H:i:s'))) ? -1 : +1;
            });
        }
    }

    /**
     * Méthode pour trier par date croissante les estimations
     * @param $estimation
     */
    private function orderByDateEstimation(&$estimation)
    {
        if (!empty($estimation[0])) {
            usort($estimation, function ($a, $b) {
                if (strtotime($a->getDate()->format('Y-m-d H:i:s')) === strtotime($b->getDate()->format('Y-m-d H:i:s'))) {
                    return 0;
                }
                return (strtotime($a->getDate()->format('Y-m-d H:i:s')) < strtotime($b->getDate()->format('Y-m-d H:i:s'))) ? -1 : +1;
            });
        }
    }

    /**
     * Méthode pour filtrer les estimations pour le mois en cours et qui ne sont pas rattaché à une facture
     * @param $estimations
     * @return array
     */
    private function getTotalEstimationThisMonth($estimations)
    {
        $estim = [];
        $total = 0;
        foreach ($estimations as $key => $estimation) {
            if ($estimation->getFacture() == null) {
                $date = $estimation->getDate();
                if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == intval($this->month)) {
                    $total += $estimation->getTotalHT();
                }
            }
        }

        return $total;
    }

    /**
     * Méthode pour trier par date croissante les factures émises
     * @param $facture
     */
    private function orderByDateFacture(&$facture)
    {
        if (!empty($facture[0])) {
            if ($facture[0]->getDateFacture()) {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) === strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) < strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            } else {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDatePreSaisie()->format('Y-m-d H:i:s')) === strtotime($b->getDatePreSaisie()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDatePreSaisie()->format('Y-m-d H:i:s')) < strtotime($b->getDatePreSaisie()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            }
        }
    }

    /**
     * Méthode pour trier par date croissante les factures reçus
     * @param $facture
     */
    private function orderByDateFactureRecus(&$facture)
    {
        if (!empty($facture[0])) {
            if ($facture[0]->getDateFacture()) {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) === strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) < strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            } else {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDateSaisie()->format('Y-m-d H:i:s')) === strtotime($b->getDateSaisie()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDateSaisie()->format('Y-m-d H:i:s')) < strtotime($b->getDateSaisie()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            }
        }
    }

    /**
     * Méthode pour calculer le total HT des factures du mois en cours
     * @param $factures
     * @return int
     */
    private function getTotalFacturationThisMonth($factures)
    {
        $total = 0;
        foreach ($factures as $key => $facture) {
            $date = $facture->getDateFacture();
            if ($date) {
                if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == intval($this->month)) {
                    $total += $facture->getTotalHT();
                }
            } else {
                $total += $facture->getTotalHT();
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des commandes du mois en cours
     * @param $commandes
     * @return int
     */
    private function getTotalCommandeThisMonth($commandes)
    {
        $total = 0;
        foreach ($commandes as $key => $commande) {
            $date = $commande->getDateCommande();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == intval($this->month)) {
                $total += $commande->getTotalHT();
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des situations du mois en cours
     * @param $situations
     * @return int
     */
    private function getTotalSituationThisMonth($situations)
    {
        $total = 0;
        foreach ($situations as $key => $situation) {
            $date = $situation->getDateAttachement();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == intval($this->month)) {
                $total += $situation->getTotalHT();
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des factures du mois précédent
     * @param $factures
     * @return int
     */
    private function getTotalFacturationLastMonth($factures)
    {
        $total = 0;
        foreach ($factures as $key => $facture) {
            $date = $facture->getDateFacture();
            if ($date) {
                if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == (intval($this->month) - 1)) {
                    $total += $facture->getTotalHT();
                } else if (intval($date->format('Y')) == intval($this->year) - 1 && (intval($this->month) - 1) == 0 && intval($date->format('m')) == 12) {
                    $total += $facture->getTotalHT();
                }
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des commandes du mois précédent
     * @param $commandes
     * @return int
     */
    private function getTotalCommandeLastMonth($commandes)
    {
        $total = 0;
        foreach ($commandes as $key => $commande) {
            $date = $commande->getDateCommande();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == (intval($this->month) - 1)) {
                $total += $commande->getTotalHT();
            } else if (intval($date->format('Y')) == intval($this->year) - 1 && (intval($this->month) - 1) == 0 && intval($date->format('m')) == 12) {
                $total += $commande->getTotalHT();
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des situations du mois précédent
     * @param $situations
     * @return int
     */
    private function getTotalSituationLastMonth($situations)
    {
        $total = 0;
        foreach ($situations as $key => $situation) {
            $date = $situation->getDateAttachement();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == (intval($this->month) - 1)) {
                $total += $situation->getTotalHT();
            } else if (intval($date->format('Y')) == intval($this->year) - 1 && (intval($this->month) - 1) == 0 && intval($date->format('m')) == 12) {
                $total += $situation->getTotalHT();
            }
        }
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des factures du mois précédent
     * @param $factures
     * @param null|string $type
     * @return int
     */
    private function getTotalFactureRecusLastMonth($factures, $type = null)
    {
        $total = 0;
        if ($type === 'saisie') {
            $temp = 'getDateSaisie';
        } else {
            $temp = 'getDateFacture';
        }
        foreach ($factures as $facture) {
            $date = $facture->$temp();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == (intval($this->month) - 1)) {
                $totalEstim = $this->getTotalFactureMinusEstimationLast($facture);
                $total += $facture->getTotalHT() - $totalEstim;
            } else if (intval($date->format('Y')) == intval($this->year) - 1 && (intval($this->month) - 1) == 0 && intval($date->format('m')) == 12) {
                $totalEstim = $this->getTotalFactureMinusEstimationLast($facture);
                $total += $facture->getTotalHT() - $totalEstim;
            }
        }

        return $total;
    }

    /**
     * Méthode pour calculer le total des estimations à enlever d'une facture pour le mois précédent
     * @param $facture
     * @return int
     */
    private function getTotalFactureMinusEstimationLast($facture)
    {
        $total = (float) 0; // Set le total à 0 pour faire des additions à la suite
        foreach ($facture->getEstimation() as $estimation) { // Pour chaque estimation
            $date = $estimation->getDate(); // Récupération de la date de l'estimation
            if (intval($date->format('m')) <= intval($this->month - 1)) { // Si la date de l'estimation est inférieur ou égale à la date du mois précédent
                $total += (float) $estimation->getTotalHT(); // Ajout du total de l'estimation
            }
        }
        // retourne le cumul des totaux des estimation respectant la condition précédente
        return $total;
    }

    /**
     * Méthode pour calculer le total HT des situations du mois en cours
     * @param $factures
     * @param null|string $type
     * @return int
     */
    private function getTotalFactureRecusThisMonth($factures, $type = null)
    {
        $total = 0; // Set le total à 0 pour faire des additions à la suite
        if ($type === 'saisie') {
            $temp = 'getDateSaisie';
        } else {
            $temp = 'getDateFacture';
        }
        foreach ($factures as $facture) {
            $date = $facture->$temp();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == intval($this->month)) {
                $totalEstim = $this->getTotalFactureMinusEstimationThis($facture);
                $total += $facture->getTotalHT() - $totalEstim;
            }
        }

        return $total;
    }

    /**
     * Méthode pour calculer le total des estimations d'une facture émise pour le mois en cours
     * @param $facture
     * @return int
     */
    private function getTotalFactureMinusEstimationThis($facture)
    {
        $total = 0; // Set le total à 0 pour faire des additions à la suite
        foreach ($facture->getEstimation() as $estimation) { // Pour chaque estimation d'une facture
            $total += $estimation->getTotalHT(); // On additionne le montant HT des estimations
        }

        return $total;
    }

    /**
     * Méthode pour calculer le total HT des estimations du mois précédent
     * @param $estimations
     * @param null|string $type
     * @return int
     */
    private function getTotalEstimLastMonth($estimations, $type = null)
    {
        $total = 0; // Set le total à 0 pour faire des additions à la suite
        foreach ($estimations as $estimation) {
            $date = $estimation->getDate();
            if (intval($date->format('Y')) == intval($this->year) && intval($date->format('m')) == (intval($this->month) - 1)) {
                $total += $estimation->getTotalHT();
            } else if (intval($date->format('Y')) == intval($this->year) - 1 && (intval($this->month) - 1) == 0 && intval($date->format('m')) == 12) {
                $total += $estimation->getTotalHT();
            }
        }

        return $total;
    }

    /**
     * Méthode pour calculer le total HT des estimations du mois précédent
     * @param $estimations
     * @return int
     */
    private function getTotalEstimCumul($estimations)
    {
        $total = 0; // Set le total à 0 pour faire des additions à la suite
        foreach ($estimations as $estimation) {
            if ($estimation->getFacture() == null) {
                $total += $estimation->getTotalHT();
            }
        }

        return $total;
    }

    private function getTotalEstimPerMonth($estimPerMonth, $estimations) {
        $total = [];
        foreach ($estimations as $estimation) {
            if (!$estimation->getFacture()) {
                $index = array_search(intval($estimation->getDate()->format('m')), $estimPerMonth);
                if (!empty($tempTot[$index])) {
                    $total[$index] += (float)$estimation->getTotalHT();
                } else {
                    $total[$index] = (float)$estimation->getTotalHT();
                }
            }
        }

        return $total;
    }

    /**
     * Méthode pour calculer le total des factures, commandes et situations de l'affaire depuis le début
     * @param $arguments
     * @return int
     */
    private function totalCumulAll($arguments)
    {
        $total = 0; // Set le total à 0 pour faire des additions à la suite
        foreach ($arguments as $argument) {
            $total += $argument->getTotalHT();
        }
        return $total;
    }

    /**
     * Méthode pour récupérer l'avancement de l'affaire du mois en cours
     * @param $avancements
     * @return int
     */
    private function getThisMonthAvancement($avancements)
    {
        foreach ($avancements as $avancement) {
            if ($avancement->getDate()->format('m') === $this->month && $avancement->getDate()->format('Y') === $this->year) {
                return $avancement->getAvancement();
            }
        }

        return 0;
    }

    /**
     * Méthode pour récupérer l'avancement de l'affaire du mois précédent
     * @param $avancements
     * @return int
     */
    private function getLastMonthAvancement($avancements)
    {
        foreach ($avancements as $avancement) {
            if (intval($avancement->getDate()->format('m')) === (intval($this->month) - 1) && intval($avancement->getDate()->format('Y')) === intval($this->year)) {
                return $avancement->getAvancement();
            } elseif (intval($avancement->getDate()->format('m')) === 12 && intval($avancement->getDate()->format('Y')) === intval($this->year) - 1) {
                return $avancement->getAvancement();
            }
        }

        return 0;
    }

    /**
     * Fonction qui va nous permettre de créer une nouvelle affaire
     * le paramètre request permet à notre fonction de récupérer les données de la requête
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        // on instancie une nouvelle affaire
        $affaire = new Affaire();
        // Récupère l'id de la dernière affaire
        $affaire->setNumeroAffaire($this->createNumber->numeroAffaire());
        $affaire->setDateDebut(new \DateTime('today'));
        $code = 400;
        $status = "error";
        $message = "";

        // on récupère une requête post depuis notre vue
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                // Récupère le corps de la requête
                $data = $request->request->get('create_affaire');
                unset($data['_token']);
                if ($data['plafond'] == "") {
                    $data['plafond'] = null;
                } else {
                    $data['plafond'] = (float)$data['plafond'];
                }
                foreach($data as $key => $value) {
                    if ($key == 'organisme') {
                        if ($value == "") {
                            $organisme = null;
                        } else {
                            $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        }
                        $affaire->setOrganisme($organisme);
                    } elseif ($key == 'adresseLivraison' || $key == 'adresseFacturation') {
                        $temp = 'set' . ucfirst($key);
                        if ($value == "") {
                            $organisme = null;
                        } else {
                            $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        }
                        $affaire->$temp($organisme);
                    } elseif ($key == 'dateDebut' || $key == 'dateEcheance') {
                        $temp = 'set' . ucfirst($key);
                        $affaire->$temp(DateTime::createFromFormat('d/m/Y', $value));
                    } else {
                        $temp = 'set' . ucfirst($key);
                        if ($value == "") {
                            $value = null;
                        }
                        $affaire->$temp($value);
                    }
                }
                $affaire->setRespectPlafond(true);

                // on transforme notre objet en entité doctrine grâce au Manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($affaire);
//                $avancement = new AvancementAffaire();
//                $date = new \DateTime('now');
//                $date->modify('last day of this month');
//                $avancement->setDate($date);
                $code = 200;
                try {
                    $em->flush();
                    $message = 'Affaire créée !';
                    $status = 'success';
                } catch (\Exception $e) {
                    dump($e); die();
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'affaire');
                }
                // on renvoie à la vue une réponse Json qui conttient plusieurs informations concernant la nouvelle affaire afin de pouvoir modifier la vue sans la recharger
                return new JsonResponse([$status => $message, 'id' => $affaire->getId(), 'nom' => $affaire->getDesignationAffaire(), 'numero' => $affaire->getNumeroAffaire(), 'user' => $this->getUser()], $code);
            } elseif ($request->isMethod('GET')) {
                $form = $this->createForm(CreateAffaireType::class, $affaire);
                return $this->render('GestionBundle:Default:createAffaire.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * fonction qui va nous permettre de récupérer toutes les opérations liées à une affaire et de les afficher sur une vue dédiée
     * @param $affaireId
     * @return Response
     */
    public function detailAction($affaireId)
    {
        if (strpos($affaireId, '#')) {
            $affaireId = explode('#', $affaireId)[0];
        }
        // on récupère l'affaire via son id passée en paramètre de la fonction
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        if (!in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles())) {
            $canAccess = false;
            foreach ($affaire->getAttributions() as $attribution) {
                if ($this->getUser() == $attribution->getUser()) {
                    $canAccess = true;
                }
            }
            if (!$canAccess) {
                $this->addFlash('warning', "Vous n'avez pas les droits nécessaire pour accéder à cette affaire");

                return $this->redirectToRoute('sudalys_gestion_home');
            }
        }
        $commandeFournisseur = $affaire->getCommandeFournisseur();
        $commandePrestataire = $affaire->getCommandePrestataire();
        $factureFournisseur = $affaire->getFactureFournisseur();
        $facturePrestataire = $affaire->getFacturePrestataire();

        /**
         *  Grâce à cet id d'affaire, je peux récupérer tous les devis et les commandes qui y sont liés
         */ 
        $devis = $affaire->getDevis()->toArray();
        $commandes = $this->getDoctrine()->getRepository(Commande::class)->findBy([
            'affaire' => $affaireId
        ]);
        $factures = $this->getDoctrine()->getRepository(Facture::class)->findBy([
            'affaire' => $affaireId
        ]);

        /**
         * Chaque commande pouvant avoir plusieurs attachements, je boucle sur le tableau des commandes, et pour itération
         * je récupère l'ensemble des attachements liés à cette commande, attachements que je push dans un tableau
         */
        $attachements = [];
        for($i = 0; $i < count($commandes); $i++) {
            $attachement = $this->getDoctrine()->getRepository(Attachement::class)->findBy([
                'commande' => $commandes[$i]->getId()
            ]);
            foreach($attachement as $key => $value) {
                array_push($attachements, $value);
            }
        }

        $this->orderByDateFacture($factures);
        $this->orderByDateDevis($devis);
        $this->orderByDateSituation($attachements);
        $this->orderByDateCommande($commandes);

        return $this->render('GestionBundle:Default:detailAffaire.html.twig', [
            'affaire' => $affaire,
            'devis' => $devis,
            'commandes' => $commandes,
            'attachements' => $attachements,
            'factures' => $factures,
            'commandeFournisseur' => $commandeFournisseur,
            'commandePrestataire' => $commandePrestataire,
            'factureFournisseur' => $factureFournisseur,
            'facturePrestataire' => $facturePrestataire
        ]);
    }

    /**
     * Méthode qui est utilisé pour vérifier si une affaire à des devis, factures, et autres avant suppression
     * @param $affaireId
     * @return JsonResponse
     */
    public function checkAffaireAction($affaireId)
    {
        $status = true;

        $devis = $this->getDoctrine()->getRepository(Devis::class)->findBy(['affaire' => $affaireId]);
        $factures = $this->getDoctrine()->getRepository(Facture::class)->findBy(['affaire' => $affaireId]);
        $commandes = $this->getDoctrine()->getRepository(Commande::class)->findBy(['affaire' => $affaireId]);

        if (empty($devis[0]) && empty($factures[0]) && empty($commandes[0])) {
            $status = false;
        }

        return new JsonResponse(['st' => $status]);
    }

    /**
     * Fonction pour supprimer une affaire
     * @param $affaireId
     * @return RedirectResponse
     */
    public function deleteAction($affaireId)
    {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $em = $this->getDoctrine()->getManager();
        $em->remove($affaire);
        $em->flush();
        $this->addFlash('success', 'L\'affaire à bien été supprimé.');

        return $this->redirectToRoute('sudalys_gestion_home');
    }

    /**
     * Fonction qui va permettre, à partir d'un devis orphelin accepté, de créer une nouvelle affaire
     * @param Request $request
     * @param null $devisId
     * @param null $createCmd
     * @return JsonResponse|Response
     */
    public function createFromDevisAction(Request $request, $devisId, $createCmd = null)
    {
        // on instancie une nouvelle affaire
        $affaire = new Affaire();
        $affaire->setNumeroAffaire($this->createNumber->numeroAffaire());

        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId);
        $affaire->setOrganisme($devis->getCLient());
        $affaire->setAdresseLivraison($devis->getAdresseLivraison());
        $affaire->setAdresseFacturation($devis->getAdresseFacturation());
        $affaire->setDateDebut(new \DateTime('today'));
        $form = $this->createForm(CreateAffaireDevisType::class, $affaire);
        $form->handleRequest($request);
        $status = "error";
        $message = "";

        // on récupère une requete post depuis notre vue
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                // Récupère le corps de la requête
                $data = $request->request->get('create_affaire');
                unset($data['_token']);
                if ($data['plafond'] == "") {
                    $data['plafond'] = null;
                } else {
                    $data['plafond'] = (float)$data['plafond'];
                }
                foreach($data as $key => $value) {
                    if ($key == 'adresseLivraison' || $key == 'adresseFacturation' || $key == 'organisme') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                            $affaire->$temp($organisme);
                        }
                    } else {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $affaire->$temp($value);
                        }
                    }
                }
                $affaire->setRespectPlafond(true);

                // on transforme notre objet en entité doctrine grâce au Manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($affaire);

                // on set l'etat du devis en accepté et on l'attribue de facto à cette nouvelle affaire
                if ($devis->getEtat() == 3) {
                    return new JsonResponse([$status => 'Le devis a était classé sans suite. La génération de l\'affaire est donc impossible.']);
                } else {
                    $devis->setAffaire($affaire);
                    $devis->setEtat(2);
                    $articleDevis = $devis->getArticleDevis();
                    foreach ($articleDevis as $key => $value) {
                        if (!$this->getDoctrine()->getRepository(ArticleAffaire::class)->findBy(['article' => $articleDevis[$key]->getArticle(), 'affaire' => $affaire])) {
                            $articleAffaire = new ArticleAffaire();
                            $articleAffaire->setAffaire($affaire);
                            $articleAffaire->setArticle($articleDevis[$key]->getArticle());
                            $em->persist($articleAffaire);
                        }
                    }
                    $id = $affaire->getId();
                    $nom = $affaire->getDesignationAffaire();
                    $numero = $affaire->getNumeroAffaire();
                    $user = $devis->getUser()->getNom();
                    $em->persist($devis);

                    $commande = new Commande();
                    // on définit les différentes propriétés de la nouvelle commande à partir des infos du devis
                    $commande->setAffaire($devis->getAffaire());
                    $commande->setMentions($devis->getLibelle() . ' [importé depuis le devis]');
                    $commande->setCLient($devis->getClient());
                    $commande->setTVA($devis->getTVA());
                    $commande->setForfaitaire($devis->getForfaitaire());
                    $commande->setAdresseFacturation($devis->getAdresseFacturation());
                    $commande->setAdresseLivraison($devis->getAdresseLivraison());
                    $commande->setRemise($devis->getRemise());
                    $commande->setUser($devis->getUser());
                    $commande->setClient($devis->getClient());
                    $commande->setDateCommande(new \DateTime());
                    $commande->setEtat(0);
                    $commande->setNumeroCommande($this->createNumber->numeroCommande($affaire->getId()));
                    $commande->setTotalHT($devis->getTotalHT());
                    $commande->setMontantTotalTTC($devis->getTotalTTC());
                    $commande->setActiveTVA($devis->getActiveTVA());
                    $commande->setDevis($devis);
                    $commande->setContact($devis->getContact());
                    // on persist cet objet en entité doctrine via le manager de l'ORM
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($commande);

                    // la commande venant d'etre créée en entité Doctrine, ce dernier lui a attribué un id que l'on récupère
                    $commandeId = $commande->getId();
                    // on récupère tous les articles du devis venant d'etre accepté
                    $articlesDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                        'devis' => $devis->getId()
                    ]);

                    // pour chaque article du devis accepté, on créé un nouvel article de commande
                    for ($i = 0; $i < count($articlesDevis); $i++) {
                        $articleCommande = new ArticleCommande();

                        $articleCommande->setCommande($this->getDoctrine()->getRepository(Commande::class)->find($commandeId));
                        $articleCommande->setArticleDevis($articlesDevis[$i]);

                        $checkExisting = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                            'commande' => $commandeId
                        ]);
                        if ($checkExisting) {
                            for ($j = 0; $j < count($checkExisting); $j++) {
                                $em->remove($checkExisting[$j]);
                            }
                        }
                        if ($articlesDevis[$i]->getOverrideMontantUnit() != null) {
                            $articleCommande->setOverrideMontantUnit($articlesDevis[$i]->getOverrideMontantUnit());
                        }
                        $articleCommande->setQuantite($articlesDevis[$i]->getQuantite());
                        $articleCommande->setMontantTotalHT($articlesDevis[$i]->getMontantTotalHT());

                        $em->persist($articleCommande);
                    }

                    try {
                        $em->flush();
                        $status = "success";
                        $message = "Opération enregistrée";
                    } catch (\Exception $e) {
                        $error = $this->exceptionHandler->getException($e);
                        $message = $this->exceptionHandler->exceptionHandler($error, 'affaire');
                    }

                    // on renvoie à la vue une réponse Json qui contient plusieurs informations concernant la nouvelle affaire afin de pouvoir modifier la vue sans la recharger
                    return new JsonResponse([$status => $message, 'id' => $id, 'nom' => $nom, 'numero' => $numero, 'user' => $user]);
                }
            } else {

                return $this->render('GestionBundle:Default:createAffaire.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }
    }

    /**
     * Méthode pour modifier une affaire
     * Uniquement pour le super sudalys
     * @param $affaireId
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function modifyAction($affaireId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $form = $this->createForm(CreateAffaireType::class, $affaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('sudalys_gestion_home'));
        }

        // permet de générer la vue de création d'affaire et d'y passer en variable le formulaire
        return $this->render('GestionBundle:Default:createAffaire.html.twig', [
            'form' => $form->createView()
        ]);
    }

    private function generateGraphForm($dateDebut)
    {
        return $this->createFormBuilder() // Créetion du formulaire
        ->add('dateDebut', DateType::class, [
            'label' => 'Date début',
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'attr' => [
                'type' => 'date',
                'data-dateStart' => $dateDebut
            ],
            'required' => true
        ])
            ->add('dateFin', DateType::class, [
                'label' => 'Date fin',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'type' => 'date'
                ],
                'required' => true
            ])
            ->getForm();
    }

    /**
     * Méthode pour afficher la gestion d'une affaire
     * @param $affaireId
     * @return Response
     */
    public function gestionAction($affaireId)
    {
        $chartService = new ChartsService();
        $dateService = $this->get('date.generate'); // Récupération du service de génération des dates
        // Initialisation des variables nécessaire à la gestion des charges, de la production et la marge par mois
        $months = [];
        $years = [];
        /**
         * Partie générale avec le informations générales de l'affaire
         */
        $user = $this->getUser(); // Récupération de l'utilisateur
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId); // Récupération de l'adresse cible
        // Vérification de l'utilisateur qui accède à la page, si il n'ap pas le bon role ou que l'affaire ne lui est pas attribué il ne peut pas y accéder
        if (!in_array('ROLE_SUPER_SUDALYS', $user->getRoles())) {
            $flagNotAllowed = true; // Flag permettant de vérifier si l'utilisateur est autorisé à visionner ou pas la page
            foreach ($affaire->getAttributions()->toArray() as $attribution) {
                if ($user->getId() == $attribution->getUser()->getId()) {
                    $flagNotAllowed = false;
                }
            }
            if ($flagNotAllowed) { // Si le flag est true alors on génére un mesage flash et redirige vers la page d'accueil
                $this->addFlash('warning', "Vous n'avez pas les droits nécessaire pour accéder à cette page.");
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        }
        /**
         * Formulaire de requête de date pour le graphique
         */
        $dateDebut = $affaire->getDateDebut()->format('Y-m-d'); // Récupération de la date de début en string
        $dateMemory = clone $affaire->getDateDebut(); // Mise en mémoire de la date de début
        $form = $this->generateGraphForm($dateDebut); // Génération du formulaire de sélection de date pour les requêtes de graphique de ROP par mois
        /**
         * Retour partie générale
         */
        $dates = $dateService->generateListOfMonthsAndYearFromDateDebut($affaire->getDateDebut()); // Récupération de la liste des dates depuis la date de début de l'affaire
        $lengthOfDates = count($dates); // Calcul de la length de l'array date reçus
        $monthToGet = 12; // Le nombre de mois maximum et minimum à afficher
        if ($monthToGet < $lengthOfDates) { // Si le nombres de mois existant dans l'array de dates et supérieur au nombre de mois max
            for ($i = ($lengthOfDates - $monthToGet); $i < $lengthOfDates; $i++) { // Dans une boucle for on récupère les 12 derniers mois de l'array
                array_push($months, intval($dates[$i]['monthNumber'])); // On insère dans l'array months le numéro de mois concaténé avec le numéro d'année
                array_push($years, intval($dates[$i]['year'])); // On insère le numéro d'année dans l'array years
            }
        } else { // Sinon
            $diff = $monthToGet - $lengthOfDates; // On calcul la différence entre le nombre de mois fixé
            $datesToAdd = $dateService->generateListOfMonthsAndYearFromDate($dates[($lengthOfDates - 1)]['dateTime'], $diff); // Puis on génère une lite de date égale à la différence
            $dates = array_merge($dates, $datesToAdd); // Puis on fusionne l'array de date d'origin et le nouvel array dans l'array de base
            foreach ($dates as $date) { // Puis on peuple les variables months et years
                array_push($months, intval($date['monthNumber']));
                array_push($years, intval($date['year']));
            }
        }
        // Pour chaque items nécessaire à la création du graphique, on copie l'array month
        $prodPerMonth = $months;
        $prestaPerMonth = $months;
        $materPerMonth = $months;
        $estimPrestaPerMonth = $months;
        $estimFournPerMonth = $months;
        $listedClients = []; // Array qui va comporter la liste des clients cible des devis de l'affaire
        foreach ($affaire->getDevis() as $devis) { // Pour chaque devis, si le client n'est pas déja présent dans la liste on l'insère dans la liste
            $clientId = $devis->getClient()->getId();
            $client = $devis->getClient();
            if (empty($listedClients[$clientId])) { // Dont l'index est l'id de l'organisme en base
                $listedClients[$clientId]['client'] = $client;
            }
        }
        /**
         * Partie production
         * Avec les factures, commandes, situations et avancement
         */
        $factures = $affaire->getFactures()->toArray(); // Récupération des factures de l'affaire
        $situations = []; // Initialisation de l'array vide de la liste des attachements
        $commandes = $affaire->getCommandes()->toArray(); // Récupération des commandes reçus de l'affaire
        foreach ($commandes as $key => $commande) { // pour chaque commande on récupère la liste des situations rattaché
            if ($commande->getEtat() != 0) { // Si l'état de la commande n'est pas pré-saisie
                $attachements = $commande->getAttachements(); // Récupération de la liste des attachements
                if ($attachements) { // Si il y a des attachements
                    foreach ($attachements as $attachement) { // Pour chaque attachement
                        if ($attachement->getEtat() != 2) { // Si l'état de l'attachement n'est pas abandonné
                            array_push($situations, $attachement); // On l'insère dans la liste des attachements
                        }
                    }
                }
            } else {
                unset($commandes[$key]); // Si la commande est à l'état de pré-saisie on supprime la commande de la liste
            }
        }
        $commandes = array_values($commandes); // Reset des index de la liste
        // Trie croissant par date des commandes, situations et factures
        $this->orderByDateFacture($factures);
        $this->orderByDateCommande($commandes);
        $this->orderByDateSituation($situations);
        // Calcul des totaux cumulé des commandes, situations et facture
        $totalCumulFacturation = $this->totalCumulAll($factures);
        $totalCumulSituation = $this->totalCumulAll($situations);
        $totalCumulCommande = $this->totalCumulAll($commandes);
        // Calcul des totaux pour le mois en cour des commandes, situations et facture
        $totalFacturationThisMonth = $this->getTotalFacturationThisMonth($factures);
        $totalSituationThisMonth = $this->getTotalSituationThisMonth($situations);
        $totalCommandeThisMonth = $this->getTotalCommandeThisMonth($commandes);
        // Calcul des totaux pour le mois précédent des commandes, situations et facture
        $totalFacturationLastMonth = $this->getTotalFacturationLastMonth($factures);
        $totalSituationLastMonth = $this->getTotalSituationLastMonth($situations);
        $totalCommandeLastMonth = $this->getTotalCommandeLastMonth($commandes);
        // Récupération des avancements et calcul des totaux pour le mois en cours, le mois précédent et cumulé
        $avancements = $affaire->getAvancement()->toArray();
        $thisAvancement = $this->getThisMonthAvancement($avancements);
        $lastAvancement = $this->getLastMonthAvancement($avancements);
        $totalAvancement = 0.00;
        foreach ($avancements as $avancement) {
            $totalAvancement += $avancement->getAvancement();
        }
        // Création du total des avancements en montant HT trié par mois
        $chartService->setPerMonth($prodPerMonth, $chartService->getTempTot($prodPerMonth, $avancements, 'date'));
        /**
         * Partie charges prestations
         */
        // EstimationsPrestataire
        $estimationPrestataire = $affaire->getEstimationPrestation()->toArray(); // Récupèration de la liste des estimations prestataire pour l'affaire
        $totalEstimCumul = $this->getTotalEstimCumul($estimationPrestataire); // Calcul du total des estimations cumulé
        $this->orderByDateEstimation($estimationPrestataire); // Trié des estimation par date croissante
        $totalEstimThis = $this->getTotalEstimationThisMonth($estimationPrestataire); // Calcul du total des estimations du mois en cour
        $totalEstimLast = $this->getTotalEstimLastMonth($estimationPrestataire); // Calcul du total des estimations du mois passé
        $chartService->setPerMonth($estimPrestaPerMonth, $chartService->getTempTotEstim($estimPrestaPerMonth, $estimationPrestataire)); // Création des totaux d'estimations par mois
        // Factures prestations
        $facturePrestation = $affaire->getFacturePrestataire()->toArray(); // Récupération de la liste des factures de prestataire
        $this->orderByDateFactureRecus($facturePrestation); // Trie par date croissante
        // Initialisation des liste de factures par état et de leur totaux
        $facturesPrestaSaisies = [];
        $totalFacturesPrestaSaisies = 0;
        $facturesPrestaValid = [];
        $totalFacturesPrestaValid = 0;
        $facturesPrestaRegle = [];
        $totalFacturesPrestaRegle = 0;
        foreach ($facturePrestation as $prestation) { // Pour chaque facture de prestation
            $etat = $prestation->getEtat(); // Récupération de l'état
            $validation = $prestation->getValidation(); // Récupération de la validate
            if ($validation == 0) { // Si la facture n'est pas validé
                array_push($facturesPrestaSaisies, $prestation); // Insertion dans la liste des factures saisies non validées
                $totalFacturesPrestaSaisies += $prestation->getTotalHT(); // Ajout au total des factures saisies non validées
            } else if ($etat == 0 && $validation == 1) { // Si la facture est validé et non réglé
                array_push($facturesPrestaValid, $prestation); // Insertion dans la liste des factures validées
                $totalFacturesPrestaValid += $prestation->getTotalHT(); // Ajout au total des factures validées
            } else if ($etat >= 1 && $validation == 1) { // SI la facture est valide et réglé
                array_push($facturesPrestaRegle, $prestation); // Insertion dans la liste des factures réglées
                $totalFacturesPrestaRegle += $prestation->getTotalHT(); // Ajout au total des factures réglées
            }
        }
        $chartService->setPerMonth($prestaPerMonth, $chartService->getTempTotFacture($prestaPerMonth, $facturePrestation, 'dateSaisie')); // Création des totaux de factures de prestation par mois pour le graphique
        $totalFacturesPrestaSaisiesThis = $this->getTotalFactureRecusThisMonth($facturesPrestaSaisies, 'saisie'); // Calcul du total des factures saisies du mois en cours
        $totalFacturesPrestaSaisiesLast = $this->getTotalFactureRecusLastMonth($facturesPrestaSaisies, 'saisie'); // Calcul du total des factures saisies du mois précédent
        $totalFacturesPrestaValidThis = $this->getTotalFactureRecusThisMonth($facturesPrestaValid); // Calcul du total des factures validées du mois en cours
        $totalFacturesPrestaValidLast = $this->getTotalFactureRecusLastMonth($facturesPrestaValid); // Calcul du total des factures validées du mois précédent
        $totalFacturesPrestaRegleThis = $this->getTotalFactureRecusThisMonth($facturesPrestaRegle); // Calcul du total des factures réglées du mois en cours
        $totalFacturesPrestaRegleLast = $this->getTotalFactureRecusLastMonth($facturesPrestaRegle); // Calcul du total des factures réglées du mois précédent
        $commandesPrestataire = $affaire->getCommandePrestataire()->toArray(); // Récupération des commandes de prestations de l'affaire
        $flagToChange = false; // Flag indicant si il y a eu du changement dans la liste ou non
        $totalCumulCommandePresta = 0; // Initialisation de la variable de total des commandes de prestations
        foreach ($commandesPrestataire as $key => $commande) { // Pour chaque commande de prestation
            if ($commande->getValidation() == 2) { // Si la commande est annulé
                unset($commandesPrestataire[$key]); // On retire la commande de la liste
                $flagToChange = true; // on indique qu'il y a eu un changement
            } else { // Sinon on ajoute le montant HT au total des commandes
                $totalCumulCommandePresta += $commande->getTotalHt();
            }
        }
        if ($flagToChange) { // Si il y a eu un changement
            $commandesPrestataire = array_values($commandesPrestataire); // on reset l'index de la liste
        }
        /**
         * Partie charges matériels
         */
        // EstimationsFournisseur
        $estimationFournisseur = $affaire->getEstimationFournisseur()->toArray(); // Récupération de la liste des estimation de matériels
        $totalEstimFournCumul = $this->getTotalEstimCumul($estimationFournisseur); // Calcul du total
        $this->orderByDateEstimation($estimationFournisseur); // Trie par date croissante
        $totalEstimFournThis = $this->getTotalEstimationThisMonth($estimationFournisseur); // Calcul du total des estimations pour le mois en cours
        $totalEstimFournLast = $this->getTotalEstimLastMonth($estimationFournisseur); // Calcul du total des estimations pour le mois précédent
        $chartService->setPerMonth($estimFournPerMonth, $chartService->getTempTotEstim($estimFournPerMonth, $estimationFournisseur)); // Création de la liste des totaux par mois des estimation de matériels pour le graphique
        $factureMateriel = $affaire->getFactureFournisseur()->toArray(); // Récupération de la liste des factures de matériels de l'affaire
        $this->orderByDateFactureRecus($factureMateriel); // Trie par date croissante
        // Initialisation des liste de factures par état et de leur totaux
        $facturesMaterielSaisies = [];
        $totalFacturesMaterielSaisies = 0;
        $facturesMaterielValid = [];
        $totalFacturesMaterielValid = 0;
        $facturesMaterielRegle = [];
        $totalFacturesMaterielRegle = 0;
        foreach ($factureMateriel as $materiel) { // Pour chaque facture
            $etat = $materiel->getEtat(); // on récupère son état
            $validation = $materiel->getValidation(); // On récupère sa validation
            if ($validation == 0) { // Si la facture n'est pas validé
                array_push($facturesMaterielSaisies, $materiel); // Insertion dans la liste des factures saisies non validées
                $totalFacturesMaterielSaisies += $materiel->getTotalHT(); // Ajout au total des factures saisies non validées
            } else if ($etat == 0 && $validation == 1) { // Si la facture est validé et non réglé
                array_push($facturesMaterielValid, $materiel); // Insertion dans la liste des factures validées
                $totalFacturesMaterielValid += $materiel->getTotalHT(); // Ajout au total des factures validées
            } else if ($etat >= 1 && $validation == 1) { // SI la facture est valide et réglé
                array_push($facturesMaterielRegle, $materiel); // Insertion dans la liste des factures réglées
                $totalFacturesMaterielRegle += $materiel->getTotalHT(); // Ajout au total des factures réglées
            }
        }
        $chartService->setPerMonth($materPerMonth, $chartService->getTempTotFacture($materPerMonth, $factureMateriel, 'dateSaisie')); // Création de la liste des totaux des factures par mois pour le graphique
        $totalFacturesMaterielSaisiesThis = $this->getTotalFactureRecusThisMonth($facturesMaterielSaisies, 'saisie'); // Calcul du total des factures saisies du mois en cours
        $totalFacturesMaterielSaisiesLast = $this->getTotalFactureRecusLastMonth($facturesMaterielSaisies, 'saisie'); // Calcul du total des factures saisies du mois précédent
        $totalFacturesMaterielValidThis = $this->getTotalFactureRecusThisMonth($facturesMaterielValid); // Calcul du total des factures validées du mois en cours
        $totalFacturesMaterielValidLast = $this->getTotalFactureRecusLastMonth($facturesMaterielValid); // Calcul du total des factures validées du mois précédent
        $totalFacturesMaterielRegleThis = $this->getTotalFactureRecusThisMonth($facturesMaterielRegle); // Calcul du total des factures réglées du mois en cours
        $totalFacturesMaterielRegleLast = $this->getTotalFactureRecusLastMonth($facturesMaterielRegle); // Calcul du total des factures réglées du mois précédent
        $commandesFournisseur = $affaire->getCommandeFournisseur ()->toArray(); // Récupération de la liste des commande de matériels
        $flagToChange = false; // Flag indicant si il y a eu du changement dans la liste ou non
        $totalCumulCommandeFourn = 0; // Initialisation de la variable de total des commandes de
        foreach ($commandesFournisseur  as $key => $commande) { // Pour chaque commande
            if ($commande->getValidation() == 2) { // Si la commande est abandonné
                unset($commandesFournisseur [$key]); // Retrait de la liste
                $flagToChange = true; // Indication qu'il y a eu un changement dans la liste
            } else { // Sinon
                $totalCumulCommandeFourn += $commande->getTotalHt(); // On ajout le total HT au total des commandes
            }
        }
        if ($flagToChange) { // Si il y a eu du changement
            $commandesFournisseur  = array_values($commandesFournisseur); // Reset des index de la liste
        }
        /**
         * Génération du graphique
         */
        // On récupère tous les totaux par mois de chaque item, puis on génère l'array selon le format fixé par google chart
        $arrayToDataTable = $chartService->arrayToTableForResultOP($materPerMonth, $prestaPerMonth, $estimPrestaPerMonth, $estimFournPerMonth, $prodPerMonth, $months, $years);
        $chartEvoROP = $chartService->generateChartEvoROPAffaire($arrayToDataTable);
        $affaire->setDateDebut($dateMemory);
        // Render de la vue
        return $this->render('GestionBundle:Default:manageAffaire.html.twig', [
            'today' => $this->today,
            'last' => $this->last,
            'affaire' => $affaire,
            'organismesClients' => $listedClients,
            'situations' => $situations,
            'factures' => $factures,
            'commandes' => $commandes,
            'totalFacturationThisMonth' => $totalFacturationThisMonth,
            'totalSituationThisMonth' => $totalSituationThisMonth,
            'totalCommandeThisMonth' => $totalCommandeThisMonth,
            'totalFacturationLastMonth' => $totalFacturationLastMonth,
            'totalSituationLastMonth' => $totalSituationLastMonth,
            'totalCommandeLastMonth' => $totalCommandeLastMonth,
            'totalCumulFacturation' => $totalCumulFacturation,
            'totalCumulSituation' => $totalCumulSituation,
            'totalCumulCommande' => $totalCumulCommande,
            'totalAvancement' => $totalAvancement,
            'thisAvancement' => $thisAvancement,
            'lastAvancement' => $lastAvancement,
            'facturesPrestaSaisies' => $facturesPrestaSaisies,
            'totalFacturesPrestaSaisies' => $totalFacturesPrestaSaisies,
            'totalFacturesPrestaSaisiesLast' => $totalFacturesPrestaSaisiesLast,
            'totalFacturesPrestaSaisiesThis' => $totalFacturesPrestaSaisiesThis,
            'facturesPrestaValid' => $facturesPrestaValid,
            'totalFacturesPrestaValid' => $totalFacturesPrestaValid,
            'totalFacturesPrestaValidThis' => $totalFacturesPrestaValidThis,
            'totalFacturesPrestaValidLast' => $totalFacturesPrestaValidLast,
            'facturesPrestaRegle' => $facturesPrestaRegle,
            'totalFacturesPrestaRegle' => $totalFacturesPrestaRegle,
            'totalFacturesPrestaRegleThis' => $totalFacturesPrestaRegleThis,
            'totalFacturesPrestaRegleLast' => $totalFacturesPrestaRegleLast,
            'facturesMaterielSaisies' => $facturesMaterielSaisies,
            'totalFacturesMaterielSaisies' => $totalFacturesMaterielSaisies,
            'totalFacturesMaterielSaisiesLast' => $totalFacturesMaterielSaisiesLast,
            'totalFacturesMaterielSaisiesThis' => $totalFacturesMaterielSaisiesThis,
            'facturesMaterielValid' => $facturesMaterielValid,
            'totalFacturesMaterielValid' => $totalFacturesMaterielValid,
            'totalFacturesMaterielValidThis' => $totalFacturesMaterielValidThis,
            'totalFacturesMaterielValidLast' => $totalFacturesMaterielValidLast,
            'facturesMaterielRegle' => $facturesMaterielRegle,
            'totalFacturesMaterielRegle' => $totalFacturesMaterielRegle,
            'totalFacturesMaterielRegleThis' => $totalFacturesMaterielRegleThis,
            'totalFacturesMaterielRegleLast' => $totalFacturesMaterielRegleLast,
            'chartEvoROP' => $chartEvoROP,
            'commandesPrestataire' => $commandesPrestataire,
            'totalCumulCommandePresta' => $totalCumulCommandePresta,
            'commandesFournisseur' => $commandesFournisseur,
            'totalCumulCommandeFourn' => $totalCumulCommandeFourn,
            'estimationPrestataire' => $estimationPrestataire,
            'totalEstimThis' => $totalEstimThis,
            'totalEstimLast' => $totalEstimLast,
            'totalEstimCumul' => $totalEstimCumul,
            'estimationFournisseur' => $estimationFournisseur,
            'totalEstimFournThis' => $totalEstimFournThis,
            'totalEstimFournLast' => $totalEstimFournLast,
            'totalEstimFournCumul' => $totalEstimFournCumul,
            'formGraph' => $form->createView()
        ]);
    }

    /**
     * Méthode pour envoyer la liste des commandes d'une affaire pour une commande fournisseur
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse
     */
    public function getCommandeListAction(Request $request, $affaireId)
    {
        $code = 400;
        $status = 'error';
        $data = [];

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                $status = 'success';
                $code = 200;
                $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
                $cmd = $this->getDoctrine()->getRepository(Commande::class)->findBy(['affaire' => $affaire]);
                foreach($cmd as $key => $commande) {
                    if ($commande->getEtat() == 0 || $commande->getEtat() == 3) {
                        $data[$key] = ['numeroCommande' => $commande->getNumeroCommande(), 'id' => $commande->getId()];
                    }
                }
                $data = array_values($data);
            }
        }

        return new JsonResponse(['status' => $status,  'data' => $data], $code);
    }

    /**
     * Méthode pour enregistrer les avancements d'une affaire et de ses commandes
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse
     */
    public function setAvancementAction(Request $request, $affaireId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $today = new \DateTime('today');
                $em = $this->getDoctrine()->getManager();
                $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
                $avancementAffaire = $this->getDoctrine()->getRepository(AvancementAffaire::class)->findOneBy(['affaire' => $affaire], ['date' => 'DESC']);

                if ($avancementAffaire) {
                    if (intval($avancementAffaire->getDate()->format('m')) != intval($this->month) && intval($avancementAffaire->getDate()->format('Y')) == intval($this->year)) {
                        $avancementAffaire = new AvancementAffaire();
                        $avancementAffaire->setDate($this->today);
                        $avancementAffaire->setDateSaisie($today);
                    }
                } else {
                    $avancementAffaire = new AvancementAffaire();
                    $avancementAffaire->setDate($this->today);
                    $avancementAffaire->setAffaire($affaire);
                }

                $avancementAffaire->setDateSaisie($today);
                $avancementAffaire->setAvancement($_POST['affaire']['avancement']);
                $avancementAffaire->setTotalHT((float)$_POST['affaire']['totalHT']);
                $em->persist($avancementAffaire);

                foreach ($_POST['commande'] as $avancement) {
                    $commande = $this->getDoctrine()->getRepository(Commande::class)->find($avancement['commande']);
                    $avancementCommande = $this->getDoctrine()->getRepository(AvancementCommande::class)->findOneBy(['commande' => $commande], ['date' => 'DESC']);
                    if ($avancementCommande) {
                        if ($avancementCommande->getDate()->format('m') != $this->month && $avancementCommande->getDate()->format('Y') != $this->year) {
                            $avancementCommande = new AvancementCommande();
                            $avancementCommande->setCommande($commande);
                            $avancementCommande->setDate($this->today);
                        }
                    } else {
                        $avancementCommande = new AvancementCommande();
                        $avancementCommande->setCommande($commande);
                        $avancementCommande->setDate($this->today);
                    }
                    $avancementCommande->setDateSaisie($today);
                    $avancementCommande->setTotalHT($avancement['totalHT']);
                    $avancementCommande->setReel($avancement['avancement']);
                    $em->persist($avancementCommande);
                }
                $code = 200;
                try {
                    $em->flush();
                    $message = "L'avancement des commandes et de l'affaire pour le mois de " . $this->today->format('M') . " a bien été enregistré";
                    $status = 'success';
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'avancement');
                }
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour enregistrer les commandes prestataire
     * @param Request $request
     * @return JsonResponse
     */
    public function setAvancementPrestataireAction(Request $request)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $today = new \DateTime('today');
                $em = $this->getDoctrine()->getManager();
                foreach ($_POST['commande'] as $avancement) {
                    $commande = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($avancement['commande']);
                    $avancementCommande = $this->getDoctrine()->getRepository(AvancementCommandePrestataire::class)->findOneBy(['commandePrestataire' => $commande], ['date' => 'DESC']);
                    if ($avancementCommande) {
                        if ($avancementCommande->getDate()->format('m') != $this->month && $avancementCommande->getDate()->format('Y') != $this->year) {
                            $avancementCommande = new AvancementCommandePrestataire();
                            $avancementCommande->setCommandePrestataire($commande);
                            $avancementCommande->setDate($this->today);
                        }
                    } else {
                        $avancementCommande = new AvancementCommandePrestataire();
                        $avancementCommande->setCommandePrestataire($commande);
                        $avancementCommande->setDate($this->today);
                    }
                    $avancementCommande->setDateSaisie($today);
                    $avancementCommande->setTotalHT($avancement['totalHT']);
                    $avancementCommande->setAvancement($avancement['avancement']);
                    $em->persist($avancementCommande);
                }
                $code = 200;
                try {
                    $em->flush();
                    $message = "L'avancement des commandes de prestation pour le mois de " . $this->today->format('M') . " a bien été enregistré";
                    $status = 'success';
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'avancement');
                }
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour enregistrer les commandes fournisseur
     * @param Request $request
     * @return JsonResponse
     */
    public function setAvancementFournisseurAction(Request $request)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $today = new \DateTime('today');
                $em = $this->getDoctrine()->getManager();foreach ($_POST['commande'] as $avancement) {
                    $commande = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($avancement['commande']);
                    $avancementCommande = $this->getDoctrine()->getRepository(AvancementCommandeFournisseur::class)->findOneBy(['commandeFournisseur' => $commande], ['date' => 'DESC']);
                    if ($avancementCommande) {
                        if ($avancementCommande->getDate()->format('m') != $this->month && $avancementCommande->getDate()->format('Y') != $this->year) {
                            $avancementCommande = new AvancementCommandeFournisseur();
                            $avancementCommande->setCommandeFournisseur($commande);
                            $avancementCommande->setDate($this->today);
                        }
                    } else {
                        $avancementCommande = new AvancementCommandeFournisseur();
                        $avancementCommande->setCommandeFournisseur($commande);
                        $avancementCommande->setDate($this->today);
                    }
                    $avancementCommande->setDateSaisie($today);
                    $avancementCommande->setTotalHT($avancement['totalHT']);
                    $avancementCommande->setAvancement($avancement['avancement']);
                    $em->persist($avancementCommande);
                }
                $code = 200;
                try {
                    $em->flush();
                    $message = "L'avancement des commandes de matériels pour le mois de " . $this->today->format('M') . " a bien été enregistré";
                    $status = 'success';
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'avancement');
                }
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    public function chartRequestAction($affaireId)
    {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $dateDebut = explode('/', $_POST['form']['dateDebut']);
        array_unshift($dateDebut, '01');
        $dateStartChart = new \DateTime(join('-', array_reverse($dateDebut)));
        $dateDebut = strtotime(join('-', array_reverse($dateDebut)));
        if ($_POST['form']['dateFin'] === "") {
            $tempDate = new \DateTime('today');
            $dateEndChart = clone $tempDate;
            $dateFin = strtotime($tempDate->format('Y-m-d'));
        } else {
            $dateFin = explode('/', $_POST['form']['dateFin']);
            array_unshift($dateFin, '01');
            $dateEndChart = new \DateTime(join('-', array_reverse($dateFin)));
            $dateFin = strtotime(date("Y-m-t", strtotime(join('-', array_reverse($dateFin)))));
        }

        $avancementAffaire = $affaire->getAvancement();
        foreach ($avancementAffaire as $key => $avancement) {
            $dateAvancement = strtotime($avancement->getDate()->format('Y-m-d'));
            if ($dateAvancement < $dateDebut || $dateAvancement > $dateFin) {
                unset($avancementAffaire[$key]);
            }
        }

        $facturePrestataire = $affaire->getFacturePrestataire();
        foreach ($facturePrestataire as $key => $facture) {
            $dateFacture = strtotime($facture->getDateSaisie()->format('Y-m-d'));
            if ($dateFacture < $dateDebut || $dateFacture > $dateFin) {
                unset($facturePrestataire[$key]);
            }
        }

        $factureFournisseur= $affaire->getFactureFournisseur();
        foreach ($factureFournisseur as $key => $facture) {
            $dateFacture = strtotime($facture->getDateSaisie()->format('Y-m-d'));
            if ($dateFacture < $dateDebut || $dateFacture > $dateFin) {
                unset($factureFournisseur[$key]);
            }
        }

        $estimationChargePresta = $affaire->getEstimationPrestation();
        foreach($estimationChargePresta as $key => $estimation) {
            $dateEstimation = strtotime($estimation->getDate()->format('Y-m-d'));
            if ($dateEstimation < $dateDebut || $dateEstimation > $dateFin) {
                unset($estimationChargePresta[$key]);
            }
        }

        $estimationChargeFourn = $affaire->getEstimationFournisseur();
        foreach($estimationChargeFourn as $key => $estimation) {
            $dateEstimation = strtotime($estimation->getDate()->format('Y-m-d'));
            if ($dateEstimation < $dateDebut || $dateEstimation > $dateFin) {
                unset($estimationChargeFourn[$key]);
            }
        }

        $dateService = $this->get('date.generate');
        $dates = $dateService->generateListOfMonthsAndYearFromStartAndEnd($dateStartChart, $dateEndChart);
        // Initialisation des variables nécessaire à la gestion des charges, de la production et la marge par mois
        $months = [];
        $years = [];
        foreach ($dates as $date) {
            array_push($months, intval($date['monthNumber']));
            array_push($years, intval($date['year']));
        }
        $prodPerMonth = $months;
        $prestaPerMonth = $months;
        $materPerMonth = $months;
        $estimPrestaPerMonth = $months;
        $estimFournPerMonth = $months;

        $chartService = $this->get('chart.generate');
        $chartService->setPerMonth($materPerMonth, $chartService->getTempTot($materPerMonth, $factureFournisseur, 'dateSaisie'));
        $chartService->setPerMonth($prodPerMonth, $chartService->getTempTot($prodPerMonth, $avancementAffaire, 'date'));
        $chartService->setPerMonth($prestaPerMonth, $chartService->getTempTot($prestaPerMonth, $facturePrestataire, 'dateSaisie'));
        $chartService->setPerMonth($estimPrestaPerMonth, $chartService->getTempTotEstim($estimPrestaPerMonth, $estimationChargePresta));
        $chartService->setPerMonth($estimFournPerMonth, $chartService->getTempTotEstim($estimFournPerMonth, $estimationChargeFourn));
        /**
         * Génération du graphique
         */
        $arrayToDataTable = $chartService->arrayToTableForResultOP($materPerMonth, $prestaPerMonth, $estimPrestaPerMonth, $estimFournPerMonth, $prodPerMonth, $months, $years);
        $chartEvoROP = $chartService->generateChartEvoROPAffaire($arrayToDataTable);
        // Retourne le template comportant le code du graphique
        return $this->render('GestionBundle:Default:chartEvoProd.html.twig', [
            'chartEvoROP' => $chartEvoROP
        ]);
    }
}