<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Services\GrhExploitationModel;
use PDO;
use PDOException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardDirectionController extends Controller
{
    /**
     * Méthode pour trier les facture par date de facturation dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateFacture(&$facture)
    {
        usort($facture, function($a, $b) {
            if (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) === strtotime($b->getDateFacture()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) < strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    /**
     * Méthode pour récupérer la liste des affaires
     * @param $args
     */
    private function getAffaireList($args, $affaireList = [])
    {
        if (is_array($args)) {
            foreach ($args as $key => $arg) {
                $affaire = $arg->getAffaire();
                if (!in_array($affaire, $affaireList)) {
                    array_push($affaireList, $affaire);
                }
            }
            return $affaireList;
        } else {
            $affaire = $args->getFacture()->getAffaire();
            if (!in_array($affaire, $affaireList)) {
                return   $args->getFacture()->getAffaire();
            }
        }
    }

    /**
     * Méthode pour trier les factures par date d'échéance dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateEcheance(&$facture)
    {
        usort($facture, function($a, $b) {
            if (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) === strtotime($b->getDateEcheance()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) < strtotime($b->getDateEcheance()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    /**
     * Méthode pour trier les avoirs par date dans l'ordre croissant
     * @param $avoirs
     */
    private function sortByDateAvoir($avoirs)
    {
        usort($avoirs, function($a, $b) {
            if (strtotime($a->getDate()->format('Y-m-d H:i:s')) === strtotime($b->getDate()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDate()->format('Y-m-d H:i:s')) < strtotime($b->getDate()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    public function showAdminAction()
    {
        try{
            // create a PostgreSQL database connection
            $conn = new GrhExploitationModel();
            $stmt = $conn->getAll();
            dump($stmt);
        }catch (\Exception $e){
            // report error message
            echo $e->getMessage();
        }
        die();
        return $this->render('GestionBundle:Default:dashboardAdmin.html.twig');
    }

    public function gestionAffairesAction()
    {
        $affaires = $this->getDoctrine()->getRepository(Affaire::class)->findAll();

    }
}