<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Avoir;
use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Facture;
use GestionBundle\Entity\FactureFournisseur;
use GestionBundle\Entity\FacturePrestataire;
use GestionBundle\Entity\InfoFournisseur;
use GestionBundle\Entity\Organisme;
use GestionBundle\Form\InfoFournisseurType;
use GestionBundle\Form\InfoPrestataireType;
use GestionBundle\Services\CheckDocumentPrestataireValidity;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\ManageFacturePayment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DashboardAdministratifController extends Controller
{
    public $exceptionHandler; // Propriété qui va contenir le service de gestio, des erreurs

    /**
     * DevisController constructor.
     */
    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler(); // invocation du service de gestion des erreurs au chargement de la classe
    }
    /**
     * Méthode pour trier les facture par date de facturation dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateFacture(&$facture)
    {
        if (!empty($facture[0])) {
            if ($facture[0]->getDateFacture()) {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) === strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) < strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            } else {
                usort($facture, function ($a, $b) {
                    if (strtotime($a->getDatePreSaisie()->format('Y-m-d H:i:s')) === strtotime($b->getDatePreSaisie()->format('Y-m-d H:i:s'))) {
                        return 0;
                    }
                    return (strtotime($a->getDatePreSaisie()->format('Y-m-d H:i:s')) < strtotime($b->getDatePreSaisie()->format('Y-m-d H:i:s'))) ? -1 : +1;
                });
            }
        }
    }

    /**
     * Méthode pour récupérer la liste des affaires
     * @param $args
     */
    private function getAffaireList($args, $affaireList = [])
    {
        if (is_array($args)) {
            foreach ($args as $key => $arg) {
                $affaire = $arg->getAffaire();
                if (!in_array($affaire, $affaireList)) {
                    array_push($affaireList, $affaire);
                }
            }
            return $affaireList;
        } else {
            $affaire = $args->getFacture()->getAffaire();
            if (!in_array($affaire, $affaireList)) {
                return   $args->getFacture()->getAffaire();
            }
        }
    }

    /**
     * Méthode pour trier les factures par date d'échéance dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateEcheance(&$facture)
    {
        if (!empty($facture[0])) {
            usort($facture, function ($a, $b) {
                if (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) === strtotime($b->getDateEcheance()->format('Y-m-d H:i:s'))) {
                    return 0;
                }
                return (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) < strtotime($b->getDateEcheance()->format('Y-m-d H:i:s'))) ? -1 : +1;
            });
        }
    }

    /**
     * Méthode pour trier les avoirs par date dans l'ordre croissant
     * @param $avoirs
     */
    private function sortByDateAvoir($avoirs)
    {
        if (!empty($avoir[0])) {
            usort($avoirs, function ($a, $b) {
                if (strtotime($a->getDate()->format('Y-m-d H:i:s')) === strtotime($b->getDate()->format('Y-m-d H:i:s'))) {
                    return 0;
                }
                return (strtotime($a->getDate()->format('Y-m-d H:i:s')) < strtotime($b->getDate()->format('Y-m-d H:i:s'))) ? -1 : +1;
            });
        }
    }

    private function checkFournisseurValidity($infoFournisseur)
    {
        $arrayOfKeys = ['RIB', 'RIBDocument'];
        foreach ($arrayOfKeys as $key) {
            $temp = 'get' . $key;
            if ($infoFournisseur->$temp() == false || $infoFournisseur->$temp() == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Méthode utilisé pour calculé le montant restant d'une commande de materiel
     * @param $existingFacture
     * @param $totalHT
     * @return int
     */
    private function ramainingTotalCommande($existingFacture, $totalHT)
    {
        if (!empty($existingFacture[0])) {
            $tempTotal = 0;
            foreach ($existingFacture as $exist) {
                if ($exist->getValidation() == 1 && $exist->getEtat() == 2) {
                    $tempTotal += $exist->getTotalHT();
                }
            }
            $totalHT -= $tempTotal;
        }

        return $totalHT;
    }

    /**
     * Méthode pour afficher le dashboard pour le service administratif
     * @return Response
     */
    public function showAction()
    {
        return $this->render('GestionBundle:Default:indexDashboardAdministratif.html.twig');
    }

    /**
     * Méthode pour afficher la gestion des ventes
     * @return Response
     */
    public function venteAction() {
        $doctrine = $this->getDoctrine();
        $facturePreSaisie = $doctrine->getRepository(Facture::class)->findBy(['etat' => 0]);
        $factureEnAttente = $doctrine->getRepository(Facture::class)->findBy(['etat' => 1]);
        $factureEnAttentePaiement = $doctrine->getRepository(Facture::class)->findBy(['etat' => 2]);
        $facturePaye = $doctrine->getRepository(Facture::class)->findBy(['etat' => 3]);
        $affaireListPayeFacture = $this->getAffaireList($facturePaye);

        $this->sortByDateFacture($facturePreSaisie);
        $affaireListPreSaisie = $this->getAffaireList($facturePreSaisie);
        $this->sortByDateEcheance($factureEnAttente);
        $affaireListEnattente = $this->getAffaireList($factureEnAttente);
        $this->sortByDateEcheance($factureEnAttentePaiement);
        $affaireListEnAttentePaiement = $this->getAffaireList($factureEnAttentePaiement);

        $checkPaymentDate = new ManageFacturePayment();
        $inLateFacture = $checkPaymentDate->checkPaymentDate($factureEnAttentePaiement);
        $affaireListInLateFacture = $this->getAffaireList($inLateFacture);

        $avoirs = $this->getDoctrine()->getRepository(Avoir::class)->findAll();
        $this->sortByDateAvoir($avoirs);

        $affaireListAvoir = [];
        foreach ($avoirs as $key => $avoir) {
            $temp = $this->getAffaireList($avoir, $affaireListAvoir);
            if ($temp) {
                array_push($affaireListAvoir, $this->getAffaireList($avoir, $affaireListAvoir));
            }
        }

        if ( !empty(count($inLateFacture) > 0) ) {
            $em = $doctrine->getManager();
            foreach ($inLateFacture as $key => $facture) {
                $em->persist($facture);
            }
            $em->flush();
        }

        return $this->render('GestionBundle:Default:dashboardAdministratifVente.html.twig', [
            'facturePreSaisie' => $facturePreSaisie,
            'affaireListPreSaisie' => $affaireListPreSaisie,
            'factureEnAttente' => $factureEnAttente,
            'affaireListEnAttente' => $affaireListEnattente,
            'factureEnAttentePaiement' => $factureEnAttentePaiement,
            'affaireListEnAttentePaiement' => $affaireListEnAttentePaiement,
            'inLateFacture' => $inLateFacture,
            'affaireListInLateFacture' => $affaireListInLateFacture,
            'facturePaye' => $facturePaye,
            'affaireListPayeFacture' => $affaireListPayeFacture,
            'avoirs' => $avoirs,
            'affaireListAvoir' => $affaireListAvoir
        ]);
    }

    public function fournisseurAction() {
        $fournisseurs = $this->getDoctrine()->getRepository(Organisme::class)->findBy(['type' => 0]);
        $notValidFournisseur = [];
        foreach($fournisseurs as $key => $fournisseur) {
            if ($fournisseur->getActif() == false) {
                unset($fournisseurs[$key]);
            } else {
                if ($fournisseur->getFournisseurValide() === false) {
                    array_push($notValidFournisseur, $fournisseur);
                    unset($fournisseurs[$key]);
                }
            }
        }
        $prestataires = $this->getDoctrine()->getRepository(Organisme::class)->findBy(['type' => 2]);
        $em = $this->getDoctrine()->getManager();
        $notValidPrestataire = [];
        foreach($prestataires as $key => $prestataire) {
            if ($prestataire->getActif() == false) {
                unset($prestataires[$key]);
            } else {
                if ($prestataire->getPrestataireValide() === false) {
                    array_push($notValidPrestataire, $prestataire);
                    unset($prestataires[$key]);
                }
            }
        }
        $hasChanged = false;
        foreach ($prestataires as $prestataire) {
            $infoprestataire = $prestataire->getInfoPrestataire();
            if ($infoprestataire->getIsValid() == true) {
                $checkValidity = new CheckDocumentPrestataireValidity($infoprestataire);
            }
            if (isset($checkValidity)) {
                if ($checkValidity->hasChanged) {
                    $hasChanged = true;
                    $infoprestataire->setIsValid(false);
                    $em->persist($infoprestataire);
                }
            }
        }
        if ($hasChanged === true) {
            $em->flush();
        }
        return $this->render('GestionBundle:Default:dashboardAdministratifFournisseur.html.twig', [
            'notValidFournisseur' => $notValidFournisseur,
            'fournisseurs' => $fournisseurs,
            'notValidPrestataire' => $notValidPrestataire,
            'prestataires' => $prestataires
        ]);
    }

    /**
     * Méthode pour afficher la gestion des achats
     * @return Response
     */
    public function achatAction()
    {
        $em = $this->getDoctrine()->getManager(); // Manageur d'entité
        /**
         * Commande founisseur
         */
        $flagCloture = 0; // Flag permétant d'indiqué si une commande a été clôturé
        $commandeFournisseurs = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findAll(); // Récupère la liste des commandes fournisseur
        foreach ($commandeFournisseurs as $commandeFournisseur) { // Pour chaque command
            if ($commandeFournisseur->getEtat() == 1) { // Si l'état de la commande est "ouverte"
                // On récupère les factures validé déja existante
                $existingFacture = $commandeFournisseur->getFactureFournisseur(); // Récupération de la liste des factures valide
                $totalHT = $this->ramainingTotalCommande($existingFacture, $commandeFournisseur->getTotalHT()); // Récupération du total des factures valide
                // Si le total retourné est à 0.0 (float)
                if ($totalHT === 0.0) {
                    $flagCloture++; // incrémentation du flag de clôture
                    $commandeFournisseur->setEtat(2); // Modification de l'état à cloturé
                    $em->persist($commandeFournisseur); // Persist de l'objet
                }
            }
        }
        if ($flagCloture > 0) { // SI le flag est supérieur à 0
            try {
                $em->flush(); // On insert la ou les modifications
                $commandeFournisseurs = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findAll(); // on récupère la liste des commandes à jour
            } catch (\Exception $e) {
                $message = '';
                if ($flagCloture == 1) {
                    $message = "Une erreur s'est produite lors de la clôture automatique d'une commande de matériels.";
                } else {
                    $message ="Une erreur s'est produite lors de la clôture automatique de $flagCloture commandes de matériels.";
                }
                $this->addFlash('warning', $message);
            }
        }
        // initialisation des variables array pour les différents états des commandes
        $commandeFournisseurAffaireEnAttente = [];
        $commandeFournisseurAffaireOuverte = [];
        $commandeFournisseurAffaireCloture = [];
        $commandeFournisseurNoAffaireEnAttente = [];
        $commandeFournisseurNoAffaireOuverte = [];
        $commandeFournisseurNoAffaireCloture = [];
        foreach ($commandeFournisseurs as $commandeFournisseur) { // Pour chaque commande
            if ($commandeFournisseur->getAffaire()) { // Si une commande a une affaire
                if ($commandeFournisseur->getValidation() == 1) { // Si la commande est validé
                    // Selon son état on l'assigne à un array différent
                    if ($commandeFournisseur->getEtat() == 0) {
                        array_push($commandeFournisseurAffaireEnAttente, $commandeFournisseur);
                    } else if ($commandeFournisseur->getEtat() == 1) {
                        array_push($commandeFournisseurAffaireOuverte, $commandeFournisseur);
                    } else if ($commandeFournisseur->getEtat() == 2) {
                        array_push($commandeFournisseurAffaireCloture, $commandeFournisseur);
                    }
                }
            } else { // Sinon
                if ($commandeFournisseur->getValidation() == 1) { // Si la commande est validé
                    // Selon son état on l'assigne à un array différent
                    if ($commandeFournisseur->getEtat() == 0) {
                        array_push($commandeFournisseurNoAffaireEnAttente, $commandeFournisseur);
                    } else if ($commandeFournisseur->getEtat() == 1) {
                        array_push($commandeFournisseurNoAffaireOuverte, $commandeFournisseur);
                    } else if ($commandeFournisseur->getEtat() == 2) {
                        array_push($commandeFournisseurNoAffaireCloture, $commandeFournisseur);
                    }
                }
            }
        }
         // Récupération des listes d'affaire pour chaque état de commande
        $listAffaireFournisseurEnAttente = $this->getAffaireList($commandeFournisseurAffaireEnAttente);
        $listAffaireFournisseurOuverte = $this->getAffaireList($commandeFournisseurAffaireOuverte);
        $listAffaireFournisseurCloture = $this->getAffaireList($commandeFournisseurAffaireCloture);
        /**
         * Commande prestataire
         */
        $flagCloture = 0; // Flag permétant d'indiqué si une commande a été clôturé
        $commandePrestataires = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findAll(); // Récupère la liste des commandes prestataire
        foreach ($commandePrestataires as $commandePrestataire) { // Pour chaque command
            if ($commandePrestataire->getEtat() == 1) { // Si l'état de la commande est "ouverte"
                // On récupère les factures validé déja existante
                $existingFacture = $commandePrestataire->getFacturePrestataire(); // Récupération de la liste des factures valide
                $totalHT = $this->ramainingTotalCommande($existingFacture, $commandePrestataire->getTotalHT()); // Récupération du total des factures valide
                // Si le total retourné est à 0.0 (float)
                dump($totalHT);
                if ($totalHT === 0.0) {
                    $flagCloture++; // incrémentation du flag de clôture
                    $commandePrestataire->setEtat(2); // Modification de l'état à cloturé
                    $em->persist($commandePrestataire); // Persist de l'objet
                }
            }
        }
//        die();
        if ($flagCloture > 0) { // SI le flag est supérieur à 0
            try {
                $em->flush(); // On insert la ou les modifications
                $commandePrestataires = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findAll(); // on récupère la liste des commandes à jour
            } catch (\Exception $e) {
                $message = '';
                if ($flagCloture == 1) {
                    $message = "Une erreur s'est produite lors de la clôture automatique d'une commande de prestataions.";
                } else {
                    $message ="Une erreur s'est produite lors de la clôture automatique de $flagCloture commandes de prestataions.";
                }
                $this->addFlash('warning', $message);
            }
        }
        // initialisation des variables array pour les différents états des commandes
        $commandePrestataireAffaireEnAttente = [];
        $commandePrestataireAffaireOuverte = [];
        $commandePrestataireAffaireCloture = [];
        $commandePrestataireNoAffaireEnAttente = [];
        $commandePrestataireNoAffaireOuverte = [];
        $commandePrestataireNoAffaireCloture = [];
        foreach ($commandePrestataires as $commandePrestataire) { // Pour chaque commande
            if ($commandePrestataire->getAffaire()) { // Si une commande a une affaire
                if ($commandePrestataire->getValidation() == 1) { // Si la commande est validé
                    // Selon son état on l'assigne à un array différent
                    if ($commandePrestataire->getEtat() == 0) {
                        array_push($commandePrestataireAffaireEnAttente, $commandePrestataire);
                    } else if ($commandePrestataire->getEtat() == 1) {
                        array_push($commandePrestataireAffaireOuverte, $commandePrestataire);
                    } else if ($commandePrestataire->getEtat() == 2) {
                        array_push($commandePrestataireAffaireCloture, $commandePrestataire);
                    }
                }
            } else { // Sinon
                if ($commandePrestataire->getValidation() == 1) { // Si la commande est validé
                    // Selon son état on l'assigne à un array différent
                    if ($commandePrestataire->getEtat() == 0) {
                        array_push($commandePrestataireNoAffaireEnAttente, $commandePrestataire);
                    } else if ($commandePrestataire->getEtat() == 1) {
                        array_push($commandePrestataireNoAffaireOuverte, $commandePrestataire);
                    } else if ($commandeFournisseur->getEtat() == 2) {
                        array_push($commandePrestataireNoAffaireCloture, $commandePrestataire);
                    }
                }
            }
        }
         // Récupération des listes d'affaire pour chaque état de commande
        $listAffairePrestataireEnAttente = $this->getAffaireList($commandePrestataireAffaireEnAttente);
        $listAffairePrestataireOuverte = $this->getAffaireList($commandePrestataireAffaireOuverte);
        $listAffairePrestataireCloture = $this->getAffaireList($commandePrestataireAffaireCloture);

        /**
         * Facture reçus
         */
        $facturesFournisseur = $this->getDoctrine()->getRepository(FactureFournisseur::class)->findAll(); // Récupération de toutes les facture fournisseur
        /**
         * TODO à mettre en place après création des entités pour les commandes et factures de prestataires
         */
        $facturesPrestataire = $this->getDoctrine()->getRepository(FacturePrestataire::class)->findAll(); // Récupération de toutes les facture prestataire
        $facturesRecus = array_merge($facturesFournisseur, $facturesPrestataire);
//        $facturesRecus = $facturesFournisseur;
        // Initialisation des arrays vide pour séparer les factures par états
        $factureAttenteValidation = [];
        $factureAttenteReglement = [];
        $factureRegle = [];
        $factureDebit = [];
        foreach ($facturesRecus as $facture) {
            if ($facture->getValidation() == 0) {
                array_push($factureAttenteValidation, $facture);
            } elseif ($facture->getValidation() == 1 && $facture->getEtat() == 0) {
                array_push($factureAttenteReglement, $facture);
            } elseif ($facture->getValidation() && $facture->getEtat() == 1) {
                array_push($factureRegle, $facture);
            } elseif ($facture->getValidation() && $facture->getEtat() == 2) {
                array_push($factureDebit, $facture);
            }
        }
        // Récupération des listes d'affaire pour chaque état de facture
        $listAffaireFactureAttenteValidation = $this->getAffaireList($factureAttenteValidation);
        $listAffaireFactureAttenteReglement = $this->getAffaireList($factureAttenteReglement);
        $listAffaireFactureRegle = $this->getAffaireList($factureRegle);
        $listAffaireFactureDebit = $this->getAffaireList($factureDebit);

        // Render de la vue
        return $this->render('GestionBundle:Default:dashboardAdministratifAchat.html.twig', [
            'commandeFournisseurAffaireEnAttente' => $commandeFournisseurAffaireEnAttente,
            'commandeFournisseurAffaireOuverte' => $commandeFournisseurAffaireOuverte,
            'commandeFournisseurAffaireCloture' => $commandeFournisseurAffaireCloture,
            'commandeFournisseurNoAffaireEnAttente' => $commandeFournisseurNoAffaireEnAttente,
            'commandeFournisseurNoAffaireOuverte' => $commandeFournisseurNoAffaireOuverte,
            'commandeFournisseurNoAffaireCloture' => $commandeFournisseurNoAffaireCloture,
            'listAffaireFournisseurEnAttente' => $listAffaireFournisseurEnAttente,
            'listAffaireFournisseurOuverte' => $listAffaireFournisseurOuverte,
            'listAffaireFournisseurCloture' => $listAffaireFournisseurCloture,
            'commandePrestataireAffaireEnAttente' => $commandePrestataireAffaireEnAttente,
            'commandePrestataireAffaireOuverte' => $commandePrestataireAffaireOuverte,
            'commandePrestataireAffaireCloture' => $commandePrestataireAffaireCloture,
            'commandePrestataireNoAffaireEnAttente' => $commandePrestataireNoAffaireEnAttente,
            'commandePrestataireNoAffaireOuverte' => $commandePrestataireNoAffaireOuverte,
            'commandePrestataireNoAffaireCloture' => $commandePrestataireNoAffaireCloture,
            'listAffairePrestataireEnAttente' => $listAffairePrestataireEnAttente,
            'listAffairePrestataireOuverte' => $listAffairePrestataireOuverte,
            'listAffairePrestataireCloture' => $listAffairePrestataireCloture,
            'factureAttenteValidation' => $factureAttenteValidation,
            'factureAttenteReglement' => $factureAttenteReglement,
            'factureRegle' => $factureRegle,
            'factureDebit' => $factureDebit,
            'listAffaireFactureAttenteValidation' => $listAffaireFactureAttenteValidation,
            'listAffaireFactureAttenteReglement' => $listAffaireFactureAttenteReglement,
            'listAffaireFactureRegle' => $listAffaireFactureRegle,
            'listAffaireFactureDebit' => $listAffaireFactureDebit
        ]);
    }

    /**
     * Méthode pour manager les documents nécessaires au référencement d'un fournisseur
     * @param Request $request
     * @param $orgaId
     * @return RedirectResponse|Response
     */
    public function manageInfoFournissseurAction(Request $request, $orgaId)
    {
        $fournisseur = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId); // Récupération du fournisseur
        $infoFournisseur = $fournisseur->getInfoFournisseur(); // Récupèration des infosFournisseur
        // initialisation de variables à null pour le rendu à la vue
        $ribDocuments = null;
        if ($infoFournisseur->getRIBDocument()) { // Vérification si l'infoFournisseur à un RIB
            $ribDocuments = $infoFournisseur->getRIBDocument(); // Si oui on l'assigne à la variable correspondante
        }
        // Génération du formulaire
        $form= $this->createForm(InfoFournisseurType::class, $infoFournisseur);
        $form->handleRequest($request); // Ajout de l'handler request

        if ($form->isSubmitted() && $form->isValid()) { // Si le formulaire est soumis et valide
            $em = $this->getDoctrine()->getManager(); // Mananger d'entité
            /** @var UploadedFile $file */ // Import de l'objet FileUploader
            $file = $request->files->get('gestionbundle_infofournisseur'); // Récupération des fichiers
            // TODO: passer les champs de documents en array et mettre en place un système d'ajout du dernier document uploadé
            if (!empty($file['RIBDocument'])) { // idem que pour le RIB
                $files = [];
                foreach($file['RIBDocument'] as $rib) {
                    $fileName = "";
                    $originalFilename = pathinfo($rib->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                    $fileName = $safeFilename . '-' . uniqid() . '.pdf';
                    $rib->move(
                        $this->getParameter('pdf_fournisseur_directory'), $fileName
                    );
                    array_push($files, $fileName);
                    if ($ribDocuments) {
                        $files = array_merge($ribDocuments, $files);
                    }
                }
                $infoFournisseur->setRIBDocument($files);
            } else {
                if($ribDocuments) {
                    $infoFournisseur->setRIBDocument($ribDocuments);
                }
            }
            $em->persist($infoFournisseur); // Persiste l'objet
            $status = 'warning';
            $message = '';
            try {
                $em->flush(); // insertion
                $message = 'Les informations du fournisseur on biet été mise à jours.';
                $status = 'success';
                $isValid = $this->checkFournisseurValidity($infoFournisseur); // Validation automatique du fournisseur si tous les documents son fournis
                if ($isValid) {
                    $fournisseur->setFournisseurValide($isValid);
                    $em->persist($fournisseur);
                    $em->flush();
                    $message .= ' Le fournisseur a été validé car tous les documents nécessaires on était fournis';
                }
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'fournisseur');
            }
            $this->addFlash($status, $message); // Message d'information du résultat du process
            return $this->redirectToRoute('sudalys_gestion_dashboard_administratif_fournisseur'); // Redirige vers la route souhaité
        }

        return $this->render('GestionBundle:Default:manageReferencementFournisseur.html.twig', [
            'form' => $form->createView(),
            'infoRounisseur' => $infoFournisseur
        ]);
    }

    /**
     * Méthode pour supprimer un document de la mémoire et de la table infoFournisseur
     * @param Request $request
     * @param $infoFournId
     * @return JsonResponse
     */
    public function removePdfFromInfoFournAction(Request $request, $infoFournId)
    {
        // initialisation des variable nécesssaire au renvoi du Json
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        if ($request->isXmlHttpRequest()) { // Vérification du type de requête et de la méthode
            if ($request->isMethod('POST')) {
                $data = $request->request;
                $dir =  $this->getParameter('pdf_fournisseur_directory');
                $em = $this->getDoctrine()->getManager(); // Manageur d'entité
                $infoFourn = $this->getDoctrine()->getRepository(InfoFournisseur::class)->find($infoFournId); // Récupération de l'infoFournisseur
                $filename = $dir . '/' . $data->get('toRemove'); // création du chemin du fichier à supprimé
                if (is_file($filename)) { // On vérifie que c'est bien un fichier
                    chmod($filename, 0777); // On donne les droits à l'application d'effectuer la suppression

                    if (unlink($filename)) { // Suppression du fichier
                        $code = 200;
                        $message = 'Fichier supprimer avec succés.';
                    } else { // en cas d'erreur on indique que la suppression n'a pas été possible
                        $code = 500;
                        $message = 'Une erreur s\'est produite et le fihsier n\'a pas pu être supprimer';
                    }
                } else { // Si le fichier n'est pas trouvé on l'indique
                    $code = 200;
                    $message = 'Le fichier demandé n\'existe pas.';
                }
                if ($code === 200) { // Si tous c'est bien passer on supprime de la BDD le nom du fichier dans l'infoFournisseur{
                    $infoFourn->setRIBDocument(null);
                    $em->persist($infoFourn);
                    try { // En cas de succès de la suppression
                        $status = 'success';
                        $em->flush();
                        $message = 'Le fichier a bien était supprimé';
                    } catch (\Exception $e) {
                        $message .= ' Mais une erreur s\'est produite et le fichier apparé toujours dans la liste. Contactez le support pour résoudre ce problème.';
                    }
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour éditer les infoPrestataire
     * @param Request $request
     * @param $orgaId
     * @return RedirectResponse|Response
     */
    public function manageInfoPrestataireAction(Request $request, $orgaId)
    {
        $arrayOfProps = ['RIBDocument', 'kbisDocument', 'responsabiliteProfessionnelDocument', 'attestationFiscaleDocument', 'attestationURSSAFDocument', 'listeSalarieEtrangerDocument', 'certificatCDAPHDocument', 'qualificationsProfessionnellesDocument',
            'recepisseDepotDeclarationDocument', 'autreDocument', 'registreProfessionelCertifInscriptionDocument', 'attestationImmatriculationDocument', 'fiscalEtrangerDocument', 'socialEtrangerDocument'];
        $prestataire = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $infoPrestataire = $prestataire->getInfoPrestataire();
        $arrayOfFiles = [];
        foreach ($arrayOfProps as $prop) {
            $temp = 'get' . ucfirst($prop);
            if (!empty($infoPrestataire->$temp()[0])) {
                $arrayOfFiles[$prop] = $infoPrestataire->$temp();
            } else {
                $arrayOfFiles[$prop] = null;
            }
        }
        $form = $this->createForm(InfoPrestataireType::class, $infoPrestataire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $request->files->get('gestionbundle_infoprestataire'); // Récupération des fichiers
            $em = $this->getDoctrine()->getManager();
            $message = '';
            $status = 'warning';
            foreach ($file as $key => $fichier) {
                $files = [];
                if (!empty($fichier[0])) {
                    if ($fichier[0] != "") {
                        foreach ($fichier as $fich) {
                            $originalFilename = pathinfo($fich->getClientOriginalName(), PATHINFO_FILENAME);
                            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                            $fileName = $safeFilename . '-' . uniqid() . '.pdf';
                            $fich->move(
                                $this->getParameter('pdf_prestataire_directory'), $fileName
                            );
                            array_push($files, $fileName);
                        }
                    }
                    if (!empty($arrayOfFiles[$key][0])) {
                        $files = array_merge($arrayOfFiles[$key], $files);
                    }
                } else {
                    $files = $arrayOfFiles[$key];
                }
                $temp = 'set' . ucfirst($key);
                $infoPrestataire->$temp($files);
            }
            if ($infoPrestataire->getRenouvellementDecembreResponsabiliteProfessionnel() == true) {
                $year = new \DateTime('today');
                $year = $year->format('Y');
                $date = new \DateTime($year . '-12-31');
                $infoPrestataire->setDateResponsabiliteProfessionnel($date);
            }
            $em->persist($infoPrestataire);
            try {
                $em->flush();
                $message = "Les informations du prestataire on bien été mise à jours.";
                $status = 'success';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'informations prestataire');
            }

            $this->addFlash($status, $message);

            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_fournisseur') . '#organismePrestataire');
        }

        return $this->render('GestionBundle:Default:manageReferencementPrestataire.html.twig', [
            'form' => $form->createView(),
            'infoPrestataire' => $infoPrestataire
        ]);
    }
}