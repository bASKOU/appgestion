<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\FactureFournisseur;
use GestionBundle\Entity\FacturePrestataire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DashboardChefProjetController extends Controller
{
    /**
     * Méthode pour trier les facture par date de facturation dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateFacture(&$facture)
    {
        usort($facture, function($a, $b) {
            if (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) === strtotime($b->getDateFacture()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateFacture()->format('Y-m-d H:i:s')) < strtotime($b->getDateFacture()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    /**
     * Méthode pour récupérer la liste des affaires
     * @param $args
     */
    private function getAffaireList($args, $affaireList = [])
    {
        if (is_array($args)) {
            foreach ($args as $key => $arg) {
                $affaire = $arg->getAffaire();
                if (!in_array($affaire, $affaireList)) {
                    array_push($affaireList, $affaire);
                }
            }
            return $affaireList;
        } else {
            $affaire = $args->getFacture()->getAffaire();
            if (!in_array($affaire, $affaireList)) {
                return   $args->getFacture()->getAffaire();
            }
        }
    }

    /**
     * Méthode pour trier les factures par date d'échéance dans l'ordre croissant
     * @param $facture
     */
    private function sortByDateEcheance(&$facture)
    {
        usort($facture, function($a, $b) {
            if (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) === strtotime($b->getDateEcheance()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateEcheance()->format('Y-m-d H:i:s')) < strtotime($b->getDateEcheance()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    /**
     * Méthode pour trier les avoirs par date dans l'ordre croissant
     * @param $avoirs
     */
    private function sortByDateAvoir($avoirs)
    {
        usort($avoirs, function($a, $b) {
            if (strtotime($a->getDate()->format('Y-m-d H:i:s')) === strtotime($b->getDate()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDate()->format('Y-m-d H:i:s')) < strtotime($b->getDate()->format('Y-m-d H:i:s'))) ? -1 : +1;
        });
    }

    /**
     * @return Response
     */
    public function showAction()
    {
        $user = $this->getUser();
        $affaires = [];
        $attributions = $user->getAttributions();
        foreach ($attributions as $attribution) {
            array_push($affaires, $attribution->getAffaire());
        }

        $devis = $this->getDoctrine()->getRepository(Devis::class)->findBy(['user' => $user, 'affaire' => null]);
        $devisAffaire = $this->getDoctrine()->getRepository(Devis::class)->findBy(['user' => $user, 'etat' => 4, 'affaire' => $affaires]);
        $devisAffaireBis = $this->getDoctrine()->getRepository(Devis::class)->findBy(['user' => $user, 'etat' => 1, 'affaire' => $affaires]);
        $devisAffaire = array_merge($devisAffaire, $devisAffaireBis);
        $listeAffaireDevis = $this->getAffaireList($devisAffaire);
        $commandeFournisseur = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findBy(['user' => $user, 'etat' => 0]);
        foreach ($commandeFournisseur as $key => $cmd) {
            if (!$cmd->getAffaire()) {
                unset($commandeFournisseur[$key]);
            }
        }
        $commandePrestataire = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findBy(['user' => $user, 'etat' => 0]);
        foreach ($commandePrestataire as $key => $cmd) {
            if (!$cmd->getAffaire()) {
                unset($commandePrestataire[$key]);
            }
        }
        $listeAffaireCommandeFournisseur = $this->getAffaireList($commandeFournisseur);
        $listeAffaireCommandePrestataire = $this->getAffaireList($commandePrestataire);
        $factureFournisseur = $this->getDoctrine()->getRepository(FactureFournisseur::class)->findBy(['user' => $user, 'validation' => 0, 'etat' => 0]);
        $facturePrestataire = $this->getDoctrine()->getRepository(FacturePrestataire::class)->findBy(['user' => $user, 'validation' => 0, 'etat' => 0]);
        $listeAffaireFactureFournisseur = $this->getAffaireList($factureFournisseur);
        $listeAffaireFacturePrestataire = $this->getAffaireList($facturePrestataire);

        return $this->render('GestionBundle:Default:indexDashboardChefProjet.html.twig', [
            'devis' => $devis,
            'commandeFournisseur' => $commandeFournisseur,
            'commandePrestataire' => $commandePrestataire,
            'listeAffaireCommandeFournisseur' => $listeAffaireCommandeFournisseur,
            'listeAffaireCommandePrestataire' => $listeAffaireCommandePrestataire,
            'factureFournisseur' => $factureFournisseur,
            'facturePrestataire' => $facturePrestataire,
            'listeAffaireFactureFournisseur' => $listeAffaireFactureFournisseur,
            'listeAffaireFacturePrestataire' => $listeAffaireFacturePrestataire,
            'affaires' => $affaires,
            'devisAffaire' => $devisAffaire,
            'listeAffaireDevis' => $listeAffaireDevis
        ]);
    }
}