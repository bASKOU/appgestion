<?php

namespace GestionBundle\Controller;

use DateTime;
use GestionBundle\Entity\Avoir;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Article;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\Facture;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\Attachement;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Form\CreateAvoirType;
use GestionBundle\Entity\ArticleFacture;
use GestionBundle\Entity\ArticleCommande;
use GestionBundle\Form\CreateFactureType;
use GestionBundle\Entity\ArticleAttachement;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\getStringBetween;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GestionBundle\Entity\ArticleLibrattachement;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FactureController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    public $exceptionHandler;
    /**
     * @var NumberCheckAndRenew
     */
    private $createNumber;

    /**
     * FactureController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }
    // constante de class qui permet de fixer le taux d'intérêt pour le calcul des pénalités de retard
    const TAUX_INTERET = 0.0258;

    /**
     * Méthode qui permet de calculer le total TTC d'une facture, à partir d'un prix HT et avec des modificateurs
     * @param $totalHT
     * @param $remise
     * @param $TVA
     * @return float|int
     */
    protected function calculateTotal($totalHT, $remise, $TVA) {
        $prixModifie = ($totalHT - ($totalHT * $remise));
        return ($prixModifie + ($prixModifie * $TVA));
    }

    /**
     * La méthode suivante permet de créer une facture directement depuis une commande
     * @param Request $request
     * @param $affaireId
     * @param $commandeId
     * @return JsonResponse
     */
    public function createFromCommandeAction(Request $request, $affaireId, $commandeId)
    {
        // on instancie une nouvelle facture depuis la Classe
        $facture = new Facture();

        $status = "error";
        $message = "";

        // on vérifie que le requete soit conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;

                // on récupère la commande concernée via son id passée en paramètre de l'url de la requete
                $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
                
                // on récupère les informations de la commande qui vont pouvoir servir à la pré-saisie de la facture
                $facture->setAffaire($commande->getAffaire());
                $facture->setClient($commande->getClient());
                $facture->setAdresseFacturation($commande->getAdresseFacturation());
                $facture->setAdresseLivraison($commande->getAdresseLivraison());
                $facture->setMentions($commande->getMentions() . ' [importé depuis la commande]');
                $facture->setDatePreSaisie(new DateTime());
                $facture->setIndemniteForfait(40);
                $facture->setTauxInteret(self::TAUX_INTERET);
                $facture->setEtat(0);
                $facture->setLinkedOperation($commandeId);
                $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFacture());
                $facture->setContact($commande->getContact());
                $facture->setActiveTVA($commande->getActiveTVA());
                $facture->setCommande($commande);
                $facture->setUser($commande->getUser());
                $facture->setInteretApplique(false);

                // on enregistre l'objet facture en temps qu'entité Doctrine via le manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($facture);
                $factureId = $facture->getId();

                // on set une varibale totalHT pour pouvoir après l'ajouter à l'entité facture
                $totalHT = 0;

                // on récupère le tableau des différents articles ajoutés à cette facture
                $dataArticles  = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                    'commande' => $commandeId
                ]);
                // on boucle sur ce tableau pour créer une entité articleFacture pour chaque entrée du tableau
                for($i = 0; $i < count($dataArticles); $i++) {
                    $articleFacture = new ArticleFacture();

                    // on incrémente notre variable totalHT avec chaque montantTotalHT des articles
                    $totalHT += (float)$dataArticles[$i]->getMontantTotalHT();

                    // on set les propriétés de chaque articleFacture que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                    $articleFacture->setFacture($this->getDoctrine()->getRepository(Facture::class)->find($factureId));
                    $articleFacture->setArticleDevis($this->getDoctrine()->getRepository(ArticleDevis::class)->find($dataArticles[$i]->getArticleDevis()->getId()));

                    // on vient vérifier qu'il n'y a pas d'autres articles liés à cette facture, si c'est le cas on les supprime
                    $checkExisting = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
                        'facture' => $factureId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                    // si on a un montant unitaire overridé, on l'ajoute à notre article
                    if($dataArticles[$i]->getOverrideMontantUnit() != null) {
                        $articleFacture->setOverrideMontantUnit($dataArticles[$i]->getOverrideMontantUnit());
                    }
                    $articleFacture->setQuantite($dataArticles[$i]->getQuantite());
                    $articleFacture->setMontantTotalHT($dataArticles[$i]->getMontantTotalHT());

                    // on persist l'entité créee à chaque itération de la boucle
                    $em->persist($articleFacture);
                }

                // on set le totalHT du devis avec la valeur finale de notre variable après le process de la boucle
                $facture->setTotalHT($totalHT);

                $id = $factureId;
                $organisme = $facture->getOrganisme()->getNom();
                $date = $facture->getDateFacture()->format('d/m/Y');
                $tauxInteret = ($facture->getTauxInteret()) * 100 ;
                $mentions = $facture->getMentions();
                if($facture->getEtat() == 0) {
                    $etat = 'A faire';
                }

                // on inscrit toutes les entités doctrine créées en BDD
                try {
                    $em->flush();
                    $status = "success";
                    $message = "Opération enregistrée";
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:facturePreSaisie.html.twig', [
                        'facture' => $facture
                    ]);
                    $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), 'Gest\'it - Statut de facture n°: ' . $facture->getNumeroFacture());
                    if ($response['status'] === 'success') {
                        $message .= ' Un email a était envoyé au service administratif pour validation.';
                    } else {
                        $message .= ' Erreur lors de l\'envoi de l\'email, veuillez contacter le service administratif pour demander la validation de la facture.';
                    }
                } catch (\Exception $e) {
                    $message = 'Une erreur s\'est produite, veuillez réessayer...';
                    $commande->setEtat(1);
                    $em->persist($commande);
                    $em->flush();
                }

                // on indique à notre vue que le processing de l'opération est terminé avec succès et on lui passe un objet contenant les infos de la nouvelle facture afin que le Javascript de la vue puisse gérer l'affichage dynamique
                return new JsonResponse([$status => $message,'id' => $id, 'organisme' => $organisme, 'mentions' => $mentions, 'date' => $date, 'totalHT' => $totalHT, 'tauxInteret' => $tauxInteret, 'etat' => $etat]);
            }
        }
    }

    /**
     * Fonction qui permet de créer une facture en pré-saisie depuis un attachement,  qu'il soit ouvert ou normal
     * @param Request $request
     * @param $affaireId
     * @param $attachementId
     * @param null $factureId
     * @return JsonResponse
     */
    public function createFromAttachementAction(Request $request, $affaireId, $attachementId, $factureId=null)
    {
        // on instancie une nouvelle facture depuis la Classe
        $facture = new Facture();
        // on récupère l'attachement concerné à partir de l'id passé en parametre de l'url de la requete
        $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($attachementId);
        // un article d'attachement normal n'a pas d'autres informations que l'id d'un article de la commande à laquelle il est lié, j'ai donc besoin de récupérer la commande
        $activeTVA = $attachement->getActiveTVA();

        $status = "error";
        $message = "";

        // on vérifie que la commande est bien conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;
                // un attachement n'ayant pas de liaison directe avec une affaire (il est lié à une commande), on récupère via un parametre dans l'url de la requete, l'id de l'affaire
                $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
                
                // on attribue à notre nouvelle facture les attributs de l'attachement concerné
                $facture->setAffaire($affaire);
                $facture->setClient($attachement->getClient());
                $facture->setMentions($attachement->getMentions() . ' [importé depuis la situation]');
                $facture->setDatePreSaisie(new DateTime());
                $facture->setIndemniteForfait(40);
                $facture->setTauxInteret(self::TAUX_INTERET);
                $facture->setEtat(0);
                $facture->setLinkedOperation($attachementId);
                $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFacture());
                $facture->setContact($attachement->getContact());
                $facture->setForfaitaire($attachement->getForfaitaire());
                $facture->setAdresseFacturation($attachement->getAdresseFacturation());
                $facture->setAdresseLivraison($attachement->getAdresseLivraison());
                $facture->setInteretApplique(false);
                if ($activeTVA == 0) {
                    $facture->setActiveTVA(false);
                } else {
                    $facture->setActiveTVA(true);
                }
                $facture->setAttachement($attachement);
                $facture->setUser($attachement->getUser());

                // on persist cet objet facture en entité Doctrine via le manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($facture);
                $factureId = $facture->getId();

                // on set une varibale totalHT pour pouvoir après l'ajouter à l'entité facture
                $totalHT = 0;

                //! ici le if permet de différencier l'attachement normal d'un attachement ouvert, les deux n'ayant pas leurs articles dans la meme table
                if(count($attachement->getArticleAttachements()->toArray()) != 0) {
                    // on récupère le tableau des différents articles ajoutés à cette facture
                    $dataArticles  = $this->getDoctrine()->getRepository(ArticleAttachement::class)->findBy([
                        'attachement' => $attachementId
                    ]);

                    // on boucle sur ce tableau pour créer une entité articleFacture pour chaque entrée du tableau
                    for($i = 0; $i < count($dataArticles); $i++) {
                        $articleFacture = new ArticleFacture();

                        // on incrémente notre variable totalHT avec chaque montantHtAv des articles
                        $totalHT += $dataArticles[$i]->getMontantHtAv();

                        // on set les propriétés de chaque articleFacture que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                        $articleFacture->setFacture($this->getDoctrine()->getRepository(Facture::class)->find($factureId));
                        $articleCommande = $this->getDoctrine()->getRepository(ArticleCommande::class)->find($dataArticles[$i]->getArticleCommande());
                        $articleFacture->setArticleDevis($articleCommande->getArticleDevis());

                        // on vient vérifier qu'il n'y a pas d'autres articles liés à cette facture, si c'est le cas on les supprime
                        $checkExisting = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
                            'facture' => $factureId
                        ]);
                        if($checkExisting) {
                            for ($j = 0; $j < count($checkExisting); $j++) {
                                $em->remove($checkExisting[$j]);
                            }
                        }
                        // si on a un montant unitaire overridé, on l'ajoute à notre article
                        if($articleCommande->getOverrideMontantUnit() != null) {
                            $articleFacture->setOverrideMontantUnit($articleCommande->getOverrideMontantUnit());
                        }
                        if($dataArticles[$i]->getTypeAvancement() == 2) {
                            $articleFacture->setQuantitePourcentage(true);
                        } else {
                            $articleFacture->setQuantitePourcentage(false);
                        }
                        $articleFacture->setQuantite($dataArticles[$i]->getAvancement());
                        $articleFacture->setMontantTotalHT($dataArticles[$i]->getMontantHtAv());

                        // on persist l'entité créee à chaque itération de la boucle
                        $em->persist($articleFacture);
                    }
                } else {
                    $dataArticles  = $this->getDoctrine()->getRepository(ArticleLibrattachement::class)->findBy([
                        'attachement' => $attachementId
                    ]);

                    for($i = 0; $i < count($dataArticles); $i++) {
                        $articleFacture = new ArticleFacture();

                        // on incrémente notre variable totalHT avec chaque montantTotalHT des articles
                        $totalHT += $dataArticles[$i]->getMontantTotalHT();

                        // on set les propriétés de chaque articleFacture que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                        $articleFacture->setFacture($this->getDoctrine()->getRepository(Facture::class)->find($factureId));
                        $articleFacture->setArticle($this->getDoctrine()->getRepository(Article::class)->find($dataArticles[$i]->getArticle()));

                        // on vient vérifier qu'il n'y a pas d'autres articles liés à cette commande, si c'est le cas on les supprime
                        $checkExisting = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
                        'facture' => $factureId
                        ]);
                        if($checkExisting) {
                            for ($j = 0; $j < count($checkExisting); $j++) {
                                $em->remove($checkExisting[$j]);
                            }
                        }
                        // si on a un montant unitaire overridé, on l'ajoute à notre article
                        if($dataArticles[$i]->getOverrideMontantUnit() != null) {
                            $articleFacture->setOverrideMontantUnit($dataArticles[$i]->getOverrideMontantUnit());
                        }
                        $articleFacture->setQuantite($dataArticles[$i]->getQuantite());
                        $articleFacture->setMontantTotalHT($dataArticles[$i]->getMontantTotalHT());

                        // on persist l'entité créee à chaque itération de la boucle
                        $em->persist($articleFacture);
                    }
                }
                if ($facture->getForfaitaire() == true) {
                    $totalHT = $attachement->getTotalHT();
                }
                // on set le totalHT du devis avec la valeur finale de notre variable après le process de la boucle
                $facture->setTotalHT($totalHT);
                $facture->setTotalTTC($totalHT * (1 + $attachement->getCommande()->getTVA()));
                $id = $factureId;
                $client = $facture->getClient()->getNom();
                $date = $facture->getDatePresaisie()->format('d/m/Y');
                $tauxInteret = ($facture->getTauxInteret()) * 100 ;
                $mentions = $facture->getMentions();
                if($facture->getEtat() == 0) {
                    $etat = 'A faire';
                }

                // on inscrit toutes les entités doctrine créées en BDD
                try {
                    $em->flush();
                    $status = "success";
                    $message = "La facture a été générée.";
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:facturePreSaisie.html.twig', [
                        'facture' => $facture,
                        'url' => $this->getParameter('adresse_web')
                    ]);
                    $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), 'Gest\'it - Statut de facture n°: ' . $facture->getNumeroFacture());
                    if ($response['status'] === 'success') {
                        $message .= ' Un email a était envoyé au service administratif pour validation.';
                    } else {
                        $message .= ' Erreur lors de l\'envoi de l\'email, veuillez contacter le service administratif pour demander la validation de la facture.';
                    }
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'facture');
                }

                // on indique à notre vue que le processing de l'opération est terminé avec succès
                return new JsonResponse([$status => $message,'id' => $id, 'organisme' => $client, 'mentions' => $mentions, 'date' => $date, 'totalHT' => $totalHT, 'tauxInteret' => $tauxInteret, 'etat' => $etat]);
            }
        }
    }

    /**
     * Méthode qui va permettre de modifier une facture
     * N'importe quel utilisateur peut uitliser les 2 requetes précédentes pour créer une facture, mais seulement en pré-saisie
     * Seul un admin pourra modifier une facture et la valider
     * @param Request $request
     * @param $affaireId
     * @param $factureId
     * @param string $display
     * @return Response
     */
    public function editAction(Request $request, $affaireId, $factureId, $display='no')
    {
        // on répcupère la facture concernée via son id passée en parametre de l'url de le requete
        $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);

        // on recupere le tableau des articles de la facture
        $linkedArticleFacture = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
            'facture' => $factureId
        ]);
        $activeTVA = $facture->getActiveTVA();

        $user = $this->getUser();

        // on récupère l'affaire ainsi que son nom
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $nomAffaire = $affaire->getDesignationAffaire();

        // on créé un formulaire à partir de son modele type et on lui attache l'objet request afin de pouvoir gérer les données saisoes dans le formulaire
        $form_facture = $this->createForm(CreateFactureType::class, $facture, ['TVA' => $activeTVA]);
        $form_facture->handleRequest($request);
        $status = "error";
        $message = "";

        // on vérifie que la requete soit bien conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;

                $tempDataFacture = $data->get('facture');
                $dataFacture = [];
                $getStringBetween = new getStringBetween('[', ']');
                foreach ($tempDataFacture as $arr) {
                    $key = $getStringBetween->getString($arr['name']);
                    if ($arr['value'] != "") {
                        $dataFacture[$key] = $arr['value'];
                    }
                }
                unset($tempDataFacture);
                unset($dataFacture['_token']);
                foreach ($dataFacture as $key => $value) {
                    if ($key == 'dateEcheance') {
                    }
                    if ($key == 'affaire') {
                        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($value);
                        $facture->setAffaire($affaire);
                    } else if ($key == 'client' || $key == 'adresseLivraison' || $key == 'adresseFacturation') {
                        $temp = 'set' . ucfirst($key);
                        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        $facture->$temp($organisme);
                    } else if ($key == 'contact') {
                        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($value);
                        $facture->setContact($contact);
                    } elseif ($key == 'TVA') {
                        $temp = 'set' . ucfirst($key);
                        $facture->$temp(((float)$value) / 100);
                    } else if ($key == 'totalHT' || $key == 'tauxInteret' || $key == 'totalTTC' || $key == 'indiceRevision' || $key == 'indemniteForfait') {
                        $temp = 'set' . ucfirst($key);
                        $facture->$temp((float)str_replace(',', '.', str_replace(' ', '', $value)));
                    } else if ($key == 'dateEcheance') {
                        $temp = 'set' . ucfirst($key);
                        $facture->$temp(DateTime::createFromFormat('d-m-Y', $value));
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $facture->$temp($value);
                    }
                }

                $facture->setDateFacture(new DateTime());
                $facture->setEtat(1);

                // on sauvegarde ces modifications avec le manager de Doctrine
                $em = $this->getDoctrine()->getManager();
                $em->persist($facture);
                $factureId = $facture->getId();

                // on set une varibale totalHT pour pouvoir après l'ajouter à l'entité facture
                $totalHT = 0;

                // on récupère le tableau des différents articles ajoutés à cette facture
                $dataArticles  = $data->get('articles');
                // on boucle sur ce tableau pour créer une entité articleFacture pour chaque entrée du tableau
                for($i = 0; $i < count($dataArticles); $i++) {
                    $articleFacture = new ArticleFacture();

                    // on incrémente notre variable totalHT avec chaque montantTotalHT des articles
                    $totalHT += (float)$dataArticles[$i]["montantTotalHT"];

                    // on set les propriétés de chaque articleFacture que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                    $articleFacture->setFacture($this->getDoctrine()->getRepository(Facture::class)->find($factureId));
                    $articleFacture->setArticleDevis($this->getDoctrine()->getRepository(ArticleDevis::class)->find($dataArticles[$i]["articleId"]));

                    // on vient vérifier qu'il n'y a pas d'autres articles liés à cette facture, si c'est le cas on les supprime
                    $checkExisting = $this->getDoctrine()->getRepository(ArticleFacture::class)->findBy([
                        'facture' => $factureId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                    // si on a un montant unitaire overridé, on l'ajoute à notre article
                    if((float)$dataArticles[$i]["overrideMontantUnit"] != null || (float)$dataArticles[$i]["overrideMontantUnit"] != "") {
                        $articleFacture->setOverrideMontantUnit((float)$dataArticles[$i]["overrideMontantUnit"]);
                    }
                    $articleFacture->setQuantite((int)$dataArticles[$i]["quantite"]);
                    $articleFacture->setMontantTotalHT((float)$dataArticles[$i]["montantTotalHT"]);

                    // on persist l'entité créee à chaque itération de la boucle
                    $em->persist($articleFacture);
                }
                $facture->getIndiceRevision() == null ? $indiceRevision = 1: $indiceRevision = $facture->getIndiceRevision();
                // on set le totalHT du facture avec la valeur finale de notre variable après le process de la boucle
                if ($facture->getForfaitaire() == 0) {
                    $totalHT = $totalHT;
                } else {
                    $totalHT = $facture->getTotalHT();
                }
                $facture->setTotalHT($totalHT);
                // on vient calculer le totalTTC de notre facture en y appliquant la remise et le taux de TVA
                $totalTTC = $this->calculateTotal($totalHT, $facture->getRemise(), $facture->getTVA());
                // on set le totalTTC du facture avec la valeur retournée par notre fonction de calcul
                $facture->setTotalTTC($totalTTC);

                $linkedOperation = $facture->getLinkedOperation();
                if($this->getDoctrine()->getRepository(Commande::class)->find($linkedOperation)) {
                    $commande = $this->getDoctrine()->getRepository(Commande::class)->find($linkedOperation);
                    $commande->setEtat(4);
                } else {
                    $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($linkedOperation);
                    $commande = $attachement->getCommande();
                    $existingAttachements = $commande->getAttachements();
                    $montantAllAttach = 0;
                    for($i = 0; $i < count($existingAttachements); $i++) {
                        $montantAllAttach += $existingAttachements[$i]->getTotalHT();
                    }
                    if($montantAllAttach == $commande->getTotalHT()) {
                        $commande->setEtat(4);
                    }
                }

                // on inscrit toutes les entités doctrine créées en BDD
                try {
                    $em->flush();
                    $status = "success";
                    $message = "Opération enregistrée";
                    if ($facture->getEtat() == 1) {
                        $html = $this->renderView('GestionBundle:Export:factureValidation.html.twig', [
                            'facture' => $facture,
                            'url' => $this->getParameter('app_url')
                        ]);
                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $facture->getUser()->getEmail(), 'Gest\'it - Statut de facture n° ' . $facture->getNumeroFacture());
                        if ($response['status'] === 'success') {
                            $this->addFlash('success', 'Le mail pour informer le chef de projet de la validation du devis a bien était envoyé.');
                        } else {
                            if ($affaireId) {
                                $this->addFlash('warning', 'Une erreur s\'est produite lors de l\'envoi du mail de demande de validation. Contactez votre responsable pour demander la validation du devis.');
                            } else {
                                $this->addFlash('warning', 'Une erreur s\'est produite lors de l\'envoi du mail pour informer le chef de projet de la validation du devis. Contactez le chef de projet pour l\'en informer.');
                            }
                        }
                    } elseif ($facture->getEtat() == 0) {
                        $html = $this->renderView('GestionBundle:Export:facturePreSaisie.html.twig', [
                            'facture' => $facture,
                            'url' => $this->getParameter('app_url')
                        ]);
                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $this->getParameter('email_administratif'), 'Gest\'it - Validation facture n° ' . $facture->getNumeroFacture());
                        if ($response['status'] === 'success') {
                            $this->addFlash('success', 'Le mail de demande de validation a étét envoyé au service administratif.');
                        } else {
                            $this->addFlash('warning', 'Une erreur s\'est produite lors de l\'envoi du mail de demande de validation. Contactez votre le service administratif pour demander la validation du devis.');
                        }
                    }
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'facture');
                }

                // on indique à notre vue que le processing de l'opération est terminé avec succès
                return new Response(json_encode(array($status => $message)));
            }
        }

        return $this->render('GestionBundle:Default:manageFacture.html.twig', [
            'form_facture' => $form_facture->createView(),
            'user' => $user,
            'affaireId' => $affaireId,
            'factureId' => $factureId,
            'nomAffaire' => $nomAffaire,
            'linkedArticleFacture' => $linkedArticleFacture,
            'facture' => $facture,
            'affaire' => $affaire,
            'display' => $display
        ]);
    }

    /**
     * Méthode de réception des fichiers uploadé
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadFilesAction(Request $request, $factureId) {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $data = '';
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $error = null;
                /** @var UploadedFile $file */
                $file = $request->files->get('file');
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $fileName = $safeFilename.'-'.uniqid().'.pdf';
                try {
                    $file->move(
                        $this->getParameter('pdf_facture_directory'), $fileName
                    );
                    $code = 200;
                    $status = 'success';
                    $message = 'Le fichier est uploadé avec succés.';
                    $data = $fileName;
                    $em = $this->getDoctrine()->getManager();
                    $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
                    $docs = $facture->getAttachementDoc();
                    $docs == null ? $docs = [] : '';
                    array_push($docs, $fileName);
                    $facture->setAttachementDoc($docs);
                    $em->persist($facture);
                    try {
                        $em->flush();
                    } catch (\Exception $e) {
                        $status = 'error';
                        $data = $e;
                        $message = 'Une erreur s\'est produite, le fichier a était uploadé mais il n\'a pas était ajouté à la liste des documents de la commande.';
                        $code = 500;
                    }
                } catch (FileException $e) {
                    $data = $e;
                    $message = 'Une erreur s\'est produite, le fichier n\'a pas était uploadé.';
                    $code = 500;
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'fileName' => $data], $code);
    }

    /**
     * Méthode pour passer l'état de la facture en attente de paiement
     * @param $factureId
     * @return RedirectResponse
     */
    public function sentToCustomerAction($factureId)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
        $facture->setEtat(2);
        $em->persist($facture);

        try {
            $em->flush();
            $this->addFlash('success', 'L\'envoie de la facture au client a bien était enregistré.');
        } catch (\Exception $e) {
            $this->addFlash('danger', 'Une erreur s\'est produite, veuillez réssayer.');
        }

        return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_vente') . '#paiementFacture');
    }

    /**
     * @param Request $request
     * @param $factureId
     * @param null $late
     * @return JsonResponse
     */
    public function paiedAction(Request $request, $factureId, $late = null)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
                $facture->setEtat(3);
                    $data = $request->request->get('date');
                    $facture->setDatePaiement(new \DateTime($data));
                $em->persist($facture);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'Le paiement de la facture a bien était enregistré';
                } catch (\Exception $e) {
                    $code = 500;
                    $message = 'Une erreur s\'est protuite lors de l\'enregistrement du paiement de la facture';
                }
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * @param Request $request
     * @param $factureId
     * @return JsonResponse
     */
    public function setIndemniteAction(Request $request, $factureId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $data = $request->request->get('indemnite');
                $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
                if ($data == 1) {
                    $facture->setInteretApplique(true);
                } else {
                    $facture->setInteretApplique(false);
                }
                $em->persist($facture);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'L\'application du taux d\'intérêt ou non a était appliqué';
                } catch (\Exception $e) {
                    $code = 500;
                    $message = 'Une erreur s\'est protuite lors de l\'application du taux d\'intérêt ou non a était appliqué';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    public function indiceRevisionAction(Request $request, $factureId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';

        if ($request->isXmlHttpRequest()) {
          if ($request->isMethod('POST')) {
              $em =$this->getDoctrine()->getManager();
              $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
              $data = $request->request;
              if ($data->get('indiceRadio') == 1 ) {
                  $facture->setIndiceRevision((float) str_replace(',', '.', $data->get('indiceNumber')));
                  $em->persist($facture);
                  try {
                      $code = 200;
                      $status = 'success';
                      $message = 'L\'indice de révision a bien était appliqué lors de la facturation';
                      $em->flush();
                  } catch (\Exception $e) {
                      $code = 500;
                  }
              } else {
                  $facture->setIndiceRevision(1.0);
                  $em->persist($facture);
                  try {
                      $em->flush();
                      $code = 200;
                      $status = 'success';
                      $message = 'Facturation sans indice de révision enregistré';
                  } catch (\Exception $e) {
                      $code = 500;
                  }
              }
          }
        }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour créer un avoir
     * @param Request $request
     * @param $factureId
     * @return JsonResponse|Response
     */
    public function createAvoirAction(Request $request, $factureId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';

        $facture = $this->getDoctrine()->getRepository(Facture::class)->find($factureId);
        $avoir = new Avoir();
        $avoir->setFacture($facture);
        if ($request->isMethod('GET')) {
            $form = $this->createForm(CreateAvoirType::class, $avoir);
        } else if ($request->isMethod('POST')) {
            $avoir->setDate(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->get('create_avoir');
            unset($data['_token']);
            $avoir->setMontant(floatval(str_replace(',', '.', $data['montant'])));
            $avoir->setMotif($data['motif']);
            $avoir->setNumeroAvoir($this->createNumber->numeroAvoir($facture->getId()));
            $em->persist($avoir);
            try {
                $code = 200;
                $status = 'success';
                $message = 'Avoir enregistré';
                $em->flush();
            } catch (\Exception $e) {
                $status = 'error';
                $message = $this->exceptionHandler->getException($e->getMessage());
                $code = 200;
            }
            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createAvoir.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAvoirAction(Request $request, $avoirId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';

        $avoir = $this->getDoctrine()->getRepository(Avoir::class)->find($avoirId);

        if ($request->isMethod('GET')) {
            $form = $this->createForm(CreateAvoirType::class, $avoir);
        } else if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->get('create_avoir');

            unset($data['_token']);
            $avoir->setMontant(floatval(str_replace(',', '.', $data['montant'])));
            $avoir->setMotif($data['motif']);
            $em->persist($avoir);
            try {
                $code = 200;
                $status = 'success';
                $message = 'Avoir édité';
                $em->flush();
            } catch (\Exception $e) {
                $status = 'error';
                $message = $this->exceptionHandler->getException($e->getMessage());
                $code = 200;
            }
            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:editAvoir.html.twig', [
            'form' => $form->createView()
        ]);
    }
}