<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Article;
use GestionBundle\Entity\ArticleAffaire;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Entity\Theme;
use GestionBundle\Form\CreateArticleType;
use GestionBundle\Form\CreateThemeType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    public $exceptionHandler = "";
    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * Méthode qui va nous permettre de créer un nouvel article
     * @param Request $request
     * @param null $affaireId
     * @return JsonResponse|Response
     */
    public function createAction(Request $request, $affaireId=null)
    {
        $article = new Article();
        $articleAffaireId = 0;
        $form = $this->createForm(CreateArticleType::class, $article);

        $form->handleRequest($request);

        /**
         * comme la plupart de nos formulaires, on récupère ici les informations via une requete AJAX de type POST
         * je vérifie donc avant de traiter les données, que ma requete est bien en AJAX et qu'elle a bien la méthode que j'attends
         */
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                // on récupère le tableau de données contenu dans ma requete
                $data = $request->request->get("create_article");
                if (is_string($data['theme'])) {
                    $theme = $this->getDoctrine()->getRepository(Theme::class)->findOneBy(['nom' => $data['theme']]);
                    if ($theme) {
                        $data['theme'] = $theme;
                    } else {
                        $theme = $this->getDoctrine()->getRepository(Theme::class)->findOneBy(['id' => intval($data['theme'])]);
                        $data['theme'] = $theme;
                    }
                }
//                $articleExist = $this->getDoctrine()->getRepository(Article::class)->findOneBy(['nom' => $data["nom"], 'theme' => $data['theme']]);
//
//                if ($articleExist && $articleExist->getUnite() == $data["unite"]) {
//                    $article = $articleExist;
//                    $article->setDescription($data['description']);
//                    $article->setUnite($data['unite']);
//                } else {
                    // on attribue les données à notre nouvel objet article via les méthodes set de la classe
                    $article->setTheme($data["theme"]);
                    $article->setNom($data["nom"]);
                    $article->setDescription($data["description"]);
                    $article->setUnite($data["unite"]);
                    // on récupère le manager d'entité Doctrine et on transforme notre objet en entité Doctrine
                    $em->persist($article);
                    if ($affaireId) {
                        $articleAffaire = new ArticleAffaire();
                        $articleAffaire->setAffaire($this->getDoctrine()->getRepository(Affaire::class)->find($affaireId));
                        $articleAffaire->setArticle($article);
                        $em->persist($articleAffaire);
                        $articleAffaireId = $articleAffaire->getId();
                    }
                    // on demande à Doctrine d'inscrire notre article en BDD
                    $em->flush();
//                }
                // on utilise les méthodes get de la classe pour récupérer les infos de l'article nouvellement créé
                $id = $article->getId();
                $theme = $article->getTheme()->getNom();
                $themeId = $article->getTheme()->getId();
                $nom = $article->getNom();
                $description = $article->getDescription();
                $unite = $article->getUnite();
                // on envoie à la vue une réponse qui va contenir un objet contenant toutes les informations de l'article créé
                return new JsonResponse(array('status' => 'success',
                                                        'id' => $id,
                                                        'articleAffaireId' => $articleAffaireId,
                                                        'theme' => $theme,
                                                        'themeId' => $themeId,
                                                        'nom' => $nom,
                                                        'description' => $description,
                                                        'unite' => $unite
                                                    ));
            }
        }

        return $this->render('GestionBundle:Default:createArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour vérifier si un article est utilisé dans:
     * Devis, affaire ou attacement libre
     * @param $article
     * @return int|null
     */
    private function verifyUsedOrNot($article){
        $test = null;
        if (count($article->getArticleDevis()) > 0 || count($article->getArticleAffaires()) > 0) {
            $test = 1;
        };
        return $test;
    }

    /**
     * Fonction qui permet d'ajouter un thème
     * Elle doit permette l'ajout d'un thème en requête AJAX
     * mais aussi envisager une interface d'ajout de theme
     * @param Request $request
     * @return JsonResponse | Response
     */
    public function addThemeAction(Request $request)
    {
        // Initialise les variables nécessaires
        $status = 'error';
        $message = 'Mauvaise requête.';
        $code = 400;
        $data = [];
        if($request->isXmlHttpRequest()) { // Vérifie que la requête soit bien une requête AJAX
            if($request->isMethod('POST')) { // Vérifie que la méthode soit bien un POST
                $em = $this->getDoctrine()->getManager(); // Entity manager
                $theme = new Theme(); // Instancie un nouveau theme
                $data = $request->getContent(); // Récupère le contenu de la requête
                try { // Lance un controlle de retour d'exeption en cas d'erreur d'insertion
                    $theme->setNom($data);
                    $em->persist($theme);
                    $em->flush();
                    $data = ['id' => $theme->getId(), 'nom' => $theme->getNom()];
                    $status = 'success';
                    $code = 200;
                    $message = 'Thème créer avec succès!';
                } catch (\Exception $e) {
                    $message = $this->exceptionHandler->getException($e);
                    $code = 200;
                }
            }
        }
            return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
    }

    /**
     * Méthode pour éditer un article
     */
    public function editAction(Request $request, $articleId = null, $articleDevisId = null)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        $data = null;
        $flag = false;
        $em = $this->getDoctrine()->getManager();

        if ($articleId != null) {
            $article = $this->getDoctrine()->getRepository(Article::class)->find($articleId);
        } else if ($articleDevisId != null) {
            $articleDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->find($articleDevisId);
            $article = $articleDevis->getArticle();
            $flag = true;
        }

        $form = $this->createForm(CreateArticleType::class, $article, ['devis' => $flag]);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {

                return $this->render('GestionBundle:Default:createArticle.html.twig', [
                    'form' => $form->createView(),
                    'editArticleDevis' => true
                ]);
            } elseif ($request->isMethod('POST')) {
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    if ($articleId) {
                        $data = $request->request->get('create_article');
                        unset($data['_token']);
                        $em->merge($article);
                        $data['theme'] = $article->getTheme()->getNom();
                    } elseif ($articleDevisId) {

                    }
                    $data['unite'] === '0' ? $data['unite'] = 'Forfaitaire' : $data['unite'] = 'Unitaire';
                    try {
                        $em->flush();
                        $code = 200;
                        $status = 'success';
                        $message = 'Edition de l\'article réussi !';
                    } catch (\Exception $e) {
                        $code = 500;
                        $error = $this->exceptionHandler->getException($e);
                        $message = $this->exceptionHandler->exceptionHandler($error, 'article');
                    }
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
    }

    /**
     * Méthode pour afficher les détails d'un article
     * Affichage de l'article dans l'entité article
     * et de l'article dans chaque devis
     * @param Request $request
     * @param $affaireId
     * @param $articleAffaireId
     * @return Response
     */
    public function showArticleAction(Request $request, $affaireId, $articleAffaireId)
    {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $articleAffaire = $this->getDoctrine()->getRepository(ArticleAffaire::class)->find($articleAffaireId);
        $articlesDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy(['article' => $articleAffaire->getArticle()]);
        $devisList = $affaire->getDevis();
        $listedArticleDevis = [];
        $listedDevis = [];

        foreach ($articlesDevis as $articleDevis) {
            if (!empty($articleDevis->getDevis()->getAffaire())) {
                foreach ($devisList as $devis) {
                    if ($articleDevis->getDevis()->getId() === $devis->getId()) {
                        if (empty($listedDevis[$devis->getId()])) {
                            $listedDevis[$devis->getId()] = $devis;
                        }
                    }
                }
                if ($articleDevis->getDevis()->getAffaire()->getId() === $affaire->getId()) {
                    array_push($listedArticleDevis, $articleDevis);
                }
            }
        }

        return $this->render('GestionBundle:Default:detailArticle.html.twig', [
            'affaire' => $affaire,
            'articleAffaire' => $articleAffaire,
            'articlesDevis' => $listedArticleDevis,
            'devisList' => $listedDevis
        ]);
    }

    /**
     * Méthode pour ajouter un article choisi parmit les produits existant dans un devis lier à une affaire
     * @param $affaireId
     * @param Request $request
     * @return JsonResponse
     */
    public function addToAffaireAction($affaireId, Request $request)
    {
        $status = 0;
        $message = "Erreur de requête";
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $data = $request->getContent();
                $articleAffaire = new ArticleAffaire();
                $article = $this->getDoctrine()->getRepository(Article::class)->find($data);
                $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
                $exist = $this->getDoctrine()->getRepository(ArticleAffaire::class)->findBy(['affaire' => $affaire,'article' => $article]);
                if (empty($exist[0])) {
                    $articleAffaire->setArticle($article);
                    $articleAffaire->setAffaire($affaire);
                    $em->persist($articleAffaire);
                    try {
                        $em->flush();
                        $status = 1;
                        $message = "L'article a bien était ajouté à l'affaire";
                    } catch (\Exception $e) {
                        $error = $this->exceptionHandler->getException($e);
                        $message = $this->exceptionHandler->exceptionHandler($error, 'article affaire');
                    }
                } else {
                    $message = "L'article sélectionné est déjà dans votre liste d'article de cette affaire";
                }
            }
        }

        return new JsonResponse(['st' => $status, "message" => $message]);
    }

    /**
     * Méthode pour afficher tous les thèmes
     * @param Request $request
     * @return Response
     */
    public function showThemeAction(Request $request)
    {
        $themes = $this->getDoctrine()->getRepository(Theme::class)->findAll();

        return $this->render('GestionBundle:Default:manageTheme.html.twig', [
            'themes' => $themes
        ]);
    }

    /**
     * Méthode pour supprimer un thème
     * @param Request $request
     * @param $themeId
     * @return JsonResponse
     */
    public function deleteThemeAction(Request $request, $themeId)
    {
        // Initialise les variables nécessaires
        $status = 'error';
        $message = 'Mauvaise Requête';
        $code = 400;
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                    $code = 200;
                    $status = 'success';
                    $message = 'Vous êtes sur le point de supprimer un thème et tous s\'est articles, cette opération est irréversible.<br> Etes vous sur de vouloir de vouloir le supprimer ?';
                } else {
                    $message = 'Vou n\'avez pas les droits nécessaire pour supprimer un thème.';
                    $code = 401;
                }
            } elseif ($request->isMethod('DELETE')) {
                if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                    $em = $this->getDoctrine()->getManager();
                    $theme = $this->getDoctrine()->getRepository(Theme::class)->find($themeId);
                    $em->remove($theme);
                    try {
                        $code = 200;
                        $message = 'Thème supprimé avec succès!';
                        $status = 'success';
                    } catch (\Exception $e) {
                        $code = 200;
                        $message = 'Suppression impossible... Car certains articles de ce thème sont utilisés.';
                    }
                } else {
                    $message = 'Vou n\'avez pas les droits nécessaire pour supprimer un thème.';
                    $code = 401;
                }
            }
        }
        return new JsonResponse(['status' =>  $status, 'message' => $message], $code);

    }

    /**
     * Méthode pour vor le détail d'un thème
     * @param Request $request
     * @param $themeId
     * @return Response
     */
    public function detailThemeAction(Request $request, $themeId)
    {
        $em = $this->getDoctrine()->getManager();
        $theme = $this->getDoctrine()->getRepository(Theme::class)->find($themeId);
        $articles = $theme->getArticles()->toArray();
        $articlesUsed = [];
        $articlesOrphan = [];
        foreach ($articles as $article) {
            $temp = $this->verifyUsedOrNot($article);
            if ($temp === 1) {
                array_push($articlesUsed, $article);
            } else {
                array_push($articlesOrphan, $article);
            }
        }

        $formTheme = $this->createForm(CreateThemeType::class, $theme);
        $formTheme->handleRequest($request);

        if ($formTheme->isSubmitted() && $formTheme->isValid()) {
            $em->persist($theme);
            try {
                $em->flush();
                $this->addFlash('success', 'Le nom du thème à bien été édité.');
            } catch (\Exception $e) {
                $exception = $this->exceptionHandler->getException($e);
                $this->addFlash('warning', $this->exceptionHandler->exceptionHandler($exception, 'thème'));
            }

        }

        return $this->render('GestionBundle:Default:detailTheme.html.twig', [
            'theme' => $theme,
            'articlesUsed' => $articlesUsed,
            'articlesOrphan' => $articlesOrphan,
            'form_theme' => $formTheme->createView()
        ]);
    }

    /**
     * Méthode pour éditer un theme
     * @param Request $request
     * @param $themeId
     * @return JsonResponse
     */
    public function editThemeAction(Request $request, $themeId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        $data = $request->request->get('data');
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('PUT')) {
                $em = $this->getDoctrine()->getManager();
                $theme = $this->getDoctrine()->getRepository(Theme::class)->find($themeId);
                $theme->setNom($data);
                $em->merge($theme);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'Thème modifié avec succès';
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
    }

    /**
     * Méthode pour afficher la liste des articles par thème pour une affaire
     * @param $affaireId
     * @return Response
     */
    public function affaireArticleListAction($affaireId)
    {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $articlesAffaire = $affaire->getArticleAffaires();
        $devisAffaire = $affaire->getDevis();
        $themes = [];
        $usedList = [];
        foreach ($articlesAffaire as $articleAffaire) {
            $usedList[$articleAffaire->getArticle()->getId()]['devis'] = 'Non';
            foreach ($devisAffaire as $devis) {
                $articlesDevis = $devis->getArticleDevis();
                foreach ($articlesDevis as $articleDevis) {
                    if ( $articleDevis->getArticle()->getId() === $articleAffaire->getArticle()->getId()) {
                        $usedList[$articleAffaire->getArticle()->getId()]['devis'] = 'Oui';
                    }
                }
            }
            if (!in_array($articleAffaire->getArticle()->getTheme(), $themes)) {
                array_push($themes, $articleAffaire->getArticle()->getTheme());
            }
        }

        return $this->render('GestionBundle:Default:manageArticle.html.twig', [
            'affaire' => $affaire,
            'articlesAffaire' => $articlesAffaire,
            'themes' => $themes,
            'usedList' => $usedList
        ]);
    }

    /**
     * Méthode pour supprimer un article dans une affaire
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteArticleAffaireAction(Request $request, $articleAffaireId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        if ($request->isXmlHttpRequest()) {
                if ($request->isMethod('GET')) {
                    if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                        $status = 'success';
                        $code = 200;
                        $message = '<h4>Vous êtes sur le point de supprimer un article de cette affaire!</h4><p>Etes vous sur de vouloir le supprimé ?</p>';
                    } else {
                        $code = 200;
                        $message = '<h4>Vous n\'avez pas les droits nécessaires pour supprimer un article de cette affaire.</h4><p>Contactez votre responsable.</p>';
                    }
                } elseif ($request->isMethod('DELETE')) {
                    $em = $this->getDoctrine()->getManager();
                    $articleAffaire = $this->getDoctrine()->getRepository(ArticleAffaire::class)->find($articleAffaireId);
                    $em->remove($articleAffaire);
                    try {
                        $em->flush();
                        $code = 200;
                        $status = 'success';
                        $message = 'L\'article a était supprimer de la liste des articles de cette affaire avec succès!';
                    } catch (\Exception $e) {
                        $code = 500;
                        $message = 'Une erreur s\'est produite, veuillez réessayer...';
                    }
                }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'id' => $articleAffaireId], $code);
    }

    /**
     * Méthode pour supprimer un article dans une affaire
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteArticleAction(Request $request, $articleId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête';
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                    $status = 'success';
                    $code = 200;
                    $message = '<h4>Vous êtes sur le point de supprimer un article!</h4><p>Etes vous sur de vouloir le supprimé ?</p>';
                } else {
                    $code = 200;
                    $message = '<h4>Vous n\'avez pas les droits nécessaires pour supprimer un article.</h4><p>Contactez votre responsable.</p>';
                }
            } elseif ($request->isMethod('DELETE')) {
                $em = $this->getDoctrine()->getManager();
                $article = $this->getDoctrine()->getRepository(Article::class)->find($articleId);
                $em->remove($article);
                try {
                    $em->flush();
                    $code = 200;
                    $status = 'success';
                    $message = 'L\'article a bien été supprimé';
                } catch (\Exception $e) {
                    $code = 200;
                    $message = $this->exceptionHandler->getException($e);
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'id' => $articleId], $code);
    }

    /**
     * Méthode pour récupérer la liste des articles d'une affaire
     * @param $affaireId
     * @return JsonResponse
     */
    public function getArticlesListAffaireAction($affaireId)
    {
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $articles = $affaire->getArticleAffaires();
        $articlesList = [];

        foreach ($articles as $article) {
            array_push($articlesList, ['id' => $article->getId(),
                'nom' => $article->getArticle()->getNom(),
                'description' => $article->getArticle()->getDescription(),
                'unit' => $article->getArticle()->getUnite(),
                'theme' => $article->getArticle()->getTheme()->getNom()]);
        }

        return new JsonResponse($articlesList);
    }
}