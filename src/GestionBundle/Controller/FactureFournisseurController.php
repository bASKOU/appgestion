<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\EstimationFournisseur;
use GestionBundle\Entity\FactureFournisseur;
use GestionBundle\Form\CreateCommandeFournisseurType;
use GestionBundle\Form\EditEstimationFournisseurType;
use GestionBundle\Form\EstimationFournisseurType;
use GestionBundle\Form\FactureFournisseurChaineType;
use GestionBundle\Form\FactureFournisseurType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FactureFournisseurController extends Controller
{
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;

    /**
     * FactureFournisseurController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode utilisé pour calculé le montant restant d'une commande de materiel
     * @param $existingFacture
     * @param $totalHT
     * @return int
     */
    private function ramainingTotalCommande($existingFacture, $totalHT)
    {
        if (!empty($existingFacture[0])) {
            $tempTotal = 0;
            foreach ($existingFacture as $exist) {
                if ($exist->getValidation() == 1) {
                    $tempTotal += $exist->getTotalHT();
                }
            }
            $totalHT -= $tempTotal;
        }

        return $totalHT;
    }

    /**
     * Méthode pour envoyer le formulaire de saisie de la facture par le service administratif
     * @param Request $request
     * @param $commandeId
     * @return JsonResponse|Response
     */
    public function saisieAdministrativeAction(Request $request, $commandeId)
    {
        $em = $this->getDoctrine()->getManager();
        $facture = new FactureFournisseur(); // Instanciation d'une nouvelle facture de fournisseur
        // Récupération de la commande de rattachement de la facture
        $commandeFournisseur = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);
        // Récupération de l'affaire de la commande si spécifié
        $commandeFournisseur->getAffaire() ? $facture->setAffaire($commandeFournisseur->getAffaire()): $facture->setAffaire(null);
        // On récupère les données directement de la commande
        $facture->setAdresseFacturation($commandeFournisseur->getAdresseFacturation());
        $facture->setAdresseLivraison($commandeFournisseur->getAdresseLivraison());
        $facture->setUser($commandeFournisseur->getUser());
        $facture->setFournisseur($commandeFournisseur->getFournisseur());
        $facture->setContact($commandeFournisseur->getContact());
        $facture->setEtat(0); // L'etat de la facture est mise en attente de validation
        $facture->setValidation(0); // La facture doit être validé par le CP
        $facture->setDateSaisie(new \DateTime('now')); // La date de saisie de la facture par le service administratif
        $facture->setCommandeFournisseur($commandeFournisseur);
        $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFactureRecus()); // On génère un numéro de facture propre à sudalys
        // On récupère les factures validé déja existante
        $existingFacture = $this->getDoctrine()->getRepository(FactureFournisseur::class)->findBy(['commandeFournisseur' => $commandeFournisseur, 'validation' => 1]);
        $totalHT = $this->ramainingTotalCommande($existingFacture, $commandeFournisseur->getTotalHT()); // On récupère le total des factures valide
        // Si le total retourné est à 0
        if ($totalHT === 0) {
            $commandeFournisseur->setEtat(2); // Modification de l'état à cloturé
            $em->persist($commandeFournisseur);
            $em->flush();
            // Affichage du message indiquant que la commande est cloturé
            $this->addFlash('warning', 'La commande fournisseur est clôturé, impossible de créer une nouvelle facture depuis cette commande');
            // Redirection vers la page de gestion des commande de matériel
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . "#commandeMateriel");
        }
        // Sinon on continue
        $facture->setTotalHT($totalHT);

        $form = $this->createForm(FactureFournisseurType::class, $facture);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $code = 200;
            $message = '';
            $status = 'error';
            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $fileName = $safeFilename . '-' . uniqid() . '.pdf';
                $file->move(
                    $this->getParameter('pdf_facture_directory'), $fileName
                );
                return new JsonResponse($fileName);
            } else {
                $facture->setTotalTTC($facture->getTotalHT() * (1 + $facture->getTVA()));
                $em->persist($facture);
                try {
                    $em->flush();
                    $url = $this->getParameter('adresse_web');
                    $status = 'success';
                    $message = "La saisie de la facture a bien été enregistré.";
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:factureFournisseurValidation.html.twig', [
                        'facture' => $facture,
                        'url' => $url
                    ]);
                    $response = $mail->sendWithoutFlash($html, $facture->getAffaire()->getAttributions()[0]->getUser()->getEmail(), "Gest'it - Demande de validation facture reçus");
                    if ($response['status'] === 'success') {
                        $message .= ' Un mail de demande de validation a été envoyé au chef de projet';
                    } else {
                        $status = 'error';
                        $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet";
                    }
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'facture de matériels');
                }

                return new JsonResponse(['status' => $status, 'message' => $message], $code);
            }
        }

        return $this->render('GestionBundle:Default:createSaisieFacture.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour visualiser la facture fournisseur avec la commande pour la valider ou la refuser
     * @param $factureId
     * @return Response
     */
    public function viewAction($factureId)
    {
        $factureFournisseur = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);
        $cmd = $factureFournisseur->getCommandeFournisseur();

        $user = $this->getUser();

        $form = $this->createForm(CreateCommandeFournisseurType::class, $cmd, ['role' => $user->getRoles()]);

        return $this->render('GestionBundle:Default:viewValideFactureFournisseur.html.twig', [
            'form' => $form->createView(),
            'commandeFournisseur' => $cmd,
            'view' => 'yes',
            'duplicate' => 'no',
            'user' => $user,
            'factureFournisseur' => $factureFournisseur
        ]);
    }

    /**
     * Methode pour valider une facture fournisseur
     * @param Request $request
     * @param $factureId
     * @return RedirectResponse
     */
    public function valideAction(Request $request, $factureId)
    {
        $status = 'danger';
        $message = '';

        $factureFournisseur = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);

        $factureFournisseur->setValidation(1);
        $factureFournisseur->setDateValidation(new \DateTime('now'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($factureFournisseur);
        try {
            $em->flush();
            $message = "La facture a bien été validé.";
            $status = 'success';
            $url = $this->getParameter('adresse_web');
            $mail = $this->get('email.action.mailer');
            $html = $this->renderView('GestionBundle:Export:factureFournisseurValid.html.twig', [
                'facture' => $factureFournisseur,
                'url' => $url
            ]);
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), "Gest'it - Retour de validation d'une facture reçus");
            if ($response['status'] === 'success') {
                $message .= " Un email pour informer le service administratif a été envoyé";
            } else {
                $status = 'warning';
                $message .= " Une erreur s'est produite lors de l'envoi de l'email au service administratif";
            }
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            dump($e); die();
        }

        $this->addFlash($status, $message);

        return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $factureFournisseur->getAffaire()->getId()]) . '#factureMateriel');
    }

    /**
     * Methode pour refuser une facture fournisseur
     * @param Request $request
     * @param $factureId
     * @return RedirectResponse
     */
    public function refuseAction(Request $request, $factureId)
    {
        $status = 'danger';
        $message = '';
        $factureFournisseur = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);

        $factureFournisseur->setValidation(2);
        $factureFournisseur->setDateValidation(new \DateTime('now'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($factureFournisseur);

        try {
            $em->flush();
            $status = 'success';
            $message = "La facture a bien été refusé.";
            $url = $this->getParameter('adresse_web');
            $mail = $this->get('email.action.mailer');
            $html = $this->renderView('GestionBundle:Export:factureFournisseurValid.html.twig', [
                'facture' => $factureFournisseur,
                'url' => $url
            ]);
            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_administratif'), "Gest'it - Retour de validation d'une facture reçus");
            if ($response['status'] === 'success') {
                $message .= " Un email pour informer le service administratif a été envoyé";
            } else {
                $status = 'warning';
                $message .= " Une erreur s'est produite lors de l'envoi de l'email au service administratif";
            }
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
        }

        $this->addFlash($status, $message);

        return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $factureFournisseur->getAffaire()->getId()]) . '#factureMateriel');
    }

    /**
     * Méthode pour envoyer le formulaire de saisie de la facture par le service administratif
     * @param Request $request
     * @param $factureId
     * @return Response
     */
    public function createAfterCommandeAction(Request $request, $factureId)
    {
        $facture = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);
        $facture->setNumeroFacture($this->createNumber->generateFromLastNumberFactureRecus());

        $existingFacture = $facture->getCommandeFournisseur()->getFactureFournisseur();
        $totalHT = $this->ramainingTotalCommande($existingFacture, $facture->getCommandeFournisseur()->getTotalHT());

        $facture->setTotalHT($totalHT);

        $form = $this->createForm(FactureFournisseurChaineType::class, $facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = '';
            $status = 'danger';
            $em = $this->getDoctrine()->getManager();
            /** @var UploadedFile $file */
            $file = $request->files->get('gestionbundle_facturefournisseur')['documentAttache'];
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $fileName = $safeFilename . '-' . uniqid() . '.pdf';
            $file->move(
                $this->getParameter('pdf_facture_directory'), $fileName
            );
            $facture->setDocumentAttache($fileName);
            $facture->setTotalTTC($facture->getTotalHT() * (1 + $facture->getTVA()));
            $em->persist($facture);
            try {
                $url = $this->getParameter('adresse_web');
                $em->flush();
                $status = 'success';
                $message = "La saisie de la facture a bien été enregistré.";
                $mail = $this->get('email.action.mailer');
                $html = $this->renderView('GestionBundle:Export:factureFournisseurValidation.html.twig', [
                    'facture' => $facture,
                    'url' => $url
                ]);
                $response = $mail->sendWithoutFlash($html, $facture->getAffaire()->getAttributions()[0]->getUser()->getEmail(), "Gest'it - Demande de validation facture reçus");
                if ($response['status'] === 'success') {
                    $message .= ' Un mail de demande de validation a été envoyé au chef de projet';
                } else {
                    $status = 'warning';
                    $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet";
                }
                $cmd = $facture->getCommandeFournisseur();
                $cmd->setEtat(2);
                $em->persist($cmd);
                $em->flush();
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture de matériels');
                dump($e); die();
            }
            $this->addFlash($status, $message);

            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#facture');
        }

        return $this->render('GestionBundle:Default:createFactureFournisseurChaine.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour enregistrer le réglement d'une facture fournisseur
     * @param Request $request
     * @param $factureId
     * @return JsonResponse|Response
     */
    public function reglementAction(Request $request, $factureId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';

        $facture = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);
        $attacheDoc = $facture->getDocumentAttache();

        $form = $this->createForm(FactureFournisseurType::class, $facture, ['type' => 'reglement']);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $facture->setDocumentAttache($attacheDoc);
            $facture->setEtat(1);
            $em->persist($facture);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "Le réglement de la facture reçus a bien été enregistré.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }
        return $this->render('GestionBundle:Default:createSaisieFacture.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }

    /**
     * Méthode pour enregistrer le debit d'une facture fournisseur
     * @param Request $request
     * @param $factureId
     * @return JsonResponse|Response
     */
    public function debitAction(Request $request, $factureId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';

        $facture = $this->getDoctrine()->getRepository(FactureFournisseur::class)->find($factureId);
        $attacheDoc = $facture->getDocumentAttache();

        $form = $this->createForm(FactureFournisseurType::class, $facture, ['type' => 'debit']);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $facture->setDocumentAttache($attacheDoc);
            $facture->setEtat(2);
            $em->persist($facture);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "Le débit de la facture reçus a bien été enregistré.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'facture reçus');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }
        return $this->render('GestionBundle:Default:createSaisieFacture.html.twig', [
            'form' => $form->createView(),
            'facture' => $facture
        ]);
    }
}