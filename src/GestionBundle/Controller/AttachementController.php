<?php

namespace GestionBundle\Controller;

use DateTime;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\Attachement;
use GestionBundle\Entity\ArticleCommande;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\getStringBetween;
use GestionBundle\Entity\ArticleAttachement;
use GestionBundle\Form\CreateAttachementType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AttachementController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    public $exceptionHandler;
    /**
     * @var NumberCheckAndRenew
     */
    private $createNumber;

    /**
     * FactureController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode pour récupérer le reste des articles et générer la liste pour le template ou retourne qu'il ne rest plus d'article
     * @param $existingAttachements
     * @param $linkedArticleCommande
     * @return array|int|mixed
     */
    private function getRemainingArticles($existingAttachements, $linkedArticleCommande)
    {
        // initinialise des arrays vides
        $attachementArticle = [];
        $quantityOfArticlePerId = [];
        $remainingArticle = [];
        $sum = 0;
        if (!empty($existingAttachements)) {
            foreach ($existingAttachements as $key => $value) { // Push la liste des attachements déja effectué dans un array
                if ($value->getEtat() <= 1) {
                    array_push($attachementArticle, $value->getArticleAttachements()->toArray());
                }
            }
            // Pour chaque article dans un attachement on push la quantité et le type d'avancement dans un array à l'index de l'id de l'article
            foreach ($attachementArticle as $key => $articlesAttachement) {
                foreach ($articlesAttachement as $key => $articleCommande) {
                    // on vérifie si l'index exist déja
                    if (!empty($quantityOfArticlePerId[$articleCommande->getArticleCommande()->getArticleDevis()->getId()])) {
                        // Si oui on ajoute la quantité
                        $quantityOfArticlePerId[$articleCommande->getArticleCommande()->getArticleDevis()->getId()]['total'] += $articleCommande->getAvancement();
                        $quantityOfArticlePerId[$articleCommande->getArticleCommande()->getArticleDevis()->getId()]['typeAvancement'] = $articleCommande->getTypeAvancement();
                    } else { // Sinon on assigne la quantité
                        $quantityOfArticlePerId[$articleCommande->getArticleCommande()->getArticleDevis()->getId()]['total'] = $articleCommande->getAvancement();
                        $quantityOfArticlePerId[$articleCommande->getArticleCommande()->getArticleDevis()->getId()]['typeAvancement'] = $articleCommande->getTypeAvancement();
                    }
                }
            }
            // Pour chaque article dans la commande on crée un nouvel array avec les données nécessaire pour le template
            foreach ($linkedArticleCommande as $key => $article) {
                $remainingArticle[$key]['id'] = $article->getArticleDevis()->getId();
                $remainingArticle[$key]['articleCommandeId'] = $article->getId();
                $remainingArticle[$key]['nom'] = $article->getArticleDevis()->getArticle()->getNom();
                $remainingArticle[$key]['description'] = $article->getArticleDevis()->getDescription();
                $remainingArticle[$key]['unite'] = $article->getArticleDevis()->getArticle()->getUnite();
                $remainingArticle[$key]['montantUnite'] = $article->getOverrideMontantUnit();
                $remainingArticle[$key]['quantite'] = $article->getQuantite();
                $remainingArticle[$key]['montantTotalHT'] = $article->getMontantTotalHT();
                $remainingArticle[$key]['typeAvancement'] = @$quantityOfArticlePerId[$remainingArticle[$key]['id']]['typeAvancement'];
                // On vérifie le type d'attachement puis effectu le calcul nécessaire
                if (@$quantityOfArticlePerId[$remainingArticle[$key]['id']]['typeAvancement'] == 2) {
                    $remainingArticle[$key]['avancement'] = (100 - $quantityOfArticlePerId[$remainingArticle[$key]['id']]['total']);
                } else {
                    $remainingArticle[$key]['avancement'] = ($article->getQuantite() - @$quantityOfArticlePerId[$remainingArticle[$key]['id']]['total']);
                }
                $sum += $remainingArticle[$key]['avancement'];
            }
        } else {
            foreach ($linkedArticleCommande as $key => $article) {
                $remainingArticle[$key]['id'] = $article->getArticleDevis()->getId();
                $remainingArticle[$key]['articleCommandeId'] = $article->getId();
                $remainingArticle[$key]['nom'] = $article->getArticleDevis()->getArticle()->getNom();
                $remainingArticle[$key]['description'] = $article->getArticleDevis()->getDescription();
                $remainingArticle[$key]['unite'] = $article->getArticleDevis()->getArticle()->getUnite();
                $remainingArticle[$key]['montantUnite'] = $article->getOverrideMontantUnit();
                $remainingArticle[$key]['quantite'] = $article->getQuantite();
                $remainingArticle[$key]['montantTotalHT'] = $article->getMontantTotalHT();
                $remainingArticle[$key]['typeAvancement'] = null;
                $remainingArticle[$key]['avancement'] = $article->getQuantite();
                $sum += $remainingArticle[$key]['avancement'];
            }
        }
        if ($sum == 0) {
            return $sum;
        }
        return $remainingArticle;
    }

    private function clotureCommande($commande, $attachement, &$em)
    {
        // on récupère tous les articles de la commande que l'on veut attacher
        $linkedArticleCommande = $commande->getArticleCommandes();
        //on vient vérifier si il existe déjà des attachements pour cette commande :
        $existingAttachements = $commande->getAttachements()->toArray();
        $remainingArticle = $this->getRemainingArticles($existingAttachements, $linkedArticleCommande);
        if ($remainingArticle == 0.0 && $attachement->getCommande()->getEtat() == 1) {
            $attachement->getCommande()->setEtat(2);
            $em->persist($commande);
        }
    }

    /**
     * fonction qui va nous permettre de gérer à la fois la création et la modification des attachements
     * on reçoit en parametre un requete, l'id de l'affaire sur laquelle on travaille
     * l'id de la commande sur laquelle on veut générer l'attachement
     * l'id de l'attachement dans le cas d'une modification, null par défault
     * @param Request $request
     * @param $affaireId
     * @param $commandeId
     * @param null $attachementId
     * @param string $display
     * @return RedirectResponse|Response
     */
    public function manageAction(Request $request, $affaireId, $commandeId, $attachementId=null, $display='no')
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
        $activeTVA = $commande->getActiveTVA();
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $attachement = null;
        /**
         * si on a un id d'attachement en parametre, on est donc dans le cas d'une modification
         * dans ce cas je récupère l'attachement concerné ainsi que ses articles liés
         * sinon je créer un nouvel attachement
         */
        if($attachementId) {
            $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($attachementId);
            
            $linkedArticleAttachement = $this->getDoctrine()->getRepository(ArticleAttachement::class)->findBy([
                'attachement' => $attachementId
            ]);
            $numAttach = explode('-', $attachement->getNumeroAttachement())[1];
            $numAttach = trim($numAttach, '\0');
        } else {
            $attachement = new Attachement();
            $linkedArticleAttachement = null;
            $attachement->setClient($commande->getClient());
            $attachement->setContact($commande->getContact());
            $attachement->setAdresseFacturation($commande->getAdresseFacturation());
            $attachement->setAdresseLivraison($commande->getAdresseLivraison());
            $attachement->setActiveTVA($activeTVA);
            $attachement->setForfaitaire($commande->getForfaitaire());
            $attachement->setRemise($commande->getRemise());
            $attachement->setTVA($commande->getTVA());
            $attachement->setCommande($commande);
            $attachement->setNumeroAttachement($this->createNumber->numeroAttachement($commande->getId()));
            $numAttach = explode('-', $attachement->getNumeroAttachement())[1];
            $numAttach = trim($numAttach, '\0');
            $attachement->setMentions('Situation n° ' . $numAttach . ' : ' . $commande->getMentions() . ' [importé depuis la commande]');
        }

        $user = $this->getUser();

        // on récupère tous les articles de la commande que l'on veut attacher
        $linkedArticleCommande = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
            'commande' => $commandeId
        ]);
        //on vient vérifier si il existe déjà des attachements pour cette commande :
        $existingAttachements = $commande->getAttachements()->toArray();
        foreach ($existingAttachements as $key => $attachements) {
            if ($attachements->getEtat() == 0) {
                $existingAttachements[$key]->isValid = 0;
            } elseif ($attachements->getEtat() == 1 ) {
                $existingAttachements[$key]->isValid = 1;
            } elseif ($attachements->getEtat() == 3 ) {
                $existingAttachements[$key]->isValid = 3;
            } else {
                $existingAttachements[$key]->isValid = 2;
            }
        }
        if ($attachementId) {
            foreach ($existingAttachements as $key => $attach) {
                if ($existingAttachements[$key]->getId() === $attachement->getId()) {
                    unset($existingAttachements[$key]);
                }
            }
        }
        foreach ($existingAttachements as $key => $attach) {
            if ($existingAttachements[$key]->isValid == 0 || $existingAttachements[$key]->isValid > 1) {
                unset($existingAttachements[$key]);
            }
        }
        empty($notValidateAttachement[0]) ? $notValidateAttachement = null: "";
        $notFirstAttachement = false;
        if($existingAttachements != null || count($existingAttachements) != 0) {
            $notFirstAttachement = true;
        }
        // Vérifie si ce n'est pas le premier attachement
        $remainingArticle = $this->getRemainingArticles($existingAttachements, $linkedArticleCommande);
        if ($remainingArticle === 0) {
            $this->addFlash('warning', 'Il n\'y a plus d\'article disponible pour réaliser un attachement');
            return $this->redirectToRoute('sudalys_gestion_detail_affaire', ['affaireId' => $affaireId]);
        }

        // on créé le formulaire que l'on attache à un objet request afin de pouvoir gérer les données saisies
        $form_attachement = $this->createForm(CreateAttachementType::class, $attachement);
        $form_attachement->handleRequest($request);
        $status = "error";
        $message = "";
        /**
         * comme la plupart de nos formulaires, on récupère ici les informations via une requete AJAX de type POST
         * je vérifie donc avant de traiter les données, que ma requete est bien en AJAX et qu'elle a bien la méthode que j'attends
         */
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                // on récupère les données contenues dans ma requete
                $data = $request->request;

                // on récupère le tableau des données liées à l'attachement en lui-même
                $tempDataAttachement = $data->get('attachement');
                $dataAttachement = [];
                $getStringBetween = new getStringBetween('[', ']');
                foreach ($tempDataAttachement as $arr) {
                    $key = $getStringBetween->getString($arr['name']);
                    if ($arr['value'] != "") {
                        $dataAttachement[$key] = $arr['value'];
                    }
                }

                if ($dataAttachement['forfaitaire'] != 0) {
                    $avancement = $dataAttachement['tauxForfaitaire'];
                }
                unset($dataAttachement['tauxForfaitaire']);
                $forfaitaire = $dataAttachement['forfaitaire'];
                unset($tempDataAttachement);
                unset($dataAttachement['_token']);

                foreach ($dataAttachement as $key => $value) {
                    if ($key == 'affaire') {
                        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($value);
                        $attachement->setAffaire($affaire);
                    } else if ($key == 'client' || $key == 'adresseLivraison' || $key == 'adresseFacturation') {
                        $temp = 'set' . ucfirst($key);
                        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        $attachement->$temp($organisme);
                    } else if ($key == 'contact') {
                        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($value);
                        $attachement->setContact($contact);
                    } elseif ($key == 'TVA' || $key == 'remise') {
                        $temp = 'set' . ucfirst($key);
                        $attachement->$temp(((float)str_replace(',', '', $value) / 100));
                    } else if ($key == 'totalHT' || $key == 'totalTTC') {
                        $temp = 'set' . ucfirst($key);
                        $attachement->$temp((float)str_replace(',', '.', str_replace(' ', '', $value)));
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $attachement->$temp($value);
                    }
                }
                // on utilise les setter de la classe pour attribue nos données aux propriétés de notre attachement
                $attachement->setUser($commande->getUser());
                $attachement->setDateAttachement(new DateTime());
                $attachement->setEtat(0);
                // on persist notre attachement en l'état afin que Doctrine, en le transformant en entité, lui génère un id que l'on va pouvoir récupérer
                $em->persist($attachement);
                $attachementId = $attachement->getId();

                // on initialise une variable totalHT
                $totalHT = 0;

                // on récupère le tableau de données des articles de l'attachement
                $dataArticles = $data->get('articles');
                // on vient boucler sur le tableau des articles de l'attachment, à chaque itération :
                for($i = 0; $i < count($dataArticles); $i++) {
                    // on crée un nouvel article
                    $articleAttachement = new ArticleAttachement();

                    // on incrémente la valeur de notre variable totalHT par la valeur de l'avancement que l'on reçoit
                    $totalHT += (float)$dataArticles[$i]["montantHtAv"];

                    // on relie l'article attaché à un attachement et à un article commandé
                    $articleAttachement->setAttachement($this->getDoctrine()->getRepository(Attachement::class)->find($attachementId));
                    $articleAttachement->setArticleCommande($this->getDoctrine()->getRepository(ArticleCommande::class)->find($dataArticles[$i]["articleCommandeId"]));

                    // avant d'aller plus loin dans la création de l'article, on vérifie qu'il n'en existe pas déjà pour cet attachement, auquel cas on supprime les existants
                    $checkExisting = $this->getDoctrine()->getRepository(ArticleAttachement::class)->findBy([
                        'attachement' => $attachementId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                    if ($forfaitaire) {
                        $articleAttachement->setTypeAvancement(2);
                        $articleAttachement->setAvancement((float) str_replace(',', '.', $avancement));
                    } else {
                        // on vient setter les dernières valeurs de notre article attaché
                        $articleAttachement->setTypeAvancement((int)$dataArticles[$i]["typeAvancement"]);
                        $articleAttachement->setAvancement((float)str_replace(',', '.', $dataArticles[$i]["avancement"]));
                    }
                    $articleAttachement->setMontantHtAv((float)$dataArticles[$i]["montantHtAv"]);
                    $attachement->addArticleAttachement($articleAttachement);
                    // on utilise l'entity manager de Doctrine pour transformer notre article en entité Doctrine
                    $em->persist($articleAttachement);
                }
            } 

            // maitenant que l'on a la valeur finale du montantHT de notre attachement, on vient set la valeur sur notre attachement
            if ($forfaitaire == 0) {
                $attachement->setTotalHT($totalHT);
            }
            //on vient vérifier si il existe déjà des attachements pour cette commande :
            array_push($existingAttachements, $attachement);
            // on inscrit notre attachement en BDD
            try {
                $em->flush();
                $status = "success";
                $message = "Attachement enregistré avec succès";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'situation');
            }

            return new JsonResponse(['status' => $status, 'message' => $message]);
        }
        return $this->render('GestionBundle:Default:manageAttachement.html.twig', [
            'form_attachement' => $form_attachement->createView(),
            'user' => $user,
            'linkedArticleCommande' => $linkedArticleCommande,
            'remainingArticle' => $remainingArticle,
            'linkedArticleAttachement' => $linkedArticleAttachement,
            'affaireId' => $affaireId,
            'commandeId' => $commandeId,
            'commande' => $commande,
            'attachementId' => $attachementId,
            'existingAttachements' => $existingAttachements,
            'notFirstAttachement' => $notFirstAttachement,
            'affaire' => $affaire,
            'attachement' => $attachement,
            'display' => $display,
            'numSituation' => $numAttach
        ]);
    }

    /**
     * Fonction qui nous permet de modifier l'état d'un attachement via une requete AJAX
     * @param Request $request
     * @param $attachementId
     * @param $commandeId
     * @param $affaireId
     * @return JsonResponse
     */
    public function changeEtatAction(Request $request, $attachementId, $commandeId, $affaireId)
    {
        $status = "error";
        $message = "Mauvaise requête !";
        $code = 400;

        // on récupère l'attachement concerné via son id passée en parametre de l'url de requete
        $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($attachementId);
        $em = $this->getDoctrine()->getManager();

        /**
         * Si la requete est conforme, on récupère le flag que nous a envoyé la vue
         * en fonction du flag, on modifie l'état de l'attachement
         * on sauvegarde cette modification via le manager de doctrine
         * et on répond en JSON à notre vue
         */
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;
                if ($data->get("statut") == "accept") {
                    $attachement->setEtat(1);
                    $this->clotureCommande($attachement->getCommande(), $attachement, $em);
                } else {
                    $attachement->setEtat(2);
                    $tempNum = $attachement->getNumeroAttachement();
                    $arrNum = explode('-', $tempNum);
                    $arrNum[1] = '000';
                    $tempNum = join('-', $arrNum);
                    $attachement->setNumeroAttachement($tempNum);
                }

                $em->persist($attachement);
                try {
                    $code = 200;
                    $em->flush();
                    $status = "success";
                    $message = "Etat de l'attachement modifié";
                } catch (\Exception $e) {
                    $code = 500;
                    $message = 'Une erreur s\'est produite, veuillez réessayer...';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'attachement' => $attachement->getId()], $code);
    }
    /**
     * Méthode pour passer l'état du devis à en attente d'accord client
     * @param $attachementId
     * @return RedirectResponse
     */
    public function sentToCustomerAction($attachementId)
    {
        $em = $this->getDoctrine()->getManager();
        $attachement = $this->getDoctrine()->getRepository(Attachement::class)->find($attachementId);
        $attachement->setEtat(3);
        $em->persist($attachement);

        try {
            $em->flush();
            $this->addFlash('success', 'L\'envoie du devis au client a bien été enregistré.');
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $this->addFlash('danger', $this->exceptionHandler->exceptionHandler($error, 'devis'));
        }
        return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $attachement->getCommande()->getAffaire()->getId()]) . '#situation');
    }
}