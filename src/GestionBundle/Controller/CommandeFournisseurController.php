<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Attribution;
use GestionBundle\Entity\AvancementCommandeFournisseur;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\FactureFournisseur;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\ProduitCommandeFournisseur;
use GestionBundle\Entity\ProduitFournisseur;
use GestionBundle\Form\CreateCommandeFournisseurType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\UserEntity;

class CommandeFournisseurController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;

    /**
     * CommandeFournisseurController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode pour vérifier si le montant total de la commande respecte le plafond par opération du chef de projet pour l'affaire concerné
     * @param $totalHT
     * @param $affaire
     * @return RedirectResponse|bool
     */
    private function checkPlafondParOperation($totalHT, $affaire)
    {
        $user = $this->getUser();
        if (!in_array('ROLE_SUDALYS_ADMINISTRATIF', $user->getRolesForm($user->getRoles()))) {
            $attribution = $this->getDoctrine()->getRepository(Attribution::class)->findOneBy(['user' => $user, 'affaire' => $affaire]);
            if ($attribution) {
                if ($attribution->getPlafondOperation()) {
                    if ($attribution->getPlafondOperation() >= $totalHT) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                $this->addFlash('danger', "Vous n'êtes pas attribué à cette affaire!");
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        } else {
            return true;
        }
    }

    /**
     * Méthode utilisé pour vérifier si le plafond de l'affaire n'est pas dépassé
     * @param $totalHT
     * @param $affaire
     * @return bool
     */
    private function checkAffairePlafond($totalHT, $affaire)
    {
        $commandes = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findBy(['affaire' => $affaire, 'validation' => 1]);
        $plafond = $affaire->getPlafond();
        if ($plafond) {
            $actualPlafond = 0;
            foreach ($commandes as $commande) {
                $actualPlafond += $commande->getTotalHt();
            }
            $tempPlafond = $actualPlafond + $totalHT;
            if ($plafond) {
                if ($tempPlafond <= $plafond) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Méthode pour rediriger selon des critères
     * @param $commande
     * @return RedirectResponse
     */
    private function redirectIfAffaire($commande)
    {
        $affaire = $commande->getAffaire();
        if (in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles())) {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandeMateriel');
        }  elseif (in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#commandeMateriel');
        } elseif ($affaire) {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_user') . '#commandeMateriel');
        } else {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandeMateriel');
        }
    }

    /**
     * Méthode pour stringifier l'adresse de facturation
     * @return string
     */
    private function buildAdresseFacturation()
    {
        $entity = $this->getDoctrine()->getRepository(UserEntity::class)->findAll();
        $entity = $entity[0];
        $adresseFacturation = $entity->getNom() . ', ';
        $adresseFacturation .= $entity->getAdresse();
        if ($entity->getAdresse2()) {
            $adresseFacturation .= ', ' . $entity->getAdresse2();
        }
        $adresseFacturation .= ', ' . $entity->getCodePostal() . ' ' . $entity->getVille();

        return $adresseFacturation;
    }

    /**
     * Méthode pour créer une commande fournisseur
     * @param Request $request
     * @param null $affaireId
     * @param string $numCommande
     * @return Response|RedirectResponse
     */
    public function createAction(Request $request, $affaireId=null, $numCommande='no')
    {
        $user = $this->getUser();
        $cmd = new CommandeFournisseur();
        $adresseFacturation = $this->buildAdresseFacturation();
        $cmd->setAdresseFacturation($adresseFacturation);
        if ($affaireId) {
            $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
            $cmd->setAffaire($affaire);
            if (!$affaire->getRespectPlafond()) {
                $this->addFlash('warning', 'Le plafond de l\'affaire a été dépassé, la création d\'une commande de matériel est impossible');
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        } else {
            $affaire = null;
        }
        $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise());
        $form = $this->createForm(CreateCommandeFournisseurType::class, $cmd, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $message = null;
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandefournisseur'];
            $products = json_decode($data['listProducts'], true);

            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            $testPlafondOP = false;
            $testPlafondAffaire = false;
            if ($affaire) {
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);

                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) {
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(false);
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            if ($cmd->getNumeroCommandeFourn() != $this->createNumber->generateFromLastNumberCommandeEmise()) {
                $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise());
            }
            $cmd->setEtat(0);
            $cmd->setUser($this->getUser());
            $cmd->setDateCommandeFourn(new \DateTime('now'));
            $em->persist($cmd);
            foreach($products as $product) {
                $productCommandeFournisseur = new ProduitCommandeFournisseur();
                foreach($product as $key => $value) {
                    if ($key === 'produitFournisseur') {
                        $value = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $productCommandeFournisseur->$temp($value);
                        }
                    } else {
                        $produitFournisseur = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                        $productCommandeFournisseur->setProduitFournisseur($produitFournisseur);
                        if ($productCommandeFournisseur->getMontantUnit() != $produitFournisseur->getMontantUnit()) {
                            $produitFournisseur->setMontantUnit($productCommandeFournisseur->getMontantUnit());
                            $em->persist($produitFournisseur);
                        }
                    }
                }
                $productCommandeFournisseur->setDate(new \DateTime('now'));
                $productCommandeFournisseur->setCommandeFournisseur($cmd);
                $em->persist($productCommandeFournisseur);
            }
            try {
                $em->flush();
                $tempMessage = 'La commande fournisseur a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                if ($affaireId) {
                    if (!$testPlafondOP || !$testPlafondAffaire) {
                        $url = $this->getParameter('app_url');
                        $mail = $this->get('email.action.mailer');
                        if (!$testPlafondOP) {
                            $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Montant maximum autorisé d\'engagement dépassé');
                        }
                        if (!$testPlafondAffaire) {
                            $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                        }

                        if ($response['status'] === 'success') {
                            $message .= ' Un email a été envoyé à la direction pour validation.';
                        } else {
                            $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                        }
                    }
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_home');
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
                $this->addFlash('danger', $message);
                if ($affaireId) {
                    return $this->redirectToRoute('sudalys_gestion_continue_CMDE', ['affaireId' => $affaireId, 'numCommande' => $cmd->getNumeroCommandeFourn()]);
                } else {
                    return $this->redirectToRoute('sudalys_gestion_continue_CMDE_noaffaire', ['numCommande' => $cmd->getNumeroCommandeFourn()]);
                }
            }
        }

        return $this->render('GestionBundle:Default:createCommandeFournisseur.html.twig', [
            'form' => $form->createView(),
            'affaire' => $affaire,
            'user' => $user,
            'cmd' => $cmd,
            'numCommande' => $numCommande
        ]);
    }

    /**
     * Méthode pour modifier une commande fournisseur
     * @param Request $request
     * @param $commandeId
     * @return Response|RedirectResponse
     */
    public function modifyAction(Request $request, $commandeId)
    {
        $user = $this->getUser();

        $cmd = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);

        $cmd->getAffaire() ? $affaire = $cmd->getAffaire() : $affaire = null;

        if ($affaire) {
            if (!$affaire->getRespectPlafond()) {
                $this->addFlash('warning', 'Le plafond de l\'affaire a été dépassé, la création d\'une commande de matériel est impossible');
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        }

        $form = $this->createForm(CreateCommandeFournisseurType::class, $cmd, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = "";
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandefournisseur'];
            $products = json_decode($data['listProducts'], true);

            if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $user->getRoles())) {
                $cmd->setEtat(0);
                $cmd->setValidation(true);
            } else {
                $cmd->setEtat(0);
            }
            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            if ($cmd->getAffaire()) {
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);

                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) {
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    if (!in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                        $cmd->setValidation(false);
                    } else {
                        $cmd->setValidation(true);
                    }
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            $em->persist($cmd);
            $existingProduct = $cmd->getProduitCommandeFournisseur();
            foreach($products as $product) {
                if (!empty($product['prodId'])) {
                    $productCommandeFournisseur = $this->getDoctrine()->getRepository(ProduitCommandeFournisseur::class)->find($product['prodId']);
                    foreach ($existingProduct as $key => $prod) {
                        if ($prod->getId() == $product['prodId']) {
                            unset($existingProduct[$key]);
                        }
                    }
                } else {
                    $productCommandeFournisseur = new ProduitCommandeFournisseur();
                }
                unset($product['prodId']);
                foreach($product as $key => $value) {
                    if ($key === 'produitFournisseur') {
                        $value = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $productCommandeFournisseur->$temp($value);
                        }
                    } else {
                        $produitFournisseur = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                        $productCommandeFournisseur->setProduitFournisseur($produitFournisseur);
                    }
                }
                $productCommandeFournisseur->setDate(new \DateTime('now'));
                $productCommandeFournisseur->setCommandeFournisseur($cmd);
                $em->persist($productCommandeFournisseur);
            }
            foreach ($existingProduct as $prod) {
                $em->remove($prod);
            }
            try {
                $em->flush();
                $status = 'success';
                $tempMessage = 'La commande fournisseur a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                if ($cmd->getAffaire() and !in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                    if (!$testPlafondOP || !$testPlafondAffaire) {
                        $url = $this->getParameter('app_url');
                        $mail = $this->get('email.action.mailer');
                        if (!$testPlafondOP) {
                            $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Validation d\'une commande fournisseur - Plafond par opération');
                        }
                        if (!$testPlafondAffaire) {
                            $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                        }

                        if ($response['status'] === 'success') {
                            $message .= ' Un email a été envoyé à la direction pour validation.';
                        } else {
                            $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                        }
                    }
                } elseif ($cmd->getAffaire() && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                    if (!$testPlafondAffaire) {
                        $status = 'warning';
                        $message .= ' Le plafond de l\'affaire a été dépassé.';
                    }
                }
                if ( in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles()) && $this->getUser() != $cmd->getUser() && $cmd->getValidation() == 1 && $affaire) {
                    $url = $this->getParameter('app_url');
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:commandeFournValidation.html.twig', [
                        'commande' => $cmd,
                        'url' => $url
                    ]);
                    $response = $mail->sendWithoutFlash($html, $cmd->getUser()->getEmail(), 'Gest\'it - Retour de validation de votre commande');

                    if ($response['status'] === 'success') {
                        $message .= ' Un email a été envoyé au Chef de projet.';
                    } else {
                        $status = 'warning';
                        $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet.";
                    }
                }

                $this->addFlash($status, $message);

                return $this->redirectIfAffaire($cmd);

            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériel');
                $this->addFlash('danger', $message);
                return $this->redirectToRoute('sudalys_gestion_modify_CMDE', ['commandeId' => $cmd->getId()]);
            }
        }

        return $this->render('GestionBundle:Default:manageCommandeFournisseur.html.twig', [
            'form' => $form->createView(),
            'commandeFournisseur' => $cmd,
            'view' => 'no',
            'duplicate' => 'no',
            'user' => $user,
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour visualiser une commande fourniseeur
     * @param $commandeId
     * @return Response
     */
    public function viewAction($commandeId)
    {
        $user = $this->getUser();

        $commandeFournisseur = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);

        if ($commandeFournisseur->getAffaire()) {
            $affaire = $commandeFournisseur->getAffaire();
        } else {
            $affaire = null;
        }

        $form = $this->createForm(CreateCommandeFournisseurType::class, $commandeFournisseur);

        return $this->render('GestionBundle:Default:manageCommandeFournisseur.html.twig', [
            'form' => $form->createView(),
            'commandeFournisseur' => $commandeFournisseur,
            'view' => 'yes',
            'duplicate' => 'no',
            'user' => $user,
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour dupliquer une commande fournisseur
     * @param Request $request
     * @param $commandeId
     * @param null $affaireId
     * @return RedirectResponse|Response
     */
    public function duplicateAction(Request $request, $commandeId, $affaireId=null)
    {
        $user = $this->getUser(); // Stockage de l'utilisateur dans une variable
        // Récupère la commande fournisseur à dupliquer
        $oldCmd = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);
        if ($oldCmd->getAffaire()) {
            $affaire = $oldCmd->getAffaire();
            $affaireId = $oldCmd->getAffaire()->getId();
        } else {
            $affaire = null;
        }
        $cmd = clone $oldCmd;
        unset($oldCmd); // Supprime l'ancienne commande fournisseur
        $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise()); // Génère le nouveau numéro de commande fournisseur

        $form = $this->createForm(CreateCommandeFournisseurType::class, $cmd); // Génération du formulaire
        $form->handleRequest($request); // Ajout le request handler

        if ($form->isSubmitted() && $form->isValid()) { // Lorsque le formulaire est soumis et valid
            $message = null; // Initialise message à null
            $em = $this->getDoctrine()->getManager(); // invoque le manageur d'entité
            $data = $_POST['gestionbundle_commandefournisseur']; // Stock les données reçus dans une variable
            unset($_POST['gestionbundle_commandefournisseur']); // Supprime les données reçus de la super globale POST
            $products = json_decode($data['listProducts'], true); // Stock et decode la liste des produits reçus
            unset($data['listProducts']); // Supprime la liste des produits en json

            $cmd->setEtat(0); // Set l'état à 0
            $cmd->setUser($this->getUser()); // Set le créateur de la commande fourniseeur
            $cmd->setDateCommandeFourn(new \DateTime('now')); // Set la date de création
            // Supprime les index et leur données inutile au set dynamique
            unset($data['_token']);
            unset($data['Enregistrer']);
            unset($data['id']);
            // Boucle dans la liste des articles
            foreach($data as $key => $value) {
                // Suite de condition pour affecter les entités en relation
                if ($key == 'affaire') {
                    if ($value!= "") {
                        $cmd->setAffaire($this->getDoctrine()->getRepository(Affaire::class)->find($value));
                        if ($value != $affaireId) { // Modifie le numéro de commande fournisseur si affaire différente de la commande originale
                            $cmd->setNumeroCommandeFourn($this->numeroCommande($value));
                        }
                    } else {
                        $cmd->setAffaire(null);
                    }
                } else if ($key == 'fournisseur') {
                    $cmd->setFournisseur($this->getDoctrine()->getRepository(Organisme::class)->find($value));
                } else if ($key == 'contact') {
                    $cmd->setContact($this->getDoctrine()->getRepository(Contact::class)->find($value));
                } else if ($key == 'commande') {
                    $cmd->setCommande($this->getDoctrine()->getRepository(Commande::class)->find($value));
                } else {
                    if ($value != "") {
                        $temp = 'set' . ucfirst($key);
                        $cmd->$temp($value);
                    }
                }
            }
            // Stock l'affaire dans une variable pour limité l'utilisation de doctrine
            $affaire = $cmd->getAffaire();
            $testPlafondOP = true;
            $testPlafondAffaire = true;
            $cmd->setTotalHT(str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC(str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            if ($affaireId) {
                // Test les plafonds
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);
                // Set la validation et le respet du plafond selon si les plafonds sont respecté ou non
                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) { // Si le plafond de l'affaire est dépassé on modifie l'affaire
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(false);
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise()); // Génère le nouveau numéro de commande fournisseur
            $em->persist($cmd);

            foreach ($products as $product) {
                $productCommandeFournisseur = new ProduitCommandeFournisseur();
                unset($product['prodId']);
                foreach ($product as $key => $value) {
                    if ($key === 'id') {
                        $value = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                        $productCommandeFournisseur->setProduitFournisseur($value);
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $productCommandeFournisseur->$temp($value);
                    }
                }
                $productCommandeFournisseur->setDate(new \DateTime('now'));
                $productCommandeFournisseur->setCommandeFournisseur($cmd);
                $em->persist($productCommandeFournisseur);
            }
            try {
                $em->flush();
                $tempMessage = 'La commande fournisseur a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }

                if (!$testPlafondOP || !$testPlafondAffaire) {
                    $url = $this->getParameter('app_url');
                    if (!$testPlafondOP) {
                        $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                            'user' => $cmd->getUser(),
                            'commande' => $cmd,
                            'url' => $url
                        ]);

                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Validation d\'une commande de matériels - Plafond par opération');
                    }
                    if (!$testPlafondAffaire) {
                        $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                            'user' => $cmd->getUser(),
                            'commande' => $cmd,
                            'url' => $url
                        ]);
                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                    }
                    if ($response['status'] === 'success') {
                        $message .= ' Un email a été envoyé à la direction pour validation.';
                    } else {
                        $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                    }
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_dashboard_administratif_achat');
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériel');
                $this->addFlash('danger', $message);
                return $this->redirectToRoute('sudalys_gestion_continue_CMDE', ['affaireId' => $cmd->getAffaire()->getId(), 'numCommande' => $cmd->getNumeroCommandeFourn()]);
            }
        }

        return $this->render('GestionBundle:Default:manageCommandeFournisseur.html.twig', [
            'form' => $form->createView(),
            'commandeFournisseur' => $cmd,
            'view' => 'no',
            'duplicate' => 'yes',
            'user' => $user,
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour refuser une commande fournisseur
     * @param Request $request
     * @param $commandeId
     */
    public function refusAction(Request $request, $commandeId)
    {
        $status = 'danger';
        $message = "Une erreur c'est produite, le refus n'a pas été enregistré.";
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);

        $commande->setValidation(2);
        $em->persist($commande);
        try {
            $em->flush();
            $message = 'Refus de la commande matériel enregistré.';
            $status = 'success';
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
            $status = 'warning';
        }

        $this->addFlash($status, $message);

        if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandeMateriel');
        } elseif ($this->getUser()->getRoles()[0] === 'ROLE_SUDALYS_ADMINISTRATIF') {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#commandeMateriel');
        } else {
            return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire') . '#commandeMateriel');
        }

    }

    /**
     * Méthode pour valider une commande fournisseur
     * @param Request $request
     * @param $commandeId
     * @return RedirectResponse
     */
    public function acceptAction(Request $request, $commandeId)
    {
        $status = 'danger';
        $message = '';
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);
        $commande->setValidation(1);
        $em->persist($commande);
        try {
            $em->flush();
            $message = 'La validation de la commande matériel enregistré.';
            $status = 'success';
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
            $status = 'warning';
        }

        $this->addFlash($status, $message);

        return $this->redirectIfAffaire($commande);
    }

    /**
     * Méthode pour enregistré l'envoi de la commande au founisseur
     * @param $commandeId
     */
    public function sendToFournisseurAction($commandeId)
    {
        $message = '';
        $status = 'warning';
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);
        $commande->setEtat(1);
        $commande->setDateEnvoi(new \DateTime('now'));
        $em->persist($commande);
        try {
            $em->flush();
            $status = 'success';
            $message = "L'envoi de la commande au fournisseur a bien était enregistré";
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
        }

        $this->addFlash($status, $message);

        return $this->redirectIfAffaire($commande);
    }

    /**
     * Méthode pour abandonner une commande au founisseur
     * @param Request $request
     * @param $commandeId
     * @return JsonResponse
     */
    public function abandonAction(Request $request, $commandeId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';
        if ($request->isMethod('POST')) {
            $commande = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->find($commandeId);
            $em = $this->getDoctrine()->getManager();
            $commande->setEtat(2);
            $commande->setRaisonAbandon($request->request->get('raisonAbandonCommandeMateriel'));
            $factureFournisseur = $commande->getFactureFournisseur()->toArray();
            $totalFactureFournisseur = 0;
            if (!empty($factureFournisseur[0])) {
                foreach ($factureFournisseur as $facture) {
                    $totalFactureFournisseur += $facture->getTotalHT();
                }
            }
            $totalAvancement = 1;
            $avancementFournisseur = $commande->getAvancement()->toArray();
            if ($totalFactureFournisseur > 0) {
                if (!empty($avancementFournisseur[0])) {
                    $totalAvancement = 0;
                    foreach ($avancementFournisseur as $avancement) {
                        $avancement->setAvancement(($avancement->getTotalHT() / $totalFactureFournisseur));
                        $em->persist($avancement);
                        $totalAvancement += $avancement->getAvancement();
                    }
                }
            } else {
                if (!empty($avancementFournisseur[0])) {
                    $totalAvancement = 0;
                    foreach ($avancementFournisseur as $avancement) {
                        $totalFactureFournisseur += $avancement->getTotalHT();
                    }
                    foreach ($avancementFournisseur as $avancement) {
                        $avancement->setAvancement(($avancement->getTotalHT() / $totalFactureFournisseur));
                        $em->persist($avancement);
                        $totalAvancement += $avancement->getAvancement();
                    }
                }
            }
            if ($totalAvancement < 1) {
                foreach ($factureFournisseur as $facture) {
                    $date = strtotime($facture->getDateSaisie()->format('Y-m'));
                    $flag = false;
                    foreach ($avancementFournisseur as $avancement) {
                        if ($date === strtotime($avancement->getDate()->format('Y-m'))) {
                            $avancement->setTotalHT($facture->getTotalHT());
                            $avancement->setAvancement($facture->getTotalHT() / $totalFactureFournisseur);
                            $em->persist($avancement);
                            $flag = true;
                        }
                    }
                    if ($flag === false) {
                        $avancement = new AvancementCommandeFournisseur();
                        $avancement->setDate($facture->getDateSaisie());
                        $avancement->setDateSaisie(new \DateTime('today'));
                        $avancement->setTotalHT($facture->getTotalHT());
                        $avancement->setAvancement($facture->getTotalHT() / $totalFactureFournisseur);
                        $avancement->setCommandeFournisseur($facture->getCommandeFournisseur());
                        $avancement->setValidation(1);
                        $em->persist($avancement);
                        dump($avancement);
                    }
                }
            }
            $commande->setTotalHT($totalFactureFournisseur);
            $commande->setMontantTotalTTC($totalFactureFournisseur * (1 + $commande->getTVA()));
            $em->persist($commande);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "L'abandon de la commande de matériel a bien été enregistré.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }


    /**
     * Méthode pour créer une commande fournisseur
     * @param Request $request
     * @param string $numCommande
     * @return Response|RedirectResponse
     */
    public function createWithFactureAction(Request $request, $numCommande = 'no')
    {
        $user = $this->getUser();
        $cmd = new CommandeFournisseur();
        $adresseFacturation = $this->buildAdresseFacturation();
        $cmd->setAdresseFacturation($adresseFacturation);
        $affaire = null;
        $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise());
        $form = $this->createForm(CreateCommandeFournisseurType::class, $cmd, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $message = null;
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandefournisseur'];
            $products = json_decode($data['listProducts'], true);

            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            $cmd->setRespectPlafond(true);
            $cmd->setValidation(true);
            if ($cmd->getNumeroCommandeFourn() != $this->createNumber->generateFromLastNumberCommandeEmise()) {
                $cmd->setNumeroCommandeFourn($this->createNumber->generateFromLastNumberCommandeEmise());
            }
            $cmd->setEtat(1);
            $cmd->setUser($this->getUser());
            $cmd->setDateCommandeFourn(new \DateTime('now'));
            $em->persist($cmd);
            foreach($products as $product) {
                $productCommandeFournisseur = new ProduitCommandeFournisseur();
                foreach($product as $key => $value) {
                    if ($key === 'produitFournisseur') {
                        $value = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $productCommandeFournisseur->$temp($value);
                        }
                    } else {
                        $produitFournisseur = $this->getDoctrine()->getRepository(ProduitFournisseur::class)->find($value);
                        $productCommandeFournisseur->setProduitFournisseur($produitFournisseur);
                        if ($productCommandeFournisseur->getMontantUnit() != $produitFournisseur->getMontantUnit()) {
                            $produitFournisseur->setMontantUnit($productCommandeFournisseur->getMontantUnit());
                            $em->persist($produitFournisseur);
                        }
                    }
                }
                $productCommandeFournisseur->setDate(new \DateTime('now'));
                $productCommandeFournisseur->setCommandeFournisseur($cmd);
                $em->persist($productCommandeFournisseur);
            }
            $factureFournisseur = new FactureFournisseur();
            $factureFournisseur->setAffaire($cmd->getAffaire());
            $factureFournisseur->setContact($cmd->getContact());
            $factureFournisseur->setUser($cmd->getUser());
            $factureFournisseur->setCommandeFournisseur($cmd);
            $factureFournisseur->setDateSaisie(new \DateTime('now'));
            $factureFournisseur->setTotalHT($cmd->getTotalHT());
            $factureFournisseur->setTotalTTC($cmd->getMontantTotalTTC());
            $factureFournisseur->setTVA($cmd->getTVA());
            $factureFournisseur->setAdresseLivraison($cmd->getAdresseLivraison());
            $factureFournisseur->setAdresseFacturation($cmd->getAdresseFacturation());
            $factureFournisseur->setMentions($cmd->getMentions());
            $factureFournisseur->setEtat(0);
            $factureFournisseur->setValidation(0);
            $factureFournisseur->setFournisseur($cmd->getFournisseur());
            $em->persist($factureFournisseur);
            try {
                $em->flush();
                $tempMessage = 'La facture et la commande de matériel ont bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_chain_factureFournisseur', ['factureId' => $factureFournisseur->getId()]);
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
                $this->addFlash('danger', $message);
            }
        }

        return $this->render('GestionBundle:Default:createCommandeFournisseur.html.twig', [
            'form' => $form->createView(),
            'affaire' => $affaire,
            'user' => $user,
            'cmd' => $cmd,
            'numCommande' => $numCommande,
            'show' => true
        ]);
    }

    /**
     * Méthode pour récupérer la liste des commandes qui non pas encore de facture ou des factures déjà validé et réglé
     * @return JsonResponse
     */
    public function getCommandeListAction()
    {
        $commandes = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findBy(['etat' => 1]);
        $listeCommande = [];
        $i = 0;
        foreach ($commandes as $key => $commande) {
            $test = false;
            foreach ($commande->getFactureFournisseur() as $facture) {
                if ($facture->getEtat() == 0 && $facture->getValidation() <= 1) {
                    $test = true;
                }
            }
            if ($test) {
                unset($commandes[$key]);
            } else {
                $listeCommande[$i++] = ['id' => $commande->getId(), 'mention' => $commande->getMentions(), 'numero' => $commande->getNumeroCommandeFourn()];
            }
        }

        return new JsonResponse($listeCommande);
    }

}