<?php

namespace GestionBundle\Controller;

use GestionBundle\Form\CreateAttributionAutoType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Attribution;
use GestionBundle\Form\CreateAttributionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Classe qui permet à un admin de gérer les attributions
 * Une attribution est l'action qui permet à un admin de donner à un user particulier l'accès à une affaire donnée
 */
class AttributionController extends Controller
{
    public $exceptionHandler = "";
    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }
    /**
     * Fonction qui va permettre, sur requete de l'utilisateur, de créer une nouvelle attribution
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        // on instancie une nouvelle attribution via la classe
        $attribution = new Attribution();
        // on créé le formulaire à partit de son modèle type
        $form = $this->createForm(CreateAttributionType::class, $attribution);
        // on lui attache l'objet request afin de pouvoir récupérer la data dans mon controller
        $form->handleRequest($request);

        /**
         * on check si le form est conforme
         * on transforme l'objet issu du form en entité doctrine via son Manager
         * une fois l'opération terminée on redirige l'utilisateur vers la page d'aceuil de l'appli
         */
        if($form->isSubmitted() && $form->isValid()) {
            $attribution = $form->getData();
            $attribExist = $this->getDoctrine()->getRepository(Attribution::class)->findOneBy(['user' => $attribution->getUser(), 'affaire' => $attribution->getAffaire()]);
            if (!$attribExist) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($attribution);
                $em->flush();
                $html = $this->renderView('@Gestion/Export/newAttribution.html.twig', [
                    'attribution' => $attribution,
                    'url' => $this->getParameter('app_url'),
                ]);
                $mail = $this->get('email.action.mailer');
                $response = $mail->sendWithoutFlash($html, $attribution->getUser()->getEmail(), 'Gest\'it - Attribution d\'une affaire');
                if ($response['status'] === 'success') {
                    $this->addFlash('success', 'Attribution effectuée, un email informant le chef de projet de son attribution à l\'affaire n° ' . $attribution->getAffaire()->getNumeroAffaire() . ' lui a était envoyé.');
                } else {
                    $this->addFlash('warning', 'Une erreur s\'est produite lors de l\'envoi du mail pour informer le chef de projet de son attribution à l\'affaire. Contactez le chef de projet pour l\'en informer.');
                }
            } else {
                $this->addFlash('warning', 'Cette utilisateur est déjà attribué à cette affaire.');
            }

            return $this->redirectToRoute('sudalys_gestion_home');
        }

        // on rend la vue au naviguateur en y attachant l'instance de notre formulaire
        return $this->render('GestionBundle:Default:createAttribution.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Quand une affaire est générée depuis un devis orphelin accepté, je dois créer automatiquement une nouvelle attribution
     * pour cette affaire et cet utilisateur
     * J'utilise donc cette fonction afin de générer mon attribution auto
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse|RedirectResponse|Response
     */
    public function autoAction(Request $request, $affaireId)
    {
        // on instancie une nouvelle attribution via la classe
        $attribution = new Attribution();
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $attribution->setAffaire($affaire);
        // on créé le formulaire à partit de son modèle type
        $form = $this->createForm(CreateAttributionAutoType::class, $attribution);
        // on lui attache l'objet request afin de pouvoir récupérer la data dans mon controller
        $form->handleRequest($request);

        /**
         * on check si le form est conforme
         * on transforme l'objet issu du form en entité doctrine via son Manager
         * une fois l'opération terminée on redirige l'utilisateur vers la page d'aceuil de l'appli
         */
        if($form->isSubmitted() && $form->isValid()) {
            $attribution = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($attribution);
            try {
                $em->flush();
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'attribution');
                return new JsonResponse(['message' => $message]);
            }

            $this->addFlash('success', "L'affaire a bien était créée à partir du devis, vous pouvez maintenant enregistrer la commande");
            $indexOnglet = 'CMDR';
            return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $affaireId, 'indexOnglet' => $indexOnglet]));
        }

        // on rend la vue au naviguateur en y attachant l'instance de notre formulaire
        return $this->render('GestionBundle:Default:createAttributionAuto.html.twig', [
            'form' => $form->createView(),
            'affaireId' => $affaireId
        ]);
    }

    
}