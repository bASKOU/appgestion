<?php

namespace GestionBundle\Controller;

use DateTime;
use GestionBundle\Entity\ArticleAffaire;
use GestionBundle\Entity\ArticleCommande;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Article;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\Attribution;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Form\CreateDevisType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use GestionBundle\Services\NumCMDR;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DevisController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    public $exceptionHandler; // Propriété qui va contenir le service de gestio, des erreurs
    /**
     * @var NumberCheckAndRenew
     */
    public $createNumber;

    /**
     * DevisController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler(); // invocation du service de gestion des erreurs au chargement de la classe
        $this->createNumber = $numberCheckAndRenew;
    }
    /**
     * Méthode qui va nous permettre de calculer le montant TTC d'un devis en lui passsant en paramètre le montant HT, un éventuelle remise ainsi que le taux de TVA
     * remise et TVA sont ici en pourcentages
     * @param $totalHT
     * @param $remise
     * @param float $TVA
     * @return float|int
     */
    protected function calculateTotal($totalHT, $remise, $TVA = 0.20) {
        $prixRemise = ($totalHT - ($totalHT * $remise));
        return ($prixRemise + ($prixRemise * $TVA));
    }

    private function getStringBetween($string)
    {
        $start = '[';
        $end = ']';
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    /**
     * Fonction qui va nous permettre de gérer à la fois la création et la modification d'une devis, avec en parametre :
     * l'objet de la requete, l'id de l'affaire sur laquelle on travail et l'id du devis dans le cas d'une modification, null par défaut
     * @param Request $request
     * @param null $affaireId
     * @param null $devisId
     * @param null $duplicate
     * @param null $display
     * @param null $numDevis
     * @return Response
     */
    public function manageAction(Request $request, $affaireId=null, $devisId=null, $duplicate=null, $display=null, $numDevis=null)
    {
        // Initialisation des variables
        $duplicateDevis = false;
        $affaire = null;
        $listedThemes = [];
        $linkedArticleDevisTheme = [];
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $role = $user->getRoles();
        $linkedArticleDevis = null;
        $listeArticles = [];
        $plafond = null;
        $userPlafond = null;
        $nomAffaire = null;
        if ($numDevis === null) { // Pour la vue, changement de numDevis en no pour qu'il soit lisible en JS
            $numDevis = 'no';
        } else { // Sinon on récupère le devis par son numéro de devis si il exist
            $devis = $this->getDoctrine()->getRepository(Devis::class)->findOneBy(['numeroDevis' => $numDevis]);
            if ($devis) {
                if ($devis->getAffaire()) { // Si le devis est rattaché à une affaire on récupère l'id de l'affaire
                    $affaireId = $devis->getAffaire()->getId();
                }
                $devis = ""; // Puis on reset la variable devis
            }
        }
        if ($duplicate) { // SI duplication d'un devis
            $oldDevis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId); // on récupère de l devis à dupliquer
            $oldDevis->clearId(); // On supprime l'id du devis
            // On récupère la liste des articles rattachés au devis
            $linkedArticleDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                'devis' => $devisId
            ]);
            $devis = clone $oldDevis; // On clone le devis
            $devis->setNumeroDevis($this->createNumber->generateFromLastNumberDevis()); // On assigne un nouveau numéro de devis
            $duplicateDevis = true; // On change l'état de la variable
            $listeArticles = $linkedArticleDevis; // On remplis la variable
            $listedThemes = [];
            foreach ($listeArticles as $article) {
                if (!in_array($article->getArticle()->getTheme(), $listedThemes)) {
                    array_push($listedThemes, $article->getArticle()->getTheme());
                }
            }
            $affaire = $devis->getAffaire();
            if ($affaire) {
                $affaireId = $affaire->getId();
            }
        }
        /**
         * si on a un id de devis, on est dans le cas d'une modification, donc je récupère l'objet à modifier ainsi que ces articles liés
         * sinon je créé un nouvel objet à partir de la classe
         */
        else if($devisId) { // Si on edit un devis existant
            $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId); // On récupère le devis
            if ($devis) { // Si le devis exist
                /**
                 * Ici on vérifie si c'est une édition du devis ou un visionnement.
                 * En cas d'édition on vérifie que l'utilisateur à le droit d'éditer le devis si le devis est déja validé
                 */
                if ($devis->getValidation() == 1 && $this->getUser()->getRoles()[0] != 'ROLE_SUPER_SUDALYS' && !$display) {
                    $this->addFlash('warning', 'Le devis a été validé par le responsable, vous ne pouvez pas le modifier');
                    if ($affaireId) {
                        $url = $this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $affaireId]) . '#devis';
                    } else {
                        $url = $this->generateUrl('sudalys_gestion_home') . '#devis';
                    }
                    return $this->redirect($url);
                }
            } else { // Si le devis n'exist pas on informe que le devis n'exist pas et redirige vers la page home
                $this->addFlash('warning',"Aucun devis trouvé");

                return $this->redirectToRoute('sudalys_gestion_home');
            }
            // On récupère la liste des articles liés au devis
            $linkedArticleDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                'devis' => $devisId
            ]);
            $listeArticles = $linkedArticleDevis;
            $listedThemes = []; // Initialisation d'une variable de type array
            foreach ($listeArticles as $article) { // Boucle sur la liste des articles
                if (!in_array($article->getArticle()->getTheme(), $listedThemes)) { // Si le thème n'existe pas dans la liste de thème
                    array_push($listedThemes, $article->getArticle()->getTheme()); // On l'ajoute
                }
            }
            $numDevis = 'no';
        } else {
            $devis = new Devis();
            if ($numDevis != 'no') { // Si on résume la saisie d'un devis
                $numTemp = $this->createNumber->generateFromLastNumberDevis(); // On génère un nouveau numéro de devis
                if ($numDevis == $numTemp) { // On vérifie si il est identique au numéro que l'on fournis en argument
                    $devis->setNumeroDevis($numDevis); // On assigne le numéro fournis si identique
                } else {
                    $devis->setNumeroDevis($numTemp); // on assigne le nouveau numéro si différent
                    // On informe du changement de numéro
                    $this->addFlash('warning', 'Le numéro de devis à été changé car un autre devis avec le même numéro vient d\'être créé');
                }
            } else { // Sinon on assigne un numéro au devis
                $devis->setNumeroDevis($this->createNumber->generateFromLastNumberDevis());
            }
        }
        // un devis pouvant ne pas être lié à une affaire, on vérifie d'abord si on en reçoit un en paramètre
        if ($affaireId) {
            // si il y a bien une affaire liée à ce devis, on en extrait le plafond
            $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
            $adresseFacturation = $affaire->getAdresseFacturation(); // On récupère l'adresse de facturation par défaut de l'affaire
            $adresseLivraison = $affaire->getAdresseLivraison(); // On récupère l'adresse de livraisonpar défaut de l'affaire
            $organismeAffaire = $affaire->getOrganisme(); // On récupère l'organisme par défaut de l'affaire
            $articleAffaires = $this->getDoctrine()->getRepository(ArticleAffaire::class)->findBy(['affaire' => $affaire]); // On récupère la liste des articles liés à l'affaire
            $listeArticles = []; // Array vide d'article lié au devis
            $listedThemes = []; // Array vide de thèmes
            $devis->setAffaire($affaire); // On rattache le devis à l'affaire
            // Ici on vérifie si il y a bien une adresse de facturation par défaut et si le devis n'a pas d'adresse
            if ($adresseFacturation != null && $devis->getAdresseFacturation() == null) { // Si oui
                $devis->setAdresseFacturation($adresseFacturation); // On assigne au devis l'adresse de l'affaire
            }
            // Ici on vérifie si il y a bien une adresse de livraison par défaut et si le devis n'a pas d'adresse
            if ($adresseLivraison != null && $devis->getAdresseLivraison() == null) { // Si oui
                $devis->setAdresseLivraison($adresseLivraison); // On assigne au devis l'adresse de l'affaire
            }
            // ici on vérifie si l'affaire à un organisme par defaut et que le devis n'a pas de client (organisme)
            if ($organismeAffaire != null && $devis->getClient() == null) { // Si oui
                $devis->setClient($organismeAffaire); // On assigne l'organisme de l'affaire en client du devis
            }
            foreach ($articleAffaires as $article) { // Pour chaque article de l'affaire
                array_push($listeArticles, $article->getArticle()); // On ajoute à la liste des articles
                if (!in_array($article->getArticle()->getTheme(), $listedThemes)) { // Si le thème n'est pas présent dans la liste
                    array_push($listedThemes, $article->getArticle()->getTheme()); // on ajoute le thème
                }
            }
            // On récupère le nom de l'affaire
            $nomAffaire = $affaire->getDesignationAffaire();
            $devis->setValidation(1);
        }

        // on créé notre formulaire pour le devis et on lui indique de gérer les requêtes
        $form_devis = $this->createForm(CreateDevisType::class, $devis);
        $form_devis->handleRequest($request); // Ajout du gestionnaire de requête au formulaire

        /**
         * Dans notre cas, on ne post pas notre formulaire directement par le système intégré de Symfony, on passe par une requête AJAX
         * qui nous permet notamment de récupérer en un formulaire (et une requête) à la fois les données du formulaire pour le devis
         * ainsi que les données concernant les articles qui auront été ajoutés par l'utilisateur dans ce devis
         */
        if($request->isXmlHttpRequest()) {
            // on vérifie bien que le requete reçue est en AJAX et qu'elle est de type POST 
            if($request->isMethod('POST')) {
                if (!$devisId || $duplicate) {
                    // Vérifie si un devis avec le même numéro exist déjà
                    $numExist = $this->getDoctrine()->getRepository(Devis::class)->findBy(['numeroDevis' => $devis->getNumeroDevis()]);
                    // Si un devis comportant le même numéro vient d'être créer
                    if (!empty($numExist[0])) {
                        $devis->setNumeroDevis($this->createNumber->generateFromLastNumberDevis()); // On assigne un nouveau numéro de devis
                    }
                }

                // on récupère l'objet de la requête
                $data = $request->request;// on recupère le tableau des informations du devis
                // On récupère la data spécifique aux données du devis
                $tempDataDevis = $data->get('devis');
                $dataDevis = []; // Arrays vide
                foreach ($tempDataDevis as $arr) { // Pour chaque donné dans l'array
                    $key = $this->getStringBetween($arr['name']); // On récupère le nom du champs correspondant
                    if ($arr['value'] != "") { // Si la value n'est pas vide
                        $dataDevis[$key] = $arr['value']; // on insert dans le tableau
                    }
                }
                unset($tempDataDevis); // Suppression de la variable temporaire
                unset($dataDevis['_token']); // Suppression du token
                unset($dataDevis['targetST']); // Suppression d'une donnée non voulu
                /**
                 * Afin de dynamisé l'insertion, on boucle sur les données formaté telle qu'on les veux
                 * Une suite de condition permet de fournir à l'objet doctrine les données
                 */
                foreach ($dataDevis as $key => $value) {
                    if ($key == 'affaire') {
                        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($value);
                        $devis->setAffaire($affaire);
                    } elseif ($key == 'client' || $key == 'adresseLivraison' || $key == 'adresseFacturation') {
                        $temp = 'set' . ucfirst($key); // On crée une variable temporaire set + le nom du champ capitalisé
                        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        $devis->$temp($organisme); // on set la valeur
                    } elseif ($key == 'contact') {
                        $temp = 'set' . ucfirst($key);
                        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($value);
                        $devis->$temp($contact);
                    } elseif ($key == 'totalHT') {
                        $temp = 'set' . ucfirst($key);
                        $devis->$temp((float)str_replace(',', '.', str_replace(' ', '', $value)));
                    } elseif ($key == 'TVA' || $key == 'remise') {
                        $temp = 'set' . ucfirst($key);
                        $devis->$temp(((float)$value) / 100);
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $devis->$temp($value);
                    }
                }
                // On récupère la clef duplicate pour vérifier si le devis est un duplication on non
                $tempDup = $data->get('duplicate'); // On récupère la valeur de duplicate
                $askValidate = $data->get('validate'); // on récupère la valeur de validate pour vérifié si l'émetteur du devis demande une validation par la direction
                if (isset($tempDup)) { // Si duplication
                    $devisId = 0; // On set l'id du devis à zéro
                    $duplicate = true; // On change l'état de la variable duplicate
                }
                if (!$devis->getUser()) { // Si le devis n'a pas d'utilisateur
                    $devis->setUser($user); // On assigne l'utilisateur
                }
                if (!$devis->getDateDevis()) { // Si le devis n'a pas de date de création
                    $devis->setDateDevis(new DateTime()); // On assigne la date now
                } else { // Sinon
                    $devis->setDateEdit(new DateTime()); // on assigne une date d'édition
                }
                if($devisId && $devisId != 0) { // Si la varible devisId exist et n'est pas égale à zéro
                    $devis->setEtat(2); // on change l'état du devis a 2
                } else { // Sinon
                    $devis->setEtat(1); // à 1
                }
                /**
                 * Dans le cas ou un devis est dans une affaire en temps normal le devis est validé automatiquement
                 * aussi si la direction émet un devis il est validé automatiquement aussi
                 * mais pour un devis sans affaire le devis doit être validé par la direction
                 */
                if (($affaireId || $devis->getAffaire() || $role[0] === 'ROLE_SUPER_SUDALYS') && $askValidate != 1) { // Si le devis est émit dans une affaire, par la direction et sans demande de validation
                    $devis->setValidation(1); // On valide le devis
                    $devis->setEtat(4); // L'état du devis est en attende d'envoi au client
                } else { // Sinon
                    $devis->setValidation(0); // Le deivs est en attende de validation par la direction
                }

                /**
                 * on persist la première partie de notre entité devis
                 * dans le but d'en récupérer l'id dont on a besoin pour créer les articlesDevis
                 */
                $em->persist($devis);
                $devisId = $devis->getId(); // On récupère l'id de la future insertion qui va servir à liés les article au devis

                // on set une varibale totalHT pour pouvoir après l'ajouter à l'entité devis
                $totalHT = 0;
                // on récupère le tableau des différents articles ajoutés à ce Devis
                $dataArticles  = $data->get('articles');
                // on boucle sur ce tableau pour créer une entité articleDevis pour chaque entrée du tableau
                for($i = 0; $i < count($dataArticles); $i++) {
                    $articleDevis = new ArticleDevis(); // Création d'un nouvel artile devis

                    // on incrémente notre variable totalHT avec chaque montantTotalHT des articles
                    $totalHT += (float)$dataArticles[$i]["montantTotalHT"];

                    // on set les propriétés de chaque articleDevis que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                    $articleDevis->setDevis($this->getDoctrine()->getRepository(Devis::class)->find($devisId));
                    $articleDevis->setArticle($this->getDoctrine()->getRepository(Article::class)->find($dataArticles[$i]["articleId"]));

                    // on vient vérifier qu'il n'y a pas d'autres articles liés à ce devis, si c'est le cas on les supprime
                    $checkExisting = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                        'devis' => $devisId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }

                    // si on a un montant unitaire overridé, on l'ajoute à notre article
                    if((float)$dataArticles[$i]["overrideMontantUnit"] != null || (float)$dataArticles[$i]["overrideMontantUnit"] != "") {
                        $articleDevis->setOverrideMontantUnit((float)$dataArticles[$i]["overrideMontantUnit"]);
                    }
                    if ((int)$dataArticles[$i]["quantite"] == 0) {
                        $dataArticles[$i]["quantite"] = 1;
                    }
                    $articleDevis->setQuantite((int)$dataArticles[$i]["quantite"]);
                    $articleDevis->setMontantTotalHT((float)$dataArticles[$i]["montantTotalHT"]);
                    $articleDevis->setDescription(preg_replace('/\\t/', '',$dataArticles[$i]["description"]));

                    // on persist l'entité créee à chaque itération de la boucle
                    $em->persist($articleDevis);
                    if ($affaireId) {
                        $checkExistingArtAff = $this->getDoctrine()->getRepository(ArticleAffaire::class)->findOneBy(['article' => $articleDevis->getArticle()]);
                        if ($checkExistingArtAff === null) {
                            $newArticleAffaires = new articleAffaire();
                            $newArticleAffaires->setAffaire($affaire);
                            $newArticleAffaires->setArticle($articleDevis->getArticle());
                            $em->persist($newArticleAffaires);
                        }
                    }
                }
                if ((float)$totalHT > 0) {
                    // on set le totalHT du devis avec la valeur finale de notre variable après le process de la boucle
                    $devis->setTotalHT($totalHT * (1 - $devis->getRemise()));
                } else {
                    $totalHT = (float)$devis->getTotalHT();
                }
                // on vient calculer le totalTTC de notre devis en y appliquant la remise et le taux de TVA
                $totalTTC = $this->calculateTotal($totalHT, $devis->getRemise(), $devis->getTVA());
                // on set le totalTTC du devis avec la valeur retournée par notre fonction de calcul
                $devis->setTotalTTC($totalTTC);

                $code = 200;
                $message = '';
                $status = 'error';
                /**
                 * Gestion des erreurs d'inscription des entité doctrines
                 */
                try {
                    $em->flush(); // On lance les requêtes
                    $status = "success"; // on assigne la valeur au status
                    $sujet = ""; // Instanciation d'une variable vide
                    $message = "Devis N°: " . $devis->getNumeroDevis() . " enregistré avec succès."; // Message indiquant que le devis est bien enregistré en BDD
                    $html = null;
                    // Si le devis nécessite une validation par la direction
                    if ($devis->getValidation() == 0) {
                        if ($devis->getValidation() == 0) { // SI le devis n'est pas déja validé
                            if (!$affaireId) { // Pour un devis orphlin
                                // Création du body de l'email
                                $html = $this->renderView('GestionBundle:Export:orphanDevisValidation.html.twig', [
                                    'devis' => $devis,
                                    'url' => $this->getParameter('app_url'),
                                    'affaire' => null
                                ]);
                                $sujet = 'Gest\'it - Demande de validation d\'un devis '; // Assignation du sujet
                            } else { // Pour un devis avec demande de validation par la direction
                                // Création du body de l'email
                                $html = $this->renderView('GestionBundle:Export:orphanDevisValidation.html.twig', [
                                    'devis' => $devis,
                                    'url' => $this->getParameter('app_url'),
                                    'affaire' => $affaire
                                ]);
                                $sujet = 'Gest\'it - Demande de validation devis'; // Assignation du sujet
                            }
                        }
                    } else {
                        if ($devis->getValidation() == 1 && $this->getUser() != $devis->getUser()) {
                            if (!$affaireId) {
                                $html = $this->renderView('GestionBundle:Export:orphanDevisValidationResponsable.html.twig', [
                                    'devis' => $devis,
                                    'url' => $this->getParameter('app_url'),
                                    'affaire' => null
                                ]);
                                $sujet = 'Gest\'it - Retour de validation de votre devis';
                            } else {
                                $html = $this->renderView('GestionBundle:Export:orphanDevisValidationResponsable.html.twig', [
                                    'devis' => $devis,
                                    'url' => $this->getParameter('app_url'),
                                    'affaire' => $affaire
                                ]);
                                $sujet = 'Gest\'it - Retour de validation de votre devis';
                            }
                        }
                    }
                    if ($html) {
                        $mail = $this->get('email.action.mailer'); // invocation du service de mail
                        if (!in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles())) {
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), $sujet); // On fournit tous les argument nécessaire à la contruction de l'objet mail et son envoi
                        } else {
                            $response = $mail->sendWithoutFlash($html, $devis->getUser()->getEmail(), $sujet);
                        }
                        if ($response['status'] === 'success') { // Si l'envoi du mail est un succés
                            $message .= ' ' . $response['message'] . ' à la direction pour validation.'; // On concatène le mesasge
                            $status = 'success'; // On indique que tous le processus est un succés
                        } else { // Sinon
                            $message .= ' Mais l\'email na pas pu être envoyé, contactez la direction pour effectuer une demande de validation.'; // on concatène avec le message d'erreur
                        }
                    }
                } catch (\Exception $e) { // En cas d'erreur d'insertion en BDD
                    $status = 'error'; // On change le status pour informer le JS en front
                    $error = $this->exceptionHandler->getException($e); // On récupère le code d'erreur
                    $message = $this->exceptionHandler->exceptionHandler($error, 'devis'); // on récupère le détail de l'erreur
                }

                // on indique à notre vue que le processing de l'opération est terminé avec succès
                if ($duplicate) { // Si on a un devis en dupplication on renvoi l'id après insertion au front
                    return new JsonResponse(array('status'=> $status, 'message' => $message, 'devisId' => $devisId), $code);
                }
                // Sinon juste le status et le message
                return new JsonResponse(array('status' => $status, 'message' => $message), $code);
            }
        }
        if (isset($linkedArticleDevis[0])) {
            foreach ($linkedArticleDevis as $article) {
                if (!isset($linkedArticleDevisTheme[$article->getArticle()->getTheme()->getId()])) {
                    $linkedArticleDevisTheme[$article->getArticle()->getTheme()->getId()] = $article->getArticle()->getTheme()->getNom();
                }
            }
        }
        if ($display) { // Toggle de la variable display pour qu'il soit compris par le JS dans la vue
            $display = 'yes';
        } else {
            $display = 'no';
        }

        return $this->render('GestionBundle:Default:manageDevis.html.twig', [
            'form_devis' => $form_devis->createView(),
            'user' => $user,
            'listeArticles' => $listeArticles,
            'affaireId' => $affaireId,
            'affaire' => $affaire,
            'devisId' => $devisId,
            'validation' => $devis->getValidation(),
            'linkedArticleDevis' => $linkedArticleDevis,
            'listedThemes' => $listedThemes,
            'nomAffaire' => $nomAffaire,
            'duplicateDevis' => $duplicateDevis,
            'devis' => $devis,
            'linkedArticleDevisTheme' => $linkedArticleDevisTheme,
            'display' => $display,
            'numDevis' => $numDevis
        ]);
    }

    /**
     * fonction qui va permettre, sur requete ajax, de modifier l'etat d'un devis
     * @param Request $request
     * @param $devisId
     * @param null $affaireId
     * @return Response
     */
    public function changeEtatAction(Request $request, $devisId, $affaireId = null)
    {
        $status = "error";
        $message = 'Mauvaise requête!';
        $code = 400;
        // on récupère le devis via son id passé en paramètre de l'url de la requete
        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId);
        $em = $this->getDoctrine()->getManager();

        // on vérifie que la requete soit bien conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;
                // on set le statut du devis en fonction de ce qu'on reçoit de la requete
                if($data->get("statut") == "accept") {
                    $devis->setEtat(2);
                } elseif ($data->get("statut") == "abandon") {
                    $devis->setEtat(3);
                } elseif ($data->get('statut') == 'revert') {
                    $devis->setEtat(1);
                } else {
                    $devis->setEtat(4);
                }
                // on sauvegarde les modifications apportées au devis via le manager de doctrine
                $em->persist($devis);
                $code = 200;
                try {
                    $em->flush();
                    $status = "success";
                    $message = "Etat du devis modifié";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'devis');
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * fonction qui va nous permettre, en tant qu'admin de valider directement un devis via une simple requete
     * plutot que de devoir à chaque fois passer par la page de modification du devis
     * @param $devisId
     * @param null $affaireId
     * @return RedirectResponse
     */
    public function validatingAction($devisId, $affaireId=null)
    {
        $message = "";
        // on récupère le devis concerné via son id passé en parametre de l'url de la requete
        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId);
        $em = $this->getDoctrine()->getManager();
        if ($_POST['statut'] === 'valid') {
            $devis->setValidation(1);
            $devis->setEtat(4);
        } else {
            $devis->setValidation(2);
        }
        $em->persist($devis);
        try {
            $em->flush();
            if ($_POST['statut'] === 'valid') {
                $message = 'Le devis a bien été validé.';
            } else {
                $message = 'Le devis a bien été refusé.';
            }
            $this->addFlash('success', $message);
            $mail = $this->get('email.action.mailer');
            $html = $this->renderView('GestionBundle:Export:orphanDevisRefusResponsable.html.twig', [
                'devis' => $devis
            ]);
            $result = $mail->sendWithoutFlash($html, $devis->getUser()->getEmail(), 'Gest\'it - Retour de validation de votre devis');
            if ($result['status'] === 'success') {
                $message = 'Le mail informant le chef de projet a été envoyé.';
                $this->addFlash('success', $message);
            } else {
                $message = ' Le mail informant le chef de projet n\'a pas été envoyé.';
                $this->addFlash('warning', $message);
            }
        } catch (\Exception $e) {
            $exception = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($exception);
            $this->addFlash('danger', $message);
        }

        return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#devis');
    }

    /**
     * Fonction qui va nous permettre de lier un devis orphelin à une affaire déjà existante
     * @param Request $request
     * @param $devisId
     * @return JsonResponse
     */
    public function linkingOrphanAction(Request $request, $devisId)
    {
        $status = "error";
        $message = 'Mauvaise requête!';

        // on récupère le devis via son id passé en parametre de l'url de la requete
        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId);
        $articleDevis = $devis->getArticleDevis();
        $em = $this->getDoctrine()->getManager();

        // on vérifie que la requete soit bien conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;
                // on récupère l'affaire concernée par son id passé via la requete Ajax
                $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($data->get("affaire"));
                // on lie le devis à cette affaire
                $devis->setAffaire($affaire);
                $devis->setEtat(2); // on valide le devis
                foreach ($articleDevis as $key => $value ) { // Pour chaque article du devis
                    // On vérifie si l'article n'existe pas déjà dans l'affaire
                    if (!$this->getDoctrine()->getRepository(ArticleAffaire::class)->findBy(['article' => $articleDevis[$key]->getArticle(), 'affaire' => $affaire])) {
                        $articleAffaire = new ArticleAffaire(); // On instancie un nouvel article affaire
                        $articleAffaire->setAffaire($affaire); // on assigne l'affaire
                        $articleAffaire->setArticle($articleDevis[$key]->getArticle()); // On assigne l'article
                        $em->persist($articleAffaire);
                    }
                }

                $em->persist($devis);

                $commande = new Commande();
                // on définit les différentes propriétés de la nouvelle commande à partir des infos du devis
                $commande->setAffaire($devis->getAffaire());
                $commande->setMentions($devis->getLibelle() . ' [importé depuis le devis]');
                $commande->setClient($devis->getClient());
                $commande->setForfaitaire($devis->getForfaitaire());
                $commande->setAdresseLivraison($devis->getAdresseLivraison());
                $commande->setAdresseFacturation($devis->getAdresseFacturation());
                $commande->setTVA($devis->getTVA());
                $commande->setRemise($devis->getRemise());
                $commande->setUser($devis->getUser());
                $commande->setDateCommande(new \DateTime());
                $commande->setEtat(0);
                $commande->setNumeroCommande($this->createNumber->numeroCommande($affaire->getId()));
                $commande->setTotalHT($devis->getTotalHT());
                $commande->setMontantTotalTTC($devis->getTotalTTC());
                $commande->setActiveTVA($devis->getActiveTVA());
                $commande->setDevis($devis);
                $commande->setContact($devis->getContact());
                // on persist cet objet en entité doctrine via le manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);

                // la commande venant d'etre créée en entité Doctrine, ce dernier lui a attribué un id que l'on récupère
                $commandeId = $commande->getId();
                // on récupère tous les articles du devis venant d'etre accepté
                $articlesDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                    'devis' => $devis->getId()
                ]);

                // pour chaque article du devis accepté, on créé un nouvel article de comamnde
                for($i = 0; $i < count($articlesDevis); $i++) {
                    $articleCommande = new ArticleCommande();

                    $articleCommande->setCommande($this->getDoctrine()->getRepository(Commande::class)->find($commandeId));
                    $articleCommande->setArticleDevis($articlesDevis[$i]);

                    $checkExisting = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                        'commande' => $commandeId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                    if($articlesDevis[$i]->getOverrideMontantUnit() != null) {
                        $articleCommande->setOverrideMontantUnit($articlesDevis[$i]->getOverrideMontantUnit());
                    }
                    $articleCommande->setQuantite($articlesDevis[$i]->getQuantite());
                    $articleCommande->setMontantTotalHT($articlesDevis[$i]->getMontantTotalHT());

                    $em->persist($articleCommande);
                }

                $user = $devis->getUser()->getNom();
                // on sauvegarde les modifications du devis avec le manager de Doctrine
                try {
                    $em->flush();
                    $status = "success";
                    $message = "devis rattaché";
                } catch (\Exception $e) {
                    dump($e); die();
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'devis');
                }
                // on réponds à la vue afin de clore sa requete
                return new JsonResponse([$status => $message, 'user' => $user]);
            }
        }
        return new JsonResponse([$status => $message]);
    }

    /**
     * Méthode pour passer l'état du devis à en attente d'accord client
     * @param $devisId
     * @param null|string $chefProjet
     * @return RedirectResponse
     */
    public function sentToCustomerAction($devisId, $chefProjet = null)
    {
        $em = $this->getDoctrine()->getManager(); // Manageur d'entité
        $devis = $this->getDoctrine()->getRepository(Devis::class)->find($devisId); // Oin récupère le devis sélectionné
        $devis->setEtat(1); // on change l'état du devis
        $em->persist($devis); // On persist l'objet
        /**
         * Gestion des erreurs
         */
        try {
            $em->flush();
            $this->addFlash('success', 'L\'envoie du devis au client a bien était enregistré.');
        } catch (\Exception $e) {
            $this->addFlash('danger', 'Une erreur s\'est produite, veuillez réssayer.');
        }
        // on redirige selon si l'état a été changé depuis une affaire ou non
        if (!$chefProjet) {
            if ($devis->getAffaire() == null) {
                return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#devis');
            } else {
                return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire', ['affaireId' => $devis->getAffaire()->getId()]) . '#devis');
            }
        } else {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_user') . '#devis');
        }
    }
}