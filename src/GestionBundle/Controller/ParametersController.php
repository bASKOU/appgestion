<?php


namespace GestionBundle\Controller;


use GestionBundle\Entity\AdresseLivraisonSudalys;
use GestionBundle\Entity\ModePaiement;
use GestionBundle\Entity\TypeContact;
use GestionBundle\Form\AdresseLivraisonSudalysType;
use GestionBundle\Form\ModePaiementType;
use GestionBundle\Form\TypeContactType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParametersController extends Controller
{
    public $exceptionHandler; // Propriété qui va contenir le service de gestio, des erreurs

    /**
     * DevisController constructor.
     */
    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler(); // invocation du service de gestion des erreurs au chargement de la classe
    }

    /**
     * Méthode pour afficher le dashboard pour le service administratif
     * @return Response
     */
    public function showAction()
    {
        return $this->render('GestionBundle:Default:indexParameters.html.twig');
    }

    /**
     * Méthode afin d'afficher la liste des types de contact et de créer un nouveau type
     * @param Request $request
     * @return Response
     */
    public function adresseAction(Request $request)
    {
        $adresse = new AdresseLivraisonSudalys();
        $adresses = $this->getDoctrine()->getRepository(AdresseLivraisonSudalys::class)->findAll();

        $form = $this->createForm(AdresseLivraisonSudalysType::class, $adresse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $status = 'danger';
            $em =$this->getDoctrine()->getManager();
            $em->persist($adresse);
            try {
                $em->flush();
                $status = 'success';
                $message = 'La nouvelle adresse à bien été enregistré.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'adresse');
            }
            $this->addFlash($status, $message);

            return $this->redirectToRoute('sudalys_gestion_parameters_adresse');
        }

        return $this->render('GestionBundle:Default:parametersAdresseLivraison.html.twig', [
            'form' => $form->createView(),
            'adresses' => $adresses
        ]);
    }

    public function adresseEditAction(Request $request, $adresseId)
    {
        $em = $this->getDoctrine()->getManager();
        $adresse = $this->getDoctrine()->getRepository(AdresseLivraisonSudalys::class)->find($adresseId);
        $form = $this->createForm(AdresseLivraisonSudalysType::class, $adresse);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $code = 400;
            $status = 'error';
            $message = 'Mauvaise requête.';

            $em->persist($adresse);
            $code = 200;

            try {
                $em->flush();
                $status = 'success';
                $message = "L'adresse de livraison à bien été lise à jours.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'adresse de livraison');
            }
            return new JsonResponse(['status'=> $status, 'message' => $message], $code);
        } else {

            return $this->render('GestionBundle:Default:createAdresseAjax.html.twig', [
                'form' => $form->createView()
            ]);
        }

    }

    /**
     * Méthode pour supprimer une adresse de livraison
     * @param Request $request
     * @param $adresseId
     */
    public function deleteAdresseAction(Request $request, $adresseId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        if ($request->isMethod('POST')) {
            $adresse = $this->getDoctrine()->getRepository(AdresseLivraisonSudalys::class)->find($adresseId);
            $em = $this->getDoctrine()->getManager();
            $em->remove($adresse);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = 'L\'adresse a bien été supprimé.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'type de contact');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return new JsonResponse(['<p>Vous êtes sur le point de supprimer une adresse de livraison</p>']);
    }

    /**
     * Méthode afin d'afficher la liste des types de contact et de créer un nouveau type
     * @param Request $request
     * @return Response
     */
    public function typeContactAction(Request $request)
    {
        $typeContact = new TypeContact();
        $typeContacts = $this->getDoctrine()->getRepository(TypeContact::class)->findAll();

        $form = $this->createForm(TypeContactType::class, $typeContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $status = 'danger';
            $em =$this->getDoctrine()->getManager();
            $em->persist($typeContact);
            try {
                $em->flush();
                $status = 'success';
                $message = 'Le nouveau type de contact à bien été enregistré.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'type de contact');
            }
            $this->addFlash($status, $message);

            return $this->redirectToRoute('sudalys_gestion_parameters_typeContact');
        }

        return $this->render('GestionBundle:Default:parametersTypeContact.html.twig', [
            'form' => $form->createView(),
            'typeContacts' => $typeContacts
        ]);
    }

    /**
     * Méthode de génération du formulaire d'édition d'un type de contact
     * @param Request $request
     * @param $typeContactId
     * @return JsonResponse|Response
     */
    public function editTypeContactAction(Request $request, $typeContactId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        $typeContact = $this->getDoctrine()->getRepository(TypeContact::class)->find($typeContactId);
        $form = $this->createForm('GestionBundle\Form\TypeContactType', $typeContact);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeContact);
            $code = 200;
            try {
                $em->flush();
                $message = 'La modification du type de contact a bien été enregistré.';
                $status = 'success';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'type de contact');
            }
            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:editTypeContact.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour supprimer un type de contact
     * @param Request $request
     * @param $typeContactId
     */
    public function deleteTypeContactAction(Request $request, $typeContactId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        if ($request->isMethod('POST')) {
            $typeContact = $this->getDoctrine()->getRepository(TypeContact::class)->find($typeContactId);
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeContact);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = 'Le type de contact a bien été supprimé.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'type de contact');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return new JsonResponse(['<p>Vous êtes sur le point de supprimer un type de contact</p>']);
    }

    /**
     * Méthode afin d'afficher la liste des types de contact et de créer un nouveau type
     * @param Request $request
     * @return Response
     */
    public function modePaiementAction(Request $request)
    {
        $modePaiement = new ModePaiement();
        $modePaiements = $this->getDoctrine()->getRepository(ModePaiement::class)->findAll();

        $form = $this->createForm(ModePaiementType::class, $modePaiement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $status = 'danger';
            $em =$this->getDoctrine()->getManager();
            $em->persist($modePaiement);
            try {
                $em->flush();
                $status = 'success';
                $message = 'Le nouveau mode de paiement à bien été enregistré.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'mode de paiement');
            }
            $this->addFlash($status, $message);

            return $this->redirectToRoute('sudalys_gestion_parameters_modePaiement');
        }

        return $this->render('GestionBundle:Default:parametersModePaiement.html.twig', [
            'form' => $form->createView(),
            'modePaiements' => $modePaiements
        ]);
    }

    /**
     * Méthode de génération du formulaire d'édition d'un type de contact
     * @param Request $request
     * @param $modePaiementId
     * @return JsonResponse|Response
     */
    public function editModePaiementAction(Request $request, $modePaiementId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        $modePaiement = $this->getDoctrine()->getRepository(ModePaiement::class)->find($modePaiementId);
        $form = $this->createForm('GestionBundle\Form\ModePaiementType', $modePaiement);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modePaiement);
            $code = 200;
            try {
                $em->flush();
                $message = 'La modification du mode de paiement a bien été enregistré.';
                $status = 'success';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'mode de paiement');
            }
            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:editModePaiement.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour supprimer un type de contact
     * @param Request $request
     * @param $modePaiementId
     */
    public function deleteModePaiementAction(Request $request, $modePaiementId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        if ($request->isMethod('POST')) {
            $modePaiement = $this->getDoctrine()->getRepository(ModePaiement::class)->find($modePaiementId);
            $em = $this->getDoctrine()->getManager();
            $em->remove($modePaiement);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = 'Le mode de paiement a bien été supprimé.';
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'mode de paiement');
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return new JsonResponse(['<p>Vous êtes sur le point de supprimer un mode de paiement</p>']);
    }
}