<?php

namespace GestionBundle\Controller;

use DateTime;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Attribution;
use GestionBundle\Entity\AvancementCommandePrestataire;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\FacturePrestataire;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\ServiceCommandePrestataire;
use GestionBundle\Entity\ServicePrestataire;
use GestionBundle\Form\CommandePrestataireType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\UserEntity;

class CommandePrestataireController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;

    /**
     * CommandePrestataireController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode pour vérifier si le montant total de la commande respecte le plafond par opération du chef de projet pour l'affaire concerné
     * @param $totalHT
     * @param $affaire
     * @return RedirectResponse|bool
     */
    private function checkPlafondParOperation($totalHT, $affaire)
    {
        $user = $this->getUser();
        if (!in_array('ROLE_SUDALYS_ADMINISTRATIF', $user->getRolesForm($user->getRoles()))) {
            $attribution = $this->getDoctrine()->getRepository(Attribution::class)->findOneBy(['user' => $user, 'affaire' => $affaire]);
            if ($attribution) {
                if ($attribution->getPlafondOperation()) {
                    if ($attribution->getPlafondOperation() >= $totalHT) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            } else {
                $this->addFlash('danger', "Vous n'êtes pas attribué à cette affaire!");
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        } else {
            return true;
        }
    }

    /**
     * Méthode utilisé pour vérifier si le plafond de l'affaire n'est pas dépassé
     * @param $totalHT
     * @param $affaire
     * @return bool
     */
    private function checkAffairePlafond($totalHT, $affaire)
    {
        $commandes = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findBy(['affaire' => $affaire, 'validation' => 1]);
        $plafond = $affaire->getPlafond();
        if ($plafond) {
            $actualPlafond = 0;
            foreach ($commandes as $commande) {
                $actualPlafond += $commande->getTotalHt();
            }
            $tempPlafond = $actualPlafond + $totalHT;
            if ($plafond) {
                if ($tempPlafond <= $plafond) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Méthode pour stringifier l'adresse de facturation
     * @return string
     */
    private function buildAdresseFacturation()
    {
        $entity = $this->getDoctrine()->getRepository(UserEntity::class)->findAll();
        $entity = $entity[0];
        $adresseFacturation = $entity->getNom() . ', ';
        $adresseFacturation .= $entity->getAdresse();
        if ($entity->getAdresse2()) {
            $adresseFacturation .= ', ' . $entity->getAdresse2();
        }
        $adresseFacturation .= ', ' . $entity->getCodePostal() . ' ' . $entity->getVille();

        return $adresseFacturation;
    }

    /**
     * Méthode pour rediriger selon des critères
     * @param $commande
     * @return RedirectResponse
     */
    private function redirectIfAffaire($commande)
    {
        $affaire = $commande->getAffaire();
        if (in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles())) {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandePrestataion');
        }  elseif (in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#commandeMateriel');
        } elseif ($affaire) {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_user') . '#commandePrestation');
        } else {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandePrestation');
        }
    }

    /**
     * Méthode pour créer une commande prestat=aire
     * @param Request $request
     * @param null $affaireId
     * @param string $numCommande
     * @return Response|RedirectResponse
     */
    public function createAction(Request $request, $affaireId=null, $numCommande='no')
    {
        $user = $this->getUser();
        $cmd = new CommandePrestataire();
        $adresseFacturation = $this->buildAdresseFacturation();
        $cmd->setAdresseFacturation($adresseFacturation);
        if ($affaireId) {
            $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
            $cmd->setAffaire($affaire);
            if (!$affaire->getRespectPlafond()) {
                $this->addFlash('warning', 'Le plafond de l\'affaire a été dépassé, la création d\'une commande de prestation est impossible');
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        } else {
            $affaire = null;
        }
        $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise());
        $form = $this->createForm(CommandePrestataireType::class, $cmd, ['role' => $this->getUser()->getRoles()]);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $message = null;
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandeprestataire'];
            $services = json_decode($data['listProducts'], true);

            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            $testPlafondOP = false;
            $testPlafondAffaire = false;
            if ($affaire) {
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);

                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) {
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(false);
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            if ($cmd->getNumeroCommandePresta() != $this->createNumber->generateFromLastNumberCommandeEmise()) {
                $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise());
            }
            $cmd->setEtat(0);
            $cmd->setUser($this->getUser());
            $cmd->setDateCommandePresta(new \DateTime('now'));
            $em->persist($cmd);
            foreach($services as $product) {
                $serviceCommandePrestataire = new ServiceCommandePrestataire();
                foreach($product as $key => $value) {
                    if ($key === 'servicePrestataire') {
                        $value = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $serviceCommandePrestataire->$temp($value);
                        }
                    } else {
                        $servicePrestataire = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                        $serviceCommandePrestataire->setServicePrestataire($servicePrestataire);
                        if ($serviceCommandePrestataire->getMontantUnit() != $servicePrestataire->getMontantUnit()) {
                            $servicePrestataire->setMontantUnit($serviceCommandePrestataire->getMontantUnit());
                            $em->persist($servicePrestataire);
                        }
                    }
                }
                $serviceCommandePrestataire->setDate(new \DateTime('now'));
                $serviceCommandePrestataire->setCommandePrestataire($cmd);
                $em->persist($serviceCommandePrestataire);
            }
            try {
                $em->flush();
                $tempMessage = 'La commande prestataire a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                if ($affaireId) {
                    if (!$testPlafondOP || !$testPlafondAffaire) {
                        $url = $this->getParameter('app_url');
                        $mail = $this->get('email.action.mailer');
                        if (!$testPlafondOP) {
                            $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Montant maximum autorisé d\'engagement dépassé');
                        }
                        if (!$testPlafondAffaire) {
                            $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                        }

                        if ($response['status'] === 'success') {
                            $message .= ' Un email a été envoyé à la direction pour validation.';
                        } else {
                            $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                        }
                    }
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_home');
            } catch (\Exception $e) {
                dump($e); die();
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande prestataire');
                $this->addFlash('danger', $message);
                if ($affaireId) {
                    return $this->redirectToRoute('sudalys_gestion_continue_CMDP', ['affaireId' => $affaireId, 'numCommande' => $cmd->getNumeroCommandePresta()]);
                } else {
                    return $this->redirectToRoute('sudalys_gestion_continue_CMDP_noaffaire', ['numCommande' => $cmd->getNumeroCommandePresta()]);
                }
            }
        }

        return $this->render('GestionBundle:Default:createCommandePrestataire.html.twig', [
            'form' => $form->createView(),
            'affaire' => $affaire,
            'user' => $user,
            'cmd' => $cmd,
            'numCommande' => $numCommande,
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour visualiser une commande prestataire
     * @param $commandeId
     * @return Response
     */
    public function viewAction($commandeId)
    {
        $user = $this->getUser();

        $commandePrestataire = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);

        if ($commandePrestataire->getAffaire()) {
            $affaire = $commandePrestataire->getAffaire();
        } else {
            $affaire = null;
        }

        $form = $this->createForm(CommandePrestataireType::class, $commandePrestataire);

        return $this->render('GestionBundle:Default:manageCommandePrestataire.html.twig', [
            'form' => $form->createView(),
            'commandePrestataire' => $commandePrestataire,
            'view' => 'yes',
            'duplicate' => 'no',
            'user' => $user,
            'numCommande' => 'no',
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour modifier une commande prestataire
     * @param Request $request
     * @param $commandeId
     * @return Response|RedirectResponse
     */
    public function modifyAction(Request $request, $commandeId)
    {
        $user = $this->getUser();

        $cmd = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);

        $cmd->getAffaire() ? $affaire = $cmd->getAffaire() : $affaire = null;

        if ($affaire) {
            if (!$affaire->getRespectPlafond()) {
                $this->addFlash('warning', 'Le plafond de l\'affaire a été dépassé, la création d\'une commande de prestations est impossible');
                return $this->redirectToRoute('sudalys_gestion_home');
            }
        }

        $form = $this->createForm(CommandePrestataireType::class, $cmd, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = "";
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandeprestataire'];
            $services = json_decode($data['listProducts'], true);

            if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $user->getRoles())) {
                $cmd->setEtat(0);
                $cmd->setValidation(true);
            } else {
                $cmd->setEtat(0);
            }
            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            if ($cmd->getAffaire()) {
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);

                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) {
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    if (!in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                        $cmd->setValidation(false);
                    } else {
                        $cmd->setValidation(true);
                    }
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            $em->persist($cmd);
            $existingService = $cmd->getServiceCommandePrestataire();
            foreach($services as $service) {
                if (!empty($service['prodId'])) {
                    $serviceCommandePrestataire= $this->getDoctrine()->getRepository(ServiceCommandePrestataire::class)->find($service['prodId']);
                    foreach ($existingService as $key => $prod) {
                        if ($prod->getId() == $service['prodId']) {
                            unset($existingService[$key]);
                        }
                    }
                } else {
                    $serviceCommandePrestataire= new ServiceCommandePrestataire();
                }
                unset($service['prodId']);
                foreach($service as $key => $value) {
                    if ($key === 'servicePrestataire') {
                        $value = $this->getDoctrine()->getRepository(servicePrestataire::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $serviceCommandePrestataire->$temp($value);
                        }
                    } else {
                        $servicePrestataire = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                        $serviceCommandePrestataire->setServicePrestataire($servicePrestataire);
                    }
                }
                $serviceCommandePrestataire->setDate(new \DateTime('now'));
                $serviceCommandePrestataire->setCommandePrestataire($cmd);
                $em->persist($serviceCommandePrestataire);
            }
            foreach ($existingService as $prod) {
                $em->remove($prod);
            }
            try {
                $em->flush();
                $status = 'success';
                $tempMessage = 'La commande prestataire a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                if ($cmd->getAffaire() and !in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                    if (!$testPlafondOP || !$testPlafondAffaire) {
                        $url = $this->getParameter('app_url');
                        $mail = $this->get('email.action.mailer');
                        if (!$testPlafondOP) {
                            $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Validation d\'une commande prestation - Plafond par opération');
                        }
                        if (!$testPlafondAffaire) {
                            $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                                'user' => $cmd->getUser(),
                                'commande' => $cmd,
                                'url' => $url
                            ]);
                            $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                        }

                        if ($response['status'] === 'success') {
                            $message .= ' Un email a été envoyé à la direction pour validation.';
                        } else {
                            $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                        }
                    }
                } elseif ($cmd->getAffaire() && in_array('ROLE_SUDALYS_ADMINISTRATIF', $this->getUser()->getRoles())) {
                    if (!$testPlafondAffaire) {
                        $status = 'warning';
                        $message .= ' Le plafond de l\'affaire a été dépassé.';
                    }
                }
                if ( in_array('ROLE_SUPER_SUDALYS', $this->getUser()->getRoles()) && $this->getUser() != $cmd->getUser() && $cmd->getValidation() == 1 && $affaire) {
                    $url = $this->getParameter('app_url');
                    $mail = $this->get('email.action.mailer');
                    $html = $this->renderView('GestionBundle:Export:commandePrestaValidation.html.twig', [
                        'commande' => $cmd,
                        'url' => $url
                    ]);
                    $response = $mail->sendWithoutFlash($html, $cmd->getUser()->getEmail(), 'Gest\'it - Retour de validation de votre commande');

                    if ($response['status'] === 'success') {
                        $message .= ' Un email a été envoyé au Chef de projet.';
                    } else {
                        $status = 'warning';
                        $message .= " Une erreur s'est produite lors de l'envoi de l'email au chef de projet.";
                    }
                }

                $this->addFlash($status, $message);

                return $this->redirectIfAffaire($cmd);

            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande prestation');
                $this->addFlash('danger', $message);
                return $this->redirectToRoute('sudalys_gestion_modify_CMDP', ['commandeId' => $cmd->getId()]);
            }
        }

        return $this->render('GestionBundle:Default:manageCommandePrestataire.html.twig', [
            'form' => $form->createView(),
            'commandePrestataire' => $cmd,
            'view' => 'no',
            'duplicate' => 'no',
            'user' => $user,
            'numCommande' => 'no',
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour dupliquer une commande prestataire
     * @param Request $request
     * @param $commandeId
     * @param null $affaireId
     * @return RedirectResponse|Response
     */
    public function duplicateAction(Request $request, $commandeId, $affaireId=null)
    {
        $user = $this->getUser(); // Stockage de l'utilisateur dans une variable
        // Récupère la commande Prestataire à dupliquer
        $oldCmd = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);
        if ($oldCmd->getAffaire()) {
            $affaire = $oldCmd->getAffaire();
            $affaireId = $oldCmd->getAffaire()->getId();
        } else {
            $affaire = null;
        }
        $cmd = clone $oldCmd;
        if ($affaireId) {
            $cmd->setAffaire($oldCmd->getAffaire());
        } else {
            $cmd->setAffaire(null);
        }

        unset($oldCmd); // Supprime l'ancienne commande Prestataire
        $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise()); // Génère le nouveau numéro de commande Prestataire

        $form = $this->createForm(CommandePrestataireType::class, $cmd); // Génération du formulaire
        $form->handleRequest($request); // Ajout le request handler

        if ($form->isSubmitted() && $form->isValid()) { // Lorsque le formulaire est soumis et valid
            $message = null; // Initialise message à null
            $em = $this->getDoctrine()->getManager(); // invoque le manageur d'entité
            $data = $_POST['gestionbundle_commandeprestataire']; // Stock les données reçus dans une variable
            unset($_POST['gestionbundle_commandeprestataire']); // Supprime les données reçus de la super globale POST
            $products = json_decode($data['listProducts'], true); // Stock et decode la liste des produits reçus
            unset($data['listProducts']); // Supprime la liste des produits en json

            $cmd->setEtat(0); // Set l'état à 0
            $cmd->setUser($this->getUser()); // Set le créateur de la commande Prestaiseeur
            $cmd->setDateCommandePresta(new \DateTime('now')); // Set la date de création
            // Supprime les index et leur données inutile au set dynamique
            unset($data['_token']);
            unset($data['Enregistrer']);
            unset($data['id']);
            // Boucle dans la liste des articles
            foreach($data as $key => $value) {
                // Suite de condition pour affecter les entités en relation
                if ($key == 'affaire') {
                    if ($value!= "") {
                        $cmd->setAffaire($this->getDoctrine()->getRepository(Affaire::class)->find($value));
                    } else {
                        $cmd->setAffaire(null);
                    }
                } else if ($key == 'prestataire') {
                    $cmd->setPrestataire($this->getDoctrine()->getRepository(Organisme::class)->find($value));
                } else if ($key == 'contact') {
                    $cmd->setContact($this->getDoctrine()->getRepository(Contact::class)->find($value));
                } else {
                    if ($value != "") {
                        $temp = 'set' . ucfirst($key);
                        $cmd->$temp($value);
                    }
                }
            }
            // Stock l'affaire dans une variable pour limité l'utilisation de doctrine
            $affaire = $cmd->getAffaire();
            $testPlafondOP = true;
            $testPlafondAffaire = true;
            $cmd->setTotalHT(str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC(str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            if ($affaireId) {
                // Test les plafonds
                $testPlafondOP = $this->checkPlafondParOperation($cmd->getTotalHT(), $affaire);
                $testPlafondAffaire = $this->checkAffairePlafond($cmd->getTotalHT(), $affaire);
                // Set la validation et le respet du plafond selon si les plafonds sont respecté ou non
                if ($testPlafondOP === false) {
                    $cmd->setRespectPlafond(false);
                    $cmd->setValidation(false);
                    $message .= 'La commande dépasse le montant par opèration que la direction à fixé.';
                }
                if ($testPlafondAffaire === false) { // Si le plafond de l'affaire est dépassé on modifie l'affaire
                    $affaire->setRespectPlafond(false);
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(false);
                    $em->persist($affaire);
                    if ($message === null) {
                        $message = 'Le plafond total de l\'affaire est depassé.';
                    } else {
                        $message .= ' Le plafond total de l\'affaire est depassé.';
                    }
                } else {
                    $cmd->setRespectPlafond(true);
                    $cmd->setValidation(true);
                }
            } else {
                $cmd->setRespectPlafond(true);
                $cmd->setValidation(true);
            }
            $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise());

            $em->persist($cmd);

            foreach ($products as $product) {
                $serviceCommandePrestataire = new ServiceCommandePrestataire();
                unset($product['prodId']);
                foreach ($product as $key => $value) {
                    if ($key === 'id') {
                        $value = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                        $serviceCommandePrestataire->setServicePrestataire($value);
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $serviceCommandePrestataire->$temp($value);
                    }
                }
                $serviceCommandePrestataire->setDate(new \DateTime('now'));
                $serviceCommandePrestataire->setCommandePrestataire($cmd);
                $em->persist($serviceCommandePrestataire);
            }
            try {
                $em->flush();
                $tempMessage = 'La commande Prestataire a bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }

                if (!$testPlafondOP || !$testPlafondAffaire) {
                    $url = $this->getParameter('app_url');
                    if (!$testPlafondOP) {
                        $html = $this->renderView('GestionBundle:Export:plafondOP.html.twig', [
                            'user' => $cmd->getUser(),
                            'commande' => $cmd,
                            'url' => $url
                        ]);

                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Validation d\'une commande prestation - Plafond par opération');
                    }
                    if (!$testPlafondAffaire) {
                        $html = $this->renderView('GestionBundle:Export:plafondAffaire.html.twig', [
                            'user' => $cmd->getUser(),
                            'commande' => $cmd,
                            'url' => $url
                        ]);
                        $mail = $this->get('email.action.mailer');
                        $response = $mail->sendWithoutFlash($html, $this->getParameter('mail_responsable'), 'Gest\'it - Alerte - Plafond d\'affaire dépassé');
                    }
                    if ($response['status'] === 'success') {
                        $message .= ' Un email a été envoyé à la direction pour validation.';
                    } else {
                        $message .= ' L\'email n\'a pas été envoyé, veuillez contactez la direction pour demander la validation';
                    }
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_dashboard_administratif_achat');
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande Prestataire');
                $this->addFlash('danger', $message);
                return $this->redirectToRoute('sudalys_gestion_continue_CMDP', ['affaireId' => $cmd->getAffaire()->getId(), 'numCommande' => $cmd->getNumeroCommandePresta()]);
            }
        }

        return $this->render('GestionBundle:Default:manageCommandePrestataire.html.twig', [
            'form' => $form->createView(),
            'commandePrestataire' => $cmd,
            'view' => 'no',
            'duplicate' => 'yes',
            'user' => $user,
            'numCommande' => 'no',
            'affaire' => $affaire
        ]);
    }

    /**
     * Méthode pour valider une commande Prestataire
     * @param Request $request
     * @param $commandeId
     * @return RedirectResponse
     */
    public function acceptAction(Request $request, $commandeId)
    {
        $status = 'danger';
        $message = '';
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);
        $commande->setValidation(1);
        $em->persist($commande);
        try {
            $em->flush();
            $message = 'La validation de la commande de prestations enregistré.';
            $status = 'success';
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de prestations');
            $status = 'warning';
        }

        $this->addFlash($status, $message);

        return $this->redirectIfAffaire($commande);
    }

    /**
     * Méthode pour refuser une commande Prestataire
     * @param Request $request
     * @param $commandeId
     */
    public function refusAction(Request $request, $commandeId)
    {
        $status = 'danger';
        $message = "Une erreur c'est produite, le refus n'a pas été enregistré.";
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);

        $commande->setValidation(2);
        $em->persist($commande);
        try {
            $em->flush();
            $message = 'Refus de la commande de prestations enregistré.';
            $status = 'success';
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de prestations');
            $status = 'warning';
        }

        $this->addFlash($status, $message);

        if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
            return $this->redirect($this->generateUrl('sudalys_gestion_home') . '#commandePrestation');
        } elseif (($this->getUser()->getRoles()[0] === 'ROLE_SUDALYS_ADMINISTRATIF')) {
            return $this->redirect($this->generateUrl('sudalys_gestion_dashboard_administratif_achat') . '#commandePrestation');
        } else {
            return $this->redirect($this->generateUrl('sudalys_gestion_detail_affaire') . '#commandePrestation');
        }
    }

    /**
     * Méthode pour abandonner une commande prestataire
     * @param Request $request
     * @param $commandeId
     * @return JsonResponse
     */
    public function abandonAction(Request $request, $commandeId)
    {
        $code = 400;
        $message = 'Mauvaise requête.';
        $status = 'error';
        if ($request->isMethod('POST')) {
            $commande = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);
            $em = $this->getDoctrine()->getManager();
            $commande->setEtat(2);
            $commande->setRaisonAbandon($request->request->get('raisonAbandonCommandeMateriel'));
            $facturePrestataire = $commande->getFacturePrestataire();
            $totalFacturePrestataire = 0;
            if (!empty($facturePrestataire[0])) {
                foreach ($facturePrestataire as $facture) {
                    $totalFacturePrestataire += $facture->getTotalHT();
                }
            }
            $totalAvancement = 1;
            $avancementPrestataire = $commande->getAvancement()->toArray();
            if ($totalFacturePrestataire > 0) {
                if (!empty($avancementPrestataire[0])) {
                    $totalAvancement = 0;
                    foreach ($avancementPrestataire as $avancement) {
                        $avancement->setAvancement(($avancement->getTotalHT() / $totalFacturePrestataire));
                        $em->persist($avancement);
                        $totalAvancement += $avancement->getAvancement();
                    }
                }
            } else {
                if (!empty($avancementPrestataire[0])) {
                    $totalAvancement = 0;
                    foreach ($avancementPrestataire as $avancement) {
                        $totalFacturePrestataire += $avancement->getTotalHT();
                    }
                    foreach ($avancementPrestataire as $avancement) {
                        $avancement->setAvancement(($avancement->getTotalHT() / $totalFacturePrestataire));
                        $em->persist($avancement);
                        $totalAvancement += $avancement->getAvancement();
                    }
                }
            }
            if ($totalAvancement < 1) {
                foreach ($facturePrestataire as $facture) {
                    $date = strtotime($facture->getDateSaisie()->format('Y-m'));
                    $flag = false;
                    foreach ($avancementPrestataire as $avancement) {
                        if ($date === strtotime($avancement->getDate()->format('Y-m'))) {
                            $avancement->setTotalHT($facture->getTotalHT());
                            $avancement->setAvancement($facture->getTotalHT() / $totalFacturePrestataire);
                            $em->persist($avancement);
                            $flag = true;
                        }
                    }
                    if ($flag === false) {
                        $avancement = new AvancementCommandePrestataire();
                        $avancement->setDate($facture->getDateSaisie());
                        $avancement->setDateSaisie(new DateTime('today'));
                        $avancement->setTotalHT($facture->getTotalHT());
                        $avancement->setAvancement($facture->getTotalHT() / $totalFacturePrestataire);
                        $avancement->setCommandePrestataire($facture->getCommandePrestataire());
                        $avancement->setValidation(1);
                        $em->persist($avancement);
                        dump($avancement);
                    }
                }
            }
            $commande->setTotalHT($totalFacturePrestataire);
            $commande->setMontantTotalTTC($totalFacturePrestataire * (1 + $commande->getTVA()));
            $em->persist($commande);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "L'abandon de la commande de prestations a bien été enregistré.";
            } catch (\Exception $e) {
                dump($e); die();
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de prestations');
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour enregistré l'envoi de la commande au prestataire
     * @param $commandeId
     * @return RedirectResponse
     */
    public function sendToPrestataireAction($commandeId)
    {
        $message = '';
        $status = 'warning';
        $em = $this->getDoctrine()->getManager();
        $commande = $this->getDoctrine()->getRepository(CommandePrestataire::class)->find($commandeId);
        $commande->setEtat(1);
        $commande->setDateEnvoi(new \DateTime('now'));
        $em->persist($commande);
        try {
            $em->flush();
            $status = 'success';
            $message = "L'envoi de la commande au prestataire a bien était enregistré";
        } catch (\Exception $e) {
            $error = $this->exceptionHandler->getException($e);
            $message = $this->exceptionHandler->exceptionHandler($error, 'commande de prestations');
        }

        $this->addFlash($status, $message);

        return $this->redirectIfAffaire($commande);
    }

    /**
     * Méthode pour récupérer la liste des commandes qui non pas encore de facture ou des factures déjà validé et réglé
     * @return JsonResponse
     */
    public function getCommandeListAction()
    {
        $commandes = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findBy(['etat' => 1]);
        $listeCommande = [];
        $i = 0;
        foreach ($commandes as $key => $commande) {
            $test = false;
            foreach ($commande->getFacturePrestataire() as $facture) {
                if ($facture->getEtat() == 0 && $facture->getValidation() <= 1) {
                    $test = true;
                }
            }
            if ($test) {
                unset($commandes[$key]);
            } else {
                $listeCommande[$i++] = ['id' => $commande->getId(), 'mention' => $commande->getMentions(), 'numero' => $commande->getNumeroCommandePresta()];
            }
        }

        return new JsonResponse($listeCommande);
    }

    /**
     * Méthode pour créer une commande Prestataire
     * @param Request $request
     * @param string $numCommande
     * @return Response|RedirectResponse
     */
    public function createWithFactureAction(Request $request, $numCommande = 'no')
    {
        $user = $this->getUser();
        $cmd = new CommandePrestataire();
        $adresseFacturation = $this->buildAdresseFacturation();
        $cmd->setAdresseFacturation($adresseFacturation);
        $affaire = null;
        $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise());
        $form = $this->createForm(CommandePrestataireType::class, $cmd, ['role' => $this->getUser()->getRoles()]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $message = null;
            $em = $this->getDoctrine()->getManager();
            $data = $_POST['gestionbundle_commandeprestataire'];
            $products = json_decode($data['listProducts'], true);

            $cmd->setTotalHT((float)str_replace(' ', '', str_replace(',', '.', $cmd->getTotalHT())));
            $cmd->setMontantTotalTTC((float)str_replace(' ', '', str_replace(',', '.', $cmd->getMontantTotalTTC())));
            $cmd->setRespectPlafond(true);
            $cmd->setValidation(true);
            $cmd->setNumeroCommandePresta($this->createNumber->generateFromLastNumberCommandeEmise());
            $cmd->setEtat(1);
            $cmd->setUser($this->getUser());
            $cmd->setDateCommandePresta(new \DateTime('now'));
            $em->persist($cmd);
            foreach($products as $product) {
                $serviceCommandePrestataire = new ServiceCommandePrestataire();
                foreach($product as $key => $value) {
                    if ($key === 'servicePrestataire') {
                        $value = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                    }
                    if ($key != 'id') {
                        if ($value != "") {
                            $temp = 'set' . ucfirst($key);
                            $serviceCommandePrestataire->$temp($value);
                        }
                    } else {
                        $servicePrestataire = $this->getDoctrine()->getRepository(ServicePrestataire::class)->find($value);
                        $serviceCommandePrestataire->setServicePrestataire($servicePrestataire);
                        if ($serviceCommandePrestataire->getMontantUnit() != $servicePrestataire->getMontantUnit()) {
                            $servicePrestataire->setMontantUnit($serviceCommandePrestataire->getMontantUnit());
                            $em->persist($servicePrestataire);
                        }
                    }
                }
                $serviceCommandePrestataire->setDate(new \DateTime('now'));
                $serviceCommandePrestataire->setCommandePrestataire($cmd);
                $em->persist($serviceCommandePrestataire);
            }
            $facturePrestataire = new FacturePrestataire();
            $facturePrestataire->setAffaire($cmd->getAffaire());
            $facturePrestataire->setContact($cmd->getContact());
            $facturePrestataire->setUser($cmd->getUser());
            $facturePrestataire->setCommandePrestataire($cmd);
            $facturePrestataire->setDateSaisie(new \DateTime('now'));
            $facturePrestataire->setTotalHT($cmd->getTotalHT());
            $facturePrestataire->setTotalTTC($cmd->getMontantTotalTTC());
            $facturePrestataire->setTVA($cmd->getTVA());
//            $facturePrestataire->setAdresseLivraison($cmd->getAdresseLivraison());
            $facturePrestataire->setAdresseFacturation($cmd->getAdresseFacturation());
            $facturePrestataire->setMentions($cmd->getMentions());
            $facturePrestataire->setEtat(0);
            $facturePrestataire->setValidation(0);
            $facturePrestataire->setPrestataire($cmd->getPrestataire());
            $em->persist($facturePrestataire);
            try {
                $em->flush();
                $tempMessage = 'La facture et la commande de matériel ont bien été enregistré.';
                if ($message === null) {
                    $message = $tempMessage;
                } else {
                    $message = $tempMessage . ' ' . $message;
                }
                $this->addFlash('success', $message);

                return $this->redirectToRoute('sudalys_gestion_chain_facturePrestataire', ['factureId' => $facturePrestataire->getId()]);
            } catch (\Exception $e) {
                dump($e); die();
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'commande de matériels');
                $this->addFlash('danger', $message);
            }
        }

        return $this->render('GestionBundle:Default:createCommandePrestataire.html.twig', [
            'form' => $form->createView(),
            'affaire' => $affaire,
            'user' => $user,
            'cmd' => $cmd,
            'numCommande' => $numCommande,
            'show' => true
        ]);
    }
}