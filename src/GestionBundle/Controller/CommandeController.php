<?php

namespace GestionBundle\Controller;

use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use GestionBundle\Entity\Contact;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Article;
use GestionBundle\Entity\Commande;
use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\Attribution;
use GestionBundle\Entity\ArticleDevis;
use GestionBundle\Entity\ArticleCommande;
use GestionBundle\Form\CreateCommandeType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\getStringBetween;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommandeController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    public $exceptionHandler;
    /**
     * @var NumberCheckAndRenew
     */
    private $createNumber;

    /**
     * FactureController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    private function reCalculAvancementAffaire(Affaire $affaire, ObjectManager $em)
    {
        $totalCommande = 0;
        $commandes = $affaire->getCommandes()->toArray();
        $avancementAffaire = $affaire->getAvancement()->toArray();
        dump($avancementAffaire);
        foreach($commandes as $commande) {
            $totalCommande += $commande->getTotalHT();
        }
        if ($avancementAffaire) {
            foreach ($avancementAffaire as $avancement) {
                $percent = ($avancement->getTotalHT() / $totalCommande);
                $avancement->setAvancement($percent);
                $em->persist($avancement);
            }
            $em->flush();
        }
    }

    /**
     * Fonction qui va nous permettre de calculer le montant TTC d'un devis en lui passsant en paramètre le montant HT, un éventuelle remise ainsi que le taux de TVA
     * remise et TVA sont ici en pourcentages
     * @param $totalHT
     * @param $remise
     * @param $TVA
     * @return float|int
     */
    protected function calculateTotal($totalHT, $remise, $TVA) {
        $prixRemise = ($totalHT - ($totalHT * $remise));
        return ($prixRemise + ($prixRemise * $TVA));
    }

    /**
     * Fonction qui va nous permettre de gérer à la fois la création et la modification d'une commande, avec en parametre :
     * l'objet de la requete, l'id de l'affaire sur laquelle on travail et l'id de la commande dans le cas d'une modification, null par défaut
     * @param Request $request
     * @param $affaireId
     * @param null $commandeId
     * @param null $display
     * @return JsonResponse|Response
     */
    public function manageAction(Request $request, $affaireId, $commandeId, $display=null)
    {
        if ($display) {
            $display = 'yes';
        } else {
            $display = 'no';
        }
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);

        $linkedArticleCommande = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
            'commande' => $commandeId
        ]);

        // on définit ici un flag qui permet de repérer si la commande possède des attachements ou pas
        if(empty($commande->getAttachements())) {
            $hasAttachements = false;
        } else {
            $hasAttachements = true;
        }

        $user = $this->getUser();
        $role = $user->getRoles();
        $userId = $this->getUser()->getId();

        // on récupère tous les articles afin de les proposer dans la liste déroulante de sélection
        $listeArticles = $this->getDoctrine()->getRepository(Article::class)->findAll();

        // on récupère l'affaire ainsi que son nom
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);
        $nomAffaire = $affaire->getDesignationAffaire();
        
        // si l'utilisateur n'est pas un admin, je récupère les différents plafonds qui le concernent
        if($role[0] != 'ROLE_SUPER_SUDALYS') {
            $plafond = $affaire->getPlafond();
            $userAttribution = $this->getDoctrine()->getRepository(Attribution::class)->findBy([
                'user' => $userId,
                'affaire' =>$affaireId
            ]);
            $userPlafond = $userAttribution[0]->getPlafondOperation();

        } else {
            $plafond = null;
            $userPlafond = null;
        }
        

        // on créé notre formulaire pour la commande et on lui indique de gérer les requêtes
        $form_commande = $this->createForm(CreateCommandeType::class, $commande, ['display' => $display]);
        $form_commande->handleRequest($request);
        $status = "error";
        $message = "Mauvaise requête !";

        /**
         * Dans notre cas, on ne post pas notre formulaire directement par le système intégré de Symfony, on passe par une requête AJAX
         * qui nous permet notamment de récupérer en un formulaire (et une requête) à la fois les données du formulaire pour la commande
         * ainsi que les données concernant les articles qui auront été ajoutés par l'utilisateur dans cette commande
         */
        if($request->isXmlHttpRequest()) {
            // on vérifie bien que le requete reçue est en AJAX et qu'elle est de type POST 
            if($request->isMethod('POST')) {
                // on récupère l'objet de la requête
                $data = $request->request;
                // on recupère le tableau des informations de la commande
                $tempDataCommande = $data->get('commande');
                $dataCommande = [];
                $getStringBetween = new getStringBetween('[', ']');
                foreach ($tempDataCommande as $arr) {
                    $key = $getStringBetween->getString($arr['name']);
                    if ($arr['value'] != "") {
                        $dataCommande[$key] = $arr['value'];
                    }
                }
                unset($tempDataCommande);
                unset($dataCommande['_token']);
                unset($dataCommande['radios']);
                foreach ($dataCommande as $key => $value) {
                    if ($key == 'affaire') {
                        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($value);
                        $commande->setAffaire($affaire);
                    } else if ($key == 'client' || $key == 'adresseLivraison' || $key == 'adresseFacturation') {
                        $temp = 'set' . ucfirst($key);
                        $organisme = $this->getDoctrine()->getRepository(Organisme::class)->find($value);
                        $commande->$temp($organisme);
                    } else if ($key == 'contact') {
                        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($value);
                        $commande->setContact($contact);
                    } elseif ($key == 'TVA') {
                        $temp = 'set' . ucfirst($key);
                        $commande->$temp(((float)$value) / 100);
                    } else if ($key == 'totalHT') {
                        $temp = 'set' . ucfirst($key);
                        $commande->$temp((float)str_replace(',', '.', str_replace(' ', '', $value)));
                    } else if ($key == 'documents') {
                        $tempArr = explode(',', $value);
                        $temp = 'set' . ucfirst($key);
                        $commande->$temp($tempArr);
                    } else {
                        $temp = 'set' . ucfirst($key);
                        $commande->$temp($value);
                    }
                }
                if(!$commandeId) {  
                    $commande->setEtat(0);
                } else {
                    $commande->setEtat(1);
                }

                /**
                 * on persist la première partie de notre entité commande
                 * dans le but d'en récupérer l'id dont on a besoin pour créer les articlesCommande
                 */
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);
                $commandeId = $commande->getId();

                // dans le cadre d'une commande ouverte, on vient récupérer ici le montant commandé par le client
                if($data->get('enveloppe')) {
                    $hasEnveloppe = true;
                    $totalHT = $data->get('enveloppe');

                    $checkExisting = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                        'commande' => $commandeId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                } else {
                    // on set une varibale totalHT pour pouvoir après l'ajouter à l'entité commande
                    $totalHT = 0;

                    // on récupère le tableau des différents articles ajoutés à cette commande
                    $dataArticles  = $data->get('articles');
                    // on boucle sur ce tableau pour créer une entité articleCommande pour chaque entrée du tableau
                    for($i = 0; $i < count($dataArticles); $i++) {
                        $articleCommande = new ArticleCommande();

                        if ($commande->getForfaitaire() == false) {
                            // on incrémente notre variable totalHT avec chaque montantTotalHT des articles
                            $totalHT += (float)$dataArticles[$i]["montantTotalHT"];
                        }

                        // on set les propriétés de chaque articleCommande que l'on créé dans la boucle avec les valeurs contenues dans le tableau
                        $articleCommande->setCommande($commande);
                        $articleCommande->setArticleDevis($this->getDoctrine()->getRepository(ArticleDevis::class)->find($dataArticles[$i]["articleId"]));

                        // on vient vérifier qu'il n'y a pas d'autres articles liés à cette commande, si c'est le cas on les supprime
                        $checkExisting = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                            'commande' => $commandeId
                        ]);
                        if($checkExisting) {
                            for ($j = 0; $j < count($checkExisting); $j++) {
                                $em->remove($checkExisting[$j]);
                            }
                        }
                        // si on a un montant unitaire overridé, on l'ajoute à notre article
                        if((float)$dataArticles[$i]["overrideMontantUnit"] != null || (float)$dataArticles[$i]["overrideMontantUnit"] != "") {
                            $articleCommande->setOverrideMontantUnit((float)$dataArticles[$i]["overrideMontantUnit"]);
                        }
                        $articleCommande->setQuantite((int)$dataArticles[$i]["quantite"]);
                        $articleCommande->setMontantTotalHT((float)$dataArticles[$i]["montantTotalHT"]);
                        // on persist l'entité créee à chaque itération de la boucle
                        $em->persist($articleCommande);
                    }
                }
                if ($commande->getForfaitaire() == true) {
                    $totalHT = $commande->getTotalHT();
                }
                // on set le totalHT du devis avec la valeur finale de notre variable après le process de la boucle
                $commande->setTotalHT($totalHT);
                // on vient calculer le totalTTC de notre devis en y appliquant la remise et le taux de TVA
                $totalTTC = $this->calculateTotal($totalHT, $commande->getRemise(), $commande->getTVA());
                // on set le totalTTC du devis avec la valeur retournée par notre fonction de calcul
                $commande->setMontantTotalTTC($totalTTC);

                $em->persist($commande);

                // on inscrit toutes les entités doctrine créées en BDD
                try {
                    $em->flush();
                    $this->reCalculAvancementAffaire($affaire, $em);
                    $status = "success";
                    $message = "Opération enregistrée";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'commande');
                }
                // on indique à notre vue que le processing de l'opération est terminé avec succès
            }
            return new JsonResponse(array('status' => $status, 'message' => $message));
        }

        return $this->render('GestionBundle:Default:manageCommande.html.twig', [
            'form_commande' => $form_commande->createView(),
            'user' => $user,
            'listeArticles' => $listeArticles,
            'affaireId' => $affaireId,
            'nomAffaire' => $nomAffaire,
            'commandeId' => $commandeId,
            'linkedArticleCommande' => $linkedArticleCommande,
            'commande' => $commande,
            'hasAttachements' => $hasAttachements,
            'affaire' => $affaire,
            'display' => $display
        ]);
    }

    /**
     * Quand un devis passe en accepté, je réalise une pré-saisie d'un BdC en requetant sur cette fonction du controller
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse
     */
    public function preSaisieAction(Request $request, $affaireId)
    {
        // on instancie une nouvelle commande via sa Classe
        $commande = new Commande();

        // on récupère l'objet user via l'utilisateur actuellement connecté
        $user = $this->getUser();

        $status = "error";
        $message = "Mauvaise requête !";

        // on vérifie que la requete soit conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;

                // on récupère le devis via son id passée dans la requete
                $devis = $this->getDoctrine()->getRepository(Devis::class)->find($data->get('key'));

                // on définit les différentes propriétés de la nouvelle commande à partir des infos du devis
                $commande->setAffaire($devis->getAffaire());
                $commande->setMentions($devis->getLibelle() . ' [importé depuis le devis]');
                $commande->setClient($devis->getClient());
                $commande->setAdresseLivraison($devis->getAdresseLivraison());
                $commande->setAdresseFacturation($devis->getAdresseFacturation());
                $commande->setForfaitaire($devis->getForfaitaire());
                $commande->setTVA($devis->getTVA());
                $commande->setRemise($devis->getRemise());
                $commande->setUser($devis->getUser());
                $commande->setDateCommande(new DateTime());
                $commande->setEtat(0);
                $commande->setNumeroCommande($this->createNumber->numeroCommande($devis->getAffaire()->getId()));
                $commande->setTotalHT($devis->getTotalHT());
                $commande->setMontantTotalTTC($devis->getTotalTTC());
                $commande->setActiveTVA($devis->getActiveTVA());
                $commande->setDevis($devis);
                $commande->setContact($devis->getContact());
                // on persist cet objet en entité doctrine via le manager de l'ORM
                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);

                // la commande venant d'etre créée en entité Doctrine, ce dernier lui a attribué un id que l'on récupère
                $commandeId = $commande->getId();
                // on récupère tous les articles du devis venant d'etre accepté
                $articlesDevis = $this->getDoctrine()->getRepository(ArticleDevis::class)->findBy([
                    'devis' => $devis->getId()
                ]);

                $totalHT = 0;
                
                // pour chaque article du devis accepté, on créé un nouvel article de comamnde
                for($i = 0; $i < count($articlesDevis); $i++) {
                    $articleCommande = new ArticleCommande();

                    $articleCommande->setCommande($this->getDoctrine()->getRepository(Commande::class)->find($commandeId));
                    $articleCommande->setArticleDevis($articlesDevis[$i]);

                    $checkExisting = $this->getDoctrine()->getRepository(ArticleCommande::class)->findBy([
                        'commande' => $commandeId
                    ]);
                    if($checkExisting) {
                        for ($j = 0; $j < count($checkExisting); $j++) {
                            $em->remove($checkExisting[$j]);
                        }
                    }
                    if($articlesDevis[$i]->getOverrideMontantUnit() != null) {
                        $articleCommande->setOverrideMontantUnit($articlesDevis[$i]->getOverrideMontantUnit());
                    }
                    $articleCommande->setQuantite($articlesDevis[$i]->getQuantite());
                    $articleCommande->setMontantTotalHT($articlesDevis[$i]->getMontantTotalHT());

                    $em->persist($articleCommande);
                }

                $id = $commandeId;
                $mentions = $commande->getMentions();
                $client = $commande->getClient()->getNom();
                $date = $commande->getDateCommande()->format('d/m/Y');
                $userName = $user->getPrenom() . ' ' . $user->getNom();
                if($commande->getEtat() == 0) {
                    $etat = 'Pré-saisie';
                }

                try {
                    $em->flush();
                    $status = "success";
                    $message = "Opération enregistrée";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'commande');
                }    

                // on indique à notre vue que le processing de l'opération est terminé avec succès et on lui passe les information de la nouvelle commande pour son affichage dynamique
                return new JsonResponse(['status' => $status, 'message' => $message,'id' => $id,'numero' => $commande->getNumeroCommande(),'tva' => $commande->getTVA(), 'totalTTC' => $commande->getMontantTotalTTC(),'mentions' => $mentions, 'client' => $client, 'date' => $date, 'user' => $userName, 'totalHT' => $totalHT, 'etat' => $etat]);
            }
        }
    }

    /**
     * Fonction qui va nous permettre de modifier, sur requete Ajax, de modifier l'etat d'une commande
     * @param Request $request
     * @param $commandeId
     * @param $affaireId
     * @return Response
     */
    public function changeEtatAction(Request $request, $commandeId, $affaireId)
    {
        $status = "error";
        $message = "Mauvaise requête !";

        // on récupère la commande concernée via son id passée en parametre de l'url de la requete
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
        $em = $this->getDoctrine()->getManager();

        // on vérifie que la requete soit bien conforme
        if($request->isXmlHttpRequest()) {
            if($request->isMethod('POST')) {
                $data = $request->request;
                // on récupère le flag passé via la requete qui nous indique l'etat à affecter à notre commande
                $data->get("statut") == "cloture" ? $commande->setEtat(2) : $commande->setEtat(3);
                // on sauvegarde notre modification via le manager de doctrine
                $em->persist($commande);
                try {
                    $em->flush();
                    $status = "success";
                    $message = "Etat de la comamnde modifié";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'commande');
                }
            }
        }
        // on réponds à notre controller afin de clore sa requete
        return new Response(json_encode(array($status => $message)));
    }

    /**
     * Méthode de réception des fichiers uploadé
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadFilesAction(Request $request, $commandeId=null) {
        $code = 400;
        $message = 'Mauvaise requête';
        $status = 'error';
        $data = '';
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $error = null;
                /** @var UploadedFile $file */
                $file = $request->files->get('file');
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $fileName = $safeFilename.'-'.uniqid().'.pdf';
                try {
                    $file->move(
                        $this->getParameter('pdf_directory'), $fileName
                    );
                    $code = 200;
                    $status = 'success';
                    $message = 'Le fichier est uploadé avec succés.';
                    $data = $fileName;
                    if ($commandeId != null) {
                        $em = $this->getDoctrine()->getManager();
                        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
                        $docs = $commande->getDocuments();
                        if ($docs == null) {
                            $docs = [];
                        }
                        array_push($docs, $fileName);
                        $commande->setDocuments($docs);
                        $em->persist($commande);
                        try {
                            $em->flush();
                        } catch (\Exception $e) {
                            $status = 'error';
                            $data = $e;
                            $message = 'Une erreur s\'est produite, le fichier a était uploadé mais il n\'a pas était ajouté à la liste des documents de la commande.';
                            $code = 500;
                        }
                    }
                } catch (FileException $e) {
                    $data = $e;
                    $message = 'Une erreur s\'est produite, le fichier n\'a pas était uploadé';
                    $code = 500;
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'fileName' => $data], $code);
    }

    /**
     * Méthode pour supprimer un pdf de la BDD et du dossier
     * @param Request $request
     * @param $commandeId
     * @return JsonResponse
     */
    public function removePdfFromCommandeAction(Request $request, $commandeId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $data = $request->request;
                $dir = $this->getParameter('pdf_directory');
                $em = $this->getDoctrine()->getManager();
                $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
                $filename = $dir . '/' . $data->get('toRemove');
                if (is_file($filename)) {
                    chmod($filename, 0777);

                    if (unlink($filename)) {
                        $code = 200;
                        $message = 'Fichier supprimer avec succés.';
                    } else {
                        $code = 500;
                        $message = 'Une erreur s\'est produite et le fihsier n\'a pas pu être supprimer';
                    }
                } else {
                    $code = 200;
                    $message = 'Le fichier demandé n\'existe pas.';
                }
                if ($code === 200) {
                    $nameList = $data->get('nameList');
                    is_array($nameList) ? $commande->setDocuments($nameList): $commande->setDocuments([]);
                    $em->persist($commande);
                    try {
                        $status = 'success';
                        $em->flush();
                        $message = 'Le fichier a bien était supprimé';
                    } catch (\Exception $e) {
                        $message .= ' Mais une erreur s\'est produite et le fichier apparé toujours dans la liste. Contactez le support pour résoudre ce problème.';
                    }
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour changer le
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDocTypeAction(Request $request, $commandeId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $em = $this->getDoctrine()->getManager();
                $commande = $this->getDoctrine()->getRepository(Commande::class)->find($commandeId);
                $data = $request->request;
                $commande->setTypeDocument($data->get('docType'));
                $commande->setNumeroCommandeClient($data->get('numCMD'));
                $em->persist($commande);
                try {
                    $em->flush();
                    $status = 'success';
                    $message = 'Informations mises à jour avec succés.';
                    $code = 200;
                } catch(\Exception $e) {
                    $code = 500;
                }
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }
}