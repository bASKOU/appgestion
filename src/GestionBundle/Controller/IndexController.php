<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\CommandeFournisseur;
use GestionBundle\Entity\CommandePrestataire;
use GestionBundle\Entity\Devis;
use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\Attribution;
use GestionBundle\Form\CreateAffaireType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * Méthode permettant de trier les devis par date
     * @param $devis
     */
    private function sortDateDevis(&$devis)
    {
        usort($devis, function($a, $b) {
            if (strtotime($a->getDateDevis()->format('Y-m-d H:i:s')) === strtotime($b->getDateDevis()->format('Y-m-d H:i:s')))
            {
                return 0;
            }
            return (strtotime($a->getDateDevis()->format('Y-m-d H:i:s')) < strtotime($b->getDateDevis()->format('Y-m-d H:i:s'))) ? +1 : -1;
        });
    }

    /**
     * Méthode pour récupérer la liste des affaires
     * @param $args
     * @return array|void
     */
    private function getAffaireList($args, $affaireList = [])
    {
        if (is_array($args)) {
            foreach ($args as $key => $arg) {
                $affaire = $arg->getAffaire();
                if (!in_array($affaire, $affaireList)) {
                    array_push($affaireList, $affaire);
                }
            }
            return $affaireList;
        } else {
            $affaire = $args->getFacture()->getAffaire();
            if (!in_array($affaire, $affaireList)) {
                return   $args->getFacture()->getAffaire();
            }
        }
    }

    private function countDevisToValidate()
    {
        $em = $this->getDoctrine()->getManager();
        $sql = 'SELECT COUNT(*) FROM devis where affaire_id IS NULL and validation = 0';
        $result = $em->getConnection()->FetchAll($sql);
        return $result[0]['count'];
    }

    private function countCommandeToValidate()
    {
        $em = $this->getDoctrine()->getManager();
        $sql = 'SELECT COUNT(*) FROM commande_fournisseur where affaire_id IS NULL and validation = 0';
        $result = $em->getConnection()->FetchAll($sql);
        return $result[0]['count'];
    }

    /**
     * Méthode de base qui permet de gérer le tableau de bord
     * @return Response
     */
    public function indexAction()
    {
        // on liste toutes les affaires
        $listeAffaire = $this->getDoctrine()->getRepository(Affaire::class)->findAll();

        // on récupère l'objet user via l'id de l'utilisateur connecté
        $userId = $this->getUser()->getId();

        $devisToValidate = $this->countDevisToValidate();
        $commandeToValidate = $this->countCommandeToValidate();

        // on récupère les attributions de l'user connecté
        $userAttribution = $this->getDoctrine()->getRepository(Attribution::class)->findBy([
            'user' => $userId
        ]);
        $commandeFournisseur = null;
        $commandePrestataire = null;
        // on récupère tous les devis n'appartenenant pas à une affaire
        if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS' || $this->getUser()->getRoles()[0] === 'ROLE_SUDALYS_ADMINISTRATIF') {
            if ($this->getUser()->getRoles()[0] === 'ROLE_SUPER_SUDALYS') {
                $orphanDevisAll = $this->getDoctrine()->getRepository(Devis::class)->findBy([
                    'affaire' => null
                ]);
            }
            $commandeFournisseur = $this->getDoctrine()->getRepository(CommandeFournisseur::class)->findBy(['affaire' => null]);
            $commandePrestataire = $this->getDoctrine()->getRepository(CommandePrestataire::class)->findBy(['affaire' => null]);
        } else {
            // on récupère les auteurs des devis orphelins
            $orphanDevisAll = $this->getDoctrine()->getRepository(Devis::class)->findBy([
                'affaire' => null,
                'user' => $userId
            ]);
        }

        $this->sortDateDevis($orphanDevisAll);

        return $this->render('GestionBundle:Default:index.html.twig', [
            "listeAffaire" => $listeAffaire,
            "userAttribution" => $userAttribution,
            "orphanDevisAll" => $orphanDevisAll,
            "commandeFournisseur" => $commandeFournisseur,
            'devisToValidate' => $devisToValidate,
            'commandeToValidate' => $commandeToValidate,
            'commandePrestataire' => $commandePrestataire
        ]);
    }
}
