<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\EstimationFournisseur;
use GestionBundle\Entity\EstimationPrestation;
use GestionBundle\Form\EditEstimationFournisseurType;
use GestionBundle\Form\EditEstimationPrestationType;
use GestionBundle\Form\EstimationFournisseurType;
use GestionBundle\Form\EstimationPrestationType;
use GestionBundle\Services\ExceptionHandler;
use GestionBundle\Services\NumberCheckAndRenew;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EstimationController extends Controller
{
    /**
     * @var ExceptionHandler
     */
    private $exceptionHandler;
    /**
     * @var NumberCheckAndRenew|object
     */
    private $createNumber;

    /**
     * FacturePrestataireController constructor.
     * @param NumberCheckAndRenew $numberCheckAndRenew
     */
    public function __construct(NumberCheckAndRenew $numberCheckAndRenew)
    {
        $this->exceptionHandler = new ExceptionHandler();
        $this->createNumber = $numberCheckAndRenew;
    }

    /**
     * Méthode qui vérifie si le montant total des estimation d'une facture déjà existante et de la nouvelle estimation ne dépasse pas le montant HT de la facture
     * @param $estimation
     * @return array
     */
    private function checkEstimLessThanFacture($estimation)
    {
        $facture = $estimation->getFacture();
        $count = 0;
        if ($facture) {
            $totalEstim = (float) $estimation->getTotalHT();
            $totalFacture = (float) $facture->getTotalHT();
            $estimations = $facture->getEstimation();
            $count = count($estimations);
            foreach ($estimations as $estim) {
                $totalEstim += (float) $estim->getTotalHT();
            }
            $difference = $totalFacture - $totalEstim;
            if ($difference < 0) {
                return ['etat' => true, 'count' => $count];
            }
        }

        return ['etat' => false, 'count' => $count];
    }

    /**
     * Méthode pour la création d'une estimation de prestation
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse|Response
     */
    public function estimationPrestationAction(Request $request, $affaireId)
    {
        $estimation = new EstimationPrestation();
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);

        $estimation->setAffaire($affaire);
        $estimation->setNumero($this->createNumber->generateFromLastNumberEstimationPrestation());
        $estimation->setActif(true);
        $estimation->setDate(new \DateTime(('today')));

        $form = $this->createForm(EstimationPrestationType::class, $estimation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $code = 200;
            $status = 'error';
            $message = '';
            $check = $this->checkEstimLessThanFacture($estimation);
            if($check['etat']) {
                if ($check['count'] > 1) {
                    $message = "L'ensemble des estimations de la facture dépassent le montant total de la facture de rattachement.";
                } else {
                    $message = "Le montant de l'estimation dépasse le montant total de la facture de rattachement.";
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($estimation);

                try {
                    $em->flush();
                    $status = 'success';
                    $message = 'La prévision de charges de prestation a bien été enregistré.';
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'prévision de charges');
                }
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createEstimationAjax.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour l'édition d'une estimation de prestations
     * @param Request $request
     * @param $estimationId
     * @return JsonResponse|Response
     */
    public function editEstimationPrestationAction(Request $request, $estimationId)
    {
        $estimation = $this->getDoctrine()->getRepository(EstimationPrestation::class)->find($estimationId);

        $form = $this->createForm(EditEstimationPrestationType::class, $estimation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $status = 'error';
            $code = 200;
            $message = '';
            $check = $this->checkEstimLessThanFacture($estimation);
            if($check['etat']) {
                if ($check['count'] > 1) {
                    $message = "L'ensemble des estimations de la facture dépassent le montant total de la facture de rattachement.";
                } else {
                    $message = "Le montant de l'estimation dépasse le montant total de la facture de rattachement.";
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($estimation);

                try {
                    $em->flush();
                    $status = 'success';
                    $message = "L'édition de l'estimation a été réalisé avec succès.";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'estimation de charges');
                }
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createEstimationAjax.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour la suppression d'une estimation de prestations
     * @param Request $request
     * @param $estimationId
     * @return JsonResponse
     */
    public function deleteEstimationPrestationAction(Request $request, $estimationId)
    {
        $status = 'error';
        $code = 400;
        $message = 'Mauvaise requête';

        if ($request->isMethod('GET')) {
            $status = 'success';
            $message = "
                <p class='text-center'>Vous êtes sur le point de supprimer une estimation de prestations.</p>
                <p class='text-center'>Etes-vous sur de vouloir supprimer l'estimation ?</p>
                ";
            $code = 200;
        } elseif ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $estimation = $this->getDoctrine()->getRepository(EstimationPrestation::class)->find($estimationId);
            $em->remove($estimation);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "L'estimations de charge de prestataions a bien été effectué.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'estimation de charges');
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }

    /**
     * Méthode pour la création d'une estimation de matériel
     * @param Request $request
     * @param $affaireId
     * @return JsonResponse|Response
     */
    public function estimationFournisseurAction(Request $request, $affaireId)
    {
        $estimation = new EstimationFournisseur();
        $affaire = $this->getDoctrine()->getRepository(Affaire::class)->find($affaireId);

        $estimation->setAffaire($affaire);
        $estimation->setNumero($this->createNumber->generateFromLastNumberEstimationFournisseur());
        $estimation->setActif(true);
        $estimation->setDate(new \DateTime(('today')));

        $form = $this->createForm(EstimationFournisseurType::class, $estimation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $code = 200;
            $status = 'error';
            $message = '';

            $check = $this->checkEstimLessThanFacture($estimation);
            if($check['etat']) {
                if ($check['count'] > 1) {
                    $message = "L'ensemble des estimations de la facture dépassent le montant total de la facture de rattachement.";
                } else {
                    $message = "Le montant de l'estimation dépasse le montant total de la facture de rattachement.";
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($estimation);

                try {
                    $em->flush();
                    $status = 'success';
                    $message = 'L\'estimation de charges de matériels a bien été enregistré.';
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'estimation de charges');
                }
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createEstimationAjax.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour l'édition d'une estimation de matériels
     * @param Request $request
     * @param $estimationId
     * @return JsonResponse|Response
     */
    public function editEstimationFournisseurAction(Request $request, $estimationId)
    {
        $estimation = $this->getDoctrine()->getRepository(EstimationFournisseur::class)->find($estimationId);

        $form = $this->createForm(EditEstimationFournisseurType::class, $estimation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $status = 'error';
            $code = 200;
            $message = '';

            $check = $this->checkEstimLessThanFacture($estimation);
            if($check['etat']) {
                if ($check['count'] > 1) {
                    $message = "L'ensemble des estimations de la facture dépassent le montant total de la facture de rattachement.";
                } else {
                    $message = "Le montant de l'estimation dépasse le montant total de la facture de rattachement.";
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($estimation);

                try {
                    $em->flush();
                    $status = 'success';
                    $message = "L'édition de l'estimation a été réalisé avec succès.";
                } catch (\Exception $e) {
                    $error = $this->exceptionHandler->getException($e);
                    $message = $this->exceptionHandler->exceptionHandler($error, 'estimation de charges');
                }
            }

            return new JsonResponse(['status' => $status, 'message' => $message], $code);
        }

        return $this->render('GestionBundle:Default:createEstimationAjax.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour la suppression d'une estimation de matériels
     * @param Request $request
     * @param $estimationId
     * @return JsonResponse
     */
    public function deleteEstimationFournisseurAction(Request $request, $estimationId)
    {
        $status = 'error';
        $code = 400;
        $message = 'Mauvaise requête';

        if ($request->isMethod('GET')) {
            $status = 'success';
            $message = "
                <p class='text-center'>Vous êtes sur le point de supprimer une estimation de matériels.</p>
                <p class='text-center'>Etes-vous sur de vouloir supprimer l'estimation ?</p>
                ";
            $code = 200;
        } elseif ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $estimation = $this->getDoctrine()->getRepository(EstimationFournisseur::class)->find($estimationId);
            $em->remove($estimation);
            $code = 200;
            try {
                $em->flush();
                $status = 'success';
                $message = "L'estimations de charge de matériels a bien été effectué.";
            } catch (\Exception $e) {
                $error = $this->exceptionHandler->getException($e);
                $message = $this->exceptionHandler->exceptionHandler($error, 'estimation de charges');
            }
        }

        return new JsonResponse(['status' => $status, 'message' => $message], $code);
    }
}