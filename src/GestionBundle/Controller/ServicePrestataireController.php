<?php

namespace GestionBundle\Controller;

use GestionBundle\Entity\Organisme;
use GestionBundle\Entity\ServicePrestataire;
use GestionBundle\Form\ServicePrestataireType;
use GestionBundle\Services\ExceptionHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ServicePrestataireController extends Controller
{
    private $exceptionHandler;

    public function __construct()
    {
        $this->exceptionHandler = new ExceptionHandler();
    }

    /**
     * Méthode pour passer les props privées en public afin de les retourner en JSON
     * @param $servicePrestataire
     * @return array
     */
    private function hydradeObjectServicePrestataire($servicePrestataire)
    {
        $servicePrestataireProps = ['id', 'designation', 'description', 'montantUnit', 'parent', 'dateValid', 'dateCreate', 'referencePrestataire', 'prixFixe', 'prestataire'];
        $toReturn = [];
        foreach ($servicePrestataireProps as $prop) {
            if ($prop == 'prestataire') {
                $toReturn[$prop] = $servicePrestataire->getPrestataire()->getId();
            } else {
                $temp = 'get' . ucfirst($prop);
                $toReturn[$prop] = $servicePrestataire->$temp();
            }
        }
        return $toReturn;
    }

    /**
     * Méthode d'hydratation
     * @param $a
     * @return array
     */
    private function hydradeObjectForEqual($a)
    {
        $produitFournisseurProps = ['designation', 'description', 'montantUnit', 'parent', 'prestataire'];
        $toReturn = [];
        foreach ($produitFournisseurProps as $prop) {
            if ($prop == 'organisme') {
                $toReturn[$prop] = $a->getOrganisme()->getId();
            } else {
                $temp = 'get' . ucfirst($prop);
                $toReturn[$prop] = $a->$temp();
            }
        }
        return $toReturn;
    }

    /**
     * Méthode pour vérifié si deux objet doctrine sont identique
     * @param $a
     * @param $b
     * @return bool
     */
    private function checkEqualEntity($a, $b)
    {
        $toReturn = false;
        $arrA = $this->hydradeObjectForEqual($a);
        $arrB = $this->hydradeObjectForEqual($b);
        $arrDiff = array_diff($arrA, $arrB);
        if (empty($arrDiff[0])) {
            $toReturn = true;
        }
        return $toReturn;
    }

    /**
     * Méthode pour créer un service pour un prestataire
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse|Response
     */
    public function createForOrgaAction(Request $request, $orgaId)
    {
        $service = new ServicePrestataire();
        $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
        $service->setPrestataire($orga);
        $form = $this->createForm(ServicePrestataireType::class, $service);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')) {
                $code = 400;
                $status = 'error';
                $message = 'Mauvaise requête.';
                $dataProduct = [];
                $em = $this->getDoctrine()->getManager();
                $data = $_POST['gestionbundle_serviceprestataire'];
                unset($data['_token']);
                foreach ($data as $key => $value) {
                    if ($key == "montantUnit" && $value != '') {
                        $value = str_replace(' ', '', str_replace(',', '.', $value));
                        $temp = 'set' . ucfirst($key);
                        $service->$temp((float)$value);
                    } else if ($value != '') {
                        $temp = 'set' . ucfirst($key);
                        $service->$temp($value);
                    }
                }
                $this->hydradeObjectServicePrestataire($service);
                $existingService = $this->getDoctrine()->getRepository(ServicePrestataire::class)->findBy(['designation' => $service->getDesignation(), 'prestataire' => $orga]);
                $parent = '';
                $match = null;
                if ($existingService) {
                    foreach ($existingService as $serv) {
                        if (!$serv->getParent()) {
                            $parent = $serv;
                        }
                        if ($this->checkEqualEntity($serv, $service)) {
                            $match = $serv;
                            goto haveMatch;
                        }
                    }
                }
                if ($match) {
                    haveMatch:
                    $dataProduct = $this->hydradeObjectServicePrestataire($match);
                    $code = 200;
                    $status = 'success';
                    $message = 'Ce service existe déjà pour ce prestataire.';
                } else {
                    if ($parent) {
                        $service->setParent($parent->getId());
                    }
                    $service->setDateCreate(new \DateTime('now'));
                    $em->persist($service);
                    try {
                        $em->flush();
                        $status = 'success';
                        $code = 200;
                        $message = 'Le service du prestataire à bien été créé';
                        $dataProduct = $this->hydradeObjectServicePrestataire($service);
                    } catch (\Exception $e) {
                        $code = 200;
                        $message = $this->exceptionHandler->getException($e);
                    }
                }
                return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $dataProduct], $code);
            }
        }

        return $this->render('GestionBundle:Default:createServicePrestataire.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Méthode pour récupérer la liste des services d'un prestataire
     * @param Request $request
     * @param $orgaId
     * @return JsonResponse
     */
    public function prestataireServiceListAction(Request $request, $orgaId)
    {
        $code = 400;
        $status = 'error';
        $message = 'Mauvaise requête.';
        $data = [];

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('GET')) {
                $orga = $this->getDoctrine()->getRepository(Organisme::class)->find($orgaId);
                $products = $orga->getServicePrestataire();
                $code = 200;
                if (!empty($products[0])) {
                    $status = 'products';
                    $message = 'Liste des services du prestataire chargé.';
                    foreach ($products as $product) {
                        array_push($data, $this->hydradeObjectServicePrestataire($product));
                    }
                } else {
                    $status = 'noproducts';
                    $message = 'Ce prestataire n\'a aucun service enregistré. Créez le premier service';
                }
            }
        }
        return new JsonResponse(['status' => $status, 'message' => $message, 'data' => $data], $code);
    }
}