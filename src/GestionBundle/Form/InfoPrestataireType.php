<?php

namespace GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfoPrestataireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isCommercantArtisant', ChoiceType::class, [
                'label' => 'Est-ce un prestataire commerçant/artisant ?',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ]
            ])
            ->add('RIB', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => 'RIB fourni'
            ])
            ->add('RIBDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => 'Télécharger le RIB'
            ])
            ->add('kbis', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => 'KBIS fourni'
            ])
            ->add('kbisDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger le KBIS"
            ])
            ->add('dateKbis', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'type' => 'date',
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => 'Date d\'émission du KBIS'
            ])
            ->add('responsabiliteProfessionnel', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => 'Justificatif de la responsabilité professionnelle fournis'
            ])
            ->add('renouvellementDecembreResponsabiliteProfessionnel', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "La responsabilité professionnelle se renouvelle au 31 Décembre ?"
            ])
            ->add('responsabiliteProfessionnelDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger le justificatif de la responsabilité professionnelle"
            ])
            ->add('dateResponsabiliteProfessionnel', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date de fin de la responsabilité professionnelle"
            ])
            ->add('attestationFiscale', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Attestation fiscale fournie"
            ])
            ->add('attestationFiscaleDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger l'attestation ficale"
            ])
            ->add('dateAttestationFiscale', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date de l'émission de l'attestation fiscale"
            ])
            ->add('attestationURSSAF', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Attestation URSSAF fournie"
            ])
            ->add('attestationURSSAFDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger l'attestation URSSAF"
            ])
            ->add('dateAttestationURSSAF', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date de l'émission de l'attestation URSSAF"
            ])
            ->add('listeSalarieEtranger', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Liste des salariés étrangé et préposés fournie"
            ])
            ->add('listeSalarieEtrangerDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger la liste des salariés étranger et préposés"
            ])
            ->add('certificatCDAPH', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Certificat CDAPH fourni"
            ])
            ->add('certificatCDAPHDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger le certificat CDAPH"
            ])
            ->add('qualificationsProfessionnelles', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'required' => true,
                'label' => "Qualifications professionnelles fournies"
            ])
            ->add('qualificationsProfessionnellesDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger les qualifications professionnelles"
            ])
            ->add('devis', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Devis fourni"
            ])
            ->add('documentPublicitaire', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Document publicitaire fourni"
            ])
            ->add('correspondanceProfessionnelle', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Document de correspondance professionnelle fourni"
            ])
            ->add('recepisseDepotDeclaration', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Récépissé du dépot de déclaration fourni"
            ])
            ->add('recepisseDepotDeclarationDocument', FileType::class, [
                    'required' => false,
                    'attr' => [
//                        'hidden' => true,
                        'accept' => '.pdf',
                        'lang' => 'fr'
                    ],
                    "data_class" => null,
                    'empty_data' => null,
                    'multiple' => true,
                'label' => "Télécharger le récépissé du depot de déclaration"
            ])
            ->add('autreDocument', FileType::class, [
                    'required' => false,
                    'attr' => [
//                        'hidden' => true,
                        'accept' => '.pdf',
                        'lang' => 'fr'
                    ],
                    "data_class" => null,
                    'empty_data' => null,
                    'multiple' => true,
                'label' => "Télécharger devis/correspondance professionnelle/document publicitaire"
            ])
            ->add('registreProfessionelCertifInscription', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Registre professionnel/Certificat d'inscription fourni"
            ])
            ->add('registreProfessionelCertifInscriptionDocument', FileType::class, [
                    'required' => false,
                    'attr' => [
//                        'hidden' => true,
                        'accept' => '.pdf',
                        'lang' => 'fr'
                    ],
                    "data_class" => null,
                    'empty_data' => null,
                    'multiple' => true,
                'label' => "Télécharger le registre proffessionnel/certificat d'inscription"
            ])
            ->add('dateRegistreProfessionelCertifInscription', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date d'émission du registre professionnel/certificat d'inscription"
            ])
            ->add('attestationImmatriculation', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Attestation d'immatriculation fournie"
            ])
            ->add('attestationImmatriculationDocument', FileType::class, [
                    'required' => false,
                    'attr' => [
//                        'hidden' => true,
                        'accept' => '.pdf',
                        'lang' => 'fr'
                    ],
                    "data_class" => null,
                    'empty_data' => null,
                    'multiple' => true,
                'label' => "Télécharger l'attestation d'immatriculation"
            ])
            ->add('dateAttestationImmatriculation', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date d'émission de l'attestation d'immatriculation"
            ])
            ->add('fiscalEtranger', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Document fiscale étrangé fourni"
            ])
            ->add('fiscalEtrangerDocument', FileType::class, [
                    'required' => false,
                    'attr' => [
//                        'hidden' => true,
                        'accept' => '.pdf',
                        'lang' => 'fr'
                    ],
                    "data_class" => null,
                    'empty_data' => null,
                    'multiple' => true,
                'label' => "Télécharger le document fiscale étrangé"
            ])
            ->add('socialEtranger', ChoiceType::class, [
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'label' => "Document de sécurité sociale étrangère fourni"
            ])
            ->add('socialEtrangerDocument', FileType::class, [
                'required' => false,
                'attr' => [
//                    'hidden' => true,
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true,
                'label' => "Télécharger le document de sécurité sociale étrangère"
            ])
            ->add('dateSocialEtranger', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'required' => false,
                'label' => "Date d'émission du document de sécurité sociale étrangère"
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\InfoPrestataire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_infoprestataire';
    }


}
