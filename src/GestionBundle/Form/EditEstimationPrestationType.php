<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditEstimationPrestationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prestataire', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'choice_label' => 'nom',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control prestataire',
                    'data-live-search' => true
                ]
            ])
            ->add('facture', EntityType::class, [
                'class' => 'GestionBundle\Entity\FacturePrestataire',
                'placeholder' => 'Choisir une facture de rattachement',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->where('f.validation = 1');
                },
                'attr' => [
                    'class' => 'selectpicker large-bootstrap-select custom-select form-control prestataire',
                    'data-live-search' => true
                ],
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'estimation',
                'attr' => [
                    'class' => 'auto-grow'
                ],
                'required' => true
            ])
            ->add('totalHT', MoneyType::class, [
                'label' => "Total de l'estimation",
                'required' => true,
                'attr' => [
                    'class' => 'comma text-right'
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\EstimationPrestation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_estimationprestation';
    }


}
