<?php

namespace GestionBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitFournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisme', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'choice_label' => 'nom',
                'required' => true,
                'attr' => [
                    'disabled' => true
                ]
            ])
            ->add('referenceFournisseur', TextType::class, [
                'label' => 'Référence produit du fournisseur',
                'attr' => [
                    'placeholder' => 'Entrez la référence fournisseur si existante'
                ],
                'required' => false
            ])
            ->add('designation', TextType::class, [
                'label' => 'Désignation',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Entrez le nom du produit'
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Entrez la desciption du produit'
                ]
            ])
            ->add('montantUnit', MoneyType::class, [
                'label' => 'Montant Unitaire',
                'attr' => [
                    'class' => 'comma'
                ],
                'required' => false,
                'empty_data' => null
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\ProduitFournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_produitfournisseur';
    }


}
