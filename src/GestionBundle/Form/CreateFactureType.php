<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Facture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateFactureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('affaire', EntityType::class, [
            'class' => 'GestionBundle:Affaire',
            'choice_label' => 'designationAffaire',
            'label' => 'Affaire',
            'required' => true
        ])
        ->add('client', EntityType::class, [
            'class' => 'GestionBundle:Organisme',
            'choice_label' => 'nom',
            'label' => 'Organisme',
            'attr' => [
                'class' => 'selectpicker custom-select form-control',
                'data-live-search' => "true"
            ],
            'required' => true
        ])
        ->add('numeroFacture', TextType::class, [
            'required' => false,
            'label' => 'Numéro facture automatique',
            'attr' => [
                'readonly' => true
            ]
        ])
        ->add('numeroFactureBis', TextType::class, [
            'required' => false,
            'label' => 'Numéro facture manuel',
            'attr' => [
                'placeholder' => 'Uniquement pour une facture saisie préalablement'
            ]
        ])
        ->add('mentions', TextareaType::class, [
            'label' => 'Mentions',
            'required' => true
        ])
        ->add('contact', EntityType::class, [
            'class' => 'GestionBundle:Contact',
            'attr' => [
                'data-live-search' => "true"
            ],
            'required' => false
        ]);
        if ($options['TVA'] == true) {
            $builder
                ->add('TVA', PercentType::class, [
                    'label' => 'Taux de TVA appliqué : ',
                    'type' => 'fractional',
                    'attr' => [
                        'value' => 20
                    ],
                    'empty_data' => null,
                    'required' => false
                ]);
        } else {
            $builder
                ->add('TVA', HiddenType::class, [
                    'label' => 'Taux de TVA appliqué : ',
                    'attr' => [
                        'value' => 0
                    ],
                    'empty_data' => null,
                    'required' => false
                ]);
        }
        $builder
            ->add('activeTVA', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Assugéti TVA',
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'required' => true
            ])
            ->add('remise', PercentType::class, [
                'label' => 'Remise accordée',
                'type' => 'fractional',
                'required' => false,
                'attr' => [
                    'readonly' => true,
                    'placeholder' => '0'
                ]
            ])
            ->add('indiceRevision', TextType::class, [
                'label' => 'Indice de révision',
                'required' => false,
                'empty_data' => 1,
                'attr' => [
                    'placeholder' => '1,00'
                ]
            ])
            ->add('tauxInteret', PercentType::class, [
                'label' => 'Taux d\'intérêt pour retard de paiement',
                'required' => false,
                'scale' => 2
            ])
            ->add('indemniteForfait', MoneyType::class, [
                'label' => 'Indemnitées forfaitaire de retard',
                'required' => true
            ])
            ->add('echeance', ChoiceType::class, [
                'label' => 'Echéance',
                'choices' => [
                    '30 jours' => 1,
                    '45 jours' => 2,
                    'à réception' => 3
                ],
                'required' => true,
                'multiple' => false
            ])
            ->add('dateEcheance', DateType::class, [
                'label' => 'Date échéance',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true
            ])
            ->add('forfaitaire', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Facture forfaitaire',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'required' => true
            ])
            ->add('adresseFacturation', ChoiceType::class, [
                'label' => 'Adresse de facturation',
                'placeholder' => 'Sélectionner une adresse de facturation',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control facturation',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('adresseLivraison', ChoiceType::class, [
                'label' => 'Adresse de livraison',
                'placeholder' => 'Sélectionner une adresse de livraison',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control livraison',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Facture::class,
            'TVA' => null
        ]);
    }
}