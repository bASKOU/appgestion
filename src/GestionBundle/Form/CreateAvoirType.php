<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Avoir;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateAvoirType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facture', EntityType::class, [
                'class' => 'GestionBundle:Facture',
                'required' => true,
                'choice_label' => 'numeroFacture',
                'attr' => [
                    'disabled' => true
                ]
            ])
            ->add('montant', TextType::class, [
                'required' => true,
                'label' => 'Montant de l\'avoir',
                'attr' => [
                    'class' => 'comma'
                ]
            ])
            ->add('motif', TextareaType::class, [
                'required' => true,
                'label' => 'Motif de l\'avoir'
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Avoir::class,
        ]);
    }
}
