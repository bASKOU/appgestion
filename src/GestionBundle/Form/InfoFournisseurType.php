<?php

namespace GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfoFournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('RIB', ChoiceType::class, [
                    'expanded' => false,
                    'label' => 'RIB reçus',
                    'choices' => [
                        'Non' => false,
                        'Oui' => true
                    ],
                    'required' => true,
                    'empty_data' => false
            ])
            ->add('RIBDocument', FileType::class, [
                'required' => false,
                'label' => 'RIB document',
                'attr' => [
                    'accept' => '.pdf',
                    'lang' => 'fr'
                ],
                "data_class" => null,
                'empty_data' => null,
                'multiple' => true
            ])
//            ->add('update', SubmitType::class, [
//                'label' => 'Mettre à jour'
//            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\InfoFournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_infofournisseur';
    }


}
