<?php


namespace GestionBundle\Form;


use GestionBundle\Entity\Contact;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class joinContactAjaxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisme', HiddenType::class);
        if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $options['role'])) {
            $builder
                ->add('type', EntityType::class, [
                    'class' => 'GestionBundle\Entity\TypeContact',
                    'choice_label' => 'type',
                    'multiple' => true,
                    'expanded' => true
                ]);
        } else {
            if (!$options['type']) {
                $builder
                    ->add('type', EntityType::class, [
                        'class' => 'GestionBundle\Entity\TypeContact',
                        'choice_label' => 'type',
                        'attr' => [
                            'readonly' => true
                        ]
                    ]);
            } else {
                $builder
                    ->add('type', HiddenType::class);
            }
        }
        $builder
            ->add('nom', TextType::class, [
                'required' => true
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Prénom',
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'required' => true
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone',
                'required' => false]
            )
            ->add('mobile', TelType::class, [
                'required' => false]
            )
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'role' => null,
            'type' => false
        ]);
    }
}