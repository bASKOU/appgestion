<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Theme;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateThemeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Veuillez indiquer la désignation de l\'Affaire',
                'required'=> true,
                'error_bubbling' => true
            ])
            ->add('ajouter', SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Theme::class
        ]);
    }
    private function numeroAffaire($id)
    {
        $debut = 'CA';
        $anne = date('y');
        $toString = strval($id);
        $length = strlen($toString);
        $loop = 3 - $length;
        $complement = '';
        for ($i = 1; $i <= $loop; $i++) {
            $complement .= '0';
        }
        return $debut . $anne . '-' . $complement . $toString;
    }
}