<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Organisme;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateOrgaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', EntityType::class, [
                'label' => 'Organisme de rattachement',
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un organisme de rattachement si besoin',
                'choice_label' => 'nom',
                'required' => false,
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ]
            ])
            ->add('nom', TextType::class, [
                'label' => 'Nom de l\'Organisme',
                'required' => true
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Client' => 1,
                    'Fournisseur' => 0,
                    'Prestataire' => 2
                ],
                'multiple' => false,
                'required' => true
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('adresseComplementaire1', TextType::class, [
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('adresseComplementaire2', TextType::class, [
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('stal', TextType::class, [
                'label' => 'Code Postal',
                'attr' => [
                    'maxlength' => 5,
                    'pattern' => '[0-9]{5}'
                ],
                'required' => true
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
                'required' => true,
                'attr' => [
                    'class' => 'upper'
                ]
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse email service comptable',
                'required' => false
            ])
            ->add('modifier', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-light'
                ]
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organisme::class,
            'allow_extra_fields' => true,
            'role' => null
        ]);
    }
}