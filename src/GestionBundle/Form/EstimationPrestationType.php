<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimationPrestationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prestataire', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un prestataire',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->addOrderBy('o.nom', 'ASC')
                        ->where('o.type = 2')
                        ->andWhere('o.actif = true')
                        ->andWhere('o.prestataireValide = true');
                },
                'attr' => [
                    'class' => 'selectpicker custom-select form-control prestataire',
                    'data-live-search' => true
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'estimation',
                'attr' => [
                    'class' => 'auto-grow'
                ],
                'required' => true
            ])
            ->add('totalHT', MoneyType::class, [
                'label' => "Total de l'estimation",
                'required' => true,
                'attr' => [
                    'class' => 'comma text-right'
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\EstimationPrestation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_estimationprestation';
    }


}
