<?php


namespace GestionBundle\Form;


use GestionBundle\Entity\Contact;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organisme', EntityType::class, [
                'class' => 'GestionBundle:Organisme',
                'choice_label' => 'nom',
                'required' => true
            ]);
        if (in_array('ROLE_SUDALYS_ADMINISTRATIF', $options['role'])) {
            $builder
                ->add('type', EntityType::class, [
                    'class' => 'GestionBundle\Entity\TypeContact',
                    'choice_label' => 'type',
                    'multiple' => true,
                    'expanded' => true
                ]);
        } else {
            $builder
                ->add('type', EntityType::class, [
                    'class' => 'GestionBundle\Entity\TypeContact',
                    'choice_label' => 'type',
                    'attr' => [
                        'readonly' => true
                    ]
                ]);
        }
        $builder
            ->add('nom', TextType::class, [
                'required' => true
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Prénom',
                'required' => true,
                'attr' => [
                    'class' => 'ucfirst'
                ]
            ])
            ->add('email', EmailType::class)
            ->add('telephone', TelType::class, [
                    'label' => 'Téléphone',
                    'required' => false
            ])
            ->add('mobile', TelType::class, [
                'required' => false
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'role' => null
        ]);
    }
}