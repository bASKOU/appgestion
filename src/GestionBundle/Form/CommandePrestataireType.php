<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandePrestataireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroCommandePresta', TextType::class, [
                'label' => 'Numéro de commande prestataire'
            ])
            ->add('mentions', TextareaType::class, [
                'label' => 'libellé'
            ])
            ->add('detailCommandePresta', TextareaType::class, [
                'label' => 'Détail de la commande',
                'attr' => [
                    'class' => 'auto-grow'
                ],
                'required' => false
            ])
            ->add('totalHT', TextType::class, [
                'label' => 'Montant total HT',
                'attr' => [
                    'class' => 'comma'
                ]
            ])
            ->add('TVA', PercentType::class, [
                'label' => 'Taux de TVA',
                'attr' => [
                    'value' => 20
                ]
            ])
            ->add('montantTotalTTC', TextType::class, [
                'label' => 'Montant total TTC',
                'attr' => [
                    'class' => 'comma'
                ]
            ])
            ->add('adresseFacturation', TextType::class, [
                'label' => 'Adresse de facturation'
            ])
            ->add('affaire', EntityType::class, [
                'placeholder' => 'Choisir une affaire si besoin',
                'class' => 'GestionBundle\Entity\Affaire',
                'choice_label' => 'designationAffaire',
                'placeholder' => 'Sélectionner une affaire',
                'required' => false
            ])
            ->add('prestataire', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un prestataire',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->addOrderBy('o.nom', 'ASC')
                        ->where('o.type = 2')
                        ->andWhere('o.actif = true')
                        ->andWhere('o.prestataireValide = true');
                }
            ])
            ->add('contact', EntityType::class, [
                'class' => 'GestionBundle\Entity\Contact',
                'choice_label' => 'nom',
                'placeholder' => 'Sélectionner un contact'
            ]);
            if($options['role']) {
                $builder->add('Enregistrer', SubmitType::class, [
                    'label' => 'Enregistrer et valider'
                ]);
            } else {
                $builder->add('Enregistrer', SubmitType::class);
            }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\CommandePrestataire',
            'allow_extra_fields' => true,
            'role' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_commandeprestataire';
    }


}
