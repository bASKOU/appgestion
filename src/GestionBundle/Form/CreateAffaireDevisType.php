<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use GestionBundle\Entity\Affaire;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CreateAffaireDevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroAffaire', TextType::class, [
                'label' => 'Numéro de l\'affaire'
            ])
            ->add('designationAffaire', TextType::class, [
                'label' => 'Veuillez indiquer la désignation de l\'Affaire',
                'required'=> true,
                'error_bubbling' => true,
            ])
            ->add('plafond', NumberType::class, [
                'label' => 'Souhaitez-vous définir un seuil de fonds engagés au-delà duquel toute opération demanderait votre validation ?',
                'required' => false,
                'error_bubbling' => true,
            ])
            ->add('dateDebut', DateType::class, [
                'label' => 'Date de début du marché',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'type' => 'date',
                    'class' => 'js-datepicker'
                ],
                'required' => true
            ])
            ->add('dateEcheance', DateType::class, [
                'label' => 'Date de fin estimée',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'type' => 'date',
                    'class' => 'js-datepicker'
                ],
                'required' => true
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Affaire::class
        ]);
    }
}