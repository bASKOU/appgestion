<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Attribution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateAttributionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => 'UserBundle:User',
                'choice_label' => function ($user) {
                    return $user->getNom() . ' ' . $user->getPrenom();
                },
                'label' => 'Veuillez sélectionner l\'utilisateur que vous souhaitez affecter à une affaire',
                'required' => true
            ])
            ->add('affaire', EntityType::class, [
                'class' => 'GestionBundle:Affaire',
                'choice_label' => 'designationAffaire',
                'label' => 'Veuillez sélectionner une affaire à attribuer à cet utilisateur',
                'required' => true
            ])
            ->add('plafondOperation', NumberType::class, [
                'label' => 'Souhaitez-vous définir un seuil de fonds engagés par opération pour cet utilisateur sur cette affaire ?',
                'required' => false
            ])
            ->add('enregistrer', SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attribution::class,
        ]);
    }
}