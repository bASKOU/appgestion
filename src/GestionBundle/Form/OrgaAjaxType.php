<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Organisme;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrgaAjaxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actif', ChoiceType::class, [
                'choices' => [
                    'Oui' => 1,
                    'Non' => 0
                ],
                'label' => 'Organisme actif',
                'required' => true,
                'expanded' => false
            ]);
        if ($options['role'][0] != 'ROLE_SUDALYS') {
            $builder
                ->add('fournisseurValide', ChoiceType::class, [
                    'choices' => [
                        'Oui' => true,
                        'Non' => false
                    ],
                    'required' => true,
                    'expanded' => false
                ]);
        }
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom de l\'Organisme',
                'required' => true
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Client' => 1,
                    'Fournisseur' => 0,
                    'Les deux' => 2
                ],
                'required' => true
            ])
            ->add('adresse', TextType::class, [
//                'placeholder' => 'adresse de l\'organisme',
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('adresseComplementaire1', TextType::class, [
//                'placeholder' => 'Complément d\'adresse',
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('adresseComplementaire2', TextType::class, [
//                'placeholder' => 'Complément d\'adresse',
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('stal', TextType::class, [
//                'placeholder' => '30000',
                'label' => 'Code Postal',
                'attr' => [
                    'maxlength' => 5,
                    'pattern' => '[0-9]{5}'
                ],
                'required' => true
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
                'required' => true
            ])
            ->add('pays', CountryType::class, [
                'preferred_choices' => ['FR'],
                'attr' => [
                    'class' => 'selectpicker',
                    'data-live-search' => 'data-live-search'
                ]
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse email service comptable',
                'required' => false
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organisme::class,
            'allow_extra_fields' => true,
            'role' => null
        ]);
    }
}