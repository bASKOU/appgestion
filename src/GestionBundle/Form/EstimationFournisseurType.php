<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimationFournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fournisseur', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un fournisseur',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->addOrderBy('c.nom', 'ASC')
                        ->where('c.type = 0')
                        ->andWhere('c.actif = true')
                        ->andWhere('c.fournisseurValide = true');
                },
                'attr' => [
                    'class' => 'selectpicker custom-select form-control fournisseur',
                    'data-live-search' => true
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'estimation',
                'attr' => [
                    'class' => 'auto-grow'
                ],
                'required' => true
            ])
            ->add('totalHT', MoneyType::class, [
                'label' => "Total de l'estimation",
                'required' => true,
                'attr' => [
                    'class' => 'comma text-right'
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\EstimationFournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_estimationfournisseur';
    }


}
