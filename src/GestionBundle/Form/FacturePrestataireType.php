<?php

namespace GestionBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturePrestataireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['type'] == null) {
            $builder
                ->add('affaire', EntityType::class, [
                    'class' => 'GestionBundle\Entity\Affaire',
                    'choice_label' => 'designationAffaire',
                    'attr' => [
                        'class' => 'selectpicker custom-select form-control',
                        'data-live-search' => true
                    ]
                ])
                ->add('numeroFacturePrestataire', TextType::class, [
                    'required' => true,
                    'label' => 'Numéro de la facture du prestataire'
                ])
                ->add('dateFacture', DateType::class, [
                    'label' => 'Date facture du prestataire',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true
                ])
                ->add('dateEcheance', DateType::class, [
                    'label' => 'Date d\'échéance',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true
                ])
                ->add('documentAttache', HiddenType::class, [
                    'required' => true,
                    'attr' => [
                        'hidden' => true,
                    ],
                    'label' => 'Document à rattacher'
                ])
                ->add('detailFacture', TextareaType::class, [
                    'label' => 'Détails des prestations'
                ])
                ->add('totalHT', MoneyType::class, [
                    'label' => 'Montant total HT'
                ])
                ->add('TVA', PercentType::class, [
                    'label' => 'Taux de TVA',
                    'required' => true,
                    'data' => 0.2
                ])
                ->add('adresseFacturation', TextType::class, [
                    'label' => 'Adresse de facturation'
                ]);
        } elseif ($options['type'] == 'reglement') {
            $builder
                ->add('modePaiement', EntityType::class, [
                    'class' => 'GestionBundle\Entity\ModePaiement',
                    'choice_label' => 'mode',
                    'required' => true,
                    'label' => 'Mode de paiement',
                    'placeholder' => 'Choisir un mode de paiement',
                    'attr' => [
                        'class' => 'selectpicker custom-select form-control',
                        'data-live-search' => true
                    ]
                ])
                ->add('dateReglement', DateType::class, [
                    'label' => 'Date du réglement',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true,
                    'attr' => [
                        'class' => 'js-datepicker'
                    ]
                ]);
        } elseif ($options['type'] == 'debit') {
            $builder
                ->add('dateDebit', DateType::class, [
                    'label' => 'Date du débit',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true,
                    'attr' => [
                        'class' => 'js-datepicker'
                    ]
                ]);
        }
        $builder
            ->add('mentions', TextareaType::class , [
                'label' => 'Libellé'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\FacturePrestataire',
            'type' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_factureprestataire';
    }
}
