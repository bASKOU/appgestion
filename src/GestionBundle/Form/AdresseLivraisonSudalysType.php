<?php

namespace GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdresseLivraisonSudalysType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('adresseComplementaire1', TextType::class, [
                'label' => 'Adresse complémentaire',
                'required' => false
            ])
            ->add('adresseComplementaire2', TextType::class, [
                'label' => 'Adresse complémentaire',
                'required' => false
            ])
            ->add('stal', NumberType::class, [
                'label' => 'Code postal'
            ])
            ->add('ville', TextType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\AdresseLivraisonSudalys'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_adresselivraisonsudalys';
    }


}
