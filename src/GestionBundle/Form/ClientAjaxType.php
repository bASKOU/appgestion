<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Organisme;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ClientAjaxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parent', EntityType::class, [
                'label' => 'Organisme de rattachement',
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un organisme de rattachement si besoin',
                'choice_label' => 'nom',
                'required' => false,
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ]
            ])
            ->add('nom', TextType::class, [
                'label' => 'Nom du client',
                'required' => true
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
                'required' => true
            ])
            ->add('adresseComplementaire1', TextType::class, [
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('adresseComplementaire2', TextType::class, [
                'label' => 'Adresse Complémentaire',
                'required' => false
            ])
            ->add('stal', TextType::class, [
                'label' => 'Code Postal',
                'attr' => [
                    'maxlength' => 5,
                    'pattern' => '[0-9]{5}'
                ],
                'required' => true
            ])
            ->add('ville', TextType::class, [
                'label' => 'Ville',
                'required' => true
            ])
            ->add('pays', CountryType::class, [
                'preferred_choices' => ['FR'],
                'attr' => [
                    'class' => 'selectpicker',
                    'data-live-search' => 'data-live-search'
                ]
            ])
            ->add('telephone', TelType::class, [
                'label' => 'Téléphone',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse email service comptable',
                'required' => false
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organisme::class
        ]);
    }
}