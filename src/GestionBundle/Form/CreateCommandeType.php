<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use GestionBundle\Entity\Commande;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateCommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('affaire', EntityType::class, [
                'class' => 'GestionBundle:Affaire',
                'choice_label' => 'designationAffaire',
                'label' => 'Affaire',
                'required' => true
            ])
            ->add('numeroCommande', TextType::class, [
                'label' => 'N° commande Sudalys',
            ])
            ->add('numeroCommandeClient', TextType::class, [
                'label' => 'N° commande client',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Entrez le numéro de la commande envoyé par la client'
                ],
                'empty_data' => null
            ])
            ->add('mentions', TextareaType::class, [
                'label' => 'Mentions',
                'required' => true
            ])
            ->add('client', EntityType::class, [
                'class' => 'GestionBundle:Organisme',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->addOrderBy('o.nom', 'ASC')
                        ->where('o.type = 1')
                        ->andWhere('o.actif = true');
                },
                'choice_label' => 'nom',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ],
                'required' => true
            ])
            ->add('contact', EntityType::class, [
                'class' => 'GestionBundle:Contact',
                'attr' => [
                    'data-live-search' => "true"
                ],
                'required' => true
            ])
            ->add('activeTVA', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Assugéti TVA',
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'required' => true
            ])
            ->add('remise', PercentType::class, [
                'label' => 'Remise accordée ',
                'type' => 'fractional',
                'attr' => [
                    'readonly' => true,
                    'placeholder' => '0'
                ],
                'required' => false
            ])
            ->add('TVA', PercentType::class, [
                'label' => 'Taux de TVA appliqué : ',
                'type' => 'fractional',
                'empty_data' => null,
                'required' => false
            ])
            ->add('forfaitaire', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Commande forfaitaire',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'required' => true
            ])
            ->add('adresseFacturation', ChoiceType::class, [
                'label' => 'Adresse de facturation',
                'placeholder' => 'Sélectionner une adresse de facturation',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control facturation',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('adresseLivraison', ChoiceType::class, [
                'label' => 'Adresse de livraison',
                'placeholder' => 'Sélectionner une adresse de livraison',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control livraison',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('typeDocument', HiddenType::class);
            if ($options['display'] === 'no') {
                $builder->add('documents', HiddenType::class);
            }
            $builder->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Commande::class,
            'display' => null
        ]);
    }
}