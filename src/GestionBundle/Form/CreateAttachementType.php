<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Attachement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateAttachementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mentions', TextareaType::class, [
                'label' => 'Mentions',
                'attr' => [
                    'class' => 'auto-grow'
                ]
            ])
            ->add('client', EntityType::class, [
                'class' => 'GestionBundle:Organisme',
                'choice_label' => 'nom',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ],
                'required' => true
            ])
            ->add('numeroAttachement', TextType::class, [
                'label' => 'Numéro Situation Automatique',
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('numeroAttachementBis', TextType::class, [
                'label' => 'Numéro Situation Manuel',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Uniquement pour une situation saisie préalablement'
                ]
            ])
            ->add('contact', EntityType::class, [
                'class' => 'GestionBundle:Contact',
                'attr' => [
                    'data-live-search' => "true"
                ],
                'required' => true
            ])
            ->add('activeTVA', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Assugéti TVA',
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'required' => true
            ])
            ->add('forfaitaire', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Situation forfaitaire',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'required' => true
            ])
            ->add('adresseFacturation', ChoiceType::class, [
                'label' => 'Adresse de facturation',
                'placeholder' => 'Sélectionner une adresse de facturation',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control facturation',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('adresseLivraison', ChoiceType::class, [
                'label' => 'Adresse de livraison',
                'placeholder' => 'Sélectionner une adresse de livraison',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control livraison',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attachement::class,
        ]);
    }
}