<?php

namespace GestionBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FacturePrestataireChaineType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('affaire', EntityType::class, [
                'class' => 'GestionBundle\Entity\Affaire',
                'choice_label' => 'designationAffaire',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ]
            ])
            ->add('numeroFacturePrestataire', TextType::class, [
                'required' => true,
                'label' => 'Numéro de la facture du prestataire'
            ])
            ->add('mentions', TextareaType::class , [
                'label' => 'Libellé'
            ])
            ->add('totalHT', MoneyType::class, [
                'label' => 'Montant total HT'
            ])
            ->add('TVA', PercentType::class, [
                'label' => 'Taux de TVA',
                'required' => true,
                'data' => 0.2
            ])
            ->add('dateFacture', DateType::class, [
                'label' => 'Date facture du fournisseur',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => true
            ])
            ->add('documentAttache', FileType::class, [
                'required' => true,
                'label' => 'Document à rattacher',
                'attr' => [
                    'accept' => '.pdf',
                    'hidden' => true
                ]
            ])
            ->add('adresseFacturation', TextType::class, [
                'label' => 'Adresse de facturation'
            ])
            ->add('detailFacture', TextareaType::class, [
                'label' => 'Détails des prestations'
            ])
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\FacturePrestataire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_factureprestataire';
    }


}
