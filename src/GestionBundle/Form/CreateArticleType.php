<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Article;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('theme', EntityType::class, [
                'class' => 'GestionBundle:Theme',
                'label' => 'Theme de l\'article',
                'choice_label' => 'nom',
                'required' => true
            ]);
            if ($options['devis'] === true) {
                $builder
                    ->add('nom', TextType::class, [
                        'label' => 'Désignation de votre article',
                        'required' => true,
                        'attr' => [
                            'readonly' => true
                        ]
                    ]);
            } else {
                $builder
                    ->add('nom', TextType::class, [
                        'label' => 'Désignation de votre article',
                        'required' => true
                    ]);
            }
            $builder
            ->add('description', TextareaType::class, [
                'label' => 'Description de l\'article',
                'required' => true
            ])
            ->add('unite', ChoiceType::class, [
                'label' => 'Unite de facturation',
                'choices' => [
                    'Forfaitaire' => 0,
                    'Unitaire' => 1
                ],
                'required' => true
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'affaireId' => null,
            'devis' => null
        ]);
    }
}