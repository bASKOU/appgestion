<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use GestionBundle\Entity\Devis;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateDevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('affaire', EntityType::class, [
                'class' => 'GestionBundle:Affaire',
                'choice_label' => 'designationAffaire',
                'placeholder' => 'Laisser vide pour devis sans rattachement',
                'required' => false,
                'attr' => [
                    'data-live-search' => "true"
                ]
            ])
            ->add('libelle', TextareaType::class, [
                'label' => 'Libellé',
                'required' => true,
            ])
            ->add('remise', PercentType::class, [
                'label' => 'Remise accordée ',
                'type' => 'fractional',
                'required' => false,
                'attr' => [
                    'placeholder' => '0'
                ],
                'empty_data' => 0
            ])
            ->add('numeroDevis', TextType::class, [
                'required' => false,
                'label' => 'Numéro devis automatique',
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('numeroDevisBis', TextType::class, [
                'required' => false,
                'label' => 'Numéro devis manuel',
                'attr' => [
                    'placeholder' => 'Uniquement pour un devis saisi préalablement'
                ]
            ])
            ->add('TVA', PercentType::class, [
                'label' => 'Taux de TVA appliqué',
                'type' => 'fractional',
                'attr' => [
                    'value' => 20
                ],
                'empty_data' => 20,
                'required' => false
            ])
            ->add('client', EntityType::class, [
                'class' => 'GestionBundle:Organisme',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->addOrderBy('o.nom', 'ASC')
                        ->where('o.type = 1')
                        ->andWhere('o.actif = true');

                },
                'choice_label' => 'nom',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control',
                    'data-live-search' => true
                ],
                'required' => true
            ])
            ->add('contact', EntityType::class, [
                'class' => 'GestionBundle:Contact',
                'attr' => [
                    'data-live-search' => true
                ],
                'label' => 'Contact',
                'placeholder' => 'Choisi un contact',
                'required' => true
            ])
            ->add('adresseFacturation', ChoiceType::class, [
                'label' => 'Adresse de facturation',
                'placeholder' => 'Sélectionner une adresse de facturation',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control facturation',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('adresseLivraison', ChoiceType::class, [
                'label' => 'Adresse de livraison',
                'placeholder' => 'Sélectionner une adresse de livraison',
                'attr' => [
                    'class' => 'selectpicker custom-select form-control livraison',
                    'data-live-search' => true
                ],
                'required' => false,
                'data' => null
            ])
            ->add('conditionsParticulieres', TextareaType::class, [
                'label' => 'Conditions particulières à ajouter au devis',
                'required' => false,
                'empty_data' => null
            ])
            ->add('activeTVA', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Assugéti TVA',
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ],
                'required' => true
            ])
            ->add('forfaitaire', ChoiceType::class, [
                'expanded' => false,
                'label' => 'Devis forfaitaire',
                'choices' => [
                    'Non' => false,
                    'Oui' => true
                ],
                'required' => true
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Devis::class,
        ]);
    }
}