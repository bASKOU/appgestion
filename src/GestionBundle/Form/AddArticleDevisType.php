<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\ArticleDevis;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class AddArticleDevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('article', EntityType::class, [
                'class' => 'GestionBundle:Article',
                'choice_label' => 'nom',
                'label' => 'Veuillez sélectionner un article'
            ])
            ->add('quantite', IntegerType::class, [
                'label' => 'Quantité à intégrer au devis'
            ])
            ->add('Ajouter l\'article', SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArticleDevis::class,
        ]);
    }
}