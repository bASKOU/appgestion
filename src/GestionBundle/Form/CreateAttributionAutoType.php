<?php

namespace GestionBundle\Form;

use GestionBundle\Entity\Attribution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CreateAttributionAutoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('affaire', EntityType::class, [
                'class' => 'GestionBundle:Affaire',
                'choice_label' => 'designationAffaire',
                'label' => 'Veuillez sélectionner une affaire à attribuer à cet utilisateur',
                'required' => true,
                'attr' => [
                    'readonly' => true,
                    'class' => 'selectpicker'
                ]
            ])
            ->add('user', EntityType::class, [
                'class' => 'UserBundle:User',
                'choice_label' => function ($user) {
                    return $user->getNom() . ' ' . $user->getPrenom();
                },
                'attr' => [
                    'class' => 'selectpicker'
                ],
                'label' => 'Veuillez sélectionner l\'utilisateur que vous souhaitez affecter à une affaire',
                'required' => true
            ])
            ->add('plafondOperation', NumberType::class, [
                'label' => 'Souhaitez-vous définir un seuil de fonds engagés par opération pour cet utilisateur sur cette affaire ?',
                'required' => false
            ])
            ->add('enregistrer', SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Attribution::class,
        ]);
    }
}