<?php

namespace GestionBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateCommandeFournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('affaire', EntityType::class, [
                'placeholder' => 'Choisir une affaire si besoin',
                'class' => 'GestionBundle\Entity\Affaire',
                'choice_label' => 'designationAffaire',
                'placeholder' => 'Sélectionner une affaire',
                'required' => false
            ])
            ->add('fournisseur', EntityType::class, [
                'class' => 'GestionBundle\Entity\Organisme',
                'placeholder' => 'Sélectionner un fournisseur',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->addOrderBy('c.nom', 'ASC')
                        ->where('c.type = 0')
                        ->andWhere('c.actif = true')
                        ->andWhere('c.fournisseurValide = true');
                }
            ])
            ->add('contact', EntityType::class, [
                'class' => 'GestionBundle\Entity\Contact',
                'choice_label' => 'nom',
                'placeholder' => 'Sélectionner un contact.'
            ])
            ->add('mentions', TextareaType::class)
            ->add('numeroCommandeFourn', TextType::class, [
                'label' => 'Numéro de commande automatique'
            ])
            ->add('adresseLivraison', EntityType::class, [
                'label' => 'Adresse de livraison',
                'class' => 'GestionBundle\Entity\AdresseLivraisonSudalys',
                'required' => false,
                'placeholder' => 'Choisir une adresse de livraison'
            ])
            ->add('adresseFacturation', TextType::class, [
                'label' => 'Adresse de facturation',
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('numeroCommandeFournBis', TextType::class, [
                'label' => 'Numéro de commande manuel',
                'required' => false
            ])
            ->add('detailCommandeFourn', TextareaType::class, [
                'label' => 'Détail de la commande',
                'required' => false
            ])
            ->add('totalHT', TextType::class, [
                'label' => 'Total HT'
            ])
            ->add('TVA', PercentType::class, [
                'label' => 'Taux de TVA',
                'attr' => [
                    'value' => 20
                ]
            ])
            ->add('montantTotalTTC', TextType::class, [
                'label' => 'Total TTC'
            ]);
            if($options['role']) {
                $builder->add('Enregistrer', SubmitType::class, [
                    'label' => 'Enregistrer et valider'
                ]);
            } else {
                $builder->add('Enregistrer', SubmitType::class);
            }
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionBundle\Entity\CommandeFournisseur',
            'allow_extra_fields' => true,
            'role' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionbundle_commandefournisseur';
    }


}
