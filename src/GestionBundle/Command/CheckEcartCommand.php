<?php

namespace GestionBundle\Command;

use DateTime;
use GestionBundle\Entity\Facture;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CheckEcartCommand extends ContainerAwareCommand
{
    // nom par lequel je vais appeler la commande
    protected static $defaultName = 'gestion:check-ecart';

    // fonction qui permet de configurer la commande
    protected function configure()
    {
        $this->setDescription('Commande qui permet de calculer l\'écart (positif ou négatif) entre une date donnée et la date d\'échéance d\'une Facture');
    }

    // coeur "logique" de ma classe
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // sortie standard en console pour annoncer le début du process de la commande
        $output->writeln([
            'Mise à jour des écarts des Factures enregistrées',
            '================================================',
            '',
        ]);

        // j'appelle mon manager doctrine via le container que ma classe implémente
        $em = $this->getContainer()->get('doctrine')->getManager();

        // on créé une nouvelle string pour la date du jour
        $now = (new DateTime())->format('Y-m-d');
        $output->writeln('Date du jour : ' . $now);

        // je récupère le tableau de toutes les factures qui sont en "attente de paiement"
        $factures = $this->getContainer()->get('doctrine')->getRepository(Facture::class)->findby([
            'etat' => 1
        ]);

        // on vient boucler sur le tableau des factures
        for($i = 0; $i < count($factures); $i++) {

            // on créer deux objets dates, un pour la date d'aujourd"hui et un pour la date d'echeance de la facture
            $dateCompareNow = date_create($now);
            $dateCompareEcheance = $factures[$i]->getDateEcheance();

            // on calcule l'interval en jours entre les deux dates
            $interval = date_diff($dateCompareNow, $dateCompareEcheance);
            $displayEcart = $interval->format('%r%a jours');

            // on set l'ecart calculé dans la facture concernée à chaque itération de la boucle
            $factures[$i]->setEcart((int)$displayEcart);
            $alreadyIndemForfait = $factures[$i]->getIndemniteForfait();

            // on affiche en sortie standard dans la console le résultat du calcul d'écart à chaque itération
            $output->writeln([
                '',
                'La facture ' . $factures[$i]->getNumeroFacture() . ' a un écart de : ' . $displayEcart
            ]);

            // si l'écart est négatif, on calcule alors les interets dûs et on affiche les 40€ de pénalités forfaitaires
            if((int)$displayEcart < 0) {
                $totalTTC = $factures[$i]->getTotalTTC();
                $tauxInteret = $factures[$i]->getTauxInteret();
                $ecart = (int)$displayEcart;

                $penalite = ($totalTTC * $tauxInteret) * (abs($ecart) / 365);
                $penalite = round($penalite, 2);
                $factures[$i]->setPenalite($penalite);

                if($alreadyIndemForfait == 0) {
                    $factures[$i]->setIndemniteForfait(1);
                    $output->writeln('écart négatif ===> ajout de 40€ d\'indemnité forfaitaire');
                }
            }

            // on persist chaque facture mise à jour
            $em->persist($factures[$i]);
        }

        $em->flush();

        $output->writeln([
            '',
            '======================================================',
            'Mise à jour terminée et entitées modifiées avec succès'
        ]);
    }
}