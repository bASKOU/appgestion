<?php


namespace GestionBundle\Command;

use GestionBundle\Entity\Affaire;
use GestionBundle\Entity\AvancementAffaire;
use GestionBundle\Entity\AvancementCommande;
use GestionBundle\Entity\AvancementCommandeFournisseur;
use GestionBundle\Entity\AvancementCommandePrestataire;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckAvancementForAffaire
 * Commande servant à :
 *  - Avant le  15 du mois, notifier aux chefs de projet n'ayant pas remplis leurs avancements du mois précédent
 *  - Le 15 du mois, générer des avancements à 0 si aucun avancement n'existe
 * @package GestionBundle\Command
 */
class CheckAvancementForAffaire extends ContainerAwareCommand
{
    // nom par lequel je vais appeler la commande
    protected static $defaultName = 'gestion:check-avancement';

    /**
     * Méthode de configuration de la commande
     */
    protected function configure()
    {
        // Description de la méthode
        $this->setDescription('Commande qui vérifie si les affaires on un avancement enregistré, dans le cas contraire envoi un email au chef de projet pour lui demander de remplir son avancement.');
        // Message qui s'affiche lorsque l'on ajout --help après la commande
        $this->setHelp('Cette commande doit être exécuté le 5, 10 et 14 de chaque mois pour vérifié que l\'avancement du mois précédent à bien était enregistré.
         Puis exécuté le 15 du mois pour créer des avancements à zéro si il n\'y a pas d\'avancement d\'affaire');
    }

    /**
     * Méthode comportant l'ensemble du traitement de la commande
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = $this->getContainer()->get('date.generate')->getDateForCheckAvancementCommand(); // Création de la date avec le numéro de mois et d'année en integer, l'objet date à la date fixé et le nom complet du mois pour l'affichage

        $output->writeln([ // Affichage du text de début du script dans la console
            "Lancement du script de vérification de l'existance d'avancement pour le mois de " . $date['fullMonthName'] . ' ' . $date['yearInt'],
            '=====================================================================================================',
            ''
            ]);
        $output->writeln('Récupération des affaires en cours...');
        $affaires = $this->getContainer()->get('doctrine')->getRepository(Affaire::class)->findAll(); // Récupération de toutes les affaires
        $output->writeln("Nombre d'affaire trouvé : " . count($affaires));
        $count = 0; // Initialisation de la variable qui va permettre de compter le nombre d'affaires sans avancement
        $mail = $this->getContainer()->get('email.action.mailer'); // invocation du service de mail

        $output->writeln('Début de vérification de la présense d\'avancement pour les affaires');
        if (intval($date['dateObject']->format('d')) < 15) { // Si le jour de la date est inférieur à 15
            $output->writeln('Recherche de la présence d\'avancements pour notification au chef de projet');
            foreach ($affaires as $affaire) { // Pour chaque affaire
                // Récupération du dernier avancement de l'affaire enregistré
                $avancement = $this->getContainer()->get('doctrine')->getRepository(AvancementAffaire::class)->findOneBy(['affaire' => $affaire], ['id' => 'DESC']);
                if ($avancement) { // Si il y a un avancement
                    if (intval($avancement->getDate()->format('m')) != $date['monthInt']) { // Si le mois n'est pas égale au mois spécifié
                        $mail->sendAvancementNotDoneAffaire($affaire, $date['dateObject']); // Envoi d'un email au chef de projet
                        $count++; // Incrémentation de la variable qui comptabilise le nombre d'affaire sans avancement
                    }
                } else { // Si il n'y a aucun avancement
                    $mail->sendAvancementNotDoneAffaire($affaire, $date); // Envoi d'un email au chef de projet
                    $count++; // Incrémentation de la variable qui comptabilise le nombre d'affaire sans avancement
                }
            }

            if ($count === 0) { // Si il aucune affaire sans avancement
                $output->writeln('Des avancements d\'affaire sont présent pour chaque affaire');
                $mail->sendCompleteAvancementAffaires(); // Envoi d'un email à la direction pour informer que les affaire son prêtes pour validation
                $output->writeln('Un email informant la direction a été envoyé.');
            } else {
                $output->writeln('Un total de ' . $count . ' affaires n\'ont pas d\'avancement, un mail informant chaque chef de projet a été envoyé.');
            }
        } else { // Si la date et égale ou supèrieur à 15
            $output->writeln('Vérification que chaque affaire à bien un avancement...');
            $date= $this->getContainer()->get('date.generate')->getDateForCheckAvancementCommandNotComplete(); // Récupération de la bonne date car la date précédemment créé et pour le mois suivant
            $em = $this->getContainer()->get('doctrine')->getManager(); // Récupération du manageur d'entité de doctrine
            foreach ($affaires as $affaire) { // Pour chaque affaire
                // Récupération du dernier avancement enregistré
                $avancement = $this->getContainer()->get('doctrine')->getRepository(AvancementAffaire::class)->findOneBy(['affaire' => $affaire], ['id' => 'DESC']);
                if ($avancement) { // SI il y a un avancement
                    if (intval($avancement->getDate()->format('m')) != $date['monthInt']) { // Si le mois et différent du mois spécifié
                        $this->generateAffaireAvancement($affaire, $date, $em, $output); // Génération d'un avancement d'affaire à 0
                        $this->generateCommandeRecusAvancement($affaire, $date, $em, $output); // Génération d'avancement de commande reçus à 0
                        $this->generateCommandeEmiseAvancement($affaire, $date, $em, $output); // Génération d'avancement pour les commandes de matériels et prestations à 0
                    } else { // Sinon
                        $output->writeln([
                            "Un avancement pour l'affaire N°: " . $affaire->getNumeroAffaire() . " exist déjà.",
                            "Vérification de la présence d'avancement pour les commandes reçus"
                            ]);
                        $this->generateCommandeRecusAvancement($affaire, $date, $em, $output); // Génération d'avancement de commande reçus à 0
                        $this->generateCommandeEmiseAvancement($affaire, $date, $em, $output); // Génération d'avancement pour les commandes de matériels et prestations à 0
                    }
                } else { // Si aucun avancement
                    $this->generateAffaireAvancement($affaire, $date, $em, $output); // Génération d'un avancement d'affaire à 0
                    $this->generateCommandeRecusAvancement($affaire, $date, $em, $output); // Génération d'avancement de commande reçus à 0
                    $this->generateCommandeEmiseAvancement($affaire, $date, $em, $output); // Génération d'avancement pour les commandes de matériels et prestations à 0
                    $count++;
                }
            }
            $em->flush(); // Enregistrement des avancements créés
            $output->writeln('Création des avancements terminé');
        }

        $output->writeln([ // Affichage du message indiquant la fin du script
            '',
            '=====================================================================================================',
            "Le script c'est exécuté avec succès!!",
            'Fin de l\'exécution du script'
        ]);
    }

    /**
     * Méthode pour l'avancement pour une affaires à zéro pour le mois en cours
     * @param Affaire $affaire
     * @param $date
     * @param $em
     */
    private function generateAffaireAvancement(Affaire $affaire, $date, $em, $output)
    {
        $output->writeln('Début génération avancement affaire');
        $avancement = new AvancementAffaire(); // Création d'un nouvel avancement
        $avancement->setAffaire($affaire); // Assignation de l'affaire
        $avancement->setDate($date['dateObject']); // Assignation de la date ciblé
        $avancement->setDateSaisie(new \DateTime('now')); // Assignation de la date de saisie
        $avancement->setTotalHT((float) 0); // Assignation du montant HT à 0
        $avancement->setAvancement((float) 0); // Assignation de l'avancement à 0
        $em->persist($avancement); // Persiter l'entité
        $output->writeln('Avancement affaire persisté');
    }

    /**
     * Méthode pour générer des avancements pour les commande reçus à zéro pour le mois en cours
     * @param Affaire $affaire
     * @param $date
     * @param $em
     * @param $output
     */
    private function generateCommandeRecusAvancement(Affaire $affaire, $date, $em, $output)
    {
        $dateSaisie = new \DateTime('today'); // Création de la date d'aujourd'hui pour la saisie
        $commandes = $affaire->getCommandes()->toArray(); // Récupération de la liste des commandes
        foreach ($commandes as $commande) { // Pour chaque commandes
            // Récupération du dernier avancement enregistré
            $avancement = $this->getContainer()->get('doctrine')->getRepository(AvancementCommande::class)->findOneBy(['commande' => $commande], ['id' => 'DESC']);
            if ($avancement) { // Si iol y a un avancement
                if (strtotime($avancement->getDate()->format('Y-m')) != strtotime($date['dateObject']->format('Y-m'))) { // Si le mois et différent du mois ciblé
                    $output->writeln('Création d\'un avancement à 0 pour la commande reçus N° : ' . $commande->getNumeroCommande());
                    $avancement = new AvancementCommande(); // Création d'un nouvel avancement
                    $avancement->setCommande($commande); // Assignation de la commande
                    $avancement->setDate($date['dateObject']); // Assignation de la date spécifié
                    $avancement->setDateSaisie($dateSaisie); // Assignation de la date de saisie
                    $avancement->setTotalHT((float)0); // Assignation du montant HT à 0
                    $avancement->setReel((float)0); // Assignation de l'avancement à 0
                    $em->persist($avancement); // Persister l'avancement
                } else {
                    $output->writeln('Un avancement pour la commande reçus N°: ' . $commande->getNumeroCommande() . ' existe déjà.');
                }
            } else { // Il n'y a pas d'avancement, même processe que si la dateest différente
                $output->writeln('Création d\'un avancement à 0 pour la commande reçus N° : ' . $commande->getNumeroCommande());
                $avancement = new AvancementCommande();
                $avancement->setCommande($commande);
                $avancement->setDate($date['dateObject']);
                $avancement->setDateSaisie($dateSaisie);
                $avancement->setTotalHT((float)0);
                $avancement->setReel((float)0);
                $em->persist($avancement);
            }
        }
    }

    /**
     * Méthode pour générer des avancements pour les commande émises à zéro pour le mois en cours
     * On répète les mêmes étapes que la méthode précédente, mais spécialement pour les commandes de matériels et prestations
     * @param Affaire $affaire
     * @param $date
     * @param $em
     * @param $output
     */
    private function generateCommandeEmiseAvancement(Affaire $affaire, $date, $em, $output)
    {
        $dateSaisie = new \DateTime('today');
        $commandesFournisseur = $affaire->getCommandeFournisseur()->toArray();
        $output->writeln("Vérification de la présence d'avancement pour les commande de matériels...");
        foreach ($commandesFournisseur as $commande) {
            $avancement = $this->getContainer()->get('doctrine')->getRepository(AvancementCommandeFournisseur::class)->findOneBy(['commandeFournisseur' => $commande], ['id' => 'DESC']);
            if ($avancement) {
                if (strtotime($avancement->getDate()->format('Y-m')) != strtotime($date['dateObject']->format('Y-m'))) {
                    $output->writeln('Création d\'un avancement à 0 pour le commande de matériels N° : ' . $commande->getNumeroCommandeFourn());
                    $avancement = new AvancementCommandeFournisseur();
                    $avancement->setCommandeFournisseur($commande);
                    $avancement->setDate($date['dateObject']);
                    $avancement->setDateSaisie($dateSaisie);
                    $avancement->setTotalHT((float)0);
                    $avancement->setAvancement((float)0);
                    $em->persist($avancement);
                } else {
                    $output->writeln('Un avancement pour la commande de matériels N°: ' . $commande->getNumeroCommandeFourn() . ' existe déjà.');
                }
            } else {
                $output->writeln('Création d\'un avancement à 0 pour la commande de matériels N° : ' . $commande->getNumeroCommandeFourn());
                $avancement = new AvancementCommandeFournisseur();
                $avancement->setCommandeFournisseur($commande);
                $avancement->setDate($date['dateObject']);
                $avancement->setDateSaisie($dateSaisie);
                $avancement->setTotalHT((float)0);
                $avancement->setAvancement((float)0);
                $em->persist($avancement);
            }
        }

        $commandesPrestataire = $affaire->getCommandePrestataire()->toArray();

        $output->writeln("Vérification de la présence d'avancements pour les commande de prestations...");
        foreach ($commandesPrestataire as $commande) {
            $avancement = $this->getContainer()->get('doctrine')->getRepository(AvancementCommandePrestataire::class)->findOneBy(['commandePrestataire' => $commande], ['id' => 'DESC']);
            if ($avancement) {
                if (strtotime($avancement->getDate()->format('Y-m')) != strtotime($date['dateObject']->format('Y-m'))) {
                    $output->writeln('Création d\'un avancement à 0 pour la commande de prestations N° : ' . $commande->getNumeroCommandePresta());
                    $avancement = new AvancementCommandePrestataire();
                    $avancement->setCommandePrestataire($commande);
                    $avancement->setDate($date['dateObject']);
                    $avancement->setDateSaisie($dateSaisie);
                    $avancement->setTotalHT((float)0);
                    $avancement->setAvancement((float)0);
                    $em->persist($avancement);
                } else {
                    $output->writeln('Un avancement pour le commande de prestations N°: ' . $commande->getNumeroCommandePresta() . ' existe déjà.');
                }
            } else {
                $output->writeln('Création d\'un avancement à 0 pour la commande de prestations N° : ' . $commande->getNumeroCommandePrestat());
                $avancement = new AvancementCommandePrestataire();
                $avancement->setCommandePrestataire($commande);
                $avancement->setDate($date['dateObject']);
                $avancement->setDateSaisie($dateSaisie);
                $avancement->setTotalHT((float)0);
                $avancement->setAvancement((float)0);
                $em->persist($avancement);
            }
        }
    }
}