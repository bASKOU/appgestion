<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleFactureRepository")
 */
class ArticleFacture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_facture_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Facture
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Facture", inversedBy="articleFacture")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facture;

    /**
     * @var ArticleDevis
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ArticleDevis", inversedBy="articleFacture")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articleDevis;

    /**
     * @var float|null
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $overrideMontantUnit;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $quantite;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default": false})
     */
    private $quantitePourcentage;

    /**
     * @var bool
     *
     * @ORM\Column(type="float")
     */
    private $montantTotalHT;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Facture|null
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @param Facture|null $facture
     * @return ArticleFacture
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * @return ArticleDevis|null
     */
    public function getArticleDevis()
    {
        return $this->articleDevis;
    }

    /**
     * @param ArticleDevis|null $articleDevis
     * @return ArticleFacture
     */
    public function setArticleDevis($articleDevis)
    {
        $this->articleDevis = $articleDevis;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOverrideMontantUnit()
    {
        return $this->overrideMontantUnit;
    }

    /**
     * @param float $overrideMontantUnit
     * @return ArticleFacture
     */
    public function setOverrideMontantUnit($overrideMontantUnit)
    {
        $this->overrideMontantUnit = $overrideMontantUnit;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param float $quantite
     * @return ArticleFacture
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getQuantitePourcentage()
    {
        return $this->quantitePourcentage;
    }

    /**
     * @param bool $quantitePourcentage
     * @return ArticleFacture
     */
    public function setQuantitePourcentage($quantitePourcentage)
    {
        $this->quantitePourcentage = $quantitePourcentage;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantTotalHT()
    {
        return $this->montantTotalHT;
    }

    /**
     * @param float $montantTotalHT
     * @return ArticleFacture
     */
    public function setMontantTotalHT($montantTotalHT)
    {
        $this->montantTotalHT = $montantTotalHT;

        return $this;
    }
}
