<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\FactureRepository")
 */
class Facture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('facture_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="factures")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $affaire;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="factures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="clientFacture")
     */
    private $client;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="factures")
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numeroFacture;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numeroFactureBis;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mentions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateFacture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePreSaisie;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $TVA;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $activeTVA;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $forfaitaire;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $remise;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $penalite;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $tauxInteret;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $indemniteForfait;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $echeance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $datePaiement;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalTTC;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $indiceRevision;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ecart;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $interetApplique;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $linkedOperation;

    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $attachementDoc = array();

    /**
     * @var ArticleFacture[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleFacture", mappedBy="facture", cascade="remove")
     */
    private $articleFacture;

    /**
     * @var Avoir[]
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Avoir", mappedBy="facture", cascade={"persist", "remove"})
     */
    private $avoir;

    /**
     * @var Commande
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Commande", mappedBy="facture")
     * @ORM\JoinColumn(nullable=true)
     */
    private $commande;

    /**
     * @var Attachement[]
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Attachement", mappedBy="facture", cascade="remove")
     * @ORM\JoinColumn(nullable=true)
     */
    private $attachement;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseFacturationFacture")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseFacturation;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseLivraisonFacture")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * Facture constructor.
     */
    public function __construct()
    {
        $this->articleFacture = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return Facture
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Facture
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Organisme|null $client
     * @return Facture
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return Facture
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroFacture()
    {
        return $this->numeroFacture;
    }

    /**
     * @param string $numeroFacture
     * @return Facture
     */
    public function setNumeroFacture($numeroFacture)
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroFactureBis()
    {
        return $this->numeroFactureBis;
    }

    /**
     * @param string $numeroFactureBis
     * @return Facture
     */
    public function setNumeroFactureBis($numeroFactureBis)
    {
        $this->numeroFactureBis = $numeroFactureBis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * @param string|null $mentions
     * @return Facture
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * @param \DateTimeInterface $dateFacture
     * @return Facture
     */
    public function setDateFacture($dateFacture)
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDatePresaisie()
    {
        return $this->datePreSaisie;
    }

    /**
     * @param \DateTimeInterface $datePreSaisie
     * @return Facture
     */
    public function setDatePreSaisie($datePreSaisie)
    {
        $this->datePreSaisie = $datePreSaisie;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * @param float|null $TVA
     * @return Facture
     */
    public function setTVA($TVA)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActiveTVA()
    {
        return $this->activeTVA;
    }

    /**
     * @param bool $activeTVA
     * @return Facture
     */
    public function setActiveTVA($activeTVA)
    {
        $this->activeTVA = $activeTVA;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @param float|null $remise
     * @return Facture
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return Facture
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPenalite()
    {
        return $this->penalite;
    }

    /**
     * @param float|null $penalite
     * @return Facture
     */
    public function setPenalite($penalite)
    {
        $this->penalite = $penalite;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTauxInteret()
    {
        return $this->tauxInteret;
    }

    /**
     * @param float|null $tauxInteret
     * @return Facture
     */
    public function setTauxInteret($tauxInteret)
    {
        $this->tauxInteret = $tauxInteret;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIndemniteForfait()
    {
        return $this->indemniteForfait;
    }

    /**
     * @param int|null $indemniteForfait
     * @return Facture
     */
    public function setIndemniteForfait($indemniteForfait)
    {
        $this->indemniteForfait = $indemniteForfait;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEcheance()
    {
        return $this->echeance;
    }

    /**
     * @param int|null $echeance
     * @return Facture
     */
    public function setEcheance($echeance)
    {
        $this->echeance = $echeance;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * @param \DateTimeInterface $dateEcheance
     * @return Facture
     */
    public function setDateEcheance($dateEcheance)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }

    /**
     * @param \DateTimeInterface $datePaiement
     * @return Facture
     */
    public function setDatePaiement($datePaiement)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

    /**
     * @param float $totalTTC
     * @return Facture
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getIndiceRevision()
    {
        return $this->indiceRevision;
    }

    /**
     * @param float $indiceRevision
     * @return Facture
     */
    public function setIndiceRevision($indiceRevision)
    {
        $this->indiceRevision= $indiceRevision;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEcart()
    {
        return $this->ecart;
    }

    /**
     * @param int|null $ecart
     * @return Facture
     */
    public function setEcart($ecart)
    {
        $this->ecart = $ecart;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     * @return Facture
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return bool
     */
    public function getInteretApplique()
    {
        return $this->interetApplique;
    }

    /**
     * @param bool $interetApplique
     * @return Facture
     */
    public function setInteretApplique($interetApplique)
    {
        $this->interetApplique = $interetApplique;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLinkedOperation()
    {
        return $this->linkedOperation;
    }

    /**
     * @param int $linkedOperation
     * @return Facture
     */
    public function setLinkedOperation($linkedOperation)
    {
        $this->linkedOperation = $linkedOperation;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttachementDoc()
    {
        return $this->attachementDoc;
    }

    /**
     * @param array $attachementDoc
     * @return Facture
     */
    public function setAttachementDoc($attachementDoc)
    {
        $this->attachementDoc = $attachementDoc;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getForfaitaire()
    {
        return $this->forfaitaire;
    }

    /**
     * @param bool|null $forfaitaire
     * @return Facture
     */
    public function setForfaitaire($forfaitaire)
    {
        $this->forfaitaire = $forfaitaire;

        return $this;
    }

    /**
     * @param Organisme|null $adresseFacturation
     *
     * @return Facture
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param Organisme|null $adresseLivraison
     *
     * @return Facture
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @return Collection|Avoir[]
     */
    public function getAvoir()
    {
        return $this->avoir;
    }

    /**
     * @param \GestionBundle\Entity\Avoir $avoir
     * @return Facture
     */
    public function setAvoir($avoir)
    {
        $this->avoir = $avoir;

        // set the owning side of the relation if necessary
        if ($avoir->getFacture() !== $this) {
            $avoir->setFacture($this);
        }

        return $this;
    }

    /**
     * @return Collection|ArticleFacture[]
     */
    public function getArticleFacture()
    {
        return $this->articleFacture;
    }

    public function addArticleFacture($articleFacture)
    {
        if (!$this->articleFacture->contains($articleFacture)) {
            $this->articleFacture[] = $articleFacture;
            $articleFacture->setFacture($this);
        }

        return $this;
    }

    /**
     * @param \GestionBundle\Entity\ArticleFacture $articleFacture
     * @return Facture
     */
    public function removeArticleFacture($articleFacture): self
    {
        if ($this->articleFacture->contains($articleFacture)) {
            $this->articleFacture->removeElement($articleFacture);
            // set the owning side to null (unless already changed)
            if ($articleFacture->getFacture() === $this) {
                $articleFacture->setFacture(null);
            }
        }

        return $this;
    }

    /**
     * @return Commande|null
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * @param Commande $commande
     * @return Facture
     */
    public function setCommande($commande)
    {
        $this->commande = $commande;

        // set the owning side of the relation if necessary
        if ($commande->getFacture() !== $this) {
            $commande->setFacture($this);
        }

        return $this;
    }

    /**
     * @return Attachement|null
     */
    public function getAttachement()
    {
        return $this->attachement;
    }

    /**
     * @param Attachement $attachement
     * @return Facture
     */
    public function setAttachement($attachement)
    {
        $this->attachement = $attachement;

        // set the owning side of the relation if necessary
        if ($attachement->getFacture() !== $this) {
            $attachement->setFacture($this);
        }

        return $this;
    }
}
