<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoFournisseur
 *
 * @ORM\Table(name="info_fournisseur")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\InfoFournisseurRepository")
 */
class InfoFournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('info_fournisseur_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="RIB", type="boolean", options={"default":false})
     */
    private $RIB;

    /**
     * @var array|null
     *
     * @ORM\Column(name="RIB_document", type="array", length=255, nullable=true, options={"default":null})
     */
    private $RIBDocument;

    /**
     * @var Organisme
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Organisme", cascade={"remove"}, inversedBy="infoFournisseur")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $fournisseur;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kbis.
     *
     * @param bool $kbis
     *
     * @return InfoFournisseur
     */
    public function setKbis($kbis=false)
    {
        $this->kbis = $kbis;

        return $this;
    }

    /**
     * Get kbis.
     *
     * @return bool
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * Set kbisDocument.
     *
     * @param array|null $kbisDocument
     *
     * @return InfoFournisseur
     */
    public function setKbisDocument($kbisDocument = null)
    {
        $this->kbisDocument[] = $kbisDocument;

        return $this;
    }

    /**
     * Get kbisDocument.
     *
     * @return string|null
     */
    public function getKbisDocument()
    {
        return $this->kbisDocument;
    }

    /**
     * Set RIB.
     *
     * @param bool $RIB
     *
     * @return InfoFournisseur
     */
    public function setRIB($RIB=false)
    {
        $this->RIB = $RIB;

        return $this;
    }

    /**
     * Get RIB.
     *
     * @return bool
     */
    public function getRIB()
    {
        return $this->RIB;
    }

    /**
     * Set RIBDocument.
     *
     * @param string|null $RIBDocument
     *
     * @return InfoFournisseur
     */
    public function setRIBDocument($RIBDocument = null)
    {
        $this->RIBDocument = $RIBDocument;

        return $this;
    }

    /**
     * Get RIBDocument.
     *
     * @return string|null
     */
    public function getRIBDocument()
    {
        return $this->RIBDocument;
    }

    /**
     * @return Organisme|null
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * @param Organisme|null $fournisseur
     * @return InfoFournisseur
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }
}
