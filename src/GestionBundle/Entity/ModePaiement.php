<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ModePaiement
 *
 * @ORM\Table(name="mode_paiement")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ModePaiementRepository")
 */
class ModePaiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=255, unique=true)
     */
    private $mode;

    /**
     * @var FactureFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="modePaiement")
     */
    private $factureFournisseur;

    /**
     * @var FacturePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="modePaiement")
     */
    private $facturePrestataire;

    public function __construct()
    {
        $this->factureFournisseur = new ArrayCollection();
        $this->facturePrestataire = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mode.
     *
     * @param string $mode
     *
     * @return ModePaiement
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode.
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set factureFournisseur
     *
     * @param FactureFournisseur $factureFournisseur
     * @return ModePaiement
     */
    public function setFactureFournisseur($factureFournisseur)
    {
        $this->factureFournisseur[] = $factureFournisseur;

        return $this;
    }

    /**
     * Get factureFournisseur
     * @return FactureFournisseur[]|Collection
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }

    /**
     * Set facturePrestataire
     *
     * @param FacturePrestataire $facturePrestataire
     * @return ModePaiement
     */
    public function setFacturePrestataire($facturePrestataire)
    {
        $this->facturePrestataire[] = $facturePrestataire;

        return $this;
    }

    /**
     * Get facturePrestataire
     * @return FacturePrestataire[]|Collection
     */
    public function getFacturePrestataire()
    {
        return $this->facturePrestataire;
    }
}
