<?php

namespace GestionBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use UserBundle\Entity\User;

/**
 * FactureFournisseur
 *
 * @ORM\Table(name="facture_fournisseur")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\FactureFournisseurRepository")
 */
class FactureFournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('commande_fournisseur_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_facture", type="string", length=255, unique=true, nullable=true)
     */
    private $numeroFacture;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_facture_fournisseur", type="string", length=255, nullable=true)
     */
    private $numeroFactureFournisseur;

    /**
     * @var string
     *
     * @ORM\Column(name="mentions", type="text")
     */
    private $mentions;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_facture", type="datetime", nullable=true)
     */
    private $dateFacture;


    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_echeance", type="datetime", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_saisie", type="datetime", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_validation", type="datetime", nullable=true)
     */
    private $dateValidation;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_reglement", type="datetime", nullable=true)
     */
    private $dateReglement;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date_debit", type="datetime", nullable=true)
     */
    private $dateDebit;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ht", type="float")
     */
    private $totalHT;

    /**
     * @var float|null
     *
     * @ORM\Column(name="TVA", type="float", nullable=true)
     */
    private $tVA;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ttc", type="float")
     */
    private $totalTTC;

    /**
     * @var int
     *
     * @ORM\Column(name="validation", type="integer")
     */
    private $validation;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_facturation", type="text")
     */
    private $adresseFacturation;

    /**
     * @var string
     *
     * @ORM\Column(name="document_attache", type="string", nullable=true)
     */
    private $documentAttache;

    /**
     * @var AdresseLivraisonSudalys
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\AdresseLivraisonSudalys", inversedBy="factureFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="factureFournisseur")
     */
    private $fournisseur;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="factureFournisseur")
     */
    private $contact;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="factureFournisseur")
     */
    private $user;

    /**
     * @var CommandeFournisseur
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandeFournisseur", inversedBy="factureFournisseur")
     */
    private $commandeFournisseur;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="factureFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $affaire;

    /**
     * @var ModePaiement
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ModePaiement", inversedBy="factureFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $modePaiement;

    /**
     * @var EstimationFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationFournisseur", mappedBy="facture")
     */
    private $estimation;

    public function __construct()
    {
        $this->estimation = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->numeroFactureFournisseur . ' - ' . $this->getMentions() . ' - ' . number_format($this->totalHT, 2, ',', ' ') . ' €';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroFacture.
     *
     * @param string|null $numeroFacture
     *
     * @return FactureFournisseur
     */
    public function setNumeroFacture($numeroFacture = null)
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    /**
     * Get numeroFacture.
     *
     * @return string
     */
    public function getNumeroFacture()
    {
        return $this->numeroFacture;
    }

    /**
     * Set numeroFactureFournisseur.
     *
     * @param string|null $numeroFactureFournisseur
     *
     * @return FactureFournisseur
     */
    public function setNumeroFactureFournisseur($numeroFactureFournisseur = null)
    {
        $this->numeroFactureFournisseur = $numeroFactureFournisseur;

        return $this;
    }

    /**
     * Get numeroFactureFournisseur.
     *
     * @return string
     */
    public function getNumeroFactureFournisseur()
    {
        return $this->numeroFactureFournisseur;
    }

    /**
     * Set mentions.
     *
     * @param string $mentions
     *
     * @return FactureFournisseur
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * Get mentions.
     *
     * @return string
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * Set dateFacture.
     *
     * @param DateTime|null $dateFacture
     *
     * @return FactureFournisseur
     */
    public function setDateFacture($dateFacture = null)
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * Get dateFacture.
     *
     * @return DateTime|null
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * Set dateEcheance.
     *
     * @param DateTime|null $dateEcheance
     *
     * @return FactureFournisseur
     */
    public function setDateEcheance($dateEcheance = null)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * Get dateEcheance.
     *
     * @return DateTime|null
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * Set dateSaisie.
     *
     * @param DateTime|null $dateSaisie
     *
     * @return FactureFournisseur
     */
    public function setDateSaisie($dateSaisie = null)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get dateSaisie.
     *
     * @return DateTime|null
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set dateValidation.
     *
     * @param DateTime|null $dateValidation
     *
     * @return FactureFournisseur
     */
    public function setDateValidation($dateValidation = null)
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation.
     *
     * @return DateTime|null
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * Set dateReglement
     *
     * @param DateTime|null $dateReglement
     *
     * @return FactureFournisseur
     */
    public function setDateReglement($dateReglement = null)
    {
        $this->dateReglement = $dateReglement;

        return $this;
    }

    /**
     * Get dateReglement.
     *
     * @return DateTime|null
     */
    public function getDateReglement()
    {
        return $this->dateReglement;
    }

    /**
     * Set dateDebit.
     *
     * @param DateTime|null $dateDebit
     *
     * @return FactureFournisseur
     */
    public function setDateDebit($dateDebit = null)
    {
        $this->dateDebit = $dateDebit;

        return $this;
    }

    /**
     * Get dateDebit.
     *
     * @return DateTime|null
     */
    public function getDateDebit()
    {
        return $this->dateDebit;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return FactureFournisseur
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set tVA.
     *
     * @param float|null $tVA
     *
     * @return FactureFournisseur
     */
    public function setTVA($tVA = null)
    {
        $this->tVA = $tVA;

        return $this;
    }

    /**
     * Get tVA.
     *
     * @return float|null
     */
    public function getTVA()
    {
        return $this->tVA;
    }

    /**
     * Set totalTTC.
     *
     * @param float $totalTTC
     *
     * @return FactureFournisseur
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * Get totalTTC.
     *
     * @return float
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

    /**
     * Set validation.
     *
     * @param int $validation
     *
     * @return FactureFournisseur
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation.
     *
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * Set etat.
     *
     * @param int $etat
     *
     * @return FactureFournisseur
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat.
     *
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set adresseFacturation.
     *
     * @param string $adresseFacturation
     *
     * @return FactureFournisseur
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * Get adresseFacturation.
     *
     * @return string
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * Set documentAttache.
     *
     * @param string|null $documentAttache
     *
     * @return FactureFournisseur
     */
    public function setDocumentAttache($documentAttache = null)
    {
        $this->documentAttache = $documentAttache;

        return $this;
    }

    /**
     * Get documentAttache.
     *
     * @return string
     */
    public function getDocumentAttache()
    {
        return $this->documentAttache;
    }

    /**
     * Set adresseLivraison
     * @param AdresseLivraisonSudalys $adresseLivraison
     *
     * @return FactureFournisseur
     */
    public function setAdresseLivraison($adresseLivraison = null)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * Get aadresseLivraison
     * @return AdresseLivraisonSudalys|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * Set fournisseur
     * @param Organisme $fournisseur
     *
     * @return FactureFournisseur
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * Get fournisseur
     * @return Organisme|null
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * Set contact
     * @param Contact $contact
     *
     * @return FactureFournisseur
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set user
     * @param User $user
     *
     * @return FactureFournisseur
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get contact
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set commandeFournisseur
     * @param CommandeFournisseur $commandeFournisseur
     *
     * @return FactureFournisseur
     */
    public function setCommandeFournisseur($commandeFournisseur)
    {
        $this->commandeFournisseur = $commandeFournisseur;

        return $this;
    }

    /**
     * Get commandeFournisseur
     * @return CommandeFournisseur|null
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return FactureFournisseur
     */
    public function setAffaire($affaire = null)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return ModePaiement|null
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * @param ModePaiement|null $modePaiement
     * @return FactureFournisseur
     */
    public function setModePaiement($modePaiement = null)
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    /**
     * @param EstimationFournisseur[] $estimation
     * @return FactureFournisseur
     */
    public function setEstimation($estimation)
    {
        $this->estimation = $estimation;

        return $this;
    }

    /**
     * @return EstimationFournisseur[]|Collection|null
     */
    public function getEstimation()
    {
        return $this->estimation;
    }
}
