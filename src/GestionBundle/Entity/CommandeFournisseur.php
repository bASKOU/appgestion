<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\CommandeFournisseurRepository")
 */
class CommandeFournisseur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('commande_fournisseur_id_seq'::regclass)"})
     */
    protected $id;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="commandeFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $affaire;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="commandeFournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="commandeFournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="commandeFournisseur")
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $mentions;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $numeroCommandeFourn;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroCommandeFournBis;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $detailCommandeFourn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateCommandeFourn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEnvoi;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $TVA;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $montantTotalTTC;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $validation;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", length=2, nullable=true)
     */
    private $respectPlafond;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $adresseFacturation;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $raisonAbandon;

    /**
     * @var AvancementCommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\AvancementCommandeFournisseur", mappedBy="commandeFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $avancement;

    /**
     * @var ProduitCommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ProduitCommandeFournisseur", mappedBy="commandeFournisseur", cascade={"remove", "persist"})
     */
    private $produitCommandeFournisseur;

    /**
     * @var AdresseLivraisonSudalys
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\AdresseLivraisonSudalys", inversedBy="commandeFournisseur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * @var FactureFournisseur
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="commandeFournisseur")
     */
    private $factureFournisseur;

    /**
     * CommandeFournisseur constructor.
     */
    public function __construct()
    {
        $this->produitCommandeFournisseur = new ArrayCollection();
        $this->factureFournisseur = new ArrayCollection();
        $this->avancement = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return CommandeFournisseur
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }

    /**
     * @param Organisme|null $fournisseur
     * @return CommandeFournisseur
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return CommandeFournisseur
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return CommandeFournisseur
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * @param string $mentions
     * @return CommandeFournisseur
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroCommandeFourn()
    {
        return $this->numeroCommandeFourn;
    }

    /**
     * @param string $numeroCommandeFourn
     * @return CommandeFournisseur
     */
    public function setNumeroCommandeFourn($numeroCommandeFourn)
    {
        $this->numeroCommandeFourn = $numeroCommandeFourn;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroCommandeFournBis()
    {
        return $this->numeroCommandeFournBis;
    }

    /**
     * @param string $numeroCommandeFournBis
     * @return CommandeFournisseur
     */
    public function setNumeroCommandeFournBis($numeroCommandeFournBis)
    {
        $this->numeroCommandeFournBis = $numeroCommandeFournBis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetailCommandeFourn()
    {
        return $this->detailCommandeFourn;
    }

    /**
     * @param string|null $detailCommandeFourn
     * @return CommandeFournisseur
     */
    public function setDetailCommandeFourn($detailCommandeFourn)
    {
        $this->detailCommandeFourn = $detailCommandeFourn;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateCommandeFourn()
    {
        return $this->dateCommandeFourn;
    }

    /**
     * @param \DateTimeInterface $dateCommandeFourn
     * @return CommandeFournisseur
     */
    public function setDateCommandeFourn($dateCommandeFourn)
    {
        $this->dateCommandeFourn = $dateCommandeFourn;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * @param \DateTimeInterface|null $dateEnvoi
     * @return CommandeFournisseur
     */
    public function setDateEnvoi($dateEnvoi = null)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return CommandeFournisseur
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * @param float $TVA
     * @return CommandeFournisseur
     */
    public function setTVA($TVA)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantTotalTTC()
    {
        return $this->montantTotalTTC;
    }

    /**
     * @param float $montantTotalTTC
     * @return CommandeFournisseur
     */
    public function setMontantTotalTTC($montantTotalTTC)
    {
        $this->montantTotalTTC = $montantTotalTTC;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @param int $validation
     * @return CommandeFournisseur
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     * @return CommandeFournisseur
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRespectPlafond()
    {
        return $this->respectPlafond;
    }

    /**
     * Set adresseFacturation
     * @param string $adresseFacturation
     *
     * @return CommandeFournisseur
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * Get raisonAbandon
     *
     * @return string|null
     */
    public function getRaisonAbandon()
    {
        return $this->raisonAbandon;
    }

    /**
     * Set raisonAbandon
     * @param string $raisonAbandon
     *
     * @return CommandeFournisseur
     */
    public function setRaisonAbandon($raisonAbandon = null)
    {
        $this->raisonAbandon = $raisonAbandon;

        return $this;
    }

    /**
     * @return Collection|AvancementCommandeFournisseur[]|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @param AvancementCommandeFournisseur $avancement
     * @return CommandeFournisseur
     */
    public function addAvancement($avancement)
    {
        if (!$this->avancement->contains($avancement)) {
            $this->avancement[] = $avancement;
            $avancement->setCommandeFournisseur($this);
        }

        return $this;
    }

    /**
     * Get adresseFacturation
     *
     * @return string|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param bool $respectPlafond
     * @return CommandeFournisseur
     */
    public function setRespectPlafond($respectPlafond)
    {
        $this->respectPlafond = $respectPlafond;

        return $this;
    }

    /**
     * @return Collection|ProduitCommandeFournisseur[]
     */
    public function getProduitCommandeFournisseur()
    {
        return $this->produitCommandeFournisseur;
    }

    /**
     * @param ProduitCommandeFournisseur $produitCommandeFournisseur
     * @return CommandeFournisseur
     */
    public function addProduitCommandeFournisseur($produitCommandeFournisseur)
    {
        if (!$this->produitCommandeFournisseur->contains($produitCommandeFournisseur)) {
            $this->produitCommandeFournisseur[] = $produitCommandeFournisseur;
            $produitCommandeFournisseur->setCommandeFournisseur($this);
        }

        return $this;
    }

    /**
     * @return AdresseLivraisonSudalys|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @param AdresseLivraisonSudalys $adresseLivraison
     * @return CommandeFournisseur
     */
    public function setAdresseLivraison($adresseLivraison = null)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * Clear id
     * @return CommandeFournisseur
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }

    /**
     * @return Collection|FactureFournisseur[]
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return CommandeFournisseur
     */
    public function addFactureFournisseur($factureFournisseur)
    {
        if (!$this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur[] = $factureFournisseur;
            $factureFournisseur->setCommandeFournisseur($this);
        }

        return $this;
    }
}
