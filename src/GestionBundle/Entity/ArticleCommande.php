<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleCommandeRepository")
 */
class ArticleCommande
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_commande_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Commande
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Commande", inversedBy="articleCommandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commande;

    /**
     * @var ArticleDevis
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ArticleDevis", inversedBy="articleCommandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articleDevis;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $overrideMontantUnit;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $montantTotalHT;

    /**
     * @var ArticleAttachement[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleAttachement", mappedBy="articleCommande")
     */
    private $articleAttachements;

    /**
     * ArticleCommande constructor.
     */
    public function __construct()
    {
        $this->articleAttachements = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Commande|null
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * @param Commande|null $commande
     * @return ArticleCommande
     */
    public function setCommande($commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * @return ArticleDevis|null
     */
    public function getArticleDevis()
    {
        return $this->articleDevis;
    }

    /**
     * @param ArticleDevis|null $articleDevis
     * @return ArticleCommande
     */
    public function setArticleDevis($articleDevis)
    {
        $this->articleDevis = $articleDevis;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOverrideMontantUnit()
    {
        return $this->overrideMontantUnit;
    }

    /**
     * @param float $overrideMontantUnit
     * @return ArticleCommande
     */
    public function setOverrideMontantUnit($overrideMontantUnit)
    {
        $this->overrideMontantUnit = $overrideMontantUnit;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     * @return ArticleCommande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantTotalHT()
    {
        return $this->montantTotalHT;
    }

    /**
     * @param float $montantTotalHT
     * @return ArticleCommande
     */
    public function setMontantTotalHT($montantTotalHT)
    {
        $this->montantTotalHT = $montantTotalHT;

        return $this;
    }

    /**
     * @return Collection|ArticleAttachement[]
     */
    public function getArticleAttachements()
    {
        return $this->articleAttachements;
    }

    /**
     * @param \GestionBundle\Entity\ArticleAttachement $articleAttachement
     * @return ArticleCommande
     */
    public function addArticleAttachement($articleAttachement)
    {
        if (!$this->articleAttachements->contains($articleAttachement)) {
            $this->articleAttachements[] = $articleAttachement;
            $articleAttachement->setArticleCommande($this);
        }

        return $this;
    }

    /**
     * @param \GestionBundle\Entity\ArticleAttachement $articleAttachement
     * @return ArticleCommande
     */
    public function removeArticleAttachement($articleAttachement)
    {
        if ($this->articleAttachements->contains($articleAttachement)) {
            $this->articleAttachements->removeElement($articleAttachement);
            // set the owning side to null (unless already changed)
            if ($articleAttachement->getArticleCommande() === $this) {
                $articleAttachement->setArticleCommande(null);
            }
        }

        return $this;
    }
}
