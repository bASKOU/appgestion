<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceCommandePrestataire
 *
 * @ORM\Table(name="service_commande_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ServiceCommandePrestataireRepository")
 */
class ServiceCommandePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('service_commande_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="montantUnit", type="float")
     */
    private $montantUnit;

    /**
     * @var float
     *
     * @ORM\Column(name="quantite", type="float")
     */
    private $quantite;

    /**
     * @var float
     *
     * @ORM\Column(name="totalHT", type="float")
     */
    private $totalHT;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var CommandePrestataire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandePrestataire", inversedBy="serviceCommandePrestataire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commandePrestataire;

    /**
     * @var ServicePrestataire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ServicePrestataire", inversedBy="serviceCommandePrestataire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $servicePrestataire;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ServiceCommandePrestataire
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ServiceCommandePrestataire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set montantUnit.
     *
     * @param float $montantUnit
     *
     * @return ServiceCommandePrestataire
     */
    public function setMontantUnit($montantUnit)
    {
        $this->montantUnit = $montantUnit;

        return $this;
    }

    /**
     * Get montantUnit.
     *
     * @return float
     */
    public function getMontantUnit()
    {
        return $this->montantUnit;
    }

    /**
     * Set quantite.
     *
     * @param float $quantite
     *
     * @return ServiceCommandePrestataire
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite.
     *
     * @return float
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return ServiceCommandePrestataire
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return ServiceCommandePrestataire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return CommandePrestataire|null
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @param CommandePrestataire|null $commandePrestataire
     * @return ServiceCommandePrestataire
     */
    public function setCommandePrestataire($commandePrestataire)
    {
        $this->commandePrestataire = $commandePrestataire;

        return $this;
    }

    /**
     * @return ServicePrestataire|null
     */
    public function getServicePrestataire()
    {
        return $this->servicePrestataire;
    }

    /**
     * @param ServicePrestataire|null $servicePrestataire
     * @return ServiceCommandePrestataire
     */
    public function setServicePrestataire($servicePrestataire)
    {
        $this->servicePrestataire = $servicePrestataire;

        return $this;
    }

    /**
     * Clear id
     * @return ServiceCommandePrestataire
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }
}
