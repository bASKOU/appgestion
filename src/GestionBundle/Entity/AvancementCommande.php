<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvancementCommande
 *
 * @ORM\Table(name="avancement_commande")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AvancementCommandeRepository")
 */
class AvancementCommande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="date", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var float|null
     *
     * @ORM\Column(name="theorique", type="float", nullable=true)
     */
    private $theorique;

    /**
     * @var float|null
     *
     * @ORM\Column(name="reel", type="float", nullable=true)
     */
    private $reel;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validation;

    /**
     * @var Commande
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Commande", inversedBy="avancement")
     */
    private $commande;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return AvancementCommande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateSaisie.
     *
     * @param \DateTime $dateSaisie
     *
     * @return AvancementCommande
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set theorique.
     *
     * @param float|null $theorique
     *
     * @return AvancementCommande
     */
    public function setTheorique($theorique = null)
    {
        $this->theorique = $theorique;

        return $this;
    }

    /**
     * Get theorique.
     *
     * @return float|null
     */
    public function getTheorique()
    {
        return $this->theorique;
    }

    /**
     * Set reel.
     *
     * @param float|null $reel
     *
     * @return AvancementCommande
     */
    public function setReel($reel = null)
    {
        $this->reel = $reel;

        return $this;
    }

    /**
     * Get reel.
     *
     * @return float|null
     */
    public function getReel()
    {
        return $this->reel;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return AvancementCommande
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @param int $validation
     * @return AvancementCommande
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @return Commande
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * @param Commande $commande
     * @return AvancementCommande
     */
    public function setCommande($commande)
    {
        $this->commande = $commande;

        return $this;
    }
}
