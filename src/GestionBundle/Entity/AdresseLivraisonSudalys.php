<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdresseLivraisonSudalys
 *
 * @ORM\Table(name="adresse_livraison_sudalys")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AdresseLivraisonSudalysRepository")
 */
class AdresseLivraisonSudalys
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('adresse_livraison_sudalys_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresseComplementaire1", type="string", length=255, nullable=true)
     */
    private $adresseComplementaire1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresseComplementaire2", type="string", length=255, nullable=true)
     */
    private $adresseComplementaire2;

    /**
     * @var int
     *
     * @ORM\Column(name="stal", type="integer")
     */
    private $stal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var CommandeFournisseur[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandeFournisseur", mappedBy="adresseLivraison")
     */
    private $commandeFournisseur;

    /**
     * @var FactureFournisseur[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="adresseLivraison")
     */
    private $factureFournisseur;

    public function __construct()
    {
        $this->commandeFournisseur = new ArrayCollection();
        $this->factureFournisseur = new ArrayCollection();
    }

    public function __toString()
    {
        $adresse = $this->nom . ' : ';
        $adresse .= $this->adresse;
        if ($this->adresseComplementaire1) {
            $adresse .= ', ' . $this->adresseComplementaire1;
        }
        if ($this->adresseComplementaire2) {
            $adresse .= ', ' . $this->adresseComplementaire2;
        }
        $adresse .= ', ' . $this->stal . ' ' . $this->ville;

        return $adresse;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return AdresseLivraisonSudalys
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return AdresseLivraisonSudalys
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresseComplementaire1.
     *
     * @param string|null $adresseComplementaire1
     *
     * @return AdresseLivraisonSudalys
     */
    public function setAdresseComplementaire1($adresseComplementaire1 = null)
    {
        $this->adresseComplementaire1 = $adresseComplementaire1;

        return $this;
    }

    /**
     * Get adresseComplementaire1.
     *
     * @return string|null
     */
    public function getAdresseComplementaire1()
    {
        return $this->adresseComplementaire1;
    }

    /**
     * Set adresseComplementaire2.
     *
     * @param string|null $adresseComplementaire2
     *
     * @return AdresseLivraisonSudalys
     */
    public function setAdresseComplementaire2($adresseComplementaire2 = null)
    {
        $this->adresseComplementaire2 = $adresseComplementaire2;

        return $this;
    }

    /**
     * Get adresseComplementaire2.
     *
     * @return string|null
     */
    public function getAdresseComplementaire2()
    {
        return $this->adresseComplementaire2;
    }

    /**
     * Set stal.
     *
     * @param int $stal
     *
     * @return AdresseLivraisonSudalys
     */
    public function setStal($stal)
    {
        $this->stal = $stal;

        return $this;
    }

    /**
     * Get stal.
     *
     * @return int
     */
    public function getStal()
    {
        return $this->stal;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return AdresseLivraisonSudalys
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set CommandeFournisseur
     * @param CommandeFournisseur $commandeFournisseur
     *
     * @return AdresseLivraisonSudalys
     */
    public function setCommandeFournisseur($commandeFournisseur)
    {
        $this->commandeFournisseur[] = $commandeFournisseur;

        return $this;
    }

    /**
     * Get CommandeFournisseur
     *
     * @return Collection|CommandeFournisseur[]
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * Set FactureFournisseur
     * @param FactureFournisseur $factureFournisseur
     *
     * @return AdresseLivraisonSudalys
     */
    public function setFactureFournisseur($factureFournisseur)
    {
        $this->factureFournisseur[] = $factureFournisseur;

        return $this;
    }

    /**
     * Get FactureFournisseur
     *
     * @return Collection|FactureFournisseur[]
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }
}
