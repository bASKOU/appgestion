<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="theme")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ThemeRepository")
 */
class Theme
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('theme_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var Article[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Article", mappedBy="theme")
     */
    private $articles;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Theme
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return Collection | Article[]
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param Article $article
     * @return Theme
     */
    public function addArticle($article)
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setTheme($this);
        }

        return $this;
    }

    /**
     * @param Article $article
     * @return Theme
     */
    public function removeArticle($article)
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            if ($article->getTheme() === $this) {
                $article->setTheme(null);
            }
        }
        return $this;
    }
}
