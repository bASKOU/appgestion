<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeContact
 *
 * @ORM\Table(name="type_contact")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\TypeContactRepository")
 */
class TypeContact
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="bigint", options={"default"="nextval('contact_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, unique=true)
     */
    private $type;

    /**
     * @var Contact[]
     *
     * @ORM\ManyToMany(targetEntity="GestionBundle\Entity\Contact", mappedBy="type")
     * @ORM\JoinColumn(name="TypeContact_id", referencedColumnName="id")
     */
    private $contact;

    public function __toString()
    {
        return $this->type;
    }

    public function __construct()
    {
        $this->contact = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return TypeContact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Contact[]
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact[] = $contact;
    }

    /**
     * @param Contact $contact
     * @return TypeContact
     */
    public function removeContact($contact)
    {
        if ($this->contact->contains($contact)) {
            $this->tcontact->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact) {
                if ($contact->getType() === $this) {
                    $contact->setType(null);
                }
            }
        }

        return $this;
//        $this->contact = null;
//        $contact->removeType($this);
    }
}
