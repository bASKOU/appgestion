<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Affaire
 *
 * @ORM\Table(name="affaire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AffaireRepository")
 */
class Affaire
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('affaire_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation_affaire", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Vous devez indiquer une désignation d'affaire")
     */
    private $designationAffaire;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_affaire", type="string", length=255, unique=true)
     */
    private $numeroAffaire;

    /**
     * @var float
     * 
     * @ORM\Column(name="plafond", type="float", nullable=true)
     */
    private $plafond;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var Facture[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $factures;

    /**
     * @var Attribution[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attribution", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $attributions;

    /**
     * @var Commande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $commandes;

    /**
     * @var CommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandeFournisseur", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $commandeFournisseur;

    /**
     * @var CommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandePrestataire", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $commandePrestataire;

    /**
     * @var FactureFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $factureFournisseur;

    /**
     * @var FacturePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $facturePrestataire;

    /**
     * @var Devis[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="affaire", cascade="remove")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $devis;

    /**
     * @var ArticleAffaire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleAffaire", mappedBy="affaire", cascade="remove")
     */
    private $articleAffaires;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", length=2, options={"default": true})
     */
    private $respectPlafond;

    /**
     * @var Organisme|null
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="affaire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $organisme;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseFacturationAffaire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseFacturation;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseLivraisonAffaire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * @var AvancementAffaire
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\AvancementAffaire", mappedBy="affaire")
     */
    private $avancement;

    /**
     * @var EstimationPrestation[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationPrestation", mappedBy="affaire")
     */
    private $estimationPrestation;

    /**
     * @var EstimationFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationFournisseur", mappedBy="affaire")
     */
    private $estimationFournisseur;

    /**
     * Affaire constructor.
     */
    public function __construct()
    {
        $this->factures = new ArrayCollection();
        $this->attributions = new ArrayCollection();
        $this->commandes = new ArrayCollection();
        $this->commandeFournisseur = new ArrayCollection();
        $this->commandePrestataire = new ArrayCollection();
        $this->factureFournisseur = new ArrayCollection();
        $this->facturePrestataire = new ArrayCollection();
        $this->devis = new ArrayCollection();
        $this->articleAffaires = new ArrayCollection();
        $this->avancement = new ArrayCollection();
        $this->estimationPrestation = new ArrayCollection();
        $this->estimationFournisseur = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designationAffaire.
     *
     * @param string $designationAffaire
     *
     * @return Affaire
     */
    public function setDesignationAffaire($designationAffaire)
    {
        $this->designationAffaire = $designationAffaire;

        return $this;
    }

    /**
     * Get designationAffaire.
     *
     * @return string
     */
    public function getDesignationAffaire()
    {
        return $this->designationAffaire;
    }

    /**
     * Set numeroAffaire.
     *
     * @param string $numeroAffaire
     *
     * @return Affaire
     */
    public function setNumeroAffaire($numeroAffaire)
    {
        $this->numeroAffaire = $numeroAffaire;

        return $this;
    }

    /**
     * Get numeroAffaire.
     *
     * @return string
     */
    public function getNumeroAffaire()
    {
        return $this->numeroAffaire;
    }

    /**
     * Set plafond.
     * 
     * @param float $plafond
     * 
     * @return Affaire
     */
    public function setPlafond($plafond)
    {
        $this->plafond = $plafond;

        return $this;
    }

    /**
     * Get plafond.
     * 
     * @return float
     */
    public function getPlafond()
    {
        return $this->plafond;
    }

    /**
     * getDateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     * @return Affaire
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * @param \DateTime $dateEcheance
     * @return Affaire
     */
    public function setDateEcheance($dateEcheance)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures()
    {
        return $this->factures;
    }

    /**
     * @param Facture $facture
     * @return Affaire
     */
    public function addFacture(Facture $facture)
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param Facture $facture
     * @return Affaire
     */
    public function removeFacture(Facture $facture)
    {
        if ($this->factures->contains($facture)) {
            $this->factures->removeElement($facture);
            // set the owning side to null (unless already changed)
            if ($facture->getAffaire() === $this) {
                $facture->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Attribution[]
     */
    public function getAttributions()
    {
        return $this->attributions;
    }

    /**
     * @param Attribution $attribution
     * @return Affaire
     */
    public function addAttribution(Attribution $attribution)
    {
        if (!$this->attributions->contains($attribution)) {
            $this->attributions[] = $attribution;
            $attribution->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param Attribution $attribution
     * @return Affaire
     */
    public function removeAttribution(Attribution $attribution)
    {
        if ($this->attributions->contains($attribution)) {
            $this->attributions->removeElement($attribution);
            // set the owning side to null (unless already changed)
            if ($attribution->getAffaire() === $this) {
                $attribution->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * @return bool|null
     */
    public function getRespectPlafond()
    {
        return $this->respectPlafond;
    }

    /**
     * @param bool $respectPlafond
     * @return Affaire
     */
    public function setRespectPlafond($respectPlafond = true)
    {
        $this->respectPlafond = $respectPlafond;

        return $this;
    }

    /**
     * @param Organisme|null $adresseFacturation
     *
     * @return Affaire
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param Organisme|null $adresseLivraison
     *
     * @return Affaire
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @param Commande $commande
     * @return Affaire
     */
    public function addCommande(Commande $commande)
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param Commande $commande
     * @return Affaire
     */
    public function removeCommande(Commande $commande)
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getAffaire() === $this) {
                $commande->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandeFournisseur[]
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Affaire
     */
    public function addCommandeFournisseur(CommandeFournisseur $commandeFournisseur)
    {
        if (!$this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur[] = $commandeFournisseur;
            $commandeFournisseur->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Affaire
     */
    public function removeCommandeFournisseur(CommandeFournisseur $commandeFournisseur)
    {
        if ($this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur->removeElement($commandeFournisseur);
            // set the owning side to null (unless already changed)
            if ($commandeFournisseur->getAffaire() === $this) {
                $commandeFournisseur->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandePrestataire[]
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Affaire
     */
    public function addCommandePrestataire(CommandePrestataire $commandePrestataire)
    {
        if (!$this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire[] = $commandePrestataire;
            $commandePrestataire->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Affaire
     */
    public function removeCommandePrestataire(CommandePrestataire $commandePrestataire)
    {
        if ($this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire->removeElement($commandePrestataire);
            // set the owning side to null (unless already changed)
            if ($commandePrestataire->getAffaire() === $this) {
                $commandePrestataire->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FactureFournisseur[]
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Affaire
     */
    public function addFactureFournisseur(FactureFournisseur $factureFournisseur)
    {
        if (!$this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur[] = $factureFournisseur;
            $factureFournisseur->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Affaire
     */
    public function removeFactureFournisseur(FactureFournisseur $factureFournisseur)
    {
        if ($this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur->removeElement($factureFournisseur);
            // set the owning side to null (unless already changed)
            if ($factureFournisseur->getAffaire() === $this) {
                $factureFournisseur->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturePrestataire[]
     */
    public function getFacturePrestataire()
    {
        return $this->facturePrestataire;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Affaire
     */
    public function addFacturePrestataire(FacturePrestataire $facturePrestataire)
    {
        if (!$this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire[] = $facturePrestataire;
            $facturePrestataire->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Affaire
     */
    public function removeFacturePrestataire(FacturePrestataire $facturePrestataire)
    {
        if ($this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire->removeElement($facturePrestataire);
            // set the owning side to null (unless already changed)
            if ($facturePrestataire->getAffaire() === $this) {
                $facturePrestataire->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * @param Devis $devi
     * @return Affaire
     */
    public function addDevi(Devis $devi)
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param Devis $devi
     * @return Affaire
     */
    public function removeDevi(Devis $devi)
    {
        if ($this->devis->contains($devi)) {
            $this->devis->removeElement($devi);
            // set the owning side to null (unless already changed)
            if ($devi->getAffaire() === $this) {
                $devi->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleAffaire[]
     */
    public function getArticleAffaires()
    {
        return $this->articleAffaires;
    }

    /**
     * @param ArticleAffaire $articleAffaires
     * @return Affaire
     */
    public function addArticleAffaires(ArticleAffaire $articleAffaires)
    {
        if (!$this->articleAffaires->contains($articleAffaires)) {
            $this->articleAffaires[] = $articleAffaires;
            $articleAffaires->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param ArticleAffaire $articleAffaires
     * @return Affaire
     */
    public function removeAricleAffaires(ArticleAffaire $articleAffaires)
    {
        if ($this->articleAffaires->contains($articleAffaires)) {
            $this->articleAffaires->removeElement($articleAffaires);
            // set the owning side to null (unless already changed)
            if ($articleAffaires->getAffaire() === $this) {
                $articleAffaires->setAffaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme|null $organisme
     * @return Affaire
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return null|AvancementAffaire[]
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @param AvancementAffaire $avancement
     * @return Affaire
     */
    public function addAvancement(AvancementAffaire $avancement)
    {
        if (!$this->avancement->contains($avancement)) {
            $this->avancement[] = $avancement;
            $avancement->setAffaire($this);
        }

        return $this;
    }

    /**
     * @param EstimationPrestation[] $estimationPrestation
     * @return Affaire
     */
    public function setEstimationPrestation($estimationPrestation)
    {
        $this->estimationPrestation = $estimationPrestation;

        return $this;
    }

    /**
     * @return EstimationPrestation[]|Collection|null
     */
    public function getEstimationPrestation()
    {
        return $this->estimationPrestation;
    }

    /**
     * @param EstimationFournisseur[] $estimationFournisseur
     * @return Affaire
     */
    public function setEstimationFournisseur($estimationFournisseur)
    {
        $this->estimationFournisseur = $estimationFournisseur;

        return $this;
    }

    /**
     * @return EstimationFournisseur[]|Collection|null
     */
    public function getEstimationFournisseur()
    {
        return $this->estimationFournisseur;
    }
}
