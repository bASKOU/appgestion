<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AttachementRepository")
 */
class Attachement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('attachement_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Commande
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Commande", inversedBy="attachements", cascade="remove")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commande;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $numeroAttachement;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $numeroAttachementBis;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="attachements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="clientAttachement")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="attachements")
     */
    private $contact;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateAttachement;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $mentions;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalTTC;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $remise;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $activeTVA;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $forfaitaire;

    /**
     * @var ArticleAttachement[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleAttachement", mappedBy="attachement", cascade="remove")
     */
    private $articleAttachements;

    /**
     * @var Facture
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Facture", inversedBy="attachement", cascade="remove")
     * @ORM\JoinColumn(nullable=true)
     */
    private $facture;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseFacturationAttachement")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseFacturation;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseLivraisonAttachement")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * Attachement constructor.
     */
    public function __construct()
    {
        $this->articleAttachements = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Commande|null
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * @param Commande|null $commande
     * @return Attachement
     */
    public function setCommande($commande)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Attachement
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return Attachement
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateAttachement()
    {
        return $this->dateAttachement;
    }

    /**
     * @param \DateTimeInterface $dateAttachement
     * @return Attachement
     */
    public function setDateAttachement(\DateTimeInterface $dateAttachement)
    {
        $this->dateAttachement = $dateAttachement;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * @param string $mentions
     * @return Attachement
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroAttachement()
    {
        return $this->numeroAttachement;
    }

    /**
     * @param string $numeroAttachement
     * @return Attachement
     */
    public function setNumeroAttachement($numeroAttachement)
    {
        $this->numeroAttachement = $numeroAttachement;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroAttachementBis()
    {
        return $this->numeroAttachementBis;
    }

    /**
     * @param string $numeroAttachementBis
     * @return Attachement
     */
    public function setNumeroAttachementBis($numeroAttachementBis)
    {
        $this->numeroAttachementBis = $numeroAttachementBis;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return Attachement
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

    /**
     * @param float $totalTTC
     * @return Attachement
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @param float $remise
     * @return Attachement
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTVA()
    {
        return $this->tva;
    }

    /**
     * @param float $tva
     * @return Attachement
     */
    public function setTVA($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     * @return Attachement
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActiveTVA()
    {
        return $this->activeTVA;
    }

    /**
     * @param bool|null $activeTVA
     * @return Attachement
     */
    public function setActiveTVA($activeTVA)
    {
        $this->activeTVA = $activeTVA;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getForfaitaire()
    {
        return $this->forfaitaire;
    }

    /**
     * @param bool|null $forfaitaire
     * @return Attachement
     */
    public function setForfaitaire($forfaitaire)
    {
        $this->forfaitaire = $forfaitaire;

        return $this;
    }

    /**
     * @return Collection|ArticleAttachement[]
     */
    public function getArticleAttachements()
    {
        return $this->articleAttachements;
    }

    /**
     * @param \GestionBundle\Entity\ArticleAttachement $articleAttachement
     * @return Attachement
     */
    public function addArticleAttachement($articleAttachement)
    {
        if (!$this->articleAttachements->contains($articleAttachement)) {
            $this->articleAttachements[] = $articleAttachement;
            $articleAttachement->setAttachement($this);
        }

        return $this;
    }

    /**
     * @param \GestionBundle\Entity\ArticleAttachement $articleAttachement
     * @return Attachement
     */
    public function removeArticleAttachement($articleAttachement)
    {
        if ($this->articleAttachements->contains($articleAttachement)) {
            $this->articleAttachements->removeElement($articleAttachement);
            // set the owning side to null (unless already changed)
            if ($articleAttachement->getAttachement() === $this) {
                $articleAttachement->setAttachement(null);
            }
        }

        return $this;
    }

    /**
     * @return Facture|null
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @param Facture $facture
     * @return Attachement
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * @param Organisme|null $adresseFacturation
     *
     * @return Attachement
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param Organisme|null $adresseLivraison
     *
     * @return Attachement
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @return Organisme|null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Organisme|null $client
     * @return Attachement
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }
}