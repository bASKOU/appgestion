<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ServicePrestataire
 *
 * @ORM\Table(name="service_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ServicePrestataireRepository")
 */
class ServicePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('service_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float|null
     *
     * @ORM\Column(name="montantUnit", type="float", nullable=true)
     */
    private $montantUnit;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateValid", type="date", nullable=true)
     */
    private $dateValid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreate", type="date")
     */
    private $dateCreate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referencePrestataire", type="string", length=255, nullable=true)
     */
    private $referencePrestataire;

    /**
     * @var float|null
     *
     * @ORM\Column(name="prixFixe", type="float", nullable=true)
     */
    private $prixFixe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parent", type="integer", nullable=true)
     */
    private $parent;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="servicePrestataire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $prestataire;

    /**
     * @var ServiceCommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ServiceCommandePrestataire", mappedBy="servicePrestataire", cascade="remove")
     */
    private $serviceCommandePrestataire;

    public function __construct()
    {
        $this->serviceCommandePrestataire = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ServicePrestataire
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ServicePrestataire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set montantUnit.
     *
     * @param float|null $montantUnit
     *
     * @return ServicePrestataire
     */
    public function setMontantUnit($montantUnit = null)
    {
        $this->montantUnit = $montantUnit;

        return $this;
    }

    /**
     * Get montantUnit.
     *
     * @return float|null
     */
    public function getMontantUnit()
    {
        return $this->montantUnit;
    }

    /**
     * Set dateValid.
     *
     * @param \DateTime|null $dateValid
     *
     * @return ServicePrestataire
     */
    public function setDateValid($dateValid = null)
    {
        $this->dateValid = $dateValid;

        return $this;
    }

    /**
     * Get dateValid.
     *
     * @return \DateTime|null
     */
    public function getDateValid()
    {
        return $this->dateValid;
    }

    /**
     * Set dateCreate.
     *
     * @param \DateTime $dateCreate
     *
     * @return ServicePrestataire
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate.
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set referencePrestataire.
     *
     * @param string|null $referencePrestataire
     *
     * @return ServicePrestataire
     */
    public function setReferencePrestataire($referencePrestataire = null)
    {
        $this->referencePrestataire = $referencePrestataire;

        return $this;
    }

    /**
     * Get referencePrestataire.
     *
     * @return string|null
     */
    public function getReferencePrestataire()
    {
        return $this->referencePrestataire;
    }

    /**
     * Set prixFixe.
     *
     * @param float|null $prixFixe
     *
     * @return ServicePrestataire
     */
    public function setPrixFixe($prixFixe = null)
    {
        $this->prixFixe = $prixFixe;

        return $this;
    }

    /**
     * Get prixFixe.
     *
     * @return float|null
     */
    public function getPrixFixe()
    {
        return $this->prixFixe;
    }

    /**
     * Set parent.
     *
     * @param int|null $parent
     *
     * @return ServicePrestataire
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return int|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Organisme|null $prestataire
     * @return ServicePrestataire
     */
    public function setPrestataire(Organisme $prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * @return Collection|ServiceCommandePrestataire[]
     */
    public function getServiceCommandePrestataire()
    {
        return $this->serviceCommandePrestataire;
    }

    /**
     * @param ServiceCommandePrestataire $serviceCommandePrestataire
     * @return ServicePrestataire
     */
    public function addProduitServiceCommandePrestataire($serviceCommandePrestataire)
    {
        if (!$this->serviceCommandePrestataire->contains($serviceCommandePrestataire)) {
            $this->serviceCommandePrestataire[] = $serviceCommandePrestataire;
            $serviceCommandePrestataire->setServicePrestataire($this);
        }

        return $this;
    }
}
