<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvancementCommandePrestataire
 *
 * @ORM\Table(name="avancement_commande_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AvancementCommandePrestataireRepository")
 */
class AvancementCommandePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(type="bigint", options={"default"="nextval('avancement_commande_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="date", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var float|null
     *
     * @ORM\Column(name="avancement", type="float", nullable=true)
     */
    private $avancement;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validation;

    /**
     * @var CommandePrestataire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandePrestataire", inversedBy="avancement")
     */
    private $commandePrestataire;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return AvancementCommandePrestataire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateSaisie.
     *
     * @param \DateTime $dateSaisie
     *
     * @return AvancementCommandePrestataire
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set avancement.
     *
     * @param float|null $avancement
     *
     * @return AvancementCommandePrestataire
     */
    public function setAvancement($avancement = null)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement.
     *
     * @return float|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return AvancementCommandePrestataire
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @param int $validation
     * @return AvancementCommandePrestataire
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @return CommandePrestataire
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return AvancementCommandePrestataire
     */
    public function setCommandePrestataire($commandePrestataire)
    {
        $this->commandePrestataire = $commandePrestataire;

        return $this;
    }
}
