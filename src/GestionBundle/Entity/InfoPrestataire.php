<?php

namespace GestionBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPrestataire
 *
 * @ORM\Table(name="info_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\InfoPrestataireRepository")
 */
class InfoPrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('info_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     */
    private $dateInfo;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $isValid;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isEtrangere;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isCommercantArtisant;

    /**
     * @var bool
     *
     * @ORM\Column(name="RIB", type="boolean", options={"default":false}, nullable=true)
     */
    private $RIB;

    /**
     * @var array|null
     *
     * @ORM\Column(name="RIB_document", type="array", length=255, nullable=true, options={"default":null})
     */
    private $RIBDocument;

    /**
     * @var bool
     *
     * @ORM\Column(name="kbis", type="boolean", options={"default":false}, nullable=true)
     */
    private $kbis;

    /**
     * @var array|null
     *
     * @ORM\Column(name="kbis_document", type="array", length=255, nullable=true, options={"default":null})
     */
    private $kbisDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateKbis;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidKbis;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $responsabiliteProfessionnel;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $responsabiliteProfessionnelDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateResponsabiliteProfessionnel;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $renouvellementDecembreResponsabiliteProfessionnel;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidResponsabiliteProfessionnel;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $attestationFiscale;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $attestationFiscaleDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateAttestationFiscale;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidAttestationFiscale;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $attestationURSSAF;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $attestationURSSAFDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateAttestationURSSAF;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidAttestationURSSAF;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $listeSalarieEtranger;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $listeSalarieEtrangerDocument;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $certificatCDAPH;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $certificatCDAPHDocument;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $qualificationsProfessionnelles;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $qualificationsProfessionnellesDocument;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $devis;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $documentPublicitaire;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $correspondanceProfessionnelle;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $recepisseDepotDeclaration;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $recepisseDepotDeclarationDocument;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $autreDocument;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $registreProfessionelCertifInscription;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $registreProfessionelCertifInscriptionDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateRegistreProfessionelCertifInscription;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidRegistreProfessionelCertifInscription;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $attestationImmatriculation;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $attestationImmatriculationDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateAttestationImmatriculation;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidAttestationImmatriculation;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $fiscalEtranger;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $fiscalEtrangerDocument;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $socialEtranger;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true, options={"default":null})
     */
    private $socialEtrangerDocument;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true, options={"default":null})
     */
    private $dateSocialEtranger;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":true}, nullable=true)
     */
    private $isValidSocialEtranger;

    /**
     * @var Organisme
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Organisme", cascade={"remove"}, inversedBy="infoPrestataire")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $prestataire;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param DateTime $dateInfo
     * @return InfoPrestataire
     */
    public function setDateInfo($dateInfo)
    {
        $this->dateInfo = $dateInfo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateInfo()
    {
        return $this->dateInfo;
    }

    /**
     * @param bool $isValid
     * @return InfoPrestataire
     */
    public function setIsValid($isValid = false)
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set RIB.
     *
     * @param bool $RIB
     *
     * @return InfoPrestataire
     */
    public function setRIB($RIB = false)
    {
        $this->RIB = $RIB;

        return $this;
    }

    /**
     * Get RIB.
     *
     * @return bool
     */
    public function getRIB()
    {
        return $this->RIB;
    }

    /**
     * Set RIBDocument.
     *
     * @param array|null $RIBDocument
     *
     * @return InfoPrestataire
     */
    public function setRIBDocument($RIBDocument = null)
    {
        $this->RIBDocument = $RIBDocument;

        return $this;
    }

    /**
     * Get RIBDocument.
     *
     * @return array|null
     */
    public function getRIBDocument()
    {
        return $this->RIBDocument;
    }

    /**
     * Set kbis.
     *
     * @param bool $kbis
     *
     * @return InfoPrestataire
     */
    public function setKbis($kbis)
    {
        $this->kbis = $kbis;

        return $this;
    }

    /**
     * Get kbis.
     *
     * @return bool
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * Set kbisDocument.
     *
     * @param array|null $kbisDocument
     *
     * @return InfoPrestataire
     */
    public function setKbisDocument($kbisDocument = null)
    {
        $this->kbisDocument = $kbisDocument;

        return $this;
    }

    /**
     * Get kbisDocument.
     *
     * @return array|null
     */
    public function getKbisDocument()
    {
        return $this->kbisDocument;
    }

    /**
     * Set dateKbis
     * @var DateTime|null $dateKbis
     *
     * @return InfoPrestataire
     */
    public function setDateKbis($dateKbis = null)
    {
        $this->dateKbis = $dateKbis;

        return $this;
    }

    /**
     * Get dateKbis
     * @return DateTime|null
     */
    public function getDateKbis()
    {
        return $this->dateKbis;
    }

    /**
     * @param bool $isValidKbis
     * @return InfoPrestataire
     */
    public function setIsValidKbis($isValidKbis = true)
    {
        $this->isValidKbis = $isValidKbis;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsValidKbis()
    {
        return $this->isValidKbis;
    }

    /**
     * @return Organisme|null
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * @param Organisme|null $prestataire
     * @return InfoPrestataire
     */
    public function setPrestataire($prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * @param bool $responsabiliteProfessionnel
     * @return InfoPrestataire
     */
    public function setResponsabiliteProfessionnel($responsabiliteProfessionnel = false)
    {
        $this->responsabiliteProfessionnel = $responsabiliteProfessionnel;

        return $this;
    }

    /**
     * @return bool
     */
    public function getResponsabiliteProfessionnel()
    {
        return $this->responsabiliteProfessionnel;
    }

    /**
     * @param array|null $responsabiliteProfessionnelDocument
     * @return InfoPrestataire
     */
    public function setResponsabiliteProfessionnelDocument($responsabiliteProfessionnelDocument = null)
    {
        $this->responsabiliteProfessionnelDocument = $responsabiliteProfessionnelDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getResponsabiliteProfessionnelDocument()
    {
        return $this->responsabiliteProfessionnelDocument;
    }

    /**
     * Set dateResponsabiliteProfessionnel
     * @var DateTime|null $dateResponsabiliteProfessionnel
     *
     * @return InfoPrestataire
     */
    public function setDateResponsabiliteProfessionnel($dateResponsabiliteProfessionnel = null)
    {
        $this->dateResponsabiliteProfessionnel = $dateResponsabiliteProfessionnel;

        return $this;
    }

    /**
     * Get dateResponsabiliteProfessionnel
     * @return DateTime|null
     */
    public function getDateResponsabiliteProfessionnel()
    {
        return $this->dateResponsabiliteProfessionnel;
    }

    /**
     * @param bool|null $renouvellementDecembreResponsabiliteProfessionnel
     * @return InfoPrestataire
     */
    public function setRenouvellementDecembreResponsabiliteProfessionnel($renouvellementDecembreResponsabiliteProfessionnel = null)
    {
        $this->renouvellementDecembreResponsabiliteProfessionnel = $renouvellementDecembreResponsabiliteProfessionnel;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRenouvellementDecembreResponsabiliteProfessionnel()
    {
        return $this->renouvellementDecembreResponsabiliteProfessionnel;
    }

    /**
     * @param bool $isValidResponsabiliteProfessionnel
     * @return InfoPrestataire
     */
    public function setIsValidResponsabiliteProfessionnel($isValidResponsabiliteProfessionnel = true)
    {
        $this->isValidResponsabiliteProfessionnel = $isValidResponsabiliteProfessionnel;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsValidResponsabiliteProfessionnel()
    {
        return $this->isValidResponsabiliteProfessionnel;
    }

    /**
     * @param bool $attestationFiscale
     * @return InfoPrestataire
     */
    public function setAttestationFiscale($attestationFiscale = true)
    {
        $this->attestationFiscale = $attestationFiscale;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAttestationFiscale()
    {
        return $this->attestationFiscale;
    }

    /**
     * @param array $attestationFiscaleDocument
     * @return InfoPrestataire
     */
    public function setAttestationFiscaleDocument($attestationFiscaleDocument = null)
    {
        $this->attestationFiscaleDocument = $attestationFiscaleDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttestationFiscaleDocument()
    {
        return $this->attestationFiscaleDocument;
    }

    /**
     * @param DateTime|null $dateAttestationFiscale
     * @return InfoPrestataire
     */
    public function setDateAttestationFiscale($dateAttestationFiscale = null)
    {
        $this->dateAttestationFiscale = $dateAttestationFiscale;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateAttestationFiscale()
    {
        return $this->dateAttestationFiscale;
    }

    /**
     * @param bool $isValidAttestationFiscale
     */
    public function setIsValidAttestationFiscale($isValidAttestationFiscale = true)
    {
        $this->isValidAttestationFiscale = $isValidAttestationFiscale;
    }

    /**
     * @return bool
     */
    public function getIsValidAttestationFiscale()
    {
        return $this->isValidAttestationFiscale;
    }

    /**
     * @param bool $attestationURSSAF
     * @return InfoPrestataire
     */
    public function setAttestationURSSAF($attestationURSSAF = true)
    {
        $this->attestationURSSAF = $attestationURSSAF;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAttestationURSSAF()
    {
        return $this->attestationURSSAF;
    }

    /**
     * @param array $attestationURSSAFDocument
     * @return InfoPrestataire
     */
    public function setAttestationURSSAFDocument($attestationURSSAFDocument = null)
    {
        $this->attestationURSSAFDocument = $attestationURSSAFDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttestationURSSAFDocument()
    {
        return $this->attestationURSSAFDocument;
    }

    /**
     * @param DateTime|null $dateAttestationURSSAF
     * @return InfoPrestataire
     */
    public function setDateAttestationURSSAF($dateAttestationURSSAF = null)
    {
        $this->dateAttestationURSSAF = $dateAttestationURSSAF;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateAttestationURSSAF()
    {
        return $this->dateAttestationURSSAF;
    }

    /**
     * @param bool $isValidAttestationURSSAF
     */
    public function setIsValidAttestationURSSAF($isValidAttestationURSSAF = true)
    {
        $this->isValidAttestationURSSAF = $isValidAttestationURSSAF;
    }

    /**
     * @return bool
     */
    public function getIsValidAttestationURSSAF()
    {
        return $this->isValidAttestationURSSAF;
    }

    /**
     * @param bool $listeSalarieEtranger
     * @return InfoPrestataire
     */
    public function setListeSalarieEtranger($listeSalarieEtranger)
    {
        $this->listeSalarieEtranger = $listeSalarieEtranger;

        return $this;
    }

    /**
     * @return bool
     */
    public function getListeSalarieEtranger()
    {
        return $this->listeSalarieEtranger;
    }

    /**
     * @param array|null $listeSalarieEtrangerDocument
     * @return InfoPrestataire
     */
    public function setListeSalarieEtrangerDocument($listeSalarieEtrangerDocument = null)
    {
        $this->listeSalarieEtrangerDocument = $listeSalarieEtrangerDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getListeSalarieEtrangerDocument()
    {
        return $this->listeSalarieEtrangerDocument;
    }

    /**
     * @param bool $certificatCDAPH
     * @return InfoPrestataire
     */
    public function setCertificatCDAPH($certificatCDAPH)
    {
        $this->certificatCDAPH = $certificatCDAPH;

        return $this;
    }

    /**
     * @return bool
     */
    public function getCertificatCDAPH()
    {
        return $this->certificatCDAPH;
    }

    /**
     * @param array $certificatCDAPHDocument
     * @return InfoPrestataire
     */
    public function setCertificatCDAPHDocument($certificatCDAPHDocument = null)
    {
        $this->certificatCDAPHDocument = $certificatCDAPHDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getCertificatCDAPHDocument()
    {
        return $this->certificatCDAPHDocument;
    }

    /**
     * @param array $autreDocument
     * @return InfoPrestataire
     */
    public function setAutreDocument($autreDocument = null)
    {
        $this->autreDocument = $autreDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getAutreDocument()
    {
        return $this->autreDocument;
    }

    /**
     * @param bool $qualificationsProfessionnelles
     * @return InfoPrestataire
     */
    public function setQualificationsProfessionnelles($qualificationsProfessionnelles)
    {
        $this->qualificationsProfessionnelles = $qualificationsProfessionnelles;

        return $this;
    }

    /**
     * @return bool
     */
    public function getQualificationsProfessionnelles()
    {
        return $this->qualificationsProfessionnelles;
    }

    /**
     * @param array $qualificationsProfessionnellesDocument
     * @return InfoPrestataire
     */
    public function setQualificationsProfessionnellesDocument($qualificationsProfessionnellesDocument = null)
    {
        $this->qualificationsProfessionnellesDocument = $qualificationsProfessionnellesDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getQualificationsProfessionnellesDocument()
    {
        return $this->qualificationsProfessionnellesDocument;
    }

    /**
     * @param bool $isEtrangere
     * @return InfoPrestataire
     */
    public function setIsEtrangere($isEtrangere = false)
    {
        $this->isEtrangere = $isEtrangere;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsEtrangere()
    {
        return $this->isEtrangere;
    }

    /**
     * @param bool $isCommercantArtisant
     * @return InfoPrestataire
     */
    public function setIsCommercantArtisant($isCommercantArtisant = true)
    {
        $this->isCommercantArtisant = $isCommercantArtisant;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsCommercantArtisant()
    {
        return $this->isCommercantArtisant;
    }

    /**
     * @param bool $devis
     * @return InfoPrestataire
     */
    public function setDevis($devis = false)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * @param bool $documentPublicitaire
     * @return InfoPrestataire
     */
    public function setDocumentPublicitaire($documentPublicitaire = false)
    {
        $this->documentPublicitaire = $documentPublicitaire;

        return $this;
    }

    public function getDocumentPublicitaire()
    {
        return $this->documentPublicitaire;
    }

    /**
     * @param bool $correspondanceProfessionnelle
     * @return InfoPrestataire
     */
    public function setCorrespondanceProfessionnelle($correspondanceProfessionnelle = false)
    {
        $this->correspondanceProfessionnelle = $correspondanceProfessionnelle;

        return $this;
    }

    /**
     * @return bool
     */
    public function getCorrespondanceProfessionnelle()
    {
        return $this->correspondanceProfessionnelle;
    }

    /**
     * @param bool $recepisseDepotDeclaration
     * @return InfoPrestataire
     */
    public function setRecepisseDepotDeclaration($recepisseDepotDeclaration = false)
    {
        $this->recepisseDepotDeclaration = $recepisseDepotDeclaration;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRecepisseDepotDeclaration()
    {
        return $this->recepisseDepotDeclaration;
    }

    /**
     * @param array|null $recepisseDepotDeclarationDocument
     */
    public function setRecepisseDepotDeclarationDocument($recepisseDepotDeclarationDocument = null)
    {
        $this->recepisseDepotDeclarationDocument = $recepisseDepotDeclarationDocument;
    }

    /**
     * @return array|null
     */
    public function getRecepisseDepotDeclarationDocument()
    {
        return $this->recepisseDepotDeclarationDocument;
    }

    /**
     * @param bool $registreProfessionelCertifInscription
     * @return InfoPrestataire
     */
    public function setRegistreProfessionelCertifInscription($registreProfessionelCertifInscription = false)
    {
        $this->registreProfessionelCertifInscription = $registreProfessionelCertifInscription;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRegistreProfessionelCertifInscription()
    {
        return $this->registreProfessionelCertifInscription;
    }

    /**
     * @param array|null $registreProfessionelCertifInscriptionDocument
     * @return InfoPrestataire
     */
    public function setRegistreProfessionelCertifInscriptionDocument($registreProfessionelCertifInscriptionDocument = null)
    {
        $this->registreProfessionelCertifInscriptionDocument = $registreProfessionelCertifInscriptionDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRegistreProfessionelCertifInscriptionDocument()
    {
        return $this->registreProfessionelCertifInscriptionDocument;
    }

    /**
     * @param DateTime|null $dateRegistreProfessionelCertifInscription
     * @return InfoPrestataire
     */
    public function setDateRegistreProfessionelCertifInscription($dateRegistreProfessionelCertifInscription = null)
    {
        $this->dateRegistreProfessionelCertifInscription = $dateRegistreProfessionelCertifInscription;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateRegistreProfessionelCertifInscription()
    {
        return $this->dateRegistreProfessionelCertifInscription;
    }

    /**
     * @param bool $isValidRegistreProfessionelCertifInscription
     * @return InfoPrestataire
     */
    public function setIsValidRegistreProfessionelCertifInscription($isValidRegistreProfessionelCertifInscription = true)
    {
        $this->isValidRegistreProfessionelCertifInscription = $isValidRegistreProfessionelCertifInscription;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsValidRegistreProfessionelCertifInscription()
    {
        return $this->isValidRegistreProfessionelCertifInscription;
    }

    /**
     * @param bool $attestationImmatriculation
     * @return InfoPrestataire
     */
    public function setAttestationImmatriculation($attestationImmatriculation = false)
    {
        $this->attestationImmatriculation = $attestationImmatriculation;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAttestationImmatriculation()
    {
        return $this->attestationImmatriculation;
    }

    /**
     * @param array|null $attestationImmatriculationDocument
     * @return InfoPrestataire
     */
    public function setAttestationImmatriculationDocument($attestationImmatriculationDocument = null)
    {
        $this->attestationImmatriculationDocument = $attestationImmatriculationDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttestationImmatriculationDocument()
    {
        return $this->attestationImmatriculationDocument;
    }

    /**
     * @param DateTime $dateAttestationImmatriculation
     * @return InfoPrestataire
     */
    public function setDateAttestationImmatriculation($dateAttestationImmatriculation = null)
    {
        $this->dateAttestationImmatriculation = $dateAttestationImmatriculation;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateAttestationImmatriculation()
    {
        return $this->dateAttestationImmatriculation;
    }

    /**
     * @param bool $isValidAttestationImmatriculation
     * @return InfoPrestataire
     */
    public function setIsValidAttestationImmatriculation($isValidAttestationImmatriculation = true)
    {
        $this->isValidAttestationImmatriculation = $isValidAttestationImmatriculation;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsValidAttestationImmatriculation()
    {
        return $this->isValidAttestationImmatriculation;
    }

    /**
     * @param bool $fiscalEtranger
     * @return InfoPrestataire
     */
    public function setFiscalEtranger($fiscalEtranger = false)
    {
        $this->fiscalEtranger = $fiscalEtranger;

        return $this;
    }

    /**
     * @return bool
     */
    public function getFiscalEtranger()
    {
        return $this->fiscalEtranger;
    }

    /**
     * @param array|null $fiscalEtrangerDocument
     * @return InfoPrestataire
     */
    public function setFiscalEtrangerDocument($fiscalEtrangerDocument = null)
    {
        $this->fiscalEtrangerDocument = $fiscalEtrangerDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getFiscalEtrangerDocument()
    {
        return $this->fiscalEtrangerDocument;
    }

    /**
     * @param bool $socialEtranger
     * @return InfoPrestataire
     */
    public function setSocialEtranger($socialEtranger = false)
    {
        $this->socialEtranger = $socialEtranger;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSocialEtranger()
    {
        return $this->socialEtranger;
    }

    /**
     * @param array|null $socialEtrangerDocument
     * @return InfoPrestataire
     */
    public function setSocialEtrangerDocument($socialEtrangerDocument = null)
    {
        $this->socialEtrangerDocument = $socialEtrangerDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSocialEtrangerDocument()
    {
        return $this->socialEtrangerDocument;
    }

    /**
     * @param DateTime|null $dateSocialEtranger
     * @return InfoPrestataire
     */
    public function setDateSocialEtranger($dateSocialEtranger = null)
    {
        $this->dateSocialEtranger = $dateSocialEtranger;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateSocialEtranger()
    {
        return $this->dateSocialEtranger;
    }

    /**
     * @param bool $isValidSocialEtranger
     * @return InfoPrestataire
     */
    public function setIsValidSocialEtranger($isValidSocialEtranger = true)
    {
        $this->isValidSocialEtranger = $isValidSocialEtranger;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsvalidSocialEtranger()
    {
        return $this->isValidSocialEtranger;
    }
}
