<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProduitCommandeFournisseur
 *
 * @ORM\Table(name="produit_commande_fournisseur")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ProduitCommandeFournisseurRepository")
 */
class ProduitCommandeFournisseur
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('produit_commande_fournisseur_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="montantUnit", type="float")
     */
    private $montantUnit;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @var float
     *
     * @ORM\Column(name="totalHT", type="float")
     */
    private $totalHT;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var CommandeFournisseur
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandeFournisseur", inversedBy="produitCommandeFournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commandeFournisseur;

    /**
     * @var ProduitFournisseur
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ProduitFournisseur", inversedBy="produitCommandeFournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produitFournisseur;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ProduitCommandeFournisseur
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ProduitCommandeFournisseur
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set montantUnit.
     *
     * @param float $montantUnit
     *
     * @return ProduitCommandeFournisseur
     */
    public function setMontantUnit($montantUnit)
    {
        $this->montantUnit = $montantUnit;

        return $this;
    }

    /**
     * Get montantUnit.
     *
     * @return float
     */
    public function getMontantUnit()
    {
        return $this->montantUnit;
    }

    /**
     * Set quantite.
     *
     * @param int $quantite
     *
     * @return ProduitCommandeFournisseur
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite.
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return ProduitCommandeFournisseur
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return ProduitCommandeFournisseur
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return CommandeFournisseur|null
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @param CommandeFournisseur|null $commandeFournisseur
     * @return ProduitCommandeFournisseur
     */
    public function setCommandeFournisseur($commandeFournisseur)
    {
        $this->commandeFournisseur = $commandeFournisseur;

        return $this;
    }

    /**
     * @return ProduitFournisseur|null
     */
    public function getProduitFournisseur()
    {
        return $this->produitFournisseur;
    }

    /**
     * @param ProduitFournisseur|null $produitFournisseur
     * @return ProduitCommandeFournisseur
     */
    public function setProduitFournisseur($produitFournisseur)
    {
        $this->produitFournisseur = $produitFournisseur;

        return $this;
    }

    /**
     * Clear id
     * @return ProduitCommandeFournisseur
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }
}
