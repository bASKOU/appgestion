<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleAttachementRepository")
 */
class ArticleAttachement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_attachement_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var ArticleCommande
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ArticleCommande", inversedBy="articleAttachements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $articleCommande;

    /**
     * @var Attachement
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Attachement", inversedBy="articleAttachements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $attachement;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $typeAvancement;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $avancement;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $montantHtAv;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArticleCommande|null
     */
    public function getArticleCommande()
    {
        return $this->articleCommande;
    }

    /**
     * @param ArticleCommande|null $articleCommande
     * @return ArticleAttachement
     */
    public function setArticleCommande($articleCommande)
    {
        $this->articleCommande = $articleCommande;

        return $this;
    }

    /**
     * @return Attachement|null
     */
    public function getAttachement()
    {
        return $this->attachement;
    }

    /**
     * @param Attachement|null $attachement
     * @return ArticleAttachement
     */
    public function setAttachement($attachement)
    {
        $this->attachement = $attachement;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTypeAvancement()
    {
        return $this->typeAvancement;
    }

    /**
     * @param int $typeAvancement
     * @return ArticleAttachement
     */
    public function setTypeAvancement($typeAvancement)
    {
        $this->typeAvancement = $typeAvancement;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @param float $avancement
     * @return ArticleAttachement
     */
    public function setAvancement($avancement)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantHtAv()
    {
        return $this->montantHtAv;
    }

    /**
     * @param float $montantHtAv
     * @return ArticleAttachement
     */
    public function setMontantHtAv($montantHtAv)
    {
        $this->montantHtAv = $montantHtAv;

        return $this;
    }
}