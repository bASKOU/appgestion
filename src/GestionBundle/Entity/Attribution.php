<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AttributionRepository")
 */
class Attribution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('attribution_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="attributions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="attributions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affaire;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $plafondOperation;

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Attribution
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return Attribution
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPlafondOperation()
    {
        return $this->plafondOperation;
    }

    /**
     * @param float|null $plafondOperation
     * @return Attribution
     */
    public function setPlafondOperation($plafondOperation)
    {
        $this->plafondOperation = $plafondOperation;

        return $this;
    }
}
