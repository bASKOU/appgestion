<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Theme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Theme", inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="text", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="unite", type="integer")
     */
    private $unite;

    /**
     * @var ArticleDevis[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleDevis", mappedBy="article", cascade="remove")
     */
    private $articleDevis;

    /**
     * @var ArticleAffaire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleAffaire", mappedBy="article", cascade="remove", cascade="remove")
     */
    private $articleAffaires;


    public function __construct()
    {
        $this->articleDevis = new ArrayCollection();
        $this->articleAffaires = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Theme|null
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param Theme|null $theme
     * @return Article
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * @param string|null $nom
     * @return Article
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * @param int|null $unite
     * @return Article
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * @param ArticleCommande $articleCommande
     * @return Article
     */
    public function removeArticleCommande($articleCommande)
    {
        if ($this->articleCommandes->contains($articleCommande)) {
            $this->articleCommandes->removeElement($articleCommande);
            // set the owning side to null (unless already changed)
            if ($articleCommande->getArticle() === $this) {
                $articleCommande->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleDevis[]|null
     */
    public function getArticleDevis()
    {
        return $this->articleDevis;
    }

    /**
     * @param ArticleDevis $articleDevi
     * @return Article
     */
    public function addArticleDevi($articleDevi)
    {
        if (!$this->articleDevis->contains($articleDevi)) {
            $this->articleDevis[] = $articleDevi;
            $articleDevi->setArticle($this);
        }

        return $this;
    }

    /**
     * @param ArticleDevis $articleDevi
     * @return Article
     */
    public function removeArticleDevi($articleDevi)
    {
        if ($this->articleDevis->contains($articleDevi)) {
            $this->articleDevis->removeElement($articleDevi);
            // set the owning side to null (unless already changed)
            if ($articleDevi->getArticle() === $this) {
                $articleDevi->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleAffaire[]|null
     */
    public function getArticleAffaires()
    {
        return $this->articleAffaires;
    }

    /**
     * @param ArticleAffaire $articleAffaires
     * @return Article
     */
    public function addArticleAffaires($articleAffaires)
    {
        if (!$this->articleAffaires->contains($articleAffaires)) {
            $this->articleAffaires[] = $articleAffaires;
            $articleAffaires->setArticle($this);
        }

        return $this;
    }

    /**
     * @param ArticleAffaire $articleAffaires
     * @return Article
     */
    public function removeAricleAffaires($articleAffaires)
    {
        if ($this->articleAffaires->contains($articleAffaires)) {
            $this->articleAffaires->removeElement($articleAffaires);
            // set the owning side to null (unless already changed)
            if ($articleAffaires->getArticle() === $this) {
                $articleAffaires->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * Clear id
     * @return Article
     */
    public function clearId()
    {
        $this->id = null; // également essayé avec "", 0, valeur de l'auto-incrément, true, false, -1

        return $this;
    }
}
