<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvancementAffaire
 *
 * @ORM\Table(name="avancement_affaire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AvancementAffaireRepository")
 */
class AvancementAffaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="date", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var float|null
     *
     * @ORM\Column(name="avancement", type="float", nullable=true)
     */
    private $avancement;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validation;

    /**
     * @var Affaire
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="avancement")
     */
    private $affaire;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return AvancementAffaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateSaisie.
     *
     * @param \DateTime $dateSaisie
     *
     * @return AvancementAffaire
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set avancement.
     *
     * @param float|null $avancement
     *
     * @return AvancementAffaire
     */
    public function setAvancement($avancement = null)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement.
     *
     * @return float|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return AvancementAffaire
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @param int $validation
     * @return AvancementAffaire
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return AvancementAffaire
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }
}
