<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstimationPrestation
 *
 * @ORM\Table(name="estimation_prestation")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\EstimationPrestationRepository")
 */
class EstimationPrestation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nexTVAl('estimation_prestation_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", unique=true)
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ht", type="float")
     */
    private $totalHT;

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean")
     */
    private $actif;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="estimationPrestation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affaire;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="estimationPrestation")
     */
    private $prestataire;

    /**
     * @var FacturePrestataire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\FacturePrestataire", inversedBy="estimation")
     * @ORM\JoinColumn(nullable=true)
     */
    private $facture;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $numero
     * @return EstimationPrestation
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return EstimationPrestation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return EstimationPrestation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return EstimationPrestation
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set actif.
     *
     * @param bool $actif
     *
     * @return EstimationPrestation
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif.
     *
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param Affaire $affaire
     * @return EstimationPrestation
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return Affaire
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Organisme $prestataire
     * @return EstimationPrestation
     */
    public function setPrestataire($prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * @return Organisme
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * @param FacturePrestataire|null $facture
     * @return EstimationPrestation
     */
    public function setFacture($facture = null)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * @return FacturePrestataire
     */
    public function getFacture()
    {
        return $this->facture;
    }
}
