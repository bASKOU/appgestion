<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('contact_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="contacts")
     */
    private $organisme;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $mobile;

    /**
     * @var TypeContact[]
     *
     * @ORM\ManyToMany(targetEntity="GestionBundle\Entity\TypeContact", inversedBy="contact")
     * @ORM\JoinTable(name="contact_typage")
     * @ORM\JoinColumn(name="Contact_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var Commande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="contact")
     */
    private $commandes;

    /**
     * @var CommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandeFournisseur", mappedBy="contact", cascade="remove")
     */
    private $commandeFournisseur;

    /**
     * @var CommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandePrestataire", mappedBy="contact", cascade="remove")
     */
    private $commandePrestataire;

    /**
     * @var FactureFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="contact", cascade="remove")
     */
    private $factureFournisseur;

    /**
     * @var FacturePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="contact", cascade="remove")
     */
    private $facturePrestataire;

    /**
     * @var Devis[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="contact", cascade="remove")
     */
    private $devis;

    /**
     * @var Facture[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="contact", cascade="remove")
     */
    private $factures;

    /**
     * @var Attachement[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="contact", cascade="remove")
     */
    private $attachements;

    /**
     * @return string
     */
    public function __toString()
    {
        return ucfirst($this->prenom) . ' ' . strtoupper($this->nom);
    }

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        $this->commandes = new ArrayCollection();
        $this->commandeFournisseur = new ArrayCollection();
        $this->commandePrestataire = new ArrayCollection();
        $this->factureFournisseur = new ArrayCollection();
        $this->facturePrestataire = new ArrayCollection();
        $this->devis = new ArrayCollection();
        $this->factures = new ArrayCollection();
        $this->attachements = new ArrayCollection();
        $this->type = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Organisme|null
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme|null $organisme
     * @return Contact
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Contact
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string|null $prenom
     * @return Contact
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     * @return Contact
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string|null $mobile
     * @return Contact
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes()
    {
        return $this->commandes;
    }

    /**
     * @param Commande $commande
     * @return Contact
     */
    public function addCommande($commande)
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setContact($this);
        }

        return $this;
    }

    /**
     * @param Commande $commande
     * @return Contact
     */
    public function removeCommande($commande)
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getContact() === $this) {
                $commande->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandeFournisseur[]
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Contact
     */
    public function addCommandeFournisseur($commandeFournisseur)
    {
        if (!$this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur[] = $commandeFournisseur;
            $commandeFournisseur->setContact($this);
        }

        return $this;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Contact
     */
    public function removeCommandeFournisseur($commandeFournisseur)
    {
        if ($this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur->removeElement($commandeFournisseur);
            // set the owning side to null (unless already changed)
            if ($commandeFournisseur->getContact() === $this) {
                $commandeFournisseur->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandePrestataire[]
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Contact
     */
    public function addCommandePrestataire($commandePrestataire)
    {
        if (!$this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire[] = $commandePrestataire;
            $commandePrestataire->setContact($this);
        }

        return $this;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Contact
     */
    public function removeCommandePrestataire($commandePrestataire)
    {
        if ($this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire->removeElement($commandePrestataire);
            // set the owning side to null (unless already changed)
            if ($commandePrestataire->getContact() === $this) {
                $commandePrestataire->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FactureFournisseur[]
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Contact
     */
    public function addFactureFournisseur($factureFournisseur)
    {
        if (!$this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur[] = $factureFournisseur;
            $factureFournisseur->setContact($this);
        }

        return $this;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Contact
     */
    public function removeFactureFournisseur($factureFournisseur)
    {
        if ($this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur->removeElement($factureFournisseur);
            // set the owning side to null (unless already changed)
            if ($factureFournisseur->getContact() === $this) {
                $factureFournisseur->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturePrestataire[]
     */
    public function getFacturePrestataire()
    {
        return $this->facturePrestataire;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Contact
     */
    public function addFacturePrestataire($facturePrestataire)
    {
        if (!$this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire[] = $facturePrestataire;
            $facturePrestataire->setContact($this);
        }

        return $this;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Contact
     */
    public function removeFacturePrestataire($facturePrestataire)
    {
        if ($this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire->removeElement($facturePrestataire);
            // set the owning side to null (unless already changed)
            if ($facturePrestataire->getContact() === $this) {
                $facturePrestataire->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * @param Devis $devi
     * @return Contact
     */
    public function addDevis($devi)
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setContact($this);
        }

        return $this;
    }

    /**
     * @param Devis $devi
     * @return Contact
     */
    public function removeDevis($devi)
    {
        if ($this->devis->contains($devi)) {
            $this->devis->removeElement($devi);
            // set the owning side to null (unless already changed)
            if ($devi->getContact() === $this) {
                $devi->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getFactures()
    {
        return $this->factures;
    }

    /**
     * @param \GestionBundle\Entity\Facture $facture
     * @return Contact
     */
    public function addFacture($facture)
    {
        if (!$this->factures->contains($facture)) {
            $this->factures[] = $facture;
            $facture->setContact($this);
        }

        return $this;
    }

    /**
     * @param \GestionBundle\Entity\Facture $facture
     * @return Contact
     */
    public function removeFacture($facture)
    {
        if ($this->factures->contains($facture)) {
            $this->factures->removeElement($facture);
            // set the owning side to null (unless already changed)
            if ($facture->getContact() === $this) {
                $facture->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Attachement[]
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * @param \GestionBundle\Entity\Attachement $attachement
     * @return Contact
     */
    public function addAttachement($attachement)
    {
        if (!$this->attachements->contains($attachement)) {
            $this->attachements[] = $attachement;
            $attachement->setContact($this);
        }

        return $this;
    }

    /**
     * @param \GestionBundle\Entity\Attachement $attachement
     * @return Contact
     */
    public function removeAttachement($attachement)
    {
        if ($this->attachements->contains($attachement)) {
            $this->attachements->removeElement($attachement);
            // set the owning side to null (unless already changed)
            if ($attachement->getContact() === $this) {
                $attachement->setContact(null);
            }
        }

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Contact
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return TypeContact[]
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param TypeContact $type
     */
    public function setType($type)
    {
        $this->type[] = $type;
    }

    /**
     * @param TypeContact $type
     * @return Contact
     */
    public function removeType($type)
    {
        if ($this->type->contains($type)) {
            $this->type->removeElement($type);
            // set the owning side to null (unless already changed)
            if ($type) {
                if ($type->getContact() === $this) {
                    $type->setContact(null);
                }
            }
        }

        return $this;
//        $this->type = null;
//        $type->removeContact($this);
    }
}
