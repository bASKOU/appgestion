<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * FacturePrestataire
 *
 * @ORM\Table(name="facture_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\FacturePrestataireRepository")
 */
class FacturePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nextval('commande_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_facture", type="string", length=255, nullable=true)
     */
    private $numeroFacture;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_facture_prestataire", type="string", length=255, nullable=true)
     */
    private $numeroFacturePrestataire;

    /**
     * @var string
     *
     * @ORM\Column(name="mentions", type="text")
     */
    private $mentions;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_facture", type="date", nullable=true)
     */
    private $dateFacture;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_echeance", type="date", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_saisie", type="date", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_validation", type="date", nullable=true)
     */
    private $dateValidation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_reglement", type="date", nullable=true)
     */
    private $dateReglement;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_debit", type="date", nullable=true)
     */
    private $dateDebit;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ht", type="float")
     */
    private $totalHT;

    /**
     * @var float|null
     *
     * @ORM\Column(name="TVA", type="float", nullable=true)
     */
    private $TVA;

    /**
     * @var float
     *
     * @ORM\Column(name="totalTTC", type="float")
     */
    private $totalTTC;

    /**
     * @var int
     *
     * @ORM\Column(name="validation", type="integer")
     */
    private $validation;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="adresseFacturation", type="text")
     */
    private $adresseFacturation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="documentAttache", type="string", length=255, nullable=true)
     */
    private $documentAttache;

    /**
     * @var string
     *
     * @ORM\Column(name="detailFacture", type="text")
     */
    private $detailFacture;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="facturePrestataire")
     */
    private $prestataire;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="facturePrestataire")
     */
    private $contact;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="facturePrestataire")
     */
    private $user;

    /**
     * @var CommandePrestataire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandePrestataire", inversedBy="facturePrestataire")
     */
    private $commandePrestataire;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="facturePrestataire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $affaire;

    /**
     * @var ModePaiement
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\ModePaiement", inversedBy="facturePrestataire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $modePaiement;

    /**
     * @var EstimationPrestation[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationPrestation", mappedBy="facture")
     */
    private $estimation;

    public function __construct()
    {
        $this->estimation = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->numeroFacturePrestataire . ' - ' . $this->detailFacture . ' - ' . number_format($this->totalHT, 2, ',', ' ') . ' €';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroFacture.
     *
     * @param string|null $numeroFacture
     *
     * @return FacturePrestataire
     */
    public function setNumeroFacture($numeroFacture = null)
    {
        $this->numeroFacture = $numeroFacture;

        return $this;
    }

    /**
     * Get numeroFacture.
     *
     * @return string
     */
    public function getNumeroFacture()
    {
        return $this->numeroFacture;
    }

    /**
     * Set numeroFacturePrestataire.
     *
     * @param string|null $numeroFacturePrestataire
     *
     * @return FacturePrestataire
     */
    public function setNumeroFacturePrestataire($numeroFacturePrestataire = null)
    {
        $this->numeroFacturePrestataire = $numeroFacturePrestataire;

        return $this;
    }

    /**
     * Get numeroFacturePrestataire.
     *
     * @return string
     */
    public function getNumeroFacturePrestataire()
    {
        return $this->numeroFacturePrestataire;
    }

    /**
     * Set mentions.
     *
     * @param string $mentions
     *
     * @return FacturePrestataire
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * Get mentions.
     *
     * @return string
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * Set dateFacture.
     *
     * @param \DateTime|null $dateFacture
     *
     * @return FacturePrestataire
     */
    public function setDateFacture($dateFacture = null)
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * Get dateFacture.
     *
     * @return \DateTime|null
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * Set dateEcheance.
     *
     * @param \DateTime|null $dateEcheance
     *
     * @return FacturePrestataire
     */
    public function setDateEcheance($dateEcheance = null)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * Get dateEcheance.
     *
     * @return \DateTime|null
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * Set dateSaisie.
     *
     * @param \DateTime|null $dateSaisie
     *
     * @return FacturePrestataire
     */
    public function setDateSaisie($dateSaisie = null)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get dateSaisie.
     *
     * @return \DateTime|null
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set dateValidation.
     *
     * @param \DateTime|null $dateValidation
     *
     * @return FacturePrestataire
     */
    public function setDateValidation($dateValidation = null)
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation.
     *
     * @return \DateTime|null
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * Set dateReglement.
     *
     * @param \DateTime|null $dateReglement
     *
     * @return FacturePrestataire
     */
    public function setDateReglement($dateReglement = null)
    {
        $this->dateReglement = $dateReglement;

        return $this;
    }

    /**
     * Get dateReglement.
     *
     * @return \DateTime|null
     */
    public function getDateReglement()
    {
        return $this->dateReglement;
    }

    /**
     * Set dateDebit.
     *
     * @param \DateTime|null $dateDebit
     *
     * @return FacturePrestataire
     */
    public function setDateDebit($dateDebit = null)
    {
        $this->dateDebit = $dateDebit;

        return $this;
    }

    /**
     * Get dateDebit.
     *
     * @return \DateTime|null
     */
    public function getDateDebit()
    {
        return $this->dateDebit;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return FacturePrestataire
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set TVA.
     *
     * @param float|null $TVA
     *
     * @return FacturePrestataire
     */
    public function setTVA($TVA = null)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * Get TVA.
     *
     * @return float|null
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * Set totalTTC.
     *
     * @param float $totalTTC
     *
     * @return FacturePrestataire
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * Get totalTTC.
     *
     * @return float
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

    /**
     * Set validation.
     *
     * @param int $validation
     *
     * @return FacturePrestataire
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation.
     *
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * Set etat.
     *
     * @param int $etat
     *
     * @return FacturePrestataire
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat.
     *
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set adresseFacturation.
     *
     * @param string $adresseFacturation
     *
     * @return FacturePrestataire
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * Get adresseFacturation.
     *
     * @return string
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * Set documentAttache.
     *
     * @param string|null $documentAttache
     *
     * @return FacturePrestataire
     */
    public function setDocumentAttache($documentAttache = null)
    {
        $this->documentAttache = $documentAttache;

        return $this;
    }

    /**
     * Get documentAttache.
     *
     * @return string|null
     */
    public function getDocumentAttache()
    {
        return $this->documentAttache;
    }

    /**
     * Set detailFacture.
     *
     * @param string $detailFacture
     *
     * @return FacturePrestataire
     */
    public function setDetailFacture($detailFacture)
    {
        $this->detailFacture = $detailFacture;

        return $this;
    }

    /**
     * Get detailFacture.
     *
     * @return string
     */
    public function getDetailFacture()
    {
        return $this->detailFacture;
    }

    /**
     * Set Prestataire
     * @param Organisme $prestataire
     *
     * @return FacturePrestataire
     */
    public function setPrestataire($prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * Get Prestataire
     * @return Organisme|null
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * Set contact
     * @param Contact $contact
     *
     * @return FacturePrestataire
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set user
     * @param User $user
     *
     * @return FacturePrestataire
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get contact
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set commandePrestataire
     * @param CommandePrestataire $commandePrestataire
     *
     * @return FacturePrestataire
     */
    public function setCommandePrestataire($commandePrestataire)
    {
        $this->commandePrestataire = $commandePrestataire;

        return $this;
    }

    /**
     * Get commandePrestataire
     * @return CommandePrestataire|null
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return FacturePrestataire
     */
    public function setAffaire($affaire = null)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return ModePaiement|null
     */
    public function getModePaiement()
    {
        return $this->modePaiement;
    }

    /**
     * @param ModePaiement|null $modePaiement
     * @return FacturePrestataire
     */
    public function setModePaiement($modePaiement = null)
    {
        $this->modePaiement = $modePaiement;

        return $this;
    }

    /**
     * @param EstimationFournisseur[] $estimation
     * @return FacturePrestataire
     */
    public function setEstimation($estimation)
    {
        $this->estimation = $estimation;

        return $this;
    }

    /**
     * @return EstimationFournisseur[]|\Symfony\Component\Ldap\Adapter\ExtLdap\Collection|null
     */
    public function getEstimation()
    {
        return $this->estimation;
    }
}
