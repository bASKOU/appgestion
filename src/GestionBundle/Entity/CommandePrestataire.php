<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * CommandePrestataire
 *
 * @ORM\Table(name="commande_prestataire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\CommandePrestataireRepository")
 */
class CommandePrestataire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"default"="nexTVAl('commande_prestataire_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="commandePrestataire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $affaire;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="commandePrestataire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $prestataire;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="commandePrestataire")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="commandePrestataire")
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_commande_presta", type="string", length=255, unique=true)
     */
    private $numeroCommandePresta;

    /**
     * @var string
     *
     * @ORM\Column(name="mentions", type="text")
     */
    private $mentions;

    /**
     * @var string|null
     *
     * @ORM\Column(name="detail_commande_presta", type="text", nullable=true)
     */
    private $detailCommandePresta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCommandePresta", type="date")
     */
    private $dateCommandePresta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_envoi", type="date", nullable=true)
     */
    private $dateEnvoi;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ht", type="float")
     */
    private $totalHT;

    /**
     * @var float
     *
     * @ORM\Column(name="TVA", type="float")
     */
    private $TVA;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_total_ttc", type="float")
     */
    private $montantTotalTTC;

    /**
     * @var int
     *
     * @ORM\Column(name="validation", type="integer")
     */
    private $validation;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(name="respect_plafond", type="boolean")
     */
    private $respectPlafond;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_facturation", type="text")
     */
    private $adresseFacturation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_abandon", type="text", nullable=true)
     */
    private $raisonAbandon;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_validation", type="date", nullable=true)
     */
    private $dateValidation;

    /**
     * @var AvancementCommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\AvancementCommandePrestataire", mappedBy="commandePrestataire")
     * @ORM\JoinColumn(nullable=true)
     */
    private $avancement;

    /**
     * @var ServiceCommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ServiceCommandePrestataire", mappedBy="commandePrestataire", cascade={"remove", "persist"})
     */
    private $serviceCommandePrestataire;

    /**
     * @var FacturePrestataire
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="commandePrestataire")
     */
    private $facturePrestataire;

    /**
     * CommandePrestataire constructor.
     */
    public function __construct()
    {
        $this->serviceCommandePrestataire = new ArrayCollection();
        $this->facturePrestataire = new ArrayCollection();
        $this->avancement = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroCommandePresta.
     *
     * @param string $numeroCommandePresta
     *
     * @return CommandePrestataire
     */
    public function setNumeroCommandePresta($numeroCommandePresta)
    {
        $this->numeroCommandePresta = $numeroCommandePresta;

        return $this;
    }

    /**
     * Get numeroCommandePresta.
     *
     * @return string
     */
    public function getNumeroCommandePresta()
    {
        return $this->numeroCommandePresta;
    }

    /**
     * Set mentions.
     *
     * @param string $mentions
     *
     * @return CommandePrestataire
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * Get mentions.
     *
     * @return string
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * Set detailCommandePresta.
     *
     * @param string|null $detailCommandePresta
     *
     * @return CommandePrestataire
     */
    public function setDetailCommandePresta($detailCommandePresta = null)
    {
        $this->detailCommandePresta = $detailCommandePresta;

        return $this;
    }

    /**
     * Get detailCommandePresta.
     *
     * @return string|null
     */
    public function getDetailCommandePresta()
    {
        return $this->detailCommandePresta;
    }

    /**
     * Set dateCommandePresta.
     *
     * @param \DateTime $dateCommandePresta
     *
     * @return CommandePrestataire
     */
    public function setDateCommandePresta($dateCommandePresta)
    {
        $this->dateCommandePresta = $dateCommandePresta;

        return $this;
    }

    /**
     * Get dateCommandePresta.
     *
     * @return \DateTime
     */
    public function getDateCommandePresta()
    {
        return $this->dateCommandePresta;
    }

    /**
     * Set dateEnvoi.
     *
     * @param \DateTime|null $dateEnvoi
     *
     * @return CommandePrestataire
     */
    public function setDateEnvoi($dateEnvoi = null)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi.
     *
     * @return \DateTime|null
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }

    /**
     * Set totalHT.
     *
     * @param float $totalHT
     *
     * @return CommandePrestataire
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT.
     *
     * @return float
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set TVA.
     *
     * @param float $TVA
     *
     * @return CommandePrestataire
     */
    public function setTVA($TVA)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * Get TVA.
     *
     * @return float
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * Set montantTotalTTC.
     *
     * @param float $montantTotalTTC
     *
     * @return CommandePrestataire
     */
    public function setMontantTotalTTC($montantTotalTTC)
    {
        $this->montantTotalTTC = $montantTotalTTC;

        return $this;
    }

    /**
     * Get montantTotalTTC.
     *
     * @return float
     */
    public function getMontantTotalTTC()
    {
        return $this->montantTotalTTC;
    }

    /**
     * Set validation.
     *
     * @param int $validation
     *
     * @return CommandePrestataire
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * Get validation.
     *
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * Set etat.
     *
     * @param int $etat
     *
     * @return CommandePrestataire
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat.
     *
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set respectPlafond.
     *
     * @param bool $respectPlafond
     *
     * @return CommandePrestataire
     */
    public function setRespectPlafond($respectPlafond)
    {
        $this->respectPlafond = $respectPlafond;

        return $this;
    }

    /**
     * Get respectPlafond.
     *
     * @return bool
     */
    public function getRespectPlafond()
    {
        return $this->respectPlafond;
    }

    /**
     * Set adresseFacturation.
     *
     * @param string $adresseFacturation
     *
     * @return CommandePrestataire
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * Get adresseFacturation.
     *
     * @return string
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * Set raisonAbandon.
     *
     * @param string|null $raisonAbandon
     *
     * @return CommandePrestataire
     */
    public function setRaisonAbandon($raisonAbandon = null)
    {
        $this->raisonAbandon = $raisonAbandon;

        return $this;
    }

    /**
     * Get raisonAbandon.
     *
     * @return string|null
     */
    public function getRaisonAbandon()
    {
        return $this->raisonAbandon;
    }

    /**
     * Set dateValidation.
     *
     * @param \DateTime|null $dateValidation
     *
     * @return CommandePrestataire
     */
    public function setDateValidation($dateValidation = null)
    {
        $this->dateValidation = $dateValidation;

        return $this;
    }

    /**
     * Get dateValidation.
     *
     * @return \DateTime|null
     */
    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    /**
     * @return Collection|AvancementCommandePrestataire[]|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @param AvancementCommandePrestataire $avancement
     * @return CommandePrestataire
     */
    public function addAvancement($avancement)
    {
        if (!$this->avancement->contains($avancement)) {
            $this->avancement[] = $avancement;
            $avancement->setCommandePrestataire($this);
        }

        return $this;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return CommandePrestataire
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getPrestataire()
    {
        return $this->prestataire;
    }

    /**
     * @param Organisme|null $prestataire
     * @return CommandePrestataire
     */
    public function setPrestataire($prestataire)
    {
        $this->prestataire = $prestataire;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return CommandePrestataire
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return CommandePrestataire
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return Collection|ServiceCommandePrestataire[]
     */
    public function getServiceCommandePrestataire()
    {
        return $this->serviceCommandePrestataire;
    }

    /**
     * @param ServiceCommandePrestataire $serviceCommandePrestataire
     * @return CommandePrestataire
     */
    public function addServiceCommandePrestataire($serviceCommandePrestataire)
    {
        if (!$this->serviceCommandePrestataire->contains($serviceCommandePrestataire)) {
            $this->serviceCommandePrestataire[] = $serviceCommandePrestataire;
            $serviceCommandePrestataire->setCommandePrestataire($this);
        }

        return $this;
    }

    /**
     * Clear id
     * @return CommandePrestataire
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }

    /**
     * @return Collection|FacturePrestataire[]
     */
    public function getFacturePrestataire()
    {
        return $this->facturePrestataire;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return CommandePrestataire
     */
    public function addFacturePrestataire($facturePrestataire)
    {
        if (!$this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire[] = $facturePrestataire;
            $facturePrestataire->setCommandePrestataire($this);
        }

        return $this;
    }
}
