<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AvancementCommandeFournisseur
 *
 * @ORM\Table(name="avancement_commande_fournisseur")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AvancementCommandeFournisseurRepository")
 */
class AvancementCommandeFournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(type="bigint", options={"default"="nextval('avancement_commande_fournisseur_id_seq'::regclass)"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="date", nullable=true)
     */
    private $dateSaisie;

    /**
     * @var float|null
     *
     * @ORM\Column(name="avancement", type="float", nullable=true)
     */
    private $avancement;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validation;

    /**
     * @var CommandeFournisseur
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\CommandeFournisseur", inversedBy="avancement")
     */
    private $commandeFournisseur;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return AvancementCommandeFournisseur
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateSaisie.
     *
     * @param \DateTime $dateSaisie
     *
     * @return AvancementCommandeFournisseur
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set avancement.
     *
     * @param float|null $avancement
     *
     * @return AvancementCommandeFournisseur
     */
    public function setAvancement($avancement = null)
    {
        $this->avancement = $avancement;

        return $this;
    }

    /**
     * Get avancement.
     *
     * @return float|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return AvancementCommandeFournisseur
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @param int $validation
     * @return AvancementCommandeFournisseur
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @return CommandeFournisseur
     */
    public function getCommandFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return AvancementCommandeFournisseur
     */
    public function setCommandeFournisseur($commandeFournisseur)
    {
        $this->commandeFournisseur = $commandeFournisseur;

        return $this;
    }
}
