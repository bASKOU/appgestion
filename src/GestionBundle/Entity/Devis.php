<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\DevisRepository")
 */
class Devis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('devis_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="devis")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $affaire;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="clientDevis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="devis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="devis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $numeroDevis;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numeroDevisBis;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateDevis;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEdit;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalHT;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $conditionsParticulieres;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $remise;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $TVA;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalTTC;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $validation;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $activeTVA;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $forfaitaire;

    /**
     * @var ArticleDevis[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleDevis", mappedBy="devis", cascade="remove")
     */
    private $articleDevis;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseFacturationDevis")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseFacturation;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseLivraisonDevis")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;

    /**
     * Devis constructor.
     */
    public function __construct()
    {
        $this->articleDevis = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return Devis
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Organisme|null $client
     * @return Devis
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getConditionsParticulieres()
    {
        return $this->conditionsParticulieres;
    }

    /**
     * @param null|string $conditionsParticulieres
     * @return Devis
     */
    public function setConditionsParticulieres($conditionsParticulieres)
    {
        $this->conditionsParticulieres = $conditionsParticulieres;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Devis
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return Devis
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroDevis()
    {
        return $this->numeroDevis;
    }

    /**
     * @param string $numeroDevis
     * @return Devis
     */
    public function setNumeroDevis($numeroDevis)
    {
        $this->numeroDevis = $numeroDevis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroDevisBis()
    {
        return $this->numeroDevisBis;
    }

    /**
     * @param string $numeroDevisBis
     * @return Devis
     */
    public function setNumeroDevisBis($numeroDevisBis)
    {
        $this->numeroDevisBis = $numeroDevisBis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     * @return Devis
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateDevis()
    {
        return $this->dateDevis;
    }

    /**
     * @param \DateTimeInterface $dateDevis
     * @return Devis
     */
    public function setDateDevis($dateDevis)
    {
        $this->dateDevis = $dateDevis;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateEdit()
    {
        return $this->dateEdit;
    }

    /**
     * @param \DateTimeInterface $dateEdit
     * @return Devis
     */
    public function setDateEdit($dateEdit)
    {
        $this->dateDevis = $dateEdit;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return Devis
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @param float|null $remise
     * @return Devis
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * @param float|null $TVA
     * @return Devis
     */
    public function setTVA($TVA)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

    /**
     * @param float $totalTTC
     * @return Devis
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getValidation()
    {
        return $this->validation;
    }

    /**
     * @param int $validation
     * @return Devis
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int|null $etat
     * @return Devis
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActiveTVA()
    {
        return $this->activeTVA;
    }

    /**
     * @param bool|null $activeTVA
     * @return Devis
     */
    public function setActiveTVA($activeTVA)
    {
        $this->activeTVA = $activeTVA;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getForfaitaire()
    {
        return $this->forfaitaire;
    }

    /**
     * @param bool|null $forfaitaire
     * @return Devis
     */
    public function setForfaitaire($forfaitaire)
    {
        $this->forfaitaire = $forfaitaire;

        return $this;
    }

    /**
     * @param Organisme|null $adresseFacturation
     *
     * @return Devis
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param Organisme|null $adresseLivraison
     *
     * @return Devis
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @return Collection|ArticleDevis[]
     */
    public function getArticleDevis()
    {
        return $this->articleDevis;
    }

    /**
     * @param ArticleDevis $articleDevi
     * @return Devis
     */
    public function addArticleDevis($articleDevi)
    {
        if (!$this->articleDevis->contains($articleDevi)) {
            $this->articleDevis[] = $articleDevi;
            $articleDevi->setDevis($this);
        }

        return $this;
    }

    /**
     * @param ArticleDevis $articleDevis
     * @return Devis
     */
    public function removeArticleDevis($articleDevis)
    {
        if ($this->articleDevis->contains($articleDevis)) {
            $this->articleDevis->removeElement($articleDevis);
            // set the owning side to null (unless already changed)
            if ($articleDevis->getDevis() === $this) {
                $articleDevis->setDevis(null);
            }
        }

        return $this;
    }

    /**
     * Clear id
     * @return Devis
     */
    public function clearId()
    {
        $this->id = null;

        return $this;
    }
}
