<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('commande_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $affaire;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="clientCommande")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Contact", inversedBy="commandes")
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $mentions;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numeroCommande;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $numeroCommandeClient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $dateCommande;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalHT;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $remise;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $TVA;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantTotalTTC;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $activeTVA;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $forfaitaire;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, length=1)
     */
    private $typeDocument;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseFacturationCommande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseFacturation;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="adresseLivraisonCommande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresseLivraison;


    /**
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $documents = array();

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var ArticleCommande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleCommande", mappedBy="commande", cascade="remove")
     */
    private $articleCommandes;

    /**
     * @var AvancementCommande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\AvancementCommande", mappedBy="commande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $avancement;

    /**
     * @var Attachement[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="commande", cascade="remove")
     */
    private $attachements;

    /**
     * @var Devis
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Devis")
     * @ORM\JoinColumn(nullable=true)
     */
    private $devis;

    /**
     * @var Facture[]
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Facture", inversedBy="commande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $facture;

    /**
     * Commande constructor.
     */
    public function __construct()
    {
        $this->articleCommandes = new ArrayCollection();
        $this->attachements = new ArrayCollection();
        $this->avancement = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return Commande
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Organisme|null $client
     * @return Commande
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Devis
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return Commande
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMentions()
    {
        return $this->mentions;
    }

    /**
     * @param string $mentions
     * @return Commande
     */
    public function setMentions($mentions)
    {
        $this->mentions = $mentions;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }

    /**
     * @param int $typeDocument
     * @return Commande
     */
    public function setTypeDocument($typeDocument)
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param array $documents
     * @return Commande
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroCommande()
    {
        return $this->numeroCommande;
    }

    /**
     * @param string|null $numeroCommande
     * @return Commande
     */
    public function setNumeroCommande($numeroCommande)
    {
        $this->numeroCommande = $numeroCommande;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroCommandeClient()
    {
        return $this->numeroCommandeClient;
    }

    /**
     * @param string|null $numeroCommandeClient
     * @return Commande
     */
    public function setNumeroCommandeClient($numeroCommandeClient)
    {
        $this->numeroCommandeClient = $numeroCommandeClient;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateCommande()
    {
        return $this->dateCommande;
    }

    /**
     * @param \DateTimeInterface $dateCommande
     * @return Commande
     */
    public function setDateCommande($dateCommande)
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * @param float $totalHT
     * @return Commande
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * @param float|null $remise
     * @return Commande
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTVA()
    {
        return $this->TVA;
    }

    /**
     * @param float|null $TVA
     * @return Commande
     */
    public function setTVA($TVA)
    {
        $this->TVA = $TVA;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantTotalTTC()
    {
        return $this->montantTotalTTC;
    }

    /**
     * @param float $montantTotalTTC
     * @return Commande
     */
    public function setMontantTotalTTC($montantTotalTTC)
    {
        $this->montantTotalTTC = $montantTotalTTC;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param int $etat
     * @return Commande
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActiveTVA()
    {
        return $this->activeTVA;
    }

    /**
     * @param bool $activeTVA
     * @return Commande
     */
    public function setActiveTVA($activeTVA)
    {
        $this->activeTVA = $activeTVA;

        return $this;
    }

    /**
     * getDateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     * @return Commande
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * @param \DateTime $dateEcheance
     * @return Commande
     */
    public function setDateEcheance($dateEcheance)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * @return Collection|ArticleCommande[]
     */
    public function getArticleCommandes()
    {
        return $this->articleCommandes;
    }

    /**
     * @param Organisme|null $adresseFacturation
     *
     * @return Commande
     */
    public function setAdresseFacturation($adresseFacturation)
    {
        $this->adresseFacturation = $adresseFacturation;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseFacturation()
    {
        return $this->adresseFacturation;
    }

    /**
     * @param Organisme|null $adresseLivraison
     *
     * @return Commande
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * @return bool|null
     */
    public function getForfaitaire()
    {
        return $this->forfaitaire;
    }

    /**
     * @param bool|null $forfaitaire
     * @return Commande
     */
    public function setForfaitaire($forfaitaire)
    {
        $this->forfaitaire = $forfaitaire;

        return $this;
    }

    /**
     * @param ArticleCommande $articleCommande
     * @return Commande
     */
    public function addArticleCommande($articleCommande)
    {
        if (!$this->articleCommandes->contains($articleCommande)) {
            $this->articleCommandes[] = $articleCommande;
            $articleCommande->setCommande($this);
        }

        return $this;
    }

    /**
     * @param ArticleCommande $articleCommande
     * @return Commande
     */
    public function removeArticleCommande($articleCommande)
    {
        if ($this->articleCommandes->contains($articleCommande)) {
            $this->articleCommandes->removeElement($articleCommande);
            // set the owning side to null (unless already changed)
            if ($articleCommande->getCommande() === $this) {
                $articleCommande->setCommande(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Attachement[]|null
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * @param Attachement $attachement
     * @return Commande
     */
    public function addAttachement($attachement)
    {
        if (!$this->attachements->contains($attachement)) {
            $this->attachements[] = $attachement;
            $attachement->setCommande($this);
        }

        return $this;
    }

    /**
     * @param Attachement $attachement
     * @return Commande
     */
    public function removeAttachement($attachement)
    {
        if ($this->attachements->contains($attachement)) {
            $this->attachements->removeElement($attachement);
            // set the owning side to null (unless already changed)
            if ($attachement->getCommande() === $this) {
                $attachement->setCommande(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AvancementCommande[]|null
     */
    public function getAvancement()
    {
        return $this->avancement;
    }

    /**
     * @param AvancementCommande $avancement
     * @return Commande
     */
    public function addAvancement($avancement)
    {
        if (!$this->avancement->contains($avancement)) {
            $this->avancement[] = $avancement;
            $avancement->setCommande($this);
        }

        return $this;
    }

    /**
     * @return Devis
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * @param Devis $devis
     * @return Commande
     */
    public function setDevis($devis)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * @return Facture|null
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @param Facture $facture
     * @return Commande
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;

        return $this;
    }
}
