<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\AvoirRepository")
 */
class Avoir
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('avoir_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Facture
     *
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\Facture", inversedBy="avoir", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $facture;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $motif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroAvoir;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \GestionBundle\Entity\Facture|null
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * @param \GestionBundle\Entity\Facture $facture
     * @return Avoir
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param float $montant
     * @return Avoir
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * @param string $motif
     * @return Avoir
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return Avoir
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumeroAvoir()
    {
        return $this->numeroAvoir;
    }

    /**
     * @param string|null $numeroAvoir
     * @return Avoir
     */
    public function setNumeroAvoir($numeroAvoir)
    {
        $this->numeroAvoir = $numeroAvoir;

        return $this;
    }
}