<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use UserBundle\Entity\User;

/**
 * Organisme
 *
 * @ORM\Table(name="organisme")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\OrganismeRepository")
 */
class Organisme
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('organisme_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $type;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="organismes")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresseComplementaire1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresseComplementaire2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stal;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $actif;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $fournisseurValide;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $prestataireValide;

    /**
     * @var Contact[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Contact", mappedBy="organisme", cascade="remove")
     */
    private $contacts;

    /**
     * @var Facture[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="client")
     */
    private $clientFacture;

    /**
     * @var CommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandeFournisseur", mappedBy="fournisseur")
     */
    private $commandeFournisseur;

    /**
     * @var CommandePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\CommandePrestataire", mappedBy="prestataire")
     */
    private $commandePrestataire;

    /**
     * @var FactureFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FactureFournisseur", mappedBy="fournisseur")
     */
    private $factureFournisseur;

    /**
     * @var FacturePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\FacturePrestataire", mappedBy="prestataire")
     */
    private $facturePrestataire;

    /**
     * @var Devis[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="client")
     */
    private $clientDevis;

    /**
     * @var Commande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="client")
     */
    private $clientCommande;

    /**
     * @var Attachement[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="client")
     */
    private $clientAttachement;

    /**
     * @var ProduitFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ProduitFournisseur", mappedBy="organisme", cascade="remove")
     */
    private $produitFournisseur;

    /**
     * @var ServicePrestataire[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ServicePrestataire", mappedBy="prestataire", cascade="remove")
     */
    private $servicePrestataire;

    /**
     * @var Organisme
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="enfants")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * @var Organisme[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Organisme", mappedBy="parent", cascade="remove")
     * @ORM\JoinColumn(nullable=true)
     */
    private $enfants;

    /**
     * @var Affaire[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Affaire", mappedBy="organisme")
     */
    private $affaire;

    /**
     * @var Affaire[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Affaire", mappedBy="adresseFacturation")
     */
    private $adresseFacturationAffaire;

    /**
     * @var Affaire[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Affaire", mappedBy="adresseLivraison")
     */
    private $adresseLivraisonAffaire;

    /**
     * @var Devis[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="adresseFacturation")
     */
    private $adresseFacturationDevis;

    /**
     * @var Devis[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Devis", mappedBy="adresseLivraison")
     */
    private $adresseLivraisonDevis;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="adresseFacturation")
     */
    private $adresseFacturationCommande;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Commande", mappedBy="adresseLivraison")
     */
    private $adresseLivraisonCommande;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="adresseFacturation")
     */
    private $adresseFacturationAttachement;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Attachement", mappedBy="adresseLivraison")
     */
    private $adresseLivraisonAttachement;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="adresseFacturation")
     */
    private $adresseFacturationFacture;

    /**
     * @var Commande[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\Facture", mappedBy="adresseLivraison")
     */
    private $adresseLivraisonFacture;

    /**
     * @var InfoFournisseur|null
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\InfoFournisseur", mappedBy="fournisseur", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $infoFournisseur;

    /**
     * @var InfoPrestataire|null
     * @ORM\OneToOne(targetEntity="GestionBundle\Entity\InfoPrestataire", cascade={"remove"}, mappedBy="prestataire")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $infoPrestataire;

    /**
     * @var EstimationPrestation[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationPrestation", mappedBy="prestataire")
     */
    private $estimationPrestation;

    /**
     * @var EstimationFournisseur[]
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\EstimationFournisseur", mappedBy="fournisseur")
     */
    private $estimationFournisseur;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->nom;
    }

    /**
     * Organisme constructor.
     */
    public function __construct()
    {
        $this->produitFournisseur = new ArrayCollection();
        $this->servicePrestataire = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->clientFacture = new ArrayCollection();
        $this->commandeFournisseur = new ArrayCollection();
        $this->commandePrestataire = new ArrayCollection();
        $this->factureFournisseur = new ArrayCollection();
        $this->facturePrestataire = new ArrayCollection();
        $this->clientDevis = new ArrayCollection();
        $this->clientCommande = new ArrayCollection();
        $this->clientAttachement = new ArrayCollection();
        $this->affaire = new ArrayCollection();
        $this->adresseFacturationAffaire = new ArrayCollection();
        $this->adresseLivraisonAffaire = new ArrayCollection();
        $this->adresseFacturationDevis = new ArrayCollection();
        $this->adresseLivraisonDevis = new ArrayCollection();
        $this->adresseFacturationCommande = new ArrayCollection();
        $this->adresseLivraisonCommande = new ArrayCollection();
        $this->adresseFacturationAttachement = new ArrayCollection();
        $this->adresseLivraisonAttachement = new ArrayCollection();
        $this->adresseFacturationFacture = new ArrayCollection();
        $this->adresseLivraisonFacture = new ArrayCollection();
        $this->affaire = new ArrayCollection();
        $this->enfants = new ArrayCollection();
        $this->estimationPrestation = new ArrayCollection();
        $this->estimationFournisseur = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Organisme
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Organisme
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     * @return Organisme
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdresseComplementaire1()
    {
        return $this->adresseComplementaire1;
    }

    /**
     * @param string $adresseComplementaire1
     * @return Organisme
     */
    public function setAdresseComplementaire1($adresseComplementaire1)
    {
        $this->adresseComplementaire1 = $adresseComplementaire1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdresseComplementaire2()
    {
        return $this->adresseComplementaire2;
    }

    /**
     * @param string $adresseComplementaire2
     * @return Organisme
     */
    public function setAdresseComplementaire2($adresseComplementaire2)
    {
        $this->adresseComplementaire2 = $adresseComplementaire2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStal()
    {
        return $this->stal;
    }

    /**
     * @param string|null $stal
     * @return Organisme
     */
    public function setStal($stal)
    {
        $this->stal = $stal;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param string|null $ville
     * @return Organisme
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string|null $telephone
     * @return Organisme
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Organisme
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param bool $actif
     * @return Organisme
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFournisseurValide()
    {
        return $this->fournisseurValide;
    }

    /**
     * @param bool $fournisseurValide
     * @return Organisme
     */
    public function setFournisseurValide($fournisseurValide)
    {
        $this->fournisseurValide = $fournisseurValide;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPrestataireValide()
    {
        return $this->prestataireValide;
    }

    /**
     * @param bool $prestataireValide
     * @return Organisme
     */
    public function setPrestataireValide($prestataireValide)
    {
        $this->prestataireValide = $prestataireValide;
        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Contact $contact
     * @return Organisme
     */
    public function addContact($contact)
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setOrganisme($this);
        }

        return $this;
    }

    /**
     * @param Contact $contact
     * @return Organisme
     */
    public function removeContact($contact)
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getOrganisme() === $this) {
                $contact->setOrganisme(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facture[]
     */
    public function getClientFacture()
    {
        return $this->clientFacture;
    }

    /**
     * @param Facture $facture
     * @return Organisme
     */
    public function addClientFacture($facture)
    {
        if (!$this->clientFacture->contains($facture)) {
            $this->clientFacture[] = $facture;
            $facture->setClient($this);
        }

        return $this;
    }

    /**
     * @param Facture $facture
     * @return Organisme
     */
    public function removeClientFacture($facture)
    {
        if ($this->clientFacture->contains($facture)) {
            $this->clientFacture->removeElement($facture);
            // set the owning side to null (unless already changed)
            if ($facture->getClient() === $this) {
                $facture->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandeFournisseur[]
     */
    public function getCommandeFournisseur()
    {
        return $this->commandeFournisseur;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Organisme
     */
    public function addCommandeFournisseur($commandeFournisseur)
    {
        if (!$this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur[] = $commandeFournisseur;
            $commandeFournisseur->setFournisseur($this);
        }

        return $this;
    }

    /**
     * @param CommandeFournisseur $commandeFournisseur
     * @return Organisme
     */
    public function removeCommandeFournisseur($commandeFournisseur)
    {
        if ($this->commandeFournisseur->contains($commandeFournisseur)) {
            $this->commandeFournisseur->removeElement($commandeFournisseur);
            // set the owning side to null (unless already changed)
            if ($commandeFournisseur->getFournisseur() === $this) {
                $commandeFournisseur->setFournisseur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommandePrestataire[]
     */
    public function getCommandePrestataire()
    {
        return $this->commandePrestataire;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Organisme
     */
    public function addCommandePrestataire($commandePrestataire)
    {
        if (!$this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire[] = $commandePrestataire;
            $commandePrestataire->setPrestataire($this);
        }

        return $this;
    }

    /**
     * @param CommandePrestataire $commandePrestataire
     * @return Organisme
     */
    public function removeCommandePrestataire($commandePrestataire)
    {
        if ($this->commandePrestataire->contains($commandePrestataire)) {
            $this->commandePrestataire->removeElement($commandePrestataire);
            // set the owning side to null (unless already changed)
            if ($commandePrestataire->getPrestataire() === $this) {
                $commandePrestataire->setPrestataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FactureFournisseur[]
     */
    public function getFactureFournisseur()
    {
        return $this->factureFournisseur;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Organisme
     */
    public function addFactureFournisseur($factureFournisseur)
    {
        if (!$this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur[] = $factureFournisseur;
            $factureFournisseur->setFournisseur($this);
        }

        return $this;
    }

    /**
     * @param FactureFournisseur $factureFournisseur
     * @return Organisme
     */
    public function removeFactureFournisseur($factureFournisseur)
    {
        if ($this->factureFournisseur->contains($factureFournisseur)) {
            $this->factureFournisseur->removeElement($factureFournisseur);
            // set the owning side to null (unless already changed)
            if ($factureFournisseur->getFournisseur() === $this) {
                $factureFournisseur->setFournisseur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturePrestataire[]
     */
    public function getFacturePrestataire()
    {
        return $this->facturePrestataire;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Organisme
     */
    public function addFacturePrestataire($facturePrestataire)
    {
        if (!$this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire[] = $facturePrestataire;
            $facturePrestataire->setPrestataire($this);
        }

        return $this;
    }

    /**
     * @param FacturePrestataire $facturePrestataire
     * @return Organisme
     */
    public function removeFacturePrestataire($facturePrestataire)
    {
        if ($this->facturePrestataire->contains($facturePrestataire)) {
            $this->facturePrestataire->removeElement($facturePrestataire);
            // set the owning side to null (unless already changed)
            if ($facturePrestataire->getPrestataire() === $this) {
                $facturePrestataire->setPrestataire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getClientDevis()
    {
        return $this->clientDevis;
    }

    /**
     * @param Devis $clientDevis
     * @return Organisme
     */
    public function addClientDevis($clientDevis)
    {
        if (!$this->clientDevis->contains($clientDevis)) {
            $this->clientDevis[] = $clientDevis;
            $clientDevis->setClient($this);
        }

        return $this;
    }

    /**
     * @param Devis $clientDevis
     * @return Organisme
     */
    public function removeClientDevis($clientDevis)
    {
        if ($this->clientDevis->contains($clientDevis)) {
            $this->clientDevis->removeElement($clientDevis);
            // set the owning side to null (unless already changed)
            if ($clientDevis->getClient() === $this) {
                $clientDevis->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getClientCommande()
    {
        return $this->clientCommande;
    }

    /**
     * @param Commande $clientCommande
     * @return Organisme
     */
    public function addClientCommande($clientCommande)
    {
        if (!$this->clientCommande->contains($clientCommande)) {
            $this->clientCommande[] = $clientCommande;
            $clientCommande->setClient($this);
        }

        return $this;
    }

    /**
     * @param Commande $clientCommande
     * @return Organisme
     */
    public function removeClientCommande($clientCommande)
    {
        if ($this->clientCommande->contains($clientCommande)) {
            $this->clientCommande->removeElement($clientCommande);
            // set the owning side to null (unless already changed)
            if ($clientCommande->getClient() === $this) {
                $clientCommande->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Attachement[]
     */
    public function getClientAttachement()
    {
        return $this->clientAttachement;
    }

    /**
     * @param Attachement $clientAttachement
     * @return Organisme
     */
    public function addClientAttachement($clientAttachement)
    {
        if (!$this->clientAttachement->contains($clientAttachement)) {
            $this->clientAttachement[] = $clientAttachement;
            $clientAttachement->setClient($this);
        }

        return $this;
    }

    /**
     * @param Attachement $clientAttachement
     * @return Organisme
     */
    public function removeClientAttachement($clientAttachement)
    {
        if ($this->clientAttachement->contains($clientAttachement)) {
            $this->clientAttachement->removeElement($clientAttachement);
            // set the owning side to null (unless already changed)
            if ($clientAttachement->getClient() === $this) {
                $clientAttachement->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProduitFournisseur[]
     */
    public function getProduitFournisseur()
    {
        return $this->produitFournisseur;
    }

    /**
     * @param ProduitFournisseur $produitFournisseur
     * @return Organisme
     */
    public function addProduitFournisseur($produitFournisseur)
    {
        if (!$this->produitFournisseur->contains($produitFournisseur)) {
            $this->produitFournisseur[] = $produitFournisseur;
            $produitFournisseur->setOrganisme($this);
        }

        return $this;
    }

    /**
     * @param ProduitFournisseur $produitFournisseur
     * @return Organisme
     */
    public function removeProduitFournisseur($produitFournisseur)
    {
        if ($this->produitFournisseur->contains($produitFournisseur)) {
            $this->produitFournisseur->removeElement($produitFournisseur);
            // set the owning side to null (unless already changed)
            if ($produitFournisseur->getOrganisme() === $this) {
                $produitFournisseur->setOrganisme(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ServicePrestataire[]
     */
    public function getServicePrestataire()
    {
        return $this->servicePrestataire;
    }

    /**
     * @param ServicePrestataire $servicePrestataire
     * @return Organisme
     */
    public function addServicePrestataire($servicePrestataire)
    {
        if (!$this->servicePrestataire->contains($servicePrestataire)) {
            $this->servicePrestataire[] = $servicePrestataire;
            $servicePrestataire->setPrestataire($this);
        }

        return $this;
    }

    /**
     * @param ServicePrestataire $servicePrestataire
     * @return Organisme
     */
    public function removeServicePrestataire($servicePrestataire)
    {
        if ($this->servicePrestataire->contains($servicePrestataire)) {
            $this->servicePrestataire->removeElement($servicePrestataire);
            // set the owning side to null (unless already changed)
            if ($servicePrestataire->getPrestataire() === $this) {
                $servicePrestataire->setPrestataire(null);
            }
        }

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userEntity.
     *
     * @param User|null $user
     *
     * @return Organisme
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Organisme|null $parent
     * @return Organisme
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Organisme[]
     */
    public function getEnfants()
    {
        return $this->enfants;
    }

    /**
     * @param Organisme $enfant
     * @return Organisme
     */
    public function addEnfants($enfant)
    {
        if (!$this->enfants->contains($enfant)) {
            $this->enfants[] = $enfant;
            $enfant->setParent($this);
        }

        return $this;
    }

    /**
     * @param Organisme $enfant
     * @return Organisme
     */
    public function removeEnfants($enfant)
    {
        if ($this->enfants->contains($enfant)) {
            $this->enfants->removeElement($enfant);
            // set the owning side to null (unless already changed)
            if ($enfant->getParent() === $this) {
                $enfant->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Affaire[]
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire $affaire
     * @return Organisme
     */
    public function addAffaire($affaire)
    {
        if (!$this->affaire->contains($affaire)) {
            $this->affaire[] = $affaire;
            $affaire->setOrganisme($this);
        }

        return $this;
    }

    /**
     * @return InfoFournisseur
     */
    public function getInfoFournisseur()
    {
        return $this->infoFournisseur;
    }

    /**
     * @param InfoFournisseur $infoFournisseur
     * @return Organisme
     */
    public function setInfoFournisseur($infoFournisseur)
    {
        $this->infoFournisseur = $infoFournisseur;

        return $this;
    }

    /**
     * @return Organisme
     */
    public function removeInfoFournisseur()
    {
        $this->infoFournisseur = null;

        return $this;
    }

    /**
     * @return InfoPrestataire
     */
    public function getInfoPrestataire()
    {
        return $this->infoPrestataire;
    }

    /**
     * @param InfoPrestataire $infoPrestataire
     * @return Organisme
     */
    public function setInfoPrestataire($infoPrestataire)
    {
        $this->infoPrestataire = $infoPrestataire;

        return $this;
    }

    /**
     * @return Organisme
     */
    public function removeInfoPrestataire()
    {
        $this->infoFournisseur = null;

        return $this;
    }

    /**
     * @param $pays
     * @return Organisme
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @return EstimationPrestation[]
     */
    public function getEstimationPrestation()
    {
        return $this->estimationPrestation;
    }

    /**
     * @param EstimationPrestation $estimationPrestation
     * @return $this
     */
    public function addEstimationPrestataion($estimationPrestation)
    {
        if (!$this->estimationPrestation->contains($estimationPrestation)) {
            $this->estimationPrestation[] = $estimationPrestation;
            $estimationPrestation->setPrestataire($this);
        }

        return $this;
    }

    /**
     * @return EstimationFournisseur[]
     */
    public function getEstimationFournisseur()
    {
        return $this->estimationFournisseur;
    }

    /**
     * @param EstimationFournisseur $estimationFournisseur
     * @return $this
     */
    public function addEstimationFournisseur($estimationFournisseur)
    {
        if (!$this->estimationFournisseur->contains($estimationFournisseur)) {
            $this->estimationFournisseur[] = $estimationFournisseur;
            $estimationFournisseur->setFournisseur($this);
        }

        return $this;
    }
}
