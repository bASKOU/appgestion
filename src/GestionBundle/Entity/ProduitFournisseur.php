<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProduitFournisseur
 *
 * @ORM\Table(name="produit_fournisseur")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ProduitFournisseurRepository")
 */
class ProduitFournisseur
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('produit_fournisseur_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="montantUnit", type="float", nullable=true)
     */
    private $montantUnit;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parent", type="integer", nullable=true)
     */
    private $parent;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateValid", type="date", nullable=true)
     */
    private $dateValid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreat", type="date")
     */
    private $dateCreat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="referenceFournisseur", type="string", length=255, nullable=true)
     */
    private $referenceFournisseur;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="prixFixe", type="boolean", nullable=true)
     */
    private $prixFixe;

    /**
     * @var Organisme
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Organisme", inversedBy="produitFournisseur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $organisme;

    /**
     * @var ProduitCommandeFournisseur[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ProduitCommandeFournisseur", mappedBy="produitFournisseur", cascade="remove")
     */
    private $produitCommandeFournisseur;

    public function __construct()
    {
        $this->produitCommandeFournisseur = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set designation.
     *
     * @param string $designation
     *
     * @return ProduitFournisseur
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation.
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return ProduitFournisseur
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set montantUnit.
     *
     * @param float $montantUnit
     *
     * @return ProduitFournisseur
     */
    public function setMontantUnit($montantUnit)
    {
        $this->montantUnit = $montantUnit;

        return $this;
    }

    /**
     * Get montantUnit.
     *
     * @return float
     */
    public function getMontantUnit()
    {
        return $this->montantUnit;
    }

    /**
     * Set parent.
     *
     * @param int|null $parent
     *
     * @return ProduitFournisseur
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return int|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set dateValid.
     *
     * @param \DateTime|null $dateValid
     *
     * @return ProduitFournisseur
     */
    public function setDateValid($dateValid = null)
    {
        $this->dateValid = $dateValid;

        return $this;
    }

    /**
     * Get dateValid.
     *
     * @return \DateTime|null
     */
    public function getDateValid()
    {
        return $this->dateValid;
    }

    /**
     * Set dateCreat.
     *
     * @param \DateTime $dateCreat
     *
     * @return ProduitFournisseur
     */
    public function setDateCreat($dateCreat)
    {
        $this->dateCreat = $dateCreat;

        return $this;
    }

    /**
     * Get dateCreat.
     *
     * @return \DateTime
     */
    public function getDateCreat()
    {
        return $this->dateCreat;
    }

    /**
     * Set referenceFournisseur.
     *
     * @param string|null $referenceFournisseur
     *
     * @return ProduitFournisseur
     */
    public function setReferenceFournisseur($referenceFournisseur = null)
    {
        $this->referenceFournisseur = $referenceFournisseur;

        return $this;
    }

    /**
     * Get referenceFournisseur.
     *
     * @return string|null
     */
    public function getReferenceFournisseur()
    {
        return $this->referenceFournisseur;
    }

    /**
     * Set prixFixe.
     *
     * @param bool|null $prixFixe
     *
     * @return ProduitFournisseur
     */
    public function setPrixFixe($prixFixe = null)
    {
        $this->prixFixe = $prixFixe;

        return $this;
    }

    /**
     * Get prixFixe.
     *
     * @return bool|null
     */
    public function getPrixFixe()
    {
        return $this->prixFixe;
    }

    /**
     * @param Organisme|null $organisme
     * @return ProduitFournisseur
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return Organisme|null
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @return Collection|ProduitCommandeFournisseur[]
     */
    public function getProduitCommandeFournisseur()
    {
        return $this->produitCommandeFournisseur;
    }

    /**
     * @param ProduitCommandeFournisseur $produitCommandeFournisseur
     * @return ProduitFournisseur
     */
    public function addProduitCommandeFournisseur($produitCommandeFournisseur)
    {
        if (!$this->produitCommandeFournisseur->contains($produitCommandeFournisseur)) {
            $this->produitCommandeFournisseur[] = $produitCommandeFournisseur;
            $produitCommandeFournisseur->setProduitFournisseur($this);
        }

        return $this;
    }
}
