<?php

namespace GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleAffaire
 *
 * @ORM\Table(name="article_affaire")
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleAffaireRepository")
 */
class ArticleAffaire
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_affaire_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Article
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Article", inversedBy="articleAffaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @var Affaire
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Affaire", inversedBy="articleAffaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affaire;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Article|null
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Article|null $article
     * @return ArticleAffaire
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Affaire|null
     */
    public function getAffaire()
    {
        return $this->affaire;
    }

    /**
     * @param Affaire|null $affaire
     * @return ArticleAffaire
     */
    public function setAffaire($affaire)
    {
        $this->affaire = $affaire;

        return $this;
    }
}
