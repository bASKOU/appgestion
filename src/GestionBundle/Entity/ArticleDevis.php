<?php

namespace GestionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="GestionBundle\Repository\ArticleDevisRepository")
 */
class ArticleDevis
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE"))
     * @ORM\Column(type="bigint", options={"default"="nextval('article_devis_id_seq'::regclass)"})
     */
    private $id;

    /**
     * @var Devis
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Devis", inversedBy="articleDevis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $devis;

    /**
     * @var Article
     *
     * @ORM\ManyToOne(targetEntity="GestionBundle\Entity\Article", inversedBy="articleDevis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $overrideMontantUnit;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $montantTotalHT;

    /**
     * @var ArticleCommande[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleCommande", mappedBy="articleDevis")
     */
    private $articleCommandes;

    /**
     * @var ArticleFacture[]
     *
     * @ORM\OneToMany(targetEntity="GestionBundle\Entity\ArticleFacture", mappedBy="articleDevis")
     */
    private $articleFacture;

    /**
     * ArticleDevis constructor.
     */
    public function __construct()
    {
        $this->articleCommandes = new ArrayCollection();
        $this->articleFacture = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Devis|null
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * @param Devis|null $devis
     * @return ArticleDevis
     */
    public function setDevis($devis)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * @return Article|null
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Article|null $article
     * @return ArticleDevis
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOverrideMontantUnit()
    {
        return $this->overrideMontantUnit;
    }

    /**
     * @param float $overrideMontantUnit
     * @return ArticleDevis
     */
    public function setOverrideMontantUnit($overrideMontantUnit)
    {
        $this->overrideMontantUnit = $overrideMontantUnit;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     * @return ArticleDevis
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ArticleDevis
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMontantTotalHT()
    {
        return $this->montantTotalHT;
    }

    /**
     * @param float $montantTotalHT
     * @return ArticleDevis
     */
    public function setMontantTotalHT($montantTotalHT)
    {
        $this->montantTotalHT = $montantTotalHT;

        return $this;
    }

    /**
     * @return Collection|ArticleCommande[]
     */
    public function getArticleCommandes()
    {
        return $this->articleCommandes;
    }

    /**
     * @param ArticleCommande $articleCommande
     * @return ArticleDevis
     */
    public function addArticleCommande($articleCommande)
    {
        if (!$this->articleCommandes->contains($articleCommande)) {
            $this->articleCommandes[] = $articleCommande;
            $articleCommande->setArticleDevis($this);
        }

        return $this;
    }

    /**
     * @param ArticleCommande $articleCommande
     * @return ArticleDevis
     */
    public function removeArticleCommande($articleCommande)
    {
        if ($this->articleCommandes->contains($articleCommande)) {
            $this->articleCommandes->removeElement($articleCommande);
            // set the owning side to null (unless already changed)
            if ($articleCommande->getArticleDevis() === $this) {
                $articleCommande->setArticleDevis(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ArticleFacture[]
     */
    public function getArticleFacture()
    {
        return $this->articleFacture;
    }

    /**
     * @param ArticleFacture $articleFacture
     * @return ArticleDevis
     */
    public function addArticleFacture($articleFacture)
    {
        if (!$this->articleFacture->contains($articleFacture)) {
            $this->articleFacture[] = $articleFacture;
            $articleFacture->setArticleDevis($this);
        }

        return $this;
    }

    /**
     * @param ArticleFacture $articleFacture
     * @return ArticleDevis
     */
    public function removeArticleFacture($articleFacture)
    {
        if ($this->articleFacture->contains($articleFacture)) {
            $this->articleFacture->removeElement($articleFacture);
            // set the owning side to null (unless already changed)
            if ($articleFacture->getArticleDevis() === $this) {
                $articleFacture->setArticleDevis(null);
            }
        }

        return $this;
    }
}
