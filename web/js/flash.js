global. showAlertFlash = (message) => {
    flash(message,{
        // background color
        'bgColor' :'#FF7F50',
        // text color
        'ftColor' :'white',
        // or 'top'
        'vPosition' :'bottom',
        // or 'left'
        'hPosition' :'right',
        // duration of animation
        'fadeIn' : 400,
        'fadeOut' : 400,
        // click to close
        'clickable' :true,
        // auto hides after a duration time
        'autohide' :true,
        // timout
        'duration' : 6000
    });
}

global.showWarningFlash = (message) => {
    flash(message,{
        // background color
        'bgColor' :'#FF1919',
        // text color
        'ftColor' :'white',
        // or 'top'
        'vPosition' :'bottom',
        // or 'left'
        'hPosition' :'right',
        // duration of animation
        'fadeIn' : 400,
        'fadeOut' : 400,
        // click to close
        'clickable' :true,
        // auto hides after a duration time
        'autohide' :true,
        // timout
        'duration' : 6000
    });
}

global.showSuccessFlash = (message) => {
    flash(message,{
        // background color
        'bgColor' :'#62D162',
        // text color
        'ftColor' :'black',
        // or 'top'
        'vPosition' :'bottom',
        // or 'left'
        'hPosition' :'right',
        // duration of animation
        'fadeIn' : 400,
        'fadeOut' : 400,
        // click to close
        'clickable' :true,
        // auto hides after a duration time
        'autohide' :true,
        // timout
        'duration' : 6000
    });
}

function successToLocalStorage(message) {
    showSuccessFlash(message);
    window.localStorage.setItem('success', message)
}

function warningToLocalStorage(message) {
    showWarningFlash(message);
    window.localStorage.setItem('warning', message)
}

function alertToLocalStorage(message) {
    showAlertFlash(message);
    window.localStorage.setItem('alert', message)
}

function getSuccessFromLocalStorage() {
    var message = window.localStorage.getItem('success');
    if (typeof message == 'string') {
        showSuccessFlash(message);
    }
    window.localStorage.removeItem('success');
}

function getWarningFromLocalStorage() {
    var message = window.localStorage.getItem('warning');
    if (typeof message == 'string') {
        showWarningFlash(message);
    }
    window.localStorage.removeItem('warning');
}

function getAlertFromLocalStorage() {
    var message = window.localStorage.getItem('alert');
    if (typeof message == 'string') {
        showAlertFlash(message);
    }
    window.localStorage.removeItem('alert');
}

global.getFlashFromLocalStorage = () => {
    getSuccessFromLocalStorage();
    getWarningFromLocalStorage();
    getAlertFromLocalStorage();
}

global.getFlashFromTwig = (type, message) => {
    if (type === 'success') {
        showSuccessFlash(message);
    } else if (type === 'warning') {
        showWarningFlash(message);
    } else if (type === 'alert') {
        showAlertFlash(message);
    }
}