global.addCommaAll = () => {
    $('.comma').each(function() {
        var value = this.value;
        if (value) {
            if (value.includes('.')) {
                var splitted = value.split('.');
                splitted[0] = splitted[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                if (splitted[1].length < 2) {
                    splitted[1] = splitted[1] + '0';
                } else {
                    $(this).val(value.replace(/\B(?=(\d{3})+(?!\d))/g, " ").replace('.', ','));
                    return true;
                }
                $(this).val(splitted.join(','));
            } else if (value.includes((','))) {
                var splitted = value.split(',');
                splitted[0] = splitted[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                if (splitted[1].length < 2) {
                    splitted[1] = splitted[1] + '0';
                }
                $(this).val(splitted.join(','));
            } else {
                $(this).val(value.replace(/\B(?=(\d{3})+(?!\d))/g, " ") + ',00')
            }
            // if (value.includes('-')) {
            //     $(this).val(value.replace('-', '- '));
            // }
        }
    })
}

global.addCommaThis = (input) => {
    var value = $(input).val()
    if (isNaN(parseFloat(value))) {
        $(input).parent('div').children('span').remove();
        $(input).val('');
        $(input).parent('div').append('<span class="text-danger">Ce champs n\'accepte que les chiffres</span>')
        return false;
    } else {
        $(input).parent('div').children('span').remove();
    }
    if (value) {
        if (value.includes('.')) {
            var splitted = value.split('.');
            if (splitted[1].length < 2) {
                splitted[1] = splitted[1] + '0';
                $(input).val(splitted.join(','));
            } else {
                $(input).val(value.replace('.', ','));
            }
        } else if (!value.includes((','))) {
            $(input).val(value + ',00')
        }
    }
}

function isFloat(x) { return !!(x % 1); }