// Fonction qui permet de gérer les exceptions
global.exceptionHandler = (code, item=null) => {
    switch (code[0]) {
        case 23505:
            console.log(code[1]);
            return selectErrorMessage(code[1], item);
        case 23502:
            return 'Un élément requis n\'a pas était renseigné, vérifiez que tous les champs comportant une astérisque soit rempli.';
        case 23503:
            return 'Cette élément ne peut être supprimer car il est lié à un autre élément.';
        case 8000:
            return 'Erreur de connexion avec la base de données, contactez l\'administrateur.';
        default:
            return 'Une erreur s\'est produite, veuillez réessayer...';
    }
}

global.selectErrorMessage = (column ,item) => {
    switch (item) {
        case 'thème':
            return 'Impossible d\'ajouter ce ' + item + ' car le nom ' + column + ' existe déjà';
        case 'article':
            return 'Impossible d\'ajouter cet ' + item + ' car il existe déjà';
        case 'nom':
            return 'Impossible d\'ajouter ce ' + item + ' car il existe déjà';
        case 'telephone':
            return 'Impossible d\'ajouter ce numéro de téléphone' + column + ' car il existe déjà';
        case 'mobile':
            return 'Impossible d\'ajouter ce numéro de téléphone mobile' + column + ' car il existe déjà';
        case 'email':
            return 'Impossible d\'ajouter cet ' + item + ' car il existe déjà';
        case 'avoir':
            return 'Cette facture possède déja un ' + item + ' reportez-vous à la section avoir pour le modifier.';
        case 'organisme':
            return 'Impossible de créer cet organisme car le ' + column + ' est présent dans un autre organisme.';
        case 'orgaMAJ':
            return 'Impossible de modifier cet organisme car le ' + column + ' est présent dans un autre organisme.';
        case 'agenceMAJ':
            return 'Impossible de modifier cette agence car le ' + column + ' est présent dans une autre agence.';
        case 'agence':
            return 'Impossible de créer cette agence car le ' + column + ' est présent dans une autre agence.';
        case 'contact':
            return 'Impossible de créer ce contact car le ' + column + ' est présent dans un autre contact.';
        case 'contactMAJ':
            return 'Impossible de modifier ce contact car le ' + column + ' est présent dans un autre contact.';
        default:
            return 'Ajout impossible car ce terme existe déjà';
    }
}