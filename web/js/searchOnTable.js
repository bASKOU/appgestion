/**
 * Fonction de recherche dans les tableaux
 * @param classAdd
 * @param elemId
 */

function searchOnTable(classAdd, elemId) {
    var value = $('#' + elemId).val().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    var count = 0;
    $('.searchTable' + classAdd).filter(function() {
        $(this).toggle($(this).text().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").indexOf(value) > -1);
        if ($(this).css('display') != 'none') {
            count ++
        }
    })
    if (count === 0) {
        alert('Aucun éléments trouvé...');
        $('#' + elemId).select()
    }
}

/**
 * Fonction qui s'auto invoque et ajout un eventListener à touts les input de type search en respectant la nomenclature searchNomDeLelement
 * Dans les elements nommé searchTableNomDeLelement
 */
(() => {
    const searchElement = document.querySelectorAll('[type="search"]')
    for (let elem of searchElement) {
        elem.addEventListener('input', () => {
            searchOnTable(elem.id.replace('search', ''), elem.id)
        })
    }
})();