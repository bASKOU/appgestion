/**
 * Script to modify labels for SF generated form
 */

global.changeLabels = () => {
    let stars = document.querySelectorAll('.star');
    for (let star of stars) {
        star.remove();
    }
    const labels = document.querySelectorAll('label'); // Get all labels
    const legends = document.querySelectorAll('legend'); // Get all legends
    for (let legend of legends) { // Loop in all labels
        if (legend.classList.value === 'col-form-label required') { // if label class is required
            let span = document.createElement('span');
            legend.style.fontWeight = 700;
            legend.style.fontSize = '13px';
            span.classList.add('text-danger');
            span.classList.add('star');
            span.innerHTML = '*';
            legend.appendChild(span); // Add a star
        } else {
            legend.style.fontSize = '13px';
            legend.style.fontWeight = 700;
            legend.classList.add('not-required'); // If not add not-required class
        }
    }
    for (let label of labels) { // Loop in all labels
        if (label.classList.value === 'required') { // if label class is required
            let span = document.createElement('span');
            span.classList.add('text-danger');
            span.classList.add('star');
            span.innerHTML = '*';
            label.appendChild(span); // Add a star
        } else {
            label.classList.add('not-required'); // If not add not-required class
        }
    }
}

changeLabels();