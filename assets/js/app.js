console.log('test');
/**
 * Import js script
 */
import $ from 'jquery';
// const $ = require('jquery');
global.$ = global.jQuery = $;
require('bootstrap/dist/js/bootstrap');
require('@fortawesome/fontawesome-free');
require('popper.js');
require('../../web/js/toast/flash.min')
require('../../web/js/loader/jquery.preloaders')
require('../../web/js/bootstrap/select')
require('../../web/js/bootstrap/datepicker')
require('../../web/js/bootstrap/datepicker.fr.min')
require('../../web/js/changeLabel')
require('../../web/js/searchOnTable')
// require('../../web/intTelInput/js/intlTelInput-jquery')
require('../../web/js/errorHandler')
require('../../web/js/periodsToComma')
require('../../web/js/flash');

/**
 * End of js script importation
 */

/**
 * Import of CSS
 */
import '../../web/css/bootstrap/origin/bootstrap.css';
import '../../web/css/font-awesome/css/all.css';
import '../../web/css/bootstrap/bootstarp-select.min.css';
import '../../web/css/bootstrap/bootstrap-datepicker.css';
import '../../web/intTelInput/css/intlTelInput.css';
import '../../web/css/baseStyle.css'
/**
 * End of CSS importation
 */
console.log('webpack working');